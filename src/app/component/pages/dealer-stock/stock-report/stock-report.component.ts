import { Component, OnInit } from "@angular/core";
import { formatDate } from "@angular/common";
import { NgxSpinnerService } from "ngx-spinner";
import { StockReportService } from "src/app/services/stock-report.service";

@Component({
  selector: "app-stock-report",
  templateUrl: "./stock-report.component.html",
  styleUrls: ["./stock-report.component.css"]
})
export class StockReportComponent implements OnInit {
  listStockReport: any = [];
  so_date_from: any;
  so_date_to: any;

  constructor(
    private dealerReport: StockReportService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.spinner.show()
    this.dealerReport.getListStockReport().subscribe(
      res => {
        const monthNames = [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December"
        ];
        let result = [];
        let list = [];
        let map = new Map();
        result.push(res);
        for (const item of result[0]) {
          if (!map.has(item.periode)) {
            map.set(item.periode, true);
            list.push(item);
          }
        }
        for (const iterator of list) {
          let monthPeriod = iterator.periode.split("-")[1];
          let monthName = new Date(monthPeriod).getMonth();
          iterator.monthName = monthNames[monthName];
          iterator.yearPeriode = iterator.periode.split("-")[0];
          iterator.purchasedSum = 0;
          for (const item of result[0].filter(
            val => val.periode == iterator.periode
          )) {
            iterator.purchasedSum =
              iterator.purchasedSum + parseInt(item.purchased);
          }
          if (isNaN(iterator.purchasedSum)) iterator.purchasedSum = 0;
        }
        console.log(list);

        // item.purchasedSum =
        // console.log(result[0])
        this.listStockReport = list
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        console.log(error);
      }
    );
  }
}
