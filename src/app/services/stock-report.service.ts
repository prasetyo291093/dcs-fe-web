import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class StockReportService {

  constructor(
    private http: HttpClient
  ) { }

  baseUrl = environment.baseUrl

  getListStockReport() {
    let sap_code = JSON.parse(localStorage.getItem('user')).sap_code
    return this.http.get(`${this.baseUrl}/api/dealer-stock-report/${sap_code}`)
  }
}
