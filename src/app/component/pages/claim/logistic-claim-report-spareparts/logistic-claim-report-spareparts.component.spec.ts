import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogisticClaimReportSparepartsComponent } from './logistic-claim-report-spareparts.component';

describe('LogisticClaimReportSparepartsComponent', () => {
  let component: LogisticClaimReportSparepartsComponent;
  let fixture: ComponentFixture<LogisticClaimReportSparepartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogisticClaimReportSparepartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogisticClaimReportSparepartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
