import { Component, OnInit } from "@angular/core";
import { ForecastReportService } from "src/app/services/forecast-report.service";
import { Router } from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: "app-forecast-report",
  templateUrl: "./forecast-report.component.html",
  styleUrls: ["./forecast-report.component.css"]
})
export class ForecastReportComponent implements OnInit {
  tableReport: boolean = false;
  startingYear: string;
  periode: string;
  forecastReportList: any = [];
  years: any[];
  month1: string;
  month2: string;
  month3: string;
  constructor(private forecastReport: ForecastReportService, private router: Router, private spinner: NgxSpinnerService) {}

  ngOnInit() {
    let thisYear = new Date().getFullYear();
    let years = [];
    console.log(thisYear);
    for (let i = 0; i < 2; i++) {
      years.push(thisYear++);
    }
    this.years = years;
  }

  trackByFn(index, item) {
    return index;
  }

  getForecastReport() {
    if(this.startingYear && this.periode){
      this.spinner.show()
      this.forecastReportList = [];
      let data = {
        starting_year: this.startingYear,
        periode: this.periode.substring(0, 2)
      };
      if(data.periode == "Q1") {
        this.month1 = "April"
        this.month2 = "May"
        this.month3 = "June"
      }
      if(data.periode == "Q2") {
        this.month1 = "July"
        this.month2 = "August"
        this.month3 = "September"
      }
      if(data.periode == "Q3") {
        this.month1 = "October"
        this.month2 = "November"
        this.month3 = "December"
      }
      if(data.periode == "Q4") {
        this.month1 = "January"
        this.month2 = "February"
        this.month3 = "March"
      }
      if (!this.startingYear) {
        alert("Choose Starting Year");
        this.forecastReportList = false;
      } else if (!this.periode) {
        alert("Choose Periode");
        this.forecastReportList = false;
      } else {
        console.log(data);
        this.forecastReport.getListForecastReport(data).subscribe(
          res => {
            this.tableReport = true;
            console.log(res);
            this.forecastReportList = res;
            this.spinner.hide()
          },
          err => {
            console.log(err);
            this.spinner.hide()
          }
        );
      }
    }else{
      alert('Please Input Starting Year and Period')
    }
  }
  totalPalletMonth1() {
    let month1 = 0;
    for (const item of this.forecastReportList) {
      month1 += item.month1;
    }
    return month1;
  }
  totalPalletMonth2() {
    let month2 = 0;
    for (const item of this.forecastReportList) {
      month2 += item.month2;
    }
    return month2;
  }
  totalPalletMonth3() {
    let month3 = 0;
    for (const item of this.forecastReportList) {
      month3 += item.month3;
    }
    return month3;
  }
  totalQtyMonth1() {
    let qty1 = 0;
    for (const item of this.forecastReportList) {
      qty1 += item.qty_per_pallet * item.month1;
    }
    return qty1;
  }
  totalQtyMonth2() {
    let qty2 = 0;
    for (const item of this.forecastReportList) {
      qty2 += item.qty_per_pallet * item.month2;
    }
    return qty2;
  }
  totalQtyMonth3() {
    let qty3 = 0;
    for (const item of this.forecastReportList) {
      qty3 += item.qty_per_pallet * item.month3;
    }
    return qty3;
  }
  totalQty() {
    let qty = 0;
    for (const item of this.forecastReportList) {
      qty += item.total;
    }
    return qty;
  }
  totalRealization() {
    let total = 0;
    for (const item of this.forecastReportList) {
      total += item.realization;
    }
    return total;
  }
  getForecastPromo() {
    this.router.navigateByUrl('/sales-promo');
    // sessionStorage.setItem("promoType", this.salesOrderCash[index].promo_type)
  }
}
