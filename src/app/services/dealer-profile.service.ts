import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class DealerProfileService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  getInfoUser(val) {
    return this.http.get(this.baseUrl + "/api/profile/" + val);
  }

  setDealerInformation(val) {
    var user = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.post(
      this.baseUrl + "/api/profileInformation/" + user,
      val
    );
  }

  setBranchInformation(val) {
    var user = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.post(this.baseUrl + "/api/branchInformation/" + user, val);
  }

  setDealerPhotoProfile(data) {
    var user_id = JSON.parse(localStorage.getItem("user")).id;
    return this.http.post(
      this.baseUrl + "/api/update-photo-profile/" + user_id,
      data
    );
  }

  updateDealerPhotoProfile(data) {
    var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.post(
      `${this.baseUrl}/api/update-photo-profile/${sap_code}`,
      data
    );
  }

  setDealerStorePhoto(data, id: number) {
    return this.http.post(this.baseUrl + "/api/update-photo/" + id, data);
  }
  updateStorePhoto(data) {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.post(`${this.baseUrl}/api/update-photo/${sap_code}`, data);
  }

  setDealerIdCard(data) {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.post(`${this.baseUrl}/api/id-card/${sap_code}`, data);
  }

  getDealerImage() {
    return this.http.get(`${this.baseUrl}/api/image-banner`)
  }
}
