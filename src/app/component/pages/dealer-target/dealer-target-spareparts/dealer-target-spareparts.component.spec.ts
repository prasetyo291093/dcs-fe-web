import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealerTargetSparepartsComponent } from './dealer-target-spareparts.component';

describe('DealerTargetSparepartsComponent', () => {
  let component: DealerTargetSparepartsComponent;
  let fixture: ComponentFixture<DealerTargetSparepartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealerTargetSparepartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealerTargetSparepartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
