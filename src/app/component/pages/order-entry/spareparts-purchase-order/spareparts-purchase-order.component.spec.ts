import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparepartsPurchaseOrderComponent } from './spareparts-purchase-order.component';

describe('SparepartsPurchaseOrderComponent', () => {
  let component: SparepartsPurchaseOrderComponent;
  let fixture: ComponentFixture<SparepartsPurchaseOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparepartsPurchaseOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparepartsPurchaseOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
