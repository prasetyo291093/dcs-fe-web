import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'


@Injectable({
  providedIn: 'root'
})
export class DealerTargetSparepartService {

  constructor(
    private http: HttpClient
  ) { }

    baseUrl = environment.baseUrl

  getDealerTargetSparepart(year) {
    let sap_code: string = JSON.parse(localStorage.getItem('user')).sap_code
      return this.http.get(`${this.baseUrl}/api/dealer-target/report-sparepart/${sap_code}/${year}`)
    }

    getYears() {
    let sap_code: string = JSON.parse(localStorage.getItem('user')).sap_code
      return this.http.get(`${this.baseUrl}/api/dealer-target/get-year-sparepart/${sap_code}`)
    }
}
