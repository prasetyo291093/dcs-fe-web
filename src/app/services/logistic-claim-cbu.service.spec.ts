import { TestBed } from '@angular/core/testing';

import { LogisticClaimCbuService } from './logistic-claim-cbu.service';

describe('LogisticClaimCbuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogisticClaimCbuService = TestBed.get(LogisticClaimCbuService);
    expect(service).toBeTruthy();
  });
});
