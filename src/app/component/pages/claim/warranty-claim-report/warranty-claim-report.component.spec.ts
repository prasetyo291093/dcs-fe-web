import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarrantyClaimReportComponent } from './warranty-claim-report.component';

describe('WarrantyClaimReportComponent', () => {
  let component: WarrantyClaimReportComponent;
  let fixture: ComponentFixture<WarrantyClaimReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarrantyClaimReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrantyClaimReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
