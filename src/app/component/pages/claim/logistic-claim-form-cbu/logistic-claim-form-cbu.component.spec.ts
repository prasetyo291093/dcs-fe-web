import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogisticClaimFormCbuComponent } from './logistic-claim-form-cbu.component';

describe('LogisticClaimFormCbuComponent', () => {
  let component: LogisticClaimFormCbuComponent;
  let fixture: ComponentFixture<LogisticClaimFormCbuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogisticClaimFormCbuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogisticClaimFormCbuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
