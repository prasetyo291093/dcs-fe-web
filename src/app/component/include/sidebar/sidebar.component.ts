import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../../../services/auth.service";
declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  public show1: boolean = false;
  public show2: boolean = false;
  public show3: boolean = false;
  public show4: boolean = false;
  public show5: boolean = false;
  public show6: boolean = false;
  public show7: boolean = false;
  public show8: boolean = false;
  public show9: boolean = false;
  public show10: boolean = false;
  dealerClass: string;
  orderClass: string;
  inquiryClass: string;
  reportClass: string;
  forecastClass: string;
  dealTarClass: string;
  claimClass: string;
  cusRegClass: string;
  proCheckClass: string;
  salesProClass: string;
  dealRewardClass: string;
  dealStockClass: string;
  infoShareClass: string;
  termsConditionClass: string;
  userClassification: any;
  user1S: boolean = false;
  user2S: boolean = false;
  user3S: boolean = false;
  userTester: boolean = false;
  user: string;

  canCreateOrder: boolean = false
  canpreviewOrderReport: boolean = false
  canPreviewDownloadDeliveryReport: boolean = false;
  canPreviewDownloadOutstandingReport: boolean = false;
  canPreviewAPAging: boolean = false;
  canPreviewInvoiceTimeliness: boolean = false;
  canInputUploadDealerForecast: boolean = false;
  canInputWarrantyCard: boolean = false;
  canClaimServiceStatus: boolean = false;
  canClaimLogisticStatus: boolean = false;
  canPreviewAvailability: boolean = false;
  canPreviewPromoDetail: boolean = false;
  canInputUploadDealerStock: boolean = false;

  constructor(private router: Router, private auth: AuthService) {}

  ngOnInit() {
    let userClassification = JSON.parse(localStorage.getItem("classification"));
    this.user = JSON.parse(localStorage.getItem("user")).sap_code;
    this.canCreateOrder = JSON.parse(localStorage.getItem("user")).permissions["Create Order"]
    this.canpreviewOrderReport = JSON.parse(localStorage.getItem("user")).permissions["Preview Order Report"]
    this.canPreviewDownloadDeliveryReport = JSON.parse(localStorage.getItem("user")).permissions["Preview, update delivery report and download document"]
    this.canPreviewDownloadOutstandingReport = JSON.parse(localStorage.getItem("user")).permissions["Preview, Download, Preview invoice information and do payment in Outstanding Invoice Report"]
    this.canPreviewAvailability = JSON.parse(localStorage.getItem("user")).permissions["Preview availability"]
    this.canPreviewAPAging = JSON.parse(localStorage.getItem("user")).permissions["Preview A/P Aging"]
    this.canPreviewInvoiceTimeliness = JSON.parse(localStorage.getItem("user")).permissions["Preview Invoice Timeliness"]
    this.canInputUploadDealerForecast = JSON.parse(localStorage.getItem("user")).permissions["Input or Upload and preview Dealer Forecast"]
    this.canInputWarrantyCard = JSON.parse(localStorage.getItem("user")).permissions["Input Warranty Card"]
    this.canClaimServiceStatus = JSON.parse(localStorage.getItem("user")).permissions["Service Claim Status"]
    this.canClaimLogisticStatus = JSON.parse(localStorage.getItem("user")).permissions["Logistic Claim Status"]
    this.canPreviewPromoDetail = JSON.parse(localStorage.getItem("user")).permissions["Preview all Promo Detail"]
    this.canInputUploadDealerStock = JSON.parse(localStorage.getItem("user")).permissions["Input or Upload and preview Dealer Stock"]

    if (this.user == "0010000024"){
      this.userTester = true;
    }
    
    if (userClassification == "1S" || userClassification == "2S") {
      this.user1S = true;
      this.user2S = true;
    } else if (userClassification == "3S") this.user3S = true;
    console.log(this.user1S, this.user2S, this.user3S);
    var url = window.location.href.split("/").pop();
    var urlsubMenu = window.location.href.split(/[\s/]+/);
    url == "account-information" ||
    url == "user-management" ||
    url == "change-password" ||
    url == "terms-condition" ? this.classActive()
      : url == "dealer-profile" ? this.openPage("dealerProfile")
      : url == "outstanding-invoice-cbu" || url == "outstanding-invoice-spareparts" ? this.openPage("report")
      : url == "redirect" ? this.openPage("orderEntry")
      : urlsubMenu[urlsubMenu.length - 2] == "order-entry" ? this.openPage("orderEntry")
      : urlsubMenu[urlsubMenu.length - 2] == "inquiry" ? this.openPage("inquiry")
      : urlsubMenu[urlsubMenu.length - 2] == "outstanding-and-overdue" ? this.openPage("report")
      : urlsubMenu[urlsubMenu.length - 2] == "forecast" ? this.openPage("forecast")
      : urlsubMenu[urlsubMenu.length - 2] == "dealer-target" ? this.openPage("dealtarget")
      : urlsubMenu[urlsubMenu.length - 2] == "claim" ? this.openPage("claim")
      : urlsubMenu[urlsubMenu.length - 2] == "custom-registration" ? this.openPage("customReg")
      : url == "product-checking" ? this.openPage("productCheck")
      : url == "sales-promo" ? this.openPage("salesPromo")
      : urlsubMenu[urlsubMenu.length - 2] == "dealer-reward" ? this.openPage("dealReward")
      : urlsubMenu[urlsubMenu.length - 2] == "dealer-stock" ? this.openPage("dealStock")
      : url == "information-sharing" ? this.openPage("infoShare")
      : this.openPage("termsCondition");
    window.scrollTo(0, 0);
  }
  classActive() {
    this.dealerClass = "";
    this.orderClass = "";
    this.inquiryClass = "";
    this.reportClass = "";
    this.forecastClass = "";
    this.dealTarClass = "";
    this.claimClass = "";
    this.cusRegClass = "";
    this.proCheckClass = "";
    this.salesProClass = "";
    this.dealRewardClass = "";
    this.dealStockClass = "";
    this.infoShareClass = "";
    this.termsConditionClass= "";
  }
  openPage(val) {
    if (val == "dealerProfile") {
      this.router.navigate(["dealer-profile"]);
      this.classActive();
      this.dealerClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      this.show4 = false;
      this.show5 = false;
      this.show6 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "orderEntry") {
      this.show1 = !this.show1;
      this.show1 ? (this.show2 = false) : "";
      this.classActive();
      this.orderClass = "actives";
      this.show3 = false;
      this.show4 = false;
      this.show5 = false;
      this.show6 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "inquiry") {
      this.show2 = !this.show2;
      this.show2 ? (this.show1 = false) : "";
      this.classActive();
      this.inquiryClass = "actives";
      this.show3 = false;
      this.show4 = false;
      this.show5 = false;
      this.show6 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "report") {
      this.show3 = !this.show3;
      this.show3 ? (this.show4 = false) : "";
      this.classActive();
      this.reportClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show5 = false;
      this.show6 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "forecast") {
      this.show4 = !this.show4;
      this.show4 ? (this.show3 = false) : "";
      this.classActive();
      this.forecastClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show5 = false;
      this.show6 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "dealtarget") {
      this.show5 = !this.show5;
      this.show5 ? (this.show4 = false) : "";
      this.classActive();
      this.dealTarClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      this.show6 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "claim") {
      this.show6 = !this.show6;
      this.show6 ? (this.show5 = false) : "";
      this.classActive();
      this.claimClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      this.show4 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "customReg") {
      this.show7 = !this.show7;
      this.show7 ? (this.show6 = false) : "";
      this.classActive();
      this.cusRegClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      this.show4 = false;
      this.show5 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "productCheck") {
      this.router.navigate(["product-checking"]);
      this.classActive();
      this.proCheckClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      this.show4 = false;
      this.show5 = false;
      this.show6 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "salesPromo") {
      this.router.navigate(["sales-promo"]);
      this.classActive();
      this.salesProClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      this.show4 = false;
      this.show5 = false;
      this.show6 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "dealReward") {
      this.show8 = !this.show8;
      this.show8 ? (this.show7 = false) : "";
      this.classActive();
      this.dealRewardClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      this.show4 = false;
      this.show5 = false;
      this.show6 = false;
      this.show9 = false;
      this.show10 = false;
    } else if (val == "dealStock") {
      this.show9 = !this.show9;
      this.show9 ? (this.show8 = false) : "";
      this.classActive();
      this.dealStockClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      this.show4 = false;
      this.show5 = false;
      this.show6 = false;
      this.show7 = false;
      this.show10 = false;
    } else if (val == "infoShare") {
      this.router.navigate(["information-sharing"]);
      this.classActive();
      this.infoShareClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      this.show4 = false;
      this.show5 = false;
      this.show6 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
      this.show10 = false;
    } else {
      this.show10 = !this.show10;
      this.classActive();
      this.termsConditionClass = "actives";
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      this.show4 = false;
      this.show5 = false;
      this.show6 = false;
      this.show7 = false;
      this.show8 = false;
      this.show9 = false;
    }
  }
  openSubPage(val) {
    if (val == "cbu") {
      this.router.navigate(["/order-entry/cbu-order-entry"]);
    } else if (val == "spareparts") {
      this.router.navigate(["/order-entry/spareparts-order-entry"]);
    } else if (val == "pending") {
      this.router.navigate(["/order-entry/pending-transaction"]);
    } else if (val == "backorder") {
      this.router.navigate(["/order-entry/back-order"]);
    } else if (val == "orderstatus") {
      this.router.navigate(["/order-entry/order-status"]);
    } else if (val == "delivery") {
      this.router.navigate(["/order-entry/delivery-information"]);
    } else if (val == "inqCbu") {
      this.router.navigate(["inquiry/cbu"]);
    } else if (val == "inqSpare") {
      this.router.navigate(["inquiry/spareparts"]);
    } else if (val == "reAging") {
      this.router.navigate(["outstanding-and-overdue/ap-aging"]);
    } else if (val == "reInvoice") {
      this.router.navigate(["outstanding-and-overdue/invoice"]);
    } else if (val == "reReport") {
      this.router.navigate(["outstanding-and-overdue/report"]);
    } else if (val == "foEntry") {
      this.router.navigate(["forecast/forecast-entry"]);
    } else if (val == "foReport") {
      this.router.navigate(["forecast/forecast-report"]);
    } else if (val == "dealtargetCBU") {
      this.router.navigate(["dealer-target/dealer-target-cbu"]);
    } else if (val == "dealtargetSpare") {
      this.router.navigate(["dealer-target/dealer-target-sparepart"]);
    } else if (val == "claimForm") {
      this.router.navigate(["claim/warranty-claim-form"]);
    } else if (val == "claimReport") {
      this.router.navigate(["claim/warranty-claim-report"]);
    } else if (val == "logisFormCBU") {
      this.router.navigate(["claim/logistic-claim-form-cbu"]);
    } else if (val == "logisReportCBU") {
      this.router.navigate(["claim/logistic-claim-report-cbu"]);
    } else if (val == "logisFormSpare") {
      this.router.navigate(["claim/logistic-claim-form-spareparts"]);
    } else if (val == "logisReportSpare") {
      this.router.navigate(["claim/logistic-claim-report-spareparts"]);
    } else if (val == "cusRegInput") {
      this.router.navigate(["custom-registration/custom-registration-input"]);
    } else if (val == "cusRegReport") {
      this.router.navigate(["custom-registration/custom-registration-report"]);
    } else if (val == "RewardCBU") {
      this.router.navigate(["dealer-reward/cbu-reward"]);
    } else if (val == "RewardSpare") {
      this.router.navigate(["dealer-reward/sparepart-reward"]);
    } else if (val == "stockInput") {
      this.router.navigate(["dealer-stock/stock-input"]);
    } else if (val == "stockReport") {
      this.router.navigate(["dealer-stock/stock-report"]);
    } else if (val == "howToPay") {
      this.router.navigate(["terms-and-condition/how-to-pay"]);
    } else if (val == "deliveryPolicy") {
      this.router.navigate(["terms-and-condition/delivery-policy"]);
    } else if (val == "returnPolicy") {
      this.router.navigate(["terms-and-condition/return-policy"]);
    } else {
      this.router.navigate(["terms-and-condition/customer-data"]);
    }
  }
}
