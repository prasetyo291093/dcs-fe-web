import { Component, OnInit } from "@angular/core";
import { ProductCheckingService } from "src/app/services/product-checking.service";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-product-checking",
  templateUrl: "./product-checking.component.html",
  styleUrls: ["./product-checking.component.css"]
})
export class ProductCheckingComponent implements OnInit {
  engineNumber: string;
  frameNumber: string;
  product: any = [];
  showMessage: boolean = false;

  constructor(
    private spinner: NgxSpinnerService,
    private productChecking: ProductCheckingService
  ) { }

  ngOnInit() { }
  submit() {
    this.showMessage = false
    this.spinner.show();
    this.product = [];
    if (this.engineNumber && this.frameNumber) {
      let data = { serial_number: this.engineNumber, user: JSON.parse(localStorage.getItem("user")).email };
      this.productChecking.checkProduct(data).subscribe(
        result => {
          if (result["status"] == "success") {
            this.product = result["result"][0];
            this.showMessage = true;
          } else if (result["status"] == "error")
            alert("This product is not registered. \nPlease Contact HPPI");
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          console.log(error);
        }
      );
      // .console.log("kirim yang engine number");
    } else if (this.frameNumber) {
      let data = { serial_number: this.frameNumber, user: JSON.parse(localStorage.getItem("user")).email };
      this.productChecking.checkProduct(data).subscribe(
        result => {
          if (result["status"] == "success") {
            this.product = result["result"][0];
            this.showMessage = true;
          } else if (result["status"] == "error")
            alert("This product is not registered. \nPlease Contact HPPI");
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          console.log(error);
        }
      );
      // console.log("kirim frame number doang");
    } else if (this.engineNumber) {
      let data = { serial_number: this.engineNumber, user: JSON.parse(localStorage.getItem("user")).email };
      this.productChecking.checkProduct(data).subscribe(
        result => {
          if (result["status"] == "success") {
            this.product = result["result"][0];
            this.showMessage = true;
          } else if (result["status"] == "error")
            alert("This product is not registered. \nPlease Contact HPPI");
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          console.log(error);
        }
      );
      // console.log("kirim engine number doang");
    } else {
      console.log("ga bisa submit");
    }
  }
}
