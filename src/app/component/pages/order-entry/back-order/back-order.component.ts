import { Component, OnInit } from "@angular/core";
import { formatDate } from "@angular/common";
import { BackorderConfirmationService } from "../../../../services/backorder-confirmation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { CbuPurchaseOrderService } from "src/app/services/cbu-purchase-order.service";
import { SparepartsPurchaseOrderService } from "src/app/services/spareparts-purchase-order.service";
import { TimeoutError } from 'rxjs';
declare var jquery: any;
declare var $: any;
import * as moment from 'moment';
import { environment } from "src/environments/environment";

@Component({
  selector: "app-back-order",
  templateUrl: "./back-order.component.html",
  styleUrls: ["./back-order.component.css"]
})
export class BackOrderComponent implements OnInit {
  appUrl = environment.appUrl;
  backorder: any = [];
  header: any = [];
  body: any = [];

  total_allocated: any;
  total_discount_before: any;
  total_discount: any;
  vat: any;
  total_allocated_with_vat: any;
  so_number: string;
  payment_deadline: any;
  spart: any;

  entryBO: boolean = true;
  confirmBO: boolean = false;
  bankTFBO: boolean = false;
  Cash: boolean = true;

  disabledPay1: string;
  disabledPay2: string;
  disabledPay3: string;
  disabledPay4: string;

  btnFilter: boolean = false;
  resetFilter: boolean = false;
  filterDate: boolean = false;
  filterNo: boolean = false;
  btnOrderPayment: string = "Confirm Payment";

  data: any;

  soNumber: any;

  so_number_from: any;
  so_number_to: any;
  so_date_from: any;
  so_date_to: any;

  name = "Get Date";
  today = new Date();
  jstoday = "";

  is_tester: boolean = false;

  po_number: any;
  order_date: any;
  bill_to: any;
  ship_to: any;
  remarks: any;
  payment_method: any;
  data_material: any = [];
  freshData: boolean = true;
  filteredData: boolean = false;
  listFiltered: any;
  cash_payment_type: string = "Bank Transfer";
  online_payment_type: string;
  virtualAccount: string;

  constructor(
    private BackorderConfirmation: BackorderConfirmationService,
    private spinner: NgxSpinnerService,
    private cbuOrder: CbuPurchaseOrderService,
    private sparepartsOrder: SparepartsPurchaseOrderService
  ) {
    this.jstoday = formatDate(this.today, "d MMMM y", "en-US", "+0530");
  }

  ngOnInit() {
    this.spinner.show();
    this.btnFilter = true;

    let va = localStorage.getItem("virtualAccount");
    this.virtualAccount = va;

    let subscription = this.BackorderConfirmation.getSONumber().subscribe(res => {
      // console.log(res);
      let listBackOrder = [];
      listBackOrder.push(res);

      // this.spinner.show();
      var sortByDate = function (a, b) {
        var dateA = new Date(b.so_date).getTime();
        var dateB = new Date(a.so_date).getTime();
        if (dateA > dateB) return 1;
        if (dateA < dateB) return -1;
        return 0;
      };
      listBackOrder[0].sort(sortByDate);
      let map = new Map();
      let header: [] = listBackOrder[0].filter(
        val => val.header_status == "YES"
      );
      let bodies: [] = listBackOrder[0].filter(
        val => val.header_status == "NO"
      );
      for (const key in header) {
        for (const key2 in bodies) {
          if (header[key]["vbeln"] == bodies[key2]["vbeln"]) {
            header[key]["so_date"] = bodies[key2]["so_date"];
          }
        }
        if (!map.has(header[key]["confirmid"])) {
          if (
            header[key]["znetwr2"] * 100 + header[key]["zmwsbp2"] * 100 !==
            0
          ) {
            map.set(header[key]["confirmid"], true);
            this.backorder.push(header[key]);
          }
        }
      }
      // for (const item of listBackOrder[0].filter(val => val.header_status == "YES")) {
      //   for (const index in bodies) {
      //     if(item.vbeln == bodies[index].vblen) {
      //       item["so_date"] = bodies[index].so_date
      //     }
      //   }
      //   if(!map.has(item.confirmid)) {
      //     map.set(item.confirmid, true)
      //     this.backorder.push(item)
      //   }
      // }
      // this.backorder = this.backorder.filter(item => item.znetwr2 * 100 + item.zmwsbp2 * 100 !== 0)
      this.backorder.sort(sortByDate);
      // this.backorder.sort(sortByDate)

      // res["headers"].sort(sortByDate)
      // for (var key in res["headers"]) {
      //   // for (const item in res["bodies"]) {
      //   //   if (res["headers"][key]["vbeln"] == res["bodies"][item]["vbeln"])
      //   //     res["headers"][key]["so_date"] = res["bodies"][item]["so_date"]
      //   // }
      //   this.backorder.push(res["headers"][key])
      // }
      this.spinner.hide();
      // console.log(this.backorder)
    }, err => {
      console.log('Request timeout: %s', err instanceof TimeoutError)
    });

    // setTimeout(() => subscription.unsubscribe(), 1);

    this.filterDate = true;
    this.disabledPay3 = "disable-radio";
    this.disabledPay4 = "disable-radio";
  }

  search() {
    var data = {
      so_number_from: this.so_number_from,
      so_number_to: this.so_number_to,
      so_date_from: new Date(this.so_date_from),
      so_date_to: new Date(this.so_date_to)
    };
    console.log(data);
    this.listFiltered = this.backorder;
    this.listFiltered = this.listFiltered.filter((item: any) => {
      let order_date = new Date(item.so_date);
      data.so_date_from.setHours(0, 0, 0, 0);
      data.so_date_to.setHours(0, 0, 0, 0);
      let result: any;

      if (!isNaN(data.so_date_from.getTime()))
        result = order_date.getTime() == data.so_date_from.getTime();

      if (!isNaN(data.so_date_to.getTime()))
        result = order_date.getTime() == data.so_date_to.getTime();

      if (
        !isNaN(data.so_date_from.getTime()) &&
        !isNaN(data.so_date_to.getTime())
      )
        result =
          order_date.getTime() >= data.so_date_from.getTime() &&
          order_date.getTime() <= data.so_date_to.getTime();

      if (data.so_number_from) result = item.vblen == data.so_number_from;

      if (data.so_number_to) result = item.vblen == data.so_number_to;

      if (data.so_number_from && data.so_number_to)
        result =
          item.vblen >= data.so_number_from && item.vblen <= data.so_number_to;

      if (
        !isNaN(data.so_date_from.getTime()) &&
        !isNaN(data.so_date_to.getTime()) &&
        data.so_number_from &&
        data.so_number_to
      )
        result =
          item.vblen >= data.so_number_from &&
          item.vblen <= data.so_number_to &&
          order_date.getTime() >= data.so_date_from.getTime() &&
          order_date.getTime() <= data.so_date_to.getTime();

      return result;
    });
    this.freshData = false;
    this.filteredData = true;
  }
  reset() {
    delete this.so_number_from;
    delete this.so_number_to;
    delete this.so_date_from;
    delete this.so_date_to;

    this.freshData = true;
    this.filteredData = false;
  }

  filter() {
    this.btnFilter = false;
    this.resetFilter = true;
  }
  resetFil() {
    this.btnFilter = true;
    this.resetFilter = false;
  }

  selectfil(event) {
    var z = event.target.value;

    if (z == "order no") {
      this.filterNo = true;
      this.filterDate = false;
    } else {
      this.filterDate = true;
      this.filterNo = false;
    }

    console.log(z);
  }

  backBO() {
    this.entryBO = true;
    this.confirmBO = false;
    this.header.length = 0;
    this.body.length = 0;
  }

  detailBO(vblen, idtrs_detail) {
    this.spinner.show();
    let index_dcs = 0;
    let index_sap = 0;
    let number = 1;
    let total_discount = 0;
    let total_discount_before = 0;

    this.entryBO = false;
    this.confirmBO = true;
    let paymentMethod = JSON.parse(localStorage.getItem("payment"));
    // if (paymentMethod == 'T000')
    //   this.Cash = true;
    // else {
    //   this.Cash = false
    //   this.btnOrderPayment = "Confirm Backorder"
    // }
    // alert(paymentMethod)

    this.BackorderConfirmation.getDetailBO(vblen, idtrs_detail).subscribe(
      res => {
        // console.log(res["body"]);
        var spart = res["header"][0].spart;
        this.spart = spart;
        // console.log(spart)
        for (var key in res["header"]) {
          this.header.push(res["header"][key]);
          this.BackorderConfirmation.storeHeaderBackOrder(this.header[0]);
        }
        for (var key in res["body"]) {
          if (res["body"][key]["zkwmeng2"] > 0)
            this.body.push(res["body"][key]);
        }
        for (var key in this.body) {
          if (this.body[index_sap].uepos == "000000") {
            this.body[index_sap].number = number + ".";
            number++;
          }
          index_sap++;
        }
        console.log(spart);
        for (const key in this.body) {
          // console.log(key)
          total_discount += parseInt(this.body[key].zdiscount);
          total_discount_before += parseInt(this.body[key].zdiscount_gros);
          if (spart == 10) {
            this.cbuOrder
              .getMaterial(this.body[key].matnr, this.body[0].zkunnr_sh)
              .subscribe(
                resmat => {
                  // console.log(resmat)
                  if (resmat) {
                    this.body[key].matwa = resmat["material_description"];
                    this.body[key].mg_description = resmat["mg_description"];
                  }
                },
                err => {
                  console.log(err);
                }
              );
          } else if (spart == 20) {
            this.sparepartsOrder
              .getMaterial(this.body[key].matnr)
              .subscribe(resmat => {
                if (resmat) {
                  this.body[key].matwa = resmat["material_description"];
                  this.body[key].model = resmat["model"];
                }
              });
          }
        }
        this.so_number = res["header"][0].vbeln;
        this.po_number = this.body[0].bstnk;
        this.order_date = this.body[0].bstdk;
        this.bill_to = this.body[0].zkunnr_bp;
        this.ship_to = this.body[0].zkunnr_sh;
        this.remarks = this.body[0].znote;
        this.so_number = this.body[0].vbeln;
        if (this.body[0].abrvw == "Z0") {
          this.Cash = true;
          var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;

          if (customer_code == '0010000024'){
            this.is_tester = true
          }
          this.payment_method = "Cash";
        } else {
          this.Cash = false;
          this.btnOrderPayment = "Confirm Backorder";
          this.payment_method = "Credit";
        }

        this.total_discount_before = total_discount_before
          .toString()
          .replace("-", "");
        this.total_discount = total_discount.toString().replace("-", "");
        this.spinner.hide();
        // for (var key in this.body){
        //   if (spart == 10) {
        //     // console.log(this.body[index_dcs].matnr);
        //     // console.log(this.body[index_dcs].kunnr);
        //     console.log(this.body);
        //     this.cbuOrder.getMaterial(this.body[index_dcs].matnr, this.body[index_dcs].kunnr).subscribe((resMaterial) => {
        //       if (resMaterial) {
        //         console.log(resMaterial)
        //         this.body[index_dcs].matwa = resMaterial["material_description"]
        //         this.body[index_dcs].mg_description = resMaterial["mg_description"]
        //       }else{
        //       }
        //       index_dcs++
        //     }, (err) => {
        //       console.log(err)
        //     })
        //   }
        //   else if (spart == 20) {
        //     this.sparepartsOrder.getMaterial(this.body[index_dcs].matnr).subscribe((resMaterial) => {
        //       if (resMaterial) {
        //         this.body[index_dcs].matwa = resMaterial["material_description"]
        //         this.body[index_dcs].mg_description = resMaterial["mg_description"]
        //       }
        //     index_dcs++
        //     }, (err) => {
        //       console.log(err)
        //     })
        //   }
        // }
      }
    );
  }

  onlinePaymentType(e) {
    console.log(e)
    if (this.disabledPay3 != "disable-radio" && this.disabledPay4 != "disable-radio") {
      if (e == 'klik-bca') {
        this.online_payment_type = "KlikBCA"
      } else if (e == 'bca-klikpay') {
        this.online_payment_type = "BCA KlikPay"
      } else if (e == 'cimb-click') {
        this.online_payment_type = "CIMB Click"
      } else if (e == 'ipay-bni') {
        this.online_payment_type = "iPay BNI"
      } else if (e == 'permata-bankva') {
        this.online_payment_type = "Permata VA"
      } else if (e == 'credit-card') {
        this.online_payment_type = "Credit Card"
      }
    } else {
      this.online_payment_type = ""
    }
    console.log(this.online_payment_type)
  }

  conOrder() {
    var data = {
      confirmid: JSON.parse(localStorage.getItem("backorderHeaderData"))
        .confirmid,
      vbeln: JSON.parse(localStorage.getItem("backorderHeaderData")).vbeln,
      idtrs: JSON.parse(localStorage.getItem("backorderHeaderData")).idtrs,
      approve: "1",
      spart: "BO" + JSON.parse(localStorage.getItem("backorderHeaderData")).spart,
      cash_payment_type: this.cash_payment_type,
      user: JSON.parse(localStorage.getItem("user")).email,
      user_id: JSON.parse(localStorage.getItem("user")).id
    };
    console.log(data);
    var paymentMethod = localStorage.getItem("payment");
    var spart = JSON.parse(localStorage.getItem("backorderHeaderData")).spart;
    this.spinner.show();

    if (spart == "10") {
      // if (remarks.toLowerCase() == 'cash') {
      // localStorage.setItem('payment', '"T000"');
      if (this.payment_method == "Cash") {
        this.cbuOrder.paymentOrder(data).subscribe(
          res => {
            console.log(res);
            if (res["status"] == "success") {
              this.BackorderConfirmation.paymentBO(JSON.parse(localStorage.getItem("backorderHeaderData")).vbeln, JSON.parse(localStorage.getItem("backorderHeaderData")).confirmid).subscribe(
                res => {
                  this.bankTFBO = true;
                  this.confirmBO = false;
                  this.so_number = JSON.parse(
                    localStorage.getItem("backorderHeaderData")
                  ).vbeln;
                  this.total_allocated =
                    JSON.parse(localStorage.getItem("backorderHeaderData"))
                      .znetwr2 * 100;
                  this.vat =
                    JSON.parse(localStorage.getItem("backorderHeaderData"))
                      .zmwsbp2 * 100;
                  this.total_allocated_with_vat =
                    this.total_allocated + this.vat;
                  this.payment_deadline = new Date();
                  this.payment_deadline.setDate(
                    this.payment_deadline.getDate() + 9
                  );
                  this.spinner.hide();
                },
                err => {
                  alert(err);
                  this.spinner.hide();
                }
              );

              let userEmail = JSON.parse(localStorage.getItem("dealer")).email1[0]

              let additionalData = `${res["result"][0]["gjahr"]} - ${res["result"][0]["belnr"]} - ${res["result"][0]["bukrs"]} - ${userEmail}`

              let dateNow = moment()

              let params = {
                type: this.online_payment_type,
                channelId: "",
                currency: "IDR",
                transactionNo: this.so_number,
                transactionAmount: 0,
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCSTRANSACTION",
                customerAccount: "",
                customerName: localStorage.getItem("dealer_name"),
                freeTexts: [{
                  indonesian: "",
                  english: ""
                }],
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect`
              }

              let paramsNonVa = {
                channelId: "",
                transactionNo: this.so_number,
                transactionAmount: 0,
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCS TRANSACTION",
                customerName: localStorage.getItem("dealer_name"),
                customerEmail: JSON.parse(localStorage.getItem("dealer")).email1[0],
                customerPhone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0],
                customerAccount: "",
                type: this.online_payment_type,
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect`
              }
              let paramsCreditCard = {
                channelId: "HondaPower",
                type: "Credit Card",
                transactionNo: this.so_number,
                transactionAmount: 0,
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCS DESC",
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect`
              }
              for (const item of this.header) {
                params.transactionAmount += item.znetwr2 * 100 + item.zmwsbp2 * 100
                paramsNonVa.transactionAmount += item.znetwr2 * 100 + item.zmwsbp2 * 100
                paramsCreditCard.transactionAmount += item.znetwr2 * 100 + item.zmwsbp2 * 100
              }

              if (this.online_payment_type == "") {
                params.type = "BCA VA"
                params.channelId = "HONDAPOWERBVA01"
                params.customerAccount = "61222" + localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "KlikBCA") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKB01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "BCA KlikPay") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKP01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "CIMB Click") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERCIMBCL01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "iPay BNI") {
                paramsNonVa.type = "iPay BNI"
                paramsNonVa.channelId = "HONDAIPAY01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "Permata VA") {
                params.type = "Permata VA"
                params.channelId = "HONDAPOWERPVA01"
                params.customerAccount = "873584" + localStorage.getItem("virtualAccount")
              }

              if (this.online_payment_type == "Permata VA" || this.online_payment_type == "BCA VA") {
                this.cbuOrder.sendPayment(params).subscribe(result => {
                  if (result["insertStatus"] == "00") {

                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  this.spinner.hide()
                  console.error(error)
                })
              } else if (this.online_payment_type == "Credit Card") {
                this.cbuOrder.sendPayment(paramsCreditCard).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    if (result["redirectURL"] !== "") {
                      location.href = result["redirectURL"]
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  this.spinner.hide()
                  console.error(error)
                })
              } else if (this.online_payment_type == "KlikBCA" ||
                this.online_payment_type == "BCA KlikPay" ||
                this.online_payment_type == "CIMB Click" ||
                this.online_payment_type == "iPay BNI") {
                this.cbuOrder.sendPayment(paramsNonVa).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    let redirectURL = result["redirectURL"]
                    let redirectData = ""
                    let urlEncodedDataPairs = [];
                    for (const key in result["redirectData"]) {
                      if (result["redirectData"].hasOwnProperty(key)) {
                        const element = result["redirectData"][key];
                        urlEncodedDataPairs.push(encodeURIComponent(key) + "=" + encodeURIComponent(element))
                      }
                    }
                    redirectData = urlEncodedDataPairs.join("&").replace(/%20/g, '+');
                    if (redirectURL) {
                      let form = document.createElement("form")
                      form.setAttribute("method", "POST")
                      form.setAttribute("enctype", "application/x-www-form-urlencoded")
                      form.setAttribute("action", redirectURL)
                      for (const key in result["redirectData"]) {
                        if (result["redirectData"].hasOwnProperty(key)) {
                          const element = result["redirectData"][key];
                          let input = document.createElement("input")
                          input.setAttribute("type", "hidden")
                          input.setAttribute("name", key)
                          input.setAttribute("value", element)
                          form.appendChild(input)
                        }
                      }
                      document.body.appendChild(form)

                      form.submit()
                    }
                  }
                }, err => {
                  alert(err);
                  this.spinner.hide()
                })
              }
            }
          })
      } else {
        //payment credit
        this.BackorderConfirmation.paymentBO(
          JSON.parse(localStorage.getItem("backorderHeaderData")).vbeln,
          JSON.parse(localStorage.getItem("backorderHeaderData")).confirmid
        ).subscribe(
          res => {
            alert("order success");
            this.spinner.hide();
            location.reload();
          },
          err => {
            alert(err);
            this.spinner.hide();
          }
        );
        alert("order success");
        this.spinner.hide();
        location.reload();
      }

      // SPART 20
    } else {
      if (this.payment_method == "Cash") {
        this.sparepartsOrder.paymentOrder(data).subscribe(
          res => {
            console.log(res);
            if (res["status"] == "success") {
              this.BackorderConfirmation.paymentBO(JSON.parse(localStorage.getItem("backorderHeaderData")).vbeln, JSON.parse(localStorage.getItem("backorderHeaderData")).confirmid).subscribe(
                res => {
                  this.bankTFBO = true;
                  this.confirmBO = false;
                  this.so_number = JSON.parse(
                    localStorage.getItem("backorderHeaderData")
                  ).vbeln;
                  this.total_allocated =
                    JSON.parse(localStorage.getItem("backorderHeaderData"))
                      .znetwr2 * 100;
                  this.vat =
                    JSON.parse(localStorage.getItem("backorderHeaderData"))
                      .zmwsbp2 * 100;
                  this.total_allocated_with_vat =
                    this.total_allocated + this.vat;
                  this.payment_deadline = new Date();
                  this.payment_deadline.setDate(
                    this.payment_deadline.getDate() + 9
                  );
                  this.spinner.hide();
                }, err => {
                  alert(err);
                  this.spinner.hide();
                });

              let userEmail = JSON.parse(localStorage.getItem("dealer")).email1[0]
              // PARAMS FOR PAYMETN GATEWAY
              let additionalData = `${res["result"][0]["gjahr"]} - ${res["result"][0]["belnr"]} - ${res["result"][0]["bukrs"]} - ${userEmail}`

              let dateNow = moment()

              let params = {
                type: this.online_payment_type,
                channelId: "",
                currency: "IDR",
                transactionNo: this.so_number,
                transactionAmount: 0,
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCSTRANSACTION",
                customerAccount: "",
                customerName: localStorage.getItem("dealer_name"),
                freeTexts: [{
                  indonesian: "",
                  english: ""
                }],
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect`
              }

              let paramsNonVa = {
                channelId: "",
                transactionNo: this.so_number,
                transactionAmount: 0,
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCS TRANSACTION",
                customerName: localStorage.getItem("dealer_name"),
                customerEmail: JSON.parse(localStorage.getItem("dealer")).email1[0],
                customerPhone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0],
                customerAccount: "",
                type: this.online_payment_type,
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect`
              }
              let paramsCreditCard = {
                channelId: "HondaPower",
                type: "Credit Card",
                transactionNo: this.so_number,
                transactionAmount: 0,
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCS DESC",
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect`
              }
              for (const item of this.header) {
                params.transactionAmount += item.znetwr2 * 100 + item.zmwsbp2 * 100
                paramsNonVa.transactionAmount += item.znetwr2 * 100 + item.zmwsbp2 * 100
                paramsCreditCard.transactionAmount += item.znetwr2 * 100 + item.zmwsbp2 * 100
              }

              if (this.online_payment_type == "") {
                params.type = "BCA VA"
                params.channelId = "HONDAPOWERBVA01"
                params.customerAccount = "61222" + localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "KlikBCA") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKB01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "BCA KlikPay") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKP01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "CIMB Click") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERCIMBCL01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "iPay BNI") {
                paramsNonVa.type = "iPay BNI"
                paramsNonVa.channelId = "HONDAIPAY01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "Permata VA") {
                params.type = "Permata VA"
                params.channelId = "HONDAPOWERPVA01"
                params.customerAccount = "873584" + localStorage.getItem("virtualAccount")
              }
              // END PARAMS

              if (this.online_payment_type == "Permata VA" || this.online_payment_type == "BCA VA") {
                this.cbuOrder.sendPayment(params).subscribe(result => {
                  if (result["insertStatus"] == "00") {

                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.error(error)
                  this.spinner.hide()
                })
              } else if (this.online_payment_type == "Credit Card") {
                this.cbuOrder.sendPayment(paramsCreditCard).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    if (result["redirectURL"] !== "") {
                      location.href = result["redirectURL"]
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.error(error)
                  this.spinner.hide()
                })
              } else if (this.online_payment_type == "KlikBCA" ||
                this.online_payment_type == "BCA KlikPay" ||
                this.online_payment_type == "CIMB Click" ||
                this.online_payment_type == "iPay BNI") {
                this.cbuOrder.sendPayment(paramsNonVa).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    let redirectURL = result["redirectURL"]
                    console.log(redirectURL)
                    // let redirectData = new FormData()
                    let redirectData = ""
                    let urlEncodedDataPairs = [];
                    for (const key in result["redirectData"]) {
                      if (result["redirectData"].hasOwnProperty(key)) {
                        const element = result["redirectData"][key];
                        urlEncodedDataPairs.push(encodeURIComponent(key) + "=" + encodeURIComponent(element))
                      }
                    }
                    console.log(urlEncodedDataPairs)
                    redirectData = urlEncodedDataPairs.join("&").replace(/%20/g, '+');
                    if (redirectURL) {
                      let form = document.createElement("form")
                      form.setAttribute("method", "POST")
                      form.setAttribute("enctype", "application/x-www-form-urlencoded")
                      form.setAttribute("action", redirectURL)
                      for (const key in result["redirectData"]) {
                        if (result["redirectData"].hasOwnProperty(key)) {
                          const element = result["redirectData"][key];
                          let input = document.createElement("input")
                          input.setAttribute("type", "hidden")
                          input.setAttribute("name", key)
                          input.setAttribute("value", element)
                          form.appendChild(input)
                        }
                      }
                      document.body.appendChild(form)

                      form.submit()
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.error(error)
                  this.spinner.hide()
                })
              }
            } else {
              alert(res["status"]);
              this.spinner.hide();
            }
          }, err => {
            this.spinner.hide();
          });
      } else {
        // CREDIT GOES HERE
        this.BackorderConfirmation.paymentBO(JSON.parse(localStorage.getItem("backorderHeaderData")).vbeln, JSON.parse(localStorage.getItem("backorderHeaderData")).confirmid).subscribe(
          res => {
            alert("order success");
            this.spinner.hide();
            location.reload();
          }, err => {
            alert(err);
            this.spinner.hide();
          });
      }
    }
  }


  totalBackorder() {
    var backorder = 0;
    $("#tableCBUConfirmation #backorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }

  radioActive() {
    this.disabledPay1 = "";
    this.disabledPay2 = "";
    this.disabledPay3 = "";
    this.disabledPay4 = "";
  }
  jenisPay(val) {
    this.radioActive();
    if (val == 1) {
      this.disabledPay3 = "disable-radio";
      this.disabledPay4 = "disable-radio";
      this.cash_payment_type = "Bank Transfer";
    } else {
      this.disabledPay1 = "disable-radio";
      this.disabledPay2 = "disable-radio";
      this.cash_payment_type = "Payment Gateway";
    }
  }

  downloadSO() {
    let data = {
      sold_to_code: this.ship_to || "-",
      bill_to_code: this.bill_to || "-",
      dealer_name: localStorage.getItem("dealer_name"),
      address: JSON.parse(localStorage.getItem("dealer")).address[0] || "-",
      phone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0] || "-",
      fax: JSON.parse(localStorage.getItem("dealer")).fax1[0] || "-",
      po_number: this.po_number || "-",
      so_number: this.so_number || "-",
      confirm_date: this.order_date || "-",
      order_type: "-",
      currency: "IDR",
      payment: this.payment_method || "-",
      email: JSON.parse(localStorage.getItem("dealer")).email1[0] || "-",
      contact: "-",
      material_code: [],
      material_description: [],
      qty: [],
      allocated: [],
      backorder: [],
      unit_price: [],
      total_amount: [],
      ttl_qty: 0,
      ttl_allocated: 0,
      ttl_backorder: 0,
      ttl_unit_price: 0,
      ttl_total_amount: 0,
      discount: [],
      total_discount: 0,
      total_without_vat: 0,
      vat: 0,
      grand_total: 0,
      total_with_vat: 0,
      title: "Order Confirmation"
    }

    // if (this.paymentMethods == "T000")
    //   data.payment = "Cash"
    // else
    //   data.payment = "Credit"

    $("#tableCBUConfirmation #matnr").each(function () {
      let item = $(this).text()
      // item = item.replace(/[, ]+/g, "").trim()
      data.material_code.push(item)
    })
    $("#tableCBUConfirmation #matwa").each(function () {
      let item = $(this).text()
      // item = item.replace(/[, ]+/g, "").trim()
      data.material_description.push(item)
    })
    $("#tableCBUConfirmation #matnr").each(function () {
      // let item = $(this).text()
      // item = item.replace(/[, ]+/g, "").trim()
      data.qty.push("-")
    })
    $("#tableCBUConfirmation #matnr").each(function () {
      // let item = $(this).text()
      // item = item.replace(/[, ]+/g, "").trim()
      data.allocated.push("-")
    })
    $("#tableCBUConfirmation #backorder").each(function () {
      let item = $(this).text()
      // item = item.replace(/[, ]+/g, "").trim()
      data.backorder.push(item)
    })
    $("#tableCBUConfirmation #znetpr").each(function () {
      let item = $(this).text()
      item = item.replace(/[, ]+/g, "").trim()
      data.unit_price.push(item)
    })
    $("#tableCBUConfirmation #zzdiscount").each(function () {
      let item = $(this).text()
      item = item.replace(/[, ]+/g, "").trim()
      data.discount.push(item)
    })
    $("#tableCBUConfirmation #znetwr2").each(function () {
      let item = $(this).text()
      item = item.replace(/[, ]+/g, "").trim()
      data.total_amount.push(item)
    })

    if (data.backorder.length != data.material_code.length) {
      for (let i = 0; i < data.material_code.length - 1; i++) {
        data.backorder.push("0")
      }
    }

    if (data.unit_price.length != data.material_code.length) {
      for (let i = 0; i < data.material_code.length - 1; i++) {
        data.unit_price.push("0")
      }
    }

    if (data.discount.length != data.material_code.length) {
      for (let i = 0; i < data.material_code.length - 1; i++) {
        data.discount.push("0")
      }
    }

    if (data.total_amount.length != data.material_code.length) {
      for (let i = 0; i < data.material_code.length - 1; i++) {
        data.total_amount.push("0")
      }
    }

    data.ttl_qty = 0
    data.ttl_allocated = 0
    data.ttl_backorder = this.totalBackorder()

    for (const item of data.unit_price) {
      data.ttl_unit_price += parseInt(item)
    }
    for (const item of data.total_amount) {
      data.ttl_total_amount += parseInt(item)
    }
    for (const item of data.discount) {
      data.total_discount += parseInt(item)
    }
    data.total_without_vat = this.header[0].znetwr2_gros * 100
    data.vat = this.header[0].zmwsbp2 * 100
    data.grand_total = this.header[0].znetwr2_gros * 100
    data.total_with_vat = this.header[0].znetwr2 * 100 + this.header[0].zmwsbp2 * 100

    console.log(data);
    this.BackorderConfirmation.downloadBackOrder(data).subscribe(result => {
      this.downloadFile(result)
    })
  }

  downloadFile(data) {
    const blob = new Blob([data], { type: 'application/pdf' })
    const url = window.URL.createObjectURL(blob)
    // window.location.href = url
    // window.open
    let link = document.createElement('a')
    link.style.display = 'none';
    link.setAttribute("download", `order-confirm.pdf`)
    link.setAttribute('href', url)
    document.body.appendChild(link)
    link.click()
  }
}
