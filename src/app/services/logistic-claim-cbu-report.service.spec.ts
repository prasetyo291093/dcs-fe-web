import { TestBed } from '@angular/core/testing';

import { LogisticClaimCbuReportService } from './logistic-claim-cbu-report.service';

describe('LogisticClaimCbuReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogisticClaimCbuReportService = TestBed.get(LogisticClaimCbuReportService);
    expect(service).toBeTruthy();
  });
});
