import { TestBed } from '@angular/core/testing';

import { CustomerRegInputService } from './customer-reg-input.service';

describe('CustomerRegInputService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomerRegInputService = TestBed.get(CustomerRegInputService);
    expect(service).toBeTruthy();
  });
});
