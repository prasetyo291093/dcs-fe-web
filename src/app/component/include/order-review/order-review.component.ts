import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PendingTransactionService } from "src/app/services/pending-transaction.service";
import { BackorderConfirmationService } from "src/app/services/backorder-confirmation.service";
import { OrderStatusService } from 'src/app/services/order-status.service';

@Component({
  selector: "app-order-review",
  templateUrl: "./order-review.component.html",
  styleUrls: ["./order-review.component.css"]
})
export class OrderReviewComponent implements OnInit {
  cbuPendingCount: number = 0;
  sparepartPendingCount: number = 0;
  cbuBackorderCount: number = 0;
  sparepartBackorderCount: number = 0;
  salesDailyCBUCount: number = 0;
  salesDailySparepartCount: number = 0;
  salesDailyCBUAmount: number = 0;
  salesDailySparepartAmount: number = 0;

  constructor(
    private router: Router,
    private PendingTransaction: PendingTransactionService,
    private BackorderConfirmation: BackorderConfirmationService,
    private OrderStatus: OrderStatusService
  ) {}

  cash: boolean = false;
  credit: boolean = false;
  user1S: boolean = false;
  user2S: boolean = false;
  user3S: boolean = false;

  ngOnInit() {
    this.cash = true;

    let userClassification = JSON.parse(localStorage.getItem("classification"));
    if (userClassification == "1S" || userClassification == "2S") {
      this.user1S = true;
      this.user2S = true;
    } else if (userClassification == "3S") this.user3S = true;

    // this.credit = true
    this.salesDailyHistoryCount();
    this.pendingTransactionCount();
    this.backOrderConfirmationCount();
  }
  salesDailyHistoryCount() {
    this.OrderStatus.getSONumber().subscribe(res => {
      let list = []
      list.push(res)
      let orderStatusList = []
      orderStatusList = list[0]
      orderStatusList = orderStatusList.filter((val: any) => {
        let order_date = new Date(val.order_date)
        let today = new Date()
        return order_date.getTime() == today.getTime()
      })
      // kwmeng2
      for (const item of orderStatusList.filter(val => val.sales_order_type == "CBU")) {
        this.salesDailyCBUCount += parseInt(item.kwmeng2)
      }
      // this.salesDailyCBUCount = orderStatusList.filter(val => val.sales_order_type == "CBU").length
      for (const item of orderStatusList.filter(val => val.sales_order_type == "Sparepart")) {
        this.salesDailySparepartCount += parseInt(item.kwmeng2)
      }
      // this.salesDailySparepartCount = orderStatusList.filter(val => val.sales_order_type == "Sparepart").length

      for (const item of orderStatusList.filter(val => val.sales_order_type == "CBU")) {
        let allocated = item.znetwr2 * 100
        let vat = item.zmwsbp2 * 100
        this.salesDailyCBUAmount += allocated + vat
      }
      for (const item of orderStatusList.filter(val => val.sales_order_type == "Sparepart")) {
        let allocated = item.znetwr2 * 100
        let vat = item.zmwsbp2 * 100
        this.salesDailySparepartAmount += allocated + vat
      }
    }, (error) => {
      console.log(error)
    });
  }
  backOrderConfirmationCount() {
    this.BackorderConfirmation.getSONumber().subscribe(
      res => {
        let listBackOrder = [];
        let resultBackOrder = [];
        listBackOrder.push(res);
        let map = new Map();
        let header: [] = listBackOrder[0].filter(
          val => val.header_status == "YES"
        );
        let bodies: [] = listBackOrder[0].filter(
          val => val.header_status == "NO"
        );
        for (const key in header) {
          for (const key2 in bodies) {
            if (header[key]["vbeln"] == bodies[key2]["vbeln"]) {
              header[key]["so_date"] = bodies[key2]["so_date"];
            }
          }
          if (!map.has(header[key]["confirmid"])) {
            if (
              header[key]["znetwr2"] * 100 + header[key]["zmwsbp2"] * 100 !==
              0
            ) {
              map.set(header[key]["confirmid"], true);
              resultBackOrder.push(header[key]);
            }
          }
        }
        this.cbuBackorderCount = resultBackOrder.filter(
          val => val.spart == 10
        ).length;
        this.sparepartBackorderCount = resultBackOrder.filter(
          val => val.spart == 20
        ).length;
      },
      err => console.log(err)
    );
  }
  pendingTransactionCount() {
    this.PendingTransaction.getSONumber().subscribe(
      res => {
        var listPending = [];
        let resultPending = [];
        listPending.push(res);
        let map = new Map();
        let mapBO = new Map();
        for (const item of listPending[0]) {
          if (item.is_bo == 0) {
            if (item.sales_order_number) {
              if (parseInt(item.total_allocated_with_vat) !== 0) {
                if (!map.has(item.sales_order_number)) {
                  map.set(item.sales_order_number, true);
                  resultPending.push(item);
                }
              }
            }
          } else {
            if (item.idtrs_detail) {
              // console.log(item.idtrs_detail)
              if (parseInt(item.znetwr2 + item.zmwsbp2) !== 0) {
                if (!mapBO.has(item.confirmid)) {
                  mapBO.set(item.confirmid, true);
                  resultPending.push(item);
                }
              }
            }
          }
        }
        this.cbuPendingCount = resultPending.filter(
          val => val.spart == 10
        ).length;
        this.sparepartPendingCount = resultPending.filter(
          val => val.spart == 20
        ).length;
      },
      err => console.log(err)
    );
  }
  goToOrderStatus() {
    this.router.navigate(["/order-entry/order-status"]);
  }
  goToPendingTransaction() {
    this.router.navigate(["/order-entry/pending-transaction"]);
  }
  goToBackorderConfirmation() {
    this.router.navigate(["/order-entry/back-order"]);
  }
}
