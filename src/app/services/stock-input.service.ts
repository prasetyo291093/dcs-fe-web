import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class StockInputService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  getMaterialDealerStock(group) {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${this.baseUrl}/api/dealer-stock/${group}/${sap_code}`
    );
  }

  submitStockInput(data) {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.post(
      `${this.baseUrl}/api/dealer_stock/input/${sap_code}`,
      data
    );
  }
}
