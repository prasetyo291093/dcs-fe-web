import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogisticClaimReportCbuComponent } from './logistic-claim-report-cbu.component';

describe('LogisticClaimReportCbuComponent', () => {
  let component: LogisticClaimReportCbuComponent;
  let fixture: ComponentFixture<LogisticClaimReportCbuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogisticClaimReportCbuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogisticClaimReportCbuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
