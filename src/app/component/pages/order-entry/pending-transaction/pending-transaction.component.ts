import { Component, OnInit } from "@angular/core";
import { PendingTransactionService } from "../../../../services/pending-transaction.service";
import { NgxSpinnerService } from "ngx-spinner";
import { CbuPurchaseOrderService } from "src/app/services/cbu-purchase-order.service";
import { SparepartsPurchaseOrderService } from "src/app/services/spareparts-purchase-order.service";
import { Router } from "@angular/router";
import * as moment from 'moment';
import { environment } from "src/environments/environment";

declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-pending-transaction",
  templateUrl: "./pending-transaction.component.html",
  styleUrls: ["./pending-transaction.component.css"]
})
export class PendingTransactionComponent implements OnInit {
  appUrl = environment.appUrl;
  btnFilter: boolean = false;
  resetFilter: boolean = false;
  filterDate: boolean = false;
  filterNo: boolean = false;
  paymentMethod: boolean = false;
  paymentDue: boolean = false;
  CashCBU: boolean = true;
  CashSparepart: boolean = true;

  cbuNotBO: boolean = true;
  partNotBO: boolean = true;
  cbuIsBO: boolean = false;
  partIsBO: boolean = false;

  customerAccount: any;
  data: any;
  soNumberCBU: any = [];
  soNumberPart: any = [];
  soNumberBoCBU: any = [];
  soNumberBoPart: any = [];
  listPendingTransaction: any = [];
  cbu: any = [];
  part: any = [];
  cbuBO: any = [];
  partBO: any = [];
  simulateCBU: any = [];

  is_tester: boolean = false

  data_material_cbu: any[];
  data_material_part: any[];

  data_material_cbu_bo: any[];
  data_material_part_bo: any[];

  total_amount_cbu: any;
  total_discount_cbu: any;
  total_allocated_cbu: any;
  vat_cbu: any;
  total_allocated_with_vat_cbu: any;

  total_amount_cbu_bo: any;
  total_discount_cbu_bo: any;
  total_before_discount_cbu_bo: any;
  total_allocated_cbu_bo: any;
  vat_cbu_bo: any;
  total_allocated_with_vat_cbu_bo: any;

  so_number: any;

  disabledPay1: string;
  disabledPay2: string;
  disabledPay3: string;
  disabledPay4: string;

  vbeln: string;
  idtrs: string;
  confirmid: string;
  orderType: string;

  total_amount_sparepart: any;
  total_discount_sparepart: any;
  total_allocated_sparepart: any;
  vat_sparepart: any;
  total_allocated_with_vat_sparepart: any;

  total_amount_sparepart_bo: any;
  total_discount_before_sparepart_bo: any;
  total_discount_sparepart_bo: any;
  total_allocated_sparepart_bo: any;
  vat_sparepart_bo: any;
  total_allocated_with_vat_sparepart_bo: any;

  so_number_from: any;
  so_number_to: any;
  so_date_from: any;
  so_date_to: any;
  confirmCBU: boolean = false;
  confirmSpare: boolean = false;
  tablePending: boolean = true;
  bankTF: boolean = false;

  payment_deadline: any;
  total_allocated_before_discount_cbu: number;
  total_allocated_before_discount_sparepart: any;
  cash_payment_type: string = "Bank Transfer";
  btnContinue: boolean = true;
  btnContinueBO: boolean = true;
  freshData: boolean = true;
  filteredData: boolean = false;
  listFiltered: any[];
  searchQuery: any;
  online_payment_type: string;
  virtualAccount: string;

  // payment_due_date: date;

  constructor(
    private cbuOrder: CbuPurchaseOrderService,
    private sparepartsOrder: SparepartsPurchaseOrderService,
    private PendingTransaction: PendingTransactionService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) { }

  ngOnInit() {
    // $( "#date-1" ).datepicker();
    this.btnFilter = true;
    let va = localStorage.getItem("virtualAccount");
    this.virtualAccount = va;

    this.disabledPay3 = "disable-radio";
    this.disabledPay4 = "disable-radio";

    this.getListPending();
    this.filterDate = true;
  }

  getListPending() {
    this.spinner.show();
    let subscription = this.PendingTransaction.getSONumber().subscribe(res => {
      var listPending = [];
      var resultPending = [];
      var sortByDate = function (a, b) {
        var dateA = new Date(b.order_date).getTime();
        var dateB = new Date(a.order_date).getTime();
        if (dateA > dateB) return 1;
        if (dateA < dateB) return -1;
        return 0;
      };
      listPending.push(res);
      listPending[0].sort(sortByDate);
      let map = new Map();
      let mapBO = new Map();
      console.log(listPending[0]);
      for (const item of listPending[0]) {
        if (item.is_bo == 0) {
          if (item.sales_order_number) {
            if (parseInt(item.total_allocated_with_vat) !== 0) {
              if (!map.has(item.sales_order_number)) {
                map.set(item.sales_order_number, true);
                resultPending.push(item);
              }
            }
          }
        } else {
          if (item.idtrs_detail) {
            // console.log(item.idtrs_detail)
            if (parseInt(item.znetwr2 + item.zmwsbp2) !== 0) {
              if (!mapBO.has(item.confirmid)) {
                mapBO.set(item.confirmid, true);
                resultPending.push(item);
              }
            }
          }
        }
      }
      // console.log(resultPending)
      for (let i = 0; i < resultPending.length; i++) {
        if (parseInt(resultPending[i].spart) == 10)
          resultPending[i].order_type = "CBU";
        else if (parseInt(resultPending[i].spart) == 20)
          resultPending[i].order_type = "Sparepart";

        if (resultPending[i].is_bo == 0) {
          resultPending[i].so_number = resultPending[i].sales_order_number;
          resultPending[i].order_date = new Date(resultPending[i].order_date);
          resultPending[i].payment_due_date = new Date();
          resultPending[i].payment_due_date.setDate(
            resultPending[i].order_date.getDate() + 9
          );
          resultPending[i].amount =
            resultPending[i].total_allocated_with_vat * 100;
        } else {
          console.log(resultPending[i].is_bo);
          resultPending[i].so_number = resultPending[i].vbeln;
          resultPending[i].order_date = new Date(resultPending[i].bstdk);
          resultPending[i].payment_due_date = new Date();
          resultPending[i].payment_due_date.setDate(
            resultPending[i].order_date.getDate() + 9
          );
          resultPending[i].amount = 0;
          listPending[0].forEach(data => {
            let idtrs_detail = resultPending[i].idtrs_detail;
            let vbeln = resultPending[i].vbeln;
            if (
              data.is_bo == 1 &&
              vbeln == data.vbeln &&
              idtrs_detail == data.idtrs_detail
            ) {
              // let total_allocated = data.znetwr2 * 100
              // let vat = data.zmwsbp2 * 100
              if (data.pstyv == "ZS06" || data.pstyv == "ZBOP")
                resultPending[i].amount =
                  resultPending[i].amount +
                  (data.znetwr2 * 100 + data.zmwsbp2 * 100);
            }
            // console.log(data.znetwr2)
          });
          // for (let item of listPending[0].filter(val => val.idtrs_detail == resultPending[i].idtrs_detail && val.vbeln == resultPending[i].vbeln)) {

          // }
          // resultPending[i].amount = resultPending[i].znetwr2 + resultPending[i].zmwsbp2 * 100
        }
      }
      this.listPendingTransaction = resultPending;
      console.log(this.listPendingTransaction);
      this.spinner.hide();
    });
    // let timeOut = 1;
    // setTimeout(() => {
    //   subscription.unsubscribe();
    //   alert(`Request timeout after ${Math.floor(timeOut / 60000)} minutes.`);
    //   this.spinner.hide();
    // }, timeOut);
  }

  search() {
    var data = {
      so_number_from: this.so_number_from,
      so_number_to: this.so_number_to,
      so_date_from: new Date(this.so_date_from),
      so_date_to: new Date(this.so_date_to)
    };
    console.log(data);
    data.so_date_from.setHours(0, 0, 0, 0);
    data.so_date_to.setHours(0, 0, 0, 0);
    this.listFiltered = this.listPendingTransaction;
    this.listFiltered = this.listFiltered.filter((item: any) => {
      let order_date = new Date(item.order_date);
      let result: any;

      if (!isNaN(data.so_date_from.getTime()))
        result = order_date.getTime() == data.so_date_from.getTime();

      if (!isNaN(data.so_date_to.getTime()))
        result = order_date.getTime() == data.so_date_to.getTime();

      if (
        !isNaN(data.so_date_from.getTime()) &&
        !isNaN(data.so_date_to.getTime())
      )
        result =
          order_date.getTime() >= data.so_date_from.getTime() &&
          order_date.getTime() <= data.so_date_to.getTime();

      if (data.so_number_from) result = item.so_number == data.so_number_from;

      if (data.so_number_to) result = item.so_number == data.so_number_to;

      if (data.so_number_from && data.so_number_to)
        result =
          item.so_number >= data.so_number_from &&
          item.so_number <= data.so_number_to;

      if (
        !isNaN(data.so_date_from.getTime()) &&
        !isNaN(data.so_date_to.getTime()) &&
        data.so_number_from &&
        data.so_number_to
      )
        result =
          item.so_number >= data.so_number_from &&
          item.so_number <= data.so_number_to &&
          order_date.getTime() >= data.so_date_from.getTime() &&
          order_date.getTime() <= data.so_date_to.getTime();

      return result;
    });
    this.freshData = false;
    this.filteredData = true;
  }
  reset() {
    delete this.so_number_from;
    delete this.so_number_to;
    delete this.so_date_from;
    delete this.so_date_to;

    this.freshData = true;
    this.filteredData = false;
  }
  searchBox(e) {
    // console.log(e)
    if (e > 0) {
      this.listFiltered = this.listPendingTransaction;
      this.listFiltered = this.listFiltered.filter((item: any) => {
        let result: any;
        // if (item.so_number.indexOf(e) > -1)
        //   result = item.so_number.indexOf(e) > -1

        result =
          item.so_number.indexOf(e) > -1 ||
          item.order_type.indexOf(e) > -1 ||
          item.cash_payment_type.indexOf(e) > -1 ||
          item.amount.indexOf(e) > -1;

        return result;
      });
      this.freshData = false;
      this.filteredData = true;
    } else {
      this.freshData = true;
      this.filteredData = false;
    }
  }

  backCBU() {
    this.confirmCBU = false;
    this.tablePending = true;
    this.confirmSpare = false;
    this.onRefresh();
  }

  backSpare() {
    this.confirmCBU = false;
    this.tablePending = true;
    this.confirmSpare = false;
    this.onRefresh();
  }

  onRefresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    let currentUrl = this.router.url + "?";
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }

  filter() {
    this.btnFilter = false;
    this.resetFilter = true;
  }
  resetFil() {
    this.btnFilter = true;
    this.resetFilter = false;
  }

  selectfil(event) {
    var z = event.target.value;

    if (z == "order no") {
      this.filterNo = true;
      this.filterDate = false;
      this.paymentMethod = false;
      this.paymentDue = false;
    } else if (z == "payment method") {
      this.paymentMethod = true;
      this.filterNo = false;
      this.filterDate = false;
      this.paymentDue = false;
    } else if (z == "payment due") {
      this.paymentDue = true;
      this.paymentMethod = false;
      this.filterDate = false;
      this.filterNo = false;
    } else {
      this.filterDate = true;
      this.filterNo = false;
      this.paymentMethod = false;
      this.paymentDue = false;
    }

    console.log(z);
  }

  conPendingOrder(val, orderType, idtrs, confirmid) {
    console.log(val, orderType, idtrs);
    let paymentMethod = localStorage.getItem("payment");
    this.customerAccount = localStorage.getItem("bank_transfer_va")
    this.orderType = orderType;
    this.spinner.show();
    try {
      if (orderType == "orderCBU") {
        this.PendingTransaction.getPendingTransaction(
          val,
          10,
          idtrs,
          confirmid
        ).subscribe(
          res => {
            console.log(res);
            var result = [];
            var res2 = [];
            result.push(res);
            for (let i = 0; i < result[0].length; i++) {
              res2.push(result[0][i]);
            }
            // console.log(res)
            var data = {
              user_id: res2[0].user_id,
              po_number: res2[0].po_number,
              bill_to: res2[0].bill_to,
              order_date: res2[0].order_date,
              ship_to: res2[0].ship_to,
              remarks: res2[0].remarks,
              material_number: [],
              pallet: [],
              payment_method: res2[0].payment_method,
              cash_payment_type: res2[0].cash_payment_type,
              payment_type: res2[0].payment_type,
              payment_status: res2[0].payment_status,
              sales_order_number: res2[0].sales_order_number,
              idtrs: res2[0].idtrs,
              amount: res2[0].amount,
              ordered: [],
              allocated: [],
              backorder: [],
              unit_price: [],
              discount: [],
              total_amount: [],
              total_allocated_before_discount: res2[0].total_ordered_gros,
              total_discount: res2[0].total_discount,
              total_allocated: res2[0].total_allocated,
              total_ordered: res2[0].total_ordered,
              qty: [],
              vat: res2[0].vat,
              total_allocated_with_vat: res2[0].total_allocated_with_vat,
              confirmid: res2[0].confirmid,
              vbeln: res2[0].sales_order_number
            };
            // console.log(data)
            for (let i = 0; i < res2.length; i++) {
              data.ordered.push(res2[i].ordered ? res2[i].ordered : 0);
              data.allocated.push(res2[i].allocated ? res2[i].allocated : 0);
              data.backorder.push(res2[i].backorder ? res2[i].backorder : 0);
              data.unit_price.push(
                res2[i].unit_price_gros ? res2[i].unit_price_gros : 0
              );
              data.discount.push(
                res2[i].discount_total_order_gros
                  ? res2[i].discount_total_order_gros
                  : 0
              );
              data.material_number.push(res2[i].material_number);
              data.qty.push(res2[i].qty);
              data.total_amount.push(res2[i].total_order_amount_gros);
              data.pallet.push(res2[i].pallet);
            }
            var material_number = [];
            var index = 0;
            for (const item of data.material_number) {
              this.cbuOrder.getMaterial(item, data.ship_to).subscribe(
                resMaterial => {
                  console.log(resMaterial);
                  material_number.push(resMaterial);
                  material_number[index].ordered = parseInt(
                    data.ordered[index]
                  ).toString();
                  material_number[index].allocated = parseInt(
                    data.allocated[index]
                  ).toString();
                  material_number[index].backorder = parseInt(
                    data.backorder[index]
                  ).toString();
                  material_number[index].unit_price =
                    data.unit_price[index] * 100;
                  material_number[index].discount =
                    data.discount[index].toString() * 100;
                  material_number[index].total_amount =
                    data.total_amount[index] * 100;
                  index++;
                },
                error => {
                  this.spinner.hide();
                  console.log(error);
                }
              );
            }
            this.cbu.punchOrnumb = data.po_number;
            this.cbu.billpay = data.bill_to;
            this.cbu.order_date = new Date(data.order_date);
            // this.cbu.order_date = this.cbu.order_date.getDate();
            this.cbu.shipto = data.ship_to;
            this.cbu.remarks = data.remarks;
            this.cbu.sales_order_number = data.sales_order_number;
            // this.cbu.payment_method = data.payment_method;
            // if (paymentMethod == '"T030"')
            //   this.cbu.payment_method = "Credit"
            // else
            this.cbu.payment_method = data.payment_method;

            // data.material_number = material_number
            this.data_material_cbu = material_number;
            this.total_amount_cbu = data.total_ordered * 100;
            this.total_allocated_before_discount_cbu =
              data.total_allocated_before_discount * 100;
            this.total_discount_cbu =
              data.total_discount.replace("-", "") * 100;
            this.total_allocated_cbu = data.total_allocated * 100;
            this.vat_cbu = data.vat * 100;
            this.total_allocated_with_vat_cbu =
              data.total_allocated_with_vat * 100;
            this.so_number = data.sales_order_number;
            this.confirmid = data.confirmid;
            this.vbeln = data.vbeln;
            this.idtrs = data.idtrs;
            if (data.payment_method == "Credit") {
              this.CashCBU = false;
              this.btnContinue = false;
            } else {
              this.CashCBU = true;
              var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;

              if (customer_code == '0010000024'){
                this.is_tester = true
              }
              if(data.cash_payment_type != null)
                this.btnContinue = false;
            }
            console.log(data);
            this.spinner.hide();
          },
          err => {
            this.spinner.hide();
            console.log(err);
          }
        );
        this.confirmCBU = true;
        this.tablePending = false;
        // this.confirmSpare = false;
      } else if (orderType == "orderSparepart") {
        this.PendingTransaction.getPendingTransaction(
          val,
          20,
          idtrs,
          confirmid
        ).subscribe(
          res => {
            var result = [];
            var res2 = [];
            result.push(res);
            for (let i = 0; i < result[0].length; i++) {
              res2.push(result[0][i]);
            }
            var data = {
              user_id: res2[0].user_id,
              po_number: res2[0].po_number,
              bill_to: res2[0].bill_to,
              order_date: res2[0].order_date,
              ship_to: res2[0].ship_to,
              delivery_priority: res2[0].is_priority,
              remarks: res2[0].remarks,
              material_number: [],
              pallet: [],
              cash_payment_type: res2[0].cash_payment_type,
              payment_type: res2[0].payment_type,
              payment_status: res2[0].payment_status,
              sales_order_number: res2[0].sales_order_number,
              idtrs: res2[0].idtrs,
              amount: res2[0].amount,
              ordered: [],
              allocated: [],
              backorder: [],
              unit_price: [],
              discount: [],
              total_amount: [],
              total_allocated_before_discount: res2[0].total_ordered_gros,
              total_discount: res2[0].total_discount,
              total_allocated: res2[0].total_allocated,
              total_ordered: res2[0].total_ordered,
              qty: [],
              vat: res2[0].vat,
              total_allocated_with_vat: res2[0].total_allocated_with_vat,
              confirmid: res2[0].confirmid,
              vbeln: res2[0].sales_order_number
            };

            for (let i = 0; i < res2.length; i++) {
              data.ordered.push(res2[i].ordered);
              data.allocated.push(res2[i].allocated);
              data.backorder.push(res2[i].backorder);
              data.unit_price.push(res2[i].unit_price_gros);
              data.discount.push(
                res2[i].discount_total_order_gros.replace("-", "")
              );
              data.qty.push(res2[i].qty);
              data.total_amount.push(res2[i].total_order_amount_gros);
              data.material_number.push(res2[i].material_number);
              data.pallet.push(res2[i].pallet);
            }
            // for (const item of data.material_number) {
            //   this.cbuOrder.getMaterial(item, data.ship_to).subscribe((resMaterial) => {
            //     material_number.push(resMaterial)
            //     material_number[index].ordered = parseInt(data.ordered[index]).toString()
            //     material_number[index].allocated = parseInt(data.allocated[index]).toString()
            //     material_number[index].backorder = parseInt(data.backorder[index]).toString()
            //     material_number[index].unit_price = data.unit_price[index] * 100
            //     material_number[index].discount = data.discount[index] * 100
            //     material_number[index].total_amount = data.total_amount[index] * 100
            //     index++
            //   }, (error) => {
            //     this.spinner.hide()
            //     console.log(error)
            //   })
            // }
            var material_number = [];
            var index = 0;
            for (let item of data.material_number) {
              this.sparepartsOrder.getMaterial(item).subscribe(
                resMaterial => {
                  console.log(resMaterial);
                  if (resMaterial) {
                    material_number.push(resMaterial);
                    material_number[index].ordered = parseInt(
                      data.ordered[index]
                    ).toString();
                    material_number[index].allocated = parseInt(
                      data.allocated[index]
                    ).toString();
                    material_number[index].backorder = parseInt(
                      data.backorder[index]
                    ).toString();
                    material_number[index].unit_price =
                      data.unit_price[index] * 100;
                    material_number[index].discount =
                      data.discount[index] * 100;
                    material_number[index].total_amount =
                      data.total_amount[index] * 100;
                    index++;
                  }
                },
                err => console.log(err)
              );
            }

            this.part.punchOrnumb = data.po_number;
            this.part.billpay = data.bill_to;
            this.part.shipto = data.ship_to;
            this.part.sales_order_number = data.sales_order_number;
            this.part.remarks = data.remarks;
            if (data.delivery_priority == 2)
              this.part.delivery_priority = "Normal";
            else this.part.delivery_priority = "Urgent";
            // this.part.payment_method = data.payment_type
            if (paymentMethod == '"T030"') {
              // alert('Credit')
              this.part.payment_method = "Credit";
            } else {
              // alert('Cash')
              this.part.payment_method = "Cash";
            }

            this.part.order_date = new Date(data.order_date);
            this.data_material_part = material_number;
            this.total_amount_sparepart = data.total_ordered * 100;
            this.total_allocated_before_discount_sparepart =
              data.total_allocated_before_discount * 100;
            this.total_discount_sparepart = data.total_discount * 100;
            this.total_discount_sparepart = this.total_discount_sparepart
              .toString()
              .replace("-", "");
            this.total_allocated_sparepart = data.total_allocated * 100;
            this.vat_sparepart = data.vat * 100;
            this.total_allocated_with_vat_sparepart =
              data.total_allocated_with_vat * 100;

            this.confirmid = data.confirmid;
            this.vbeln = data.vbeln;
            this.idtrs = data.idtrs;
            this.so_number = data.sales_order_number;

            if (paymentMethod == '"T030"') {
              // if (data.cash_payment_type.toString().toLowerCase() == 'cash') {
              this.CashSparepart = false;
              this.btnContinue = false;
            } else {
              this.CashSparepart = true;
              var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;

              if (customer_code == '0010000024'){
                this.is_tester = true
              }
              if(data.cash_payment_type != null)
                this.btnContinue = false;
              // }
            }

            this.spinner.hide();
            this.tablePending = false;
            this.confirmCBU = false;
          },
          err => {
            this.spinner.hide();
            console.log(err);
          }
        );
        this.confirmSpare = true;
        this.CashSparepart = true;
        var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;

        if (customer_code == '0010000024'){
          this.is_tester = true
        }
        
      } else if (orderType == "orderBOCBU") {
        this.PendingTransaction.getPendingTransaction(
          val,
          10,
          idtrs,
          confirmid
        ).subscribe(res => {
          var result = [];
          var res2 = [];
          result.push(res);
          for (let i = 0; i < result[0].length; i++) {
            res2.push(result[0][i]);
          }
          this.cbuBO.punchOrnumb = res2[0].bstnk;
          this.cbuBO.order_date = res2[0].bstdk;
          this.cbuBO.billpay = res2[0].zkunnr_bp;
          this.cbuBO.shipto = res2[0].zkunnr_sh;
          this.cbuBO.remarks = res2[0].znote;
          this.cbuBO.sales_order_number = res2[0].vbeln;

          this.confirmid = res2[0].confirmid;
          this.vbeln = res2[0].vbeln;
          this.idtrs = res2[0].idtrs;

          if (res2[0].abrvw == "Z0") this.cbuBO.payment_method = "Cash";
          else this.cbuBO.payment_method = "Credit";

          if (res2[0].is_paid == 1) {
            this.btnContinueBO = false;
          } else {
            this.btnContinueBO = true;
          }

          this.total_amount_cbu_bo = 0;
          this.total_discount_cbu_bo = 0;
          this.total_before_discount_cbu_bo = 0;
          this.total_allocated_cbu_bo = 0;
          this.vat_cbu_bo = 0;

          let listOfMaterials = [];
          for (const key in res2) {
            listOfMaterials.push(res2[key].matnr);
            if (res2[key].pstyv == "ZS06" || res2[key].pstyv == "ZBOP") {
              this.total_amount_cbu_bo += res2[key].netpr_gros * 100;
              this.total_before_discount_cbu_bo += res2[key].znetwr2_gros * 100;
              this.total_discount_cbu_bo +=
                res2[key].zdiscount.replace("-", "") * 100;
              this.total_allocated_cbu_bo += res2[key].znetwr2 * 100;
              this.vat_cbu_bo += res2[key].zmwsbp2 * 100;
            }
          }
          this.total_allocated_with_vat_cbu_bo =
            this.total_allocated_cbu_bo + this.vat_cbu_bo;
          let materials = [];
          let i = 0;
          let numb = 1;
          // for (let item in res2) {
          for (const iterator of res2) {
            if (parseInt(iterator.zkwmeng2) !== 0)
              materials.push(iterator);
          }
          for (let item of materials) {
            if (item.uepos == "000000") {
              item.number = numb;
              numb++;
            }
            this.cbuOrder.getMaterial(item.matnr, item.zkunnr_sh).subscribe(
              resMat => {
                // if (resMat) {
                item.material_description = resMat["material_description"];
                item.base_unit_of_measure = resMat["base_unit_of_measure"];
                item.is_continue = resMat["is_continue"];
                item.material_group2 = resMat["material_group2"];
                item.material_number = resMat["material_number"];
                item.material_type = resMat["material_type"];
                item.numerator_for_conversion_to_base_units_of_measure =
                  resMat["numerator_for_conversion_to_base_units_of_measure"];
                item.storage_location = resMat["storage_location"];
                item.volume_unit = resMat["volume_unit"];
                item.mg_description = resMat["mg_description"];

                item.ordered = item.kwmeng;
                item.allocated = item.zkwmeng2;
                item.backorder = item.kwmeng - item.zkwmeng2;
                item.unit_price = item.netpr_gros * 100;
                item.total_amount = item.netpr_gros * item.kwmeng * 100;
                item.discount = item.zdiscount_gros.replace("-", "") * 100;
                i++;
                // }
              },
              error => console.log(error)
            );
          }
          this.data_material_cbu_bo = materials;
          console.log(this.data_material_cbu_bo);
          this.spinner.hide();
        });

        this.confirmCBU = true;
        this.tablePending = false;
        this.confirmSpare = false;

        this.cbuNotBO = false;
        this.cbuIsBO = true;
      } else if (orderType == "orderBOSparepart") {
        this.PendingTransaction.getPendingTransaction(
          val,
          20,
          idtrs,
          confirmid
        ).subscribe(res => {
          var result = [];
          var res2 = [];
          result.push(res);
          for (let i = 0; i < result[0].length; i++) {
            res2.push(result[0][i]);
          }
          this.partBO.punchOrnumb = res2[0].bstnk;
          this.partBO.order_date = res2[0].bstdk;
          this.partBO.billpay = res2[0].zkunnr_bp;
          this.partBO.shipto = res2[0].zkunnr_sh;
          if (res2[0].lprio == "02") this.partBO.delivery_priority = "Normal";
          else this.partBO.delivery_priority = "Urgent";
          this.partBO.remarks = res2[0].znote;
          this.partBO.sales_order_number = res2[0].vbeln;

          if (res2[0].abrvw == "Z0") this.partBO.payment_method = "Cash";
          else this.partBO.payment_method = "Credit";

          console.log(res2)

          if (res2[0].is_paid == 1) {
            this.btnContinueBO = false;
          } else {
            this.btnContinueBO = true;
          }

          this.confirmid = res2[0].confirmid;
          this.vbeln = res2[0].vbeln;
          this.idtrs = res2[0].idtrs;

          this.total_amount_sparepart_bo = 0;
          this.total_discount_sparepart_bo = 0;
          this.total_discount_before_sparepart_bo = 0;
          this.total_allocated_sparepart_bo = 0;
          this.vat_sparepart_bo = 0;
          this.total_allocated_with_vat_sparepart_bo = 0;

          let listOfMaterials = [];
          for (const key in res2) {
            listOfMaterials.push(res2[key].matnr);

            this.total_amount_sparepart_bo +=
              res2[key].kwmeng * res2[key].netpr_gros * 100;
            this.total_discount_before_sparepart_bo +=
              res2[key].znetwr2_gros * 100;
            this.total_discount_sparepart_bo +=
              res2[key].zdiscount.replace("-", "") * 100;
            this.total_allocated_sparepart_bo += res2[key].znetwr2 * 100;
            this.vat_sparepart_bo += res2[key].zmwsbp2 * 100;
          }
          this.total_allocated_with_vat_sparepart_bo =
            this.total_allocated_sparepart_bo + this.vat_sparepart_bo;
          console.log(listOfMaterials);
          let materials = [];
          let index = 0;
          let numb = 1;
          for (const iterator of res2) {
            if (parseInt(iterator.zkwmeng2) !== 0)
              materials.push(iterator);
          }
          for (const item of materials) {
            if (item.uepos == "000000") {
              item.number = numb;
              numb++;
            }
            this.sparepartsOrder.getMaterial(item.matnr).subscribe(resMat => {
              if (resMat) {
                item.base_unit_of_measure = resMat["base_unit_of_measure"];
                item.is_continue = resMat["is_continue"];
                item.material_description = resMat["material_description"];
                item.material_group1 = resMat["material_group1"];
                item.material_number = resMat["material_number"];
                item.material_type = resMat["material_type"];
                item.mg_description = resMat["mg_description"];
                item.model = resMat["model"];
                item.numerator_for_conversion_to_base_units_of_measure =
                  resMat["numerator_for_conversion_to_base_units_of_measure"];
                item.storage_location = resMat["storage_location"];
                item.volume_unit = resMat["volume_unit"];
                item.weight_unit = resMat["weight_unit"];

                item.ordered = item.kwmeng;
                item.allocated = item.zkwmeng2;
                item.backorder = item.kwmeng - item.zkwmeng2;
                item.unit_price = item.netpr_gros * 100;
                item.total_amount = item.netpr_gros * item.kwmeng * 100;
                item.discount = item.zdiscount_gros.replace("-", "") * 100;
                index++;
              }
            });
          }
          // for (let i = 0; i < res2.length; i++) {
          //   this.sparepartsOrder.getMaterial(res2[i].matnr).subscribe((resMat) => {
          //     if (resMat) {
          //       materials.push(resMat)
          //       materials[index].ordered = res2[index].kwmeng
          //       materials[index].allocated = res2[index].zkwmeng2
          //       materials[index].backorder = res2[index].kwmeng - res2[index].zkwmeng2
          //       materials[index].unit_price = res2[index].netpr_gros * 100
          //       materials[index].total_amount = res2[index].kwmeng * res2[index].netpr_gros * 100
          //       materials[index].discount = res2[index].zdiscount_gros.replace('-', '') * 100
          //       materials[i].pstyv = res2[i].pstyv
          //       if (materials[i].pstyv == 'ZS06' || materials[i].pstyv == 'ZBOP') {
          //         materials[i].number = numb
          //         numb++
          //       }
          //       index++
          //     }
          //   }, (error) => console.log(error))
          // }
          // }
          // for (let item of listOfMaterials) {
          //   this.sparepartsOrder.getMaterial(item).subscribe((resMat) => {
          //     if (resMat) {
          //       materials.push(resMat)
          //       materials[index].ordered = res2[index].kwmeng
          //       materials[index].allocated = res2[index].zkwmeng2
          //       materials[index].backorder = res2[index].kwmeng - res2[index].zkwmeng2
          //       materials[index].unit_price = res2[index].netpr_gros * 100
          //       materials[index].total_amount = res2[index].kwmeng * res2[index].netpr_gros * 100
          //       materials[index].discount = res2[index].zdiscount_gros.replace('-', '') * 100
          //       index++
          //     }
          //   }, (error) => console.log(error))
          // }
          this.data_material_part_bo = materials;
          console.log(this.data_material_part_bo)
          this.spinner.hide();
        });

        this.tablePending = false;
        this.confirmCBU = false;
        this.confirmSpare = true;
        this.CashSparepart = true;

        var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;

        if (customer_code == '0010000024'){
          this.is_tester = true
        }

        this.partNotBO = false;
        this.partIsBO = true;
      }
      window.scroll(0, 0);
    } catch (error) {
      this.spinner.hide();
      console.log(error);
    }
  }

  conOrderCBU() {
    var data = {
      confirmid: this.confirmid,
      vbeln: this.vbeln,
      idtrs: this.idtrs,
      approve: "1",
      spart: "10",
      cash_payment_type: this.cash_payment_type,
      user: JSON.parse(localStorage.getItem("user")).email
    };
    var allocated_material = 0;
    $("#tableCBUConfirmation #zkwmeng2").each(function () {
      allocated_material += $(this).text();
    });
    this.spinner.show();
    if (this.total_amount_cbu == 0 || allocated_material == 0) {
      this.spinner.hide();
      alert("Order being process in back order!");
      location.reload();
    } else {
      var paymentMethod = JSON.parse(localStorage.getItem("payment"));
      if (paymentMethod !== '"T030"') {

        this.cbuOrder.paymentOrder(data).subscribe(
          res => {
            var data = [];
            data.push(res);
            // console.log(res);
            // if(res.status == 'success')
            // {
            if (data[0].status_item == "Unknown") {
              this.spinner.hide();
              alert(data[0].messages);
            } else {
              this.bankTF = true;
              this.confirmCBU = false;
              this.payment_deadline = new Date(this.cbu.order_date);
              this.payment_deadline.setDate(
                this.payment_deadline.getDate() + 9
              );

              let userEmail = JSON.parse(localStorage.getItem("dealer")).email1[0]

              // let additionalData = `${res["result"][0]["gjahr"]} - ${res["result"][0]["belnr"]} - ${res["result"][0]["bukrs"]} - ${userEmail}`
              let additionalData =  - `'' - '' - '' - ${userEmail}`
              let dateNow = moment()

              let params = {
                type: this.online_payment_type,
                channelId: "",
                currency: "IDR",
                transactionNo: this.cbu.sales_order_number,
                transactionAmount: this.total_allocated_with_vat_cbu.toString(),
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCSTRANSACTION",
                customerAccount: "",
                customerName: localStorage.getItem("dealer_name"),
                freeTexts: [{
                  indonesian: "",
                  english: ""
                }],
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + this.cbu.sales_order_number
              }

              let paramsNonVa = {
                channelId: "",
                transactionNo: this.cbu.sales_order_number,
                transactionAmount: this.total_allocated_with_vat_cbu.toString(),
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCS TRANSACTION",
                customerName: localStorage.getItem("dealer_name"),
                customerEmail: userEmail,
                customerPhone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0],
                customerAccount: "",
                type: this.online_payment_type,
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + this.cbu.sales_order_number
              }

              let paramsCreditCard = {
                channelId: "HondaPower",
                type: "Credit Card",
                transactionNo: this.cbu.sales_order_number,
                transactionAmount: this.total_allocated_with_vat_cbu.toString(),
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCS DESC",
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + this.cbu.sales_order_number
              }

              if (this.online_payment_type == "") {
                params.type = "BCA VA"
                params.channelId = "HONDAPOWERBVA01"
                params.customerAccount = "61222" + localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "KlikBCA") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKB01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "BCA KlikPay") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKP01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "CIMB Click") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERCIMBCL01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "iPay BNI") {
                paramsNonVa.type = "iPay BNI"
                paramsNonVa.channelId = "HONDAIPAY01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "Permata VA") {
                params.type = "Permata VA"
                params.channelId = "HONDAPOWERPVA01"
                params.customerAccount = "873584" + localStorage.getItem("virtualAccount")
              }
              if (this.online_payment_type == "Permata VA" || this.online_payment_type == "BCA VA") {
                this.cbuOrder.sendPayment(params).subscribe(result => {
                  if (result["insertStatus"] == "00") {

                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.error(error)
                  this.spinner.show()
                });
              } else if (this.online_payment_type == "Credit Card") {
                this.cbuOrder.sendPayment(paramsCreditCard).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    if (result["redirectURL"] !== "") {
                      location.href = result["redirectURL"]
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.error(error)
                  this.spinner.show()
                });
              } else if (this.online_payment_type == "KlikBCA" ||
                this.online_payment_type == "BCA KlikPay" ||
                this.online_payment_type == "CIMB Click" ||
                this.online_payment_type == "iPay BNI") {
                this.cbuOrder.sendPayment(paramsNonVa).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    let redirectURL = result["redirectURL"]
                    console.log(redirectURL)
                    let redirectData = ""
                    let urlEncodedDataPairs = [];
                    for (const key in result["redirectData"]) {
                      if (result["redirectData"].hasOwnProperty(key)) {
                        const element = result["redirectData"][key];
                        urlEncodedDataPairs.push(encodeURIComponent(key) + "=" + encodeURIComponent(element))
                      }
                    }
                    console.log(urlEncodedDataPairs)
                    redirectData = urlEncodedDataPairs.join("&").replace(/%20/g, '+');
                    if (redirectURL) {
                      let form = document.createElement("form")
                      form.setAttribute("method", "POST")
                      form.setAttribute("enctype", "application/x-www-form-urlencoded")
                      form.setAttribute("action", redirectURL)
                      for (const key in result["redirectData"]) {
                        if (result["redirectData"].hasOwnProperty(key)) {
                          const element = result["redirectData"][key];
                          let input = document.createElement("input")
                          input.setAttribute("type", "hidden")
                          input.setAttribute("name", key)
                          input.setAttribute("value", element)
                          form.appendChild(input)
                        }
                      }
                      document.body.appendChild(form)

                      form.submit()
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  this.spinner.hide()
                  console.error(error)
                });
              } else {

              }
              this.spinner.hide();
            }
          },
          err => {
            this.spinner.hide();
            console.log(err);
          }
        );
        // !!!!!!!!! Uncomment to HERE
      } else {
        alert("order success");
        this.spinner.hide();
        location.reload();
      }
    }
  }

  conOrderSparepart() {
    this.spinner.show();
    var data = {
      confirmid: this.confirmid,
      vbeln: this.vbeln,
      idtrs: this.idtrs,
      approve: "1",
      spart: "20",
      cash_payment_type: this.cash_payment_type,
      user: JSON.parse(localStorage.getItem("user")).email
    };
    console.log(data);
    
    // var paymentMethod = localStorage.getItem('payment');
    var allocated_material = 0;
    $("#tableSparepartConfirm #zkwmeng2").each(function () {
      allocated_material += $(this).text();
    });
    if (this.total_amount_sparepart == 0 || allocated_material == 0) {
      this.spinner.hide();
      alert("Order being process in back order!");
      location.reload();
    } else {
      var paymentMethod = JSON.parse(localStorage.getItem("payment"));
      if (paymentMethod !== '"T030"') {

        // !!!!!!! Uncomment from HERE
        this.sparepartsOrder.paymentOrder(data).subscribe(
          res => {
            var data = [];
            console.log(res)
            data.push(res);
            console.log(res[0]);
            if (data[0].status_item == "Unknown") {
              this.spinner.hide();
              alert(data[0].messages);
            } else {
              this.payment_deadline = new Date(this.part.order_date);
              this.payment_deadline.setDate(
                this.payment_deadline.getDate() + 9
              );
              this.bankTF = true;
              this.confirmSpare = false;

              let userEmail = JSON.parse(localStorage.getItem("dealer")).email1[0]

              // let additionalData = `${res["result"][0]["gjahr"]} - ${res["result"][0]["belnr"]} - ${res["result"][0]["bukrs"]} - ${userEmail}`
              let additionalData =  - `'' - '' - '' - ${userEmail}`

              let dateNow = moment()

              let params = {
                type: this.online_payment_type,
                channelId: "",
                currency: "IDR",
                transactionNo: this.part.sales_order_number,
                transactionAmount: this.total_allocated_with_vat_sparepart.toString(),
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCSTRANSACTION",
                customerAccount: "",
                customerName: localStorage.getItem("dealer_name"),
                freeTexts: [{
                  indonesian: "",
                  english: ""
                }],
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + this.part.sales_order_number
              }

              let paramsNonVa = {
                channelId: "",
                transactionNo: this.part.sales_order_number,
                transactionAmount: this.total_allocated_with_vat_sparepart.toString(),
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCS TRANSACTION",
                customerName: localStorage.getItem("dealer_name"),
                customerEmail: userEmail,
                customerPhone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0],
                customerAccount: "",
                type: this.online_payment_type,
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + this.part.sales_order_number
              }

              let paramsCreditCard = {
                channelId: "HondaPower",
                type: "Credit Card",
                transactionNo: this.cbu.sales_order_number,
                transactionAmount: this.total_allocated_with_vat_sparepart.toString(),
                transactionDate: dateNow.format("YYYY-MM-DD HH:mm:ss"),
                transactionExpire: dateNow.add('1', 'year').format("YYYY-MM-DD HH:mm:ss"),
                description: "DCS DESC",
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + this.part.sales_order_number
              }

              if (this.online_payment_type == "") {
                params.type = "BCA VA"
                params.channelId = "HONDAPOWERBVA01"
                params.customerAccount = "61222" + localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "KlikBCA") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKB01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "BCA KlikPay") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKP01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "CIMB Click") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERCIMBCL01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "iPay BNI") {
                paramsNonVa.type = "iPay BNI"
                paramsNonVa.channelId = "HONDAIPAY01"
                paramsNonVa.customerAccount = localStorage.getItem("virtualAccount")
              } else if (this.online_payment_type == "Permata VA") {
                params.type = "Permata VA"
                params.channelId = "HONDAPOWERPVA01"
                params.customerAccount = "873584" + localStorage.getItem("virtualAccount")
              }

              if (this.online_payment_type == "Permata VA" || this.online_payment_type == "BCA VA") {
                this.cbuOrder.sendPayment(params).subscribe(result => {
                  if (result["insertStatus"] == "00") {

                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  this.spinner.hide()
                  console.error(error)
                });
              } else if (this.online_payment_type == "Credit Card") {
                this.cbuOrder.sendPayment(paramsCreditCard).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    if (result["redirectURL"] !== "") {
                      location.href = result["redirectURL"]
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  this.spinner.hide()
                  console.error(error)
                });
              } else if (this.online_payment_type == "KlikBCA" ||
                this.online_payment_type == "BCA KlikPay" ||
                this.online_payment_type == "CIMB Click" ||
                this.online_payment_type == "iPay BNI") {
                this.cbuOrder.sendPayment(paramsNonVa).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    let redirectURL = result["redirectURL"]
                    let redirectData = ""
                    let urlEncodedDataPairs = [];
                    for (const key in result["redirectData"]) {
                      if (result["redirectData"].hasOwnProperty(key)) {
                        const element = result["redirectData"][key];
                        urlEncodedDataPairs.push(encodeURIComponent(key) + "=" + encodeURIComponent(element))
                      }
                    }
                    console.log(urlEncodedDataPairs)
                    redirectData = urlEncodedDataPairs.join("&").replace(/%20/g, '+');
                    if (redirectURL) {
                      let form = document.createElement("form")
                      form.setAttribute("method", "POST")
                      form.setAttribute("enctype", "application/x-www-form-urlencoded")
                      form.setAttribute("action", redirectURL)
                      for (const key in result["redirectData"]) {
                        if (result["redirectData"].hasOwnProperty(key)) {
                          const element = result["redirectData"][key];
                          let input = document.createElement("input")
                          input.setAttribute("type", "hidden")
                          input.setAttribute("name", key)
                          input.setAttribute("value", element)
                          form.appendChild(input)
                        }
                      }
                      document.body.appendChild(form)

                      form.submit()
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {

                });
              }


              this.spinner.hide();
            }
          },
          err => {
            this.spinner.hide();
            console.log(err);
          }
        );
        // !!!!!!!! Uncomment to HERE
      } else {
        alert("order success");
        this.spinner.hide();
        location.reload();
      }
    }
  }
  totalConfirmationOrderQtyCBU() {
    var totalOrder = 0;
    $("#tableCBUConfirmation #kwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }
  totalOrderCBUBO() {
    var total = 0;
    $("#tableCBUConfirmationBO #netwr").each(function () {
      total =
        total +
        parseInt(
          $(this)
            .text()
            .toString()
            .replace(/,/g, "")
        );
    });
    return total;
  }

  totalConfirmationOrderQtyCBUBO() {
    var totalOrder = 0;
    $("#tableCBUConfirmationBO #kwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalAllocatedCBU() {
    var allocated = 0;
    $("#tableCBUConfirmation #zkwmeng2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    return allocated;
  }

  totalAllocatedCBUBO() {
    var allocated = 0;
    $("#tableCBUConfirmationBO #zkwmeng2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    return allocated;
  }

  totalBackorderCBU() {
    var backorder = 0;
    $("#tableCBUConfirmation #backorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }
  totalBackorderCBUBO() {
    var backorder = 0;
    $("#tableCBUConfirmationBO #backorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }
  totalBackorderSparepart() {
    var backorder = 0;
    $("#tableSparepartConfirm #backorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }

  totalBackorderSparepartBO() {
    var backorder = 0;
    $("#tableSparepartConfirmBO #backorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }

  radioActive() {
    this.disabledPay1 = "";
    this.disabledPay2 = "";
    this.disabledPay3 = "";
    this.disabledPay4 = "";
  }
  jenisPay(val) {
    this.radioActive();
    if (val == 1) {
      this.disabledPay3 = "disable-radio";
      this.disabledPay4 = "disable-radio";
      this.cash_payment_type = "Bank Transfer";
    } else {
      this.disabledPay1 = "disable-radio";
      this.disabledPay2 = "disable-radio";
      this.cash_payment_type = "Payment Gateway";
    }
  }

  onlinePaymentType(e) {
    console.log(e)
    if (this.disabledPay3 != "disable-radio" && this.disabledPay4 != "disable-radio") {
      if (e == 'klik-bca') {
        this.online_payment_type = "KlikBCA"
      } else if (e == 'bca-klikpay') {
        this.online_payment_type = "BCA KlikPay"
      } else if (e == 'cimb-click') {
        this.online_payment_type = "CIMB Click"
      } else if (e == 'ipay-bni') {
        this.online_payment_type = "iPay BNI"
      } else if (e == 'permata-bankva') {
        this.online_payment_type = "Permata VA"
      } else if (e == 'credit-card') {
        this.online_payment_type = "Credit Card"
      }
    } else {
      this.online_payment_type = ""
    }
  }

  downloadSO() {
    let data = {
      sold_to_code: this.cbu.shipto || this.part.shipto || this.cbuBO.shipto || this.partBO.shipto,
      bill_to_code: this.cbu.billpay || this.part.billpay || this.partBO.billpay || this.cbuBO.billpay,
      dealer_name: localStorage.getItem("dealer_name"),
      address: JSON.parse(localStorage.getItem("dealer")).address[0] || "-",
      phone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0] || "-",
      fax: JSON.parse(localStorage.getItem("dealer")).fax1[0] || "-",
      po_number: this.cbu.punchOrnumb || this.cbuBO.punchOrnumb || this.part.punchOrnumb || this.partBO.punchOrnumb,
      so_number: this.cbu.sales_order_number || this.cbuBO.sales_order_number || this.part.sales_order_number || this.partBO.sales_order_number,
      confirm_date: this.cbu.order_date || this.cbuBO.order_date || this.part.order_date || this.partBO.order_date,
      order_type: this.part.delivery_priority || this.partBO.delivery_priority || "-",
      currency: "IDR",
      payment: "-",
      email: JSON.parse(localStorage.getItem("dealer")).email1[0] || "-",
      contact: "-",
      material_code: [],
      material_description: [],
      qty: [],
      allocated: [],
      backorder: [],
      unit_price: [],
      total_amount: [],
      ttl_qty: 0,
      ttl_allocated: 0,
      ttl_backorder: 0,
      ttl_unit_price: 0,
      ttl_total_amount: 0,
      discount: [],
      total_discount: 0,
      total_without_vat: 0,
      vat: 0,
      grand_total: 0,
      total_with_vat: 0,
      title: "Order Confirmation"
    }

    // if (this.paymentMethods == "T000")
    //   data.payment = "Cash"
    // else
    //   data.payment = "Credit"

    if (this.orderType == "orderCBU") {
      data.payment = this.cbu.payment_method

      $("#tableCBUConfirmation #matCode").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.material_code.push(item)
      })
      $("#tableCBUConfirmation #matDesc").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.material_description.push(item)
      })
      $("#tableCBUConfirmation #kwmeng").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.qty.push(item)
      })
      $("#tableCBUConfirmation #zkwmeng2").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.allocated.push(item)
      })
      $("#tableCBUConfirmation #backorder").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.backorder.push(item)
      })
      $("#tableCBUConfirmation #netpr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.unit_price.push(item)
      })
      $("#tableCBUConfirmation #zdiscount").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.discount.push(item)
      })
      $("#tableCBUConfirmation #netwr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.total_amount.push(item)
      })

      data.ttl_qty = this.totalConfirmationOrderQtyCBU()
      data.ttl_allocated = this.totalAllocatedCBU()
      data.ttl_backorder = this.totalBackorderCBU()

      for (const item of data.unit_price) {
        data.ttl_unit_price += parseInt(item)
      }
      for (const item of data.total_amount) {
        data.ttl_total_amount += parseInt(item)
      }
      for (const item of data.discount) {
        data.total_discount += parseInt(item)
      }
      data.total_without_vat = this.total_allocated_before_discount_cbu
      data.vat = this.vat_cbu
      data.grand_total = this.total_amount_cbu
      data.total_with_vat = this.total_allocated_with_vat_cbu
    } else if (this.orderType == "orderSparepart") {
      data.payment = this.part.payment_method
      $("#tableSparepartConfirm #matnr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.material_code.push(item)
      })
      $("#tableSparepartConfirm #matwa").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.material_description.push(item)
      })
      $("#tableSparepartConfirm #kwmeng").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.qty.push(item)
      })
      $("#tableSparepartConfirm #zkwmeng2").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.allocated.push(item)
      })
      $("#tableSparepartConfirm #backorder").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.backorder.push(item)
      })
      $("#tableSparepartConfirm #netpr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.unit_price.push(item)
      })
      $("#tableSparepartConfirm #zdiscount").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.discount.push(item)
      })
      $("#tableSparepartConfirm #netwr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.total_amount.push(item)
      })

      data.ttl_qty = this.totalConfirmationOrderQtySparepart()
      data.ttl_allocated = this.totalAllocatedQtySubmitSparepart()
      data.ttl_backorder = this.totalBackorderSparepart()

      for (const item of data.unit_price) {
        data.ttl_unit_price += parseInt(item)
      }
      for (const item of data.total_amount) {
        data.ttl_total_amount += parseInt(item)
      }
      for (const item of data.discount) {
        data.total_discount += parseInt(item)
      }

      data.total_without_vat = this.total_allocated_before_discount_sparepart
      data.vat = this.vat_sparepart
      data.grand_total = this.total_amount_sparepart
      data.total_with_vat = this.total_allocated_with_vat_sparepart

    } else if (this.orderType == "orderBOCBU") {

      data.payment = this.cbuBO.payment_method
      $("#tableCBUConfirmationBO #matCode").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.material_code.push(item)
      })
      $("#tableCBUConfirmationBO #matDesc").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.material_description.push(item)
      })
      $("#tableCBUConfirmationBO #matCode").each(function () {
        // let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.qty.push("-")
      })
      $("#tableCBUConfirmationBO #matCode").each(function () {
        // let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.allocated.push("-")
      })
      $("#tableCBUConfirmationBO #backorder").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.backorder.push(item)
      })

      $("#tableCBUConfirmationBO #netpr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.unit_price.push(item)
      })
      $("#tableCBUConfirmationBO #zdiscount").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.discount.push(item)
      })
      $("#tableCBUConfirmationBO #netwr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.total_amount.push(item)
      })

      if (data.backorder.length != data.material_code.length) {
        for (let i = 0; i < data.material_code.length - 1; i++) {
          data.backorder.push("0")
        }
      }

      if (data.unit_price.length != data.material_code.length) {
        for (let i = 0; i < data.material_code.length - 1; i++) {
          data.unit_price.push("0")
        }
      }

      if (data.discount.length != data.material_code.length) {
        for (let i = 0; i < data.material_code.length - 1; i++) {
          data.discount.push("0")
        }
      }

      if (data.total_amount.length != data.material_code.length) {
        for (let i = 0; i < data.material_code.length - 1; i++) {
          data.total_amount.push("0")
        }
      }


      data.ttl_qty = 0
      data.ttl_allocated = 0
      data.ttl_backorder = this.totalBackorderCBUBO()

      for (const item of data.unit_price) {
        data.ttl_unit_price += parseInt(item)
      }
      for (const item of data.total_amount) {
        data.ttl_total_amount += parseInt(item)
      }
      for (const item of data.discount) {
        data.total_discount += parseInt(item)
      }
      data.total_without_vat = this.total_before_discount_cbu_bo
      data.vat = this.vat_cbu_bo
      data.grand_total = this.totalOrderCBUBO()
      data.total_with_vat = this.total_allocated_with_vat_cbu_bo

    } else if (this.orderType == "orderBOSparepart") {

      data.payment = this.partBO.payment_method

      $("#tableSparepartConfirmBO #matnr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.material_code.push(item)
      })
      $("#tableSparepartConfirmBO #matwa").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.material_description.push(item)
      })
      $("#tableSparepartConfirmBO #matnr").each(function () {
        // let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.qty.push("-")
      })
      $("#tableSparepartConfirmBO #matnr").each(function () {
        // let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.allocated.push("-")
      })
      $("#tableSparepartConfirmBO #backorder").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.backorder.push(item)
      })
      $("#tableSparepartConfirmBO #netpr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.unit_price.push(item)
      })
      $("#tableSparepartConfirmBO #zdiscount").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.discount.push(item)
      })
      $("#tableSparepartConfirmBO #netwr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.total_amount.push(item)
      })

      if (data.backorder.length != data.material_code.length) {
        for (let i = 0; i < data.material_code.length - 1; i++) {
          data.backorder.push("0")
        }
      }

      if (data.unit_price.length != data.material_code.length) {
        for (let i = 0; i < data.material_code.length - 1; i++) {
          data.unit_price.push("0")
        }
      }

      if (data.discount.length != data.material_code.length) {
        for (let i = 0; i < data.material_code.length - 1; i++) {
          data.discount.push("0")
        }
      }

      if (data.total_amount.length != data.material_code.length) {
        for (let i = 0; i < data.material_code.length - 1; i++) {
          data.total_amount.push("0")
        }
      }

      data.ttl_qty = 0
      data.ttl_allocated = 0
      data.ttl_backorder = this.totalBackorderSparepartBO()

      for (const item of data.unit_price) {
        data.ttl_unit_price += parseInt(item)
      }
      for (const item of data.total_amount) {
        data.ttl_total_amount += parseInt(item)
      }
      for (const item of data.discount) {
        data.total_discount += parseInt(item)
      }
      data.total_without_vat = this.total_discount_before_sparepart_bo
      data.vat = this.vat_sparepart_bo
      data.grand_total = this.total_amount_sparepart_bo
      data.total_with_vat = this.total_allocated_with_vat_sparepart_bo

    }
    console.log(data);
    this.cbuOrder.downloadSO(data).subscribe(result => {
      this.downloadFile(result)
    })
  }

  downloadFile(data) {
    const blob = new Blob([data], { type: 'application/pdf' })
    const url = window.URL.createObjectURL(blob)
    // window.location.href = url
    // window.open
    let link = document.createElement('a')
    link.style.display = 'none';
    link.setAttribute("download", `order-confirm.pdf`)
    link.setAttribute('href', url)
    document.body.appendChild(link)
    link.click()
  }

  totalConfirmationOrderQtySparepart() {
    var totalOrder = 0;
    $("#tableSparepartConfirm #kwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }
  totalConfirmationOrderQtySparepartBO() {
    var totalOrder = 0;
    $("#tableSparepartConfirmBO #kwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }
  totalAllocatedQtySubmitSparepart() {
    var allocated = 0;
    $("#tableSparepartConfirm #zkwmeng2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    return allocated;
  }

  totalAllocatedQtySubmitSparepartBO() {
    var allocated = 0;
    $("#tableSparepartConfirmBO #zkwmeng2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    return allocated;
  }

  totalConfirmationOrderAmountSparepart() {
    var totalOrder = 0;
    $("#tableSparepartConfirm #netwr").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }
}
