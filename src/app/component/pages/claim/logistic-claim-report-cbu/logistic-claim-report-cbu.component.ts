import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import {
  NgxGalleryOptions,
  NgxGalleryImage,
  NgxGalleryAnimation,
  NgxGalleryImageSize
} from "ngx-gallery";
import { LogisticClaimCbuReportService } from "src/app/services/logistic-claim-cbu-report.service";
import { NgxSpinnerService } from "ngx-spinner";
declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-logistic-claim-report-cbu",
  templateUrl: "./logistic-claim-report-cbu.component.html",
  styleUrls: ["./logistic-claim-report-cbu.component.css"]
})
export class LogisticClaimReportCbuComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  claimReport: any = [];
  claimNoList: any = [];
  matCodeList: any = [];
  dnNoList: any = [];
  claimDateFrom: any;
  claimDateTo: any;
  dnNumber: string;
  claimNumber: string;
  claimStatus: string;
  materialCode: string;
  listReportFiltered: any = [];
  freshData: boolean = true;
  filteredData: boolean = false;
  ChronoClaimMore: string;

  constructor(
    private router: Router,
    private modalService: BsModalService,
    private spinner: NgxSpinnerService,
    private claimReportCbu: LogisticClaimCbuReportService
  ) {}

  btnFilter: boolean = false;
  resetFilter: boolean = false;
  modalRef: BsModalRef;

  ngOnInit() {
    this.btnFilter = true;
    this.spinner.show();
    this.claimReportCbu.getLogisticClaimReportCbu().subscribe(
      _result => {
        if (Object.entries(_result).length > 0) {
          let sortByDate = function(a, b) {
            let dateA = new Date(b.created_at).getTime();
            let dateB = new Date(a.created_at).getTime();
            if (dateA > dateB) return 1;
            if (dateA < dateB) return -1;
            return 0;
          };

          this.claimReport = _result;
          this.claimReport.sort(sortByDate);

          for (const item of this.claimReport) {
            if(item.state == 0) {
              item.approval_state = "Rejected"
            } else if(item.state == 1) {
              item.approval_state = "Submitted"
            } else if(item.state == 2) {
              item.approval_state = "Revise"
            } else if(item.state == 9) {
              item.approval_state = "Approved"
            }
          }

          let mapClaimNumber = new Map();
          let mapMaterialCode = new Map();
          let mapDnNumber = new Map();

          for (const item of this.claimReport) {
            if (
              !mapClaimNumber.has(item.claim_number) &&
              item.claim_number != null
            ) {
              mapClaimNumber.set(item.claim_number, true);
              this.claimNoList.push(item.claim_number);
            }
          }

          for (const item of this.claimReport) {
            if (
              !mapMaterialCode.has(item.material_code) &&
              item.material_code != null
            ) {
              mapMaterialCode.set(item.material_code, true);
              this.matCodeList.push(item.material_code);
            }
          }

          for (const item of this.claimReport) {
            if (!mapDnNumber.has(item.dn_number) && item.dn_number != null) {
              mapDnNumber.set(item.dn_number, true);
              this.dnNoList.push(item.dn_number);
            }
          }
        }
        this.spinner.hide();
      },
      (error: Error) => {
        console.log(error.message);
        this.spinner.hide();
      }
    );

    // $('#lightSlider').lightSlider({
    //   gallery: true,
    //   item: 1,
    //   loop: true,
    //   slideMargin: 0,
    //   thumbItem: 4
    // });

    this.galleryOptions = [
      {
        width: "600px",
        height: "400px",
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide,
        thumbnails: false,
        imageSize: NgxGalleryImageSize.Contain,
        previewFullscreen: false,
        preview: false,
        imageInfinityMove: true
      },
      // max-width 800
      {
        breakpoint: 800,
        width: "100%",
        height: "600px",
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];

    // this.galleryImages = [
    //   {
    //     medium: "assets/img/landscape.jpg"
    //   },
    //   {
    //     medium: "assets/img/potrait.jpg"
    //   },
    //   {
    //     medium: "assets/img/gall1.png"
    //   }
    // ];
  }

  openModal(template, i) {
    this.spinner.show();
    if (this.freshData == true) {
      this.galleryImages = [
        {
          medium: this.claimReport[i].img_front
        },
        {
          medium: this.claimReport[i].img_rear
        },
        {
          medium: this.claimReport[i].img_packaging
        },
        {
          medium: this.claimReport[i].img_right
        },
        {
          medium: this.claimReport[i].img_left
        },
        {
          medium: this.claimReport[i].img_barcode
        }
      ];
    } else {
      this.galleryImages = [
        {
          medium: this.listReportFiltered[i].img_front
        },
        {
          medium: this.listReportFiltered[i].img_rear
        },
        {
          medium: this.listReportFiltered[i].img_packaging
        },
        {
          medium: this.listReportFiltered[i].img_right
        },
        {
          medium: this.listReportFiltered[i].img_left
        },
        {
          medium: this.listReportFiltered[i].img_barcode
        }
      ];
    }

    this.modalRef = this.modalService.show(template);
    this.spinner.hide();
  }

  openModalChrono(template, i) {
    this.spinner.show();
    if (this.freshData == true) {
      this.ChronoClaimMore = this.claimReport[i].chronology_claim;
    } else {
      this.ChronoClaimMore = this.listReportFiltered[i].chronology_claim;
    }

    this.modalRef = this.modalService.show(template);
    this.spinner.hide();
  }

  filter() {
    this.btnFilter = false;
    this.resetFilter = true;
  }
  resetFil() {
    this.btnFilter = true;
    this.resetFilter = false;
  }

  reset() {
    delete this.claimDateFrom;
    delete this.claimDateTo;
    delete this.claimNumber;
    delete this.dnNumber;
    delete this.materialCode;
    delete this.claimStatus;
    this.listReportFiltered.length = 0;

    this.freshData = true;
    this.filteredData = false;
  }

  edit() {
    this.router.navigate(["claim/logistic-claim-form-cbu"]);
  }

  submitFilter() {
    let data = {
      claim_date_from: new Date(this.claimDateFrom),
      claim_date_to: new Date(this.claimDateTo),
      claim_number: this.claimNumber,
      dn_number: this.dnNumber,
      material_code: this.materialCode,
      claim_status: this.claimStatus
    };

    this.listReportFiltered = this.claimReport;
    this.listReportFiltered = this.listReportFiltered.filter((item: any) => {
      let claim_date = new Date(item.created_at);
      let claim_number = item.claim_number;
      let claim_status = item.approval_status;
      let material_code = item.material_code;
      let dn_number = item.dn_number;

      claim_date.setHours(0, 0, 0, 0);
      data.claim_date_from.setHours(0, 0, 0, 0);
      data.claim_date_to.setHours(0, 0, 0, 0);
      let result: any;

      if (!isNaN(data.claim_date_from.getTime())) {
        result = claim_date.getTime() == data.claim_date_from.getTime();
      }
      if (!isNaN(data.claim_date_to.getTime())) {
        result = claim_date.getTime() == data.claim_date_to.getTime();
      }
      if (
        !isNaN(data.claim_date_from.getTime()) &&
        !isNaN(data.claim_date_to.getTime())
      ) {
        result =
          claim_date.getTime() >= data.claim_date_from.getTime() &&
          claim_date.getTime() <= data.claim_date_to.getTime();
      }
      if (data.claim_number) {
        result = claim_number == data.claim_number;
      }
      if (data.claim_status) {
        result = claim_status == data.claim_status;
      }
      if (data.material_code) {
        result = material_code == data.material_code;
      }
      if (data.dn_number) {
        result = dn_number == data.dn_number;
      }
      if (
        !isNaN(data.claim_date_from.getTime()) &&
        !isNaN(data.claim_date_to.getTime()) &&
        data.claim_number &&
        data.claim_status &&
        data.material_code &&
        data.dn_number
      ) {
        result =
          claim_date.getTime() >= data.claim_date_from.getTime() &&
          claim_date.getTime() <= data.claim_date_to.getTime() &&
          claim_number == data.claim_number &&
          claim_status == data.claim_status &&
          material_code == data.material_code &&
          dn_number == data.dn_number;
      }

      return result;
    });
    this.freshData = false;
    this.filteredData = true;
  }

  downloadReport() {
    let data, filename, link, reportClaimData;

    reportClaimData =
      this.filteredData == true ? this.listReportFiltered : this.claimReport;

    // console.log(this.outstanding);
    // {
    //   medium: this.listReportFiltered[i].img_front
    // },
    // {
    //   medium: this.listReportFiltered[i].img_rear
    // },
    // {
    //   medium: this.listReportFiltered[i].img_packaging
    // },
    // {
    //   medium: this.listReportFiltered[i].img_right
    // },
    // {
    //   medium: this.listReportFiltered[i].img_left
    // },
    // {
    //   medium: this.listReportFiltered[i].img_barcode
    // }
    reportClaimData.forEach(e => {
      delete e.img_front;
      delete e.img_rear;
      delete e.img_packaging;
      delete e.img_right;
      delete e.img_left;
      delete e.img_barcode;
    });
    let csv = this.convertToCSV({
      data: reportClaimData
    });

    if (csv == null) return;
    filename = "Report-Claim-CBU.csv";

    if (!csv.match(/^data:text\/csv/i)) {
      csv = "data:text/csv;charset=utf-8," + csv;
    }

    data = encodeURI(csv);
    link = document.createElement("a");
    link.setAttribute("href", data);
    link.setAttribute("download", filename);
    link.click();

    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };
    let currentUrl = this.router.url + "?";
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }

  convertToCSV(args) {
    let result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
      return null;
    }

    columnDelimiter = args.columnDelimiter || ",";
    lineDelimiter = args.lineDelimiter || "\n";

    keys = Object.keys(data[0]);

    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(item => {
      ctr = 0;
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter;

        result += item[key];
        ctr++;
      });
      result += lineDelimiter;
    });

    return result;
  }

  editClaimCbu(claimNumber) {
    this.router.navigate(["claim/logistic-claim-form-cbu"], {
      queryParams: { claimNumber: claimNumber }
    });
  }

  openUrl(url) {
    if(url === null) {
      alert("URL not found.")
      return
    }
    window.open(url, "_blank")
  }
}
