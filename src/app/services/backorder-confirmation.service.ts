import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TimeoutError } from 'rxjs/internal/util/TimeoutError';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class BackorderConfirmationService {

  constructor(
    private http: HttpClient
  ) { }

  baseUrl = environment.baseUrl

  getSONumber() {
    var sap_code = JSON.parse(localStorage.getItem('user')).sap_code;
    return this.http.get(this.baseUrl + '/api/backorder-confirmation-fix/' + sap_code)
  }

  search(val) {
    return this.http.post(this.baseUrl + '/api/backorder-confirmation/search', val);
  }

  getDetailBO(so_number, idtrs_detail) {
    return this.http.get(this.baseUrl + `/api/backorder-confirmation/get/${so_number}/${idtrs_detail}`);
  }

  paymentBO(val, confirmId) {
    return this.http.post(this.baseUrl + '/api/backorder-confirmation/pay/' + val + '/' + confirmId, null);
  }

  storeHeaderBackOrder(val) {
    var data = JSON.stringify(val);
    var backorderHeaderData = localStorage.setItem('backorderHeaderData', data);
  }

  downloadBackOrder(data) {
    return this.http.post(`${this.baseUrl}/api/download-bo`, data, { responseType: 'blob' })
  }
}
