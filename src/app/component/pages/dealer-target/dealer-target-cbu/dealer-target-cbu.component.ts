import { Component, OnInit } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker/public_api';
import { BsDatepickerViewMode } from 'ngx-bootstrap/datepicker/models';
import { DealerTargetCbuService } from 'src/app/services/dealer-target-cbu.service';

@Component({
  selector: 'app-dealer-target-cbu',
  templateUrl: './dealer-target-cbu.component.html',
  styleUrls: ['./dealer-target-cbu.component.css']
})
export class DealerTargetCbuComponent implements OnInit {
  dealerTargetCBU: any[];

  constructor(
    private dealerTargetCbu: DealerTargetCbuService
  ) { }
  bsValue: Date = new Date(2017, 7);
  minMode: BsDatepickerViewMode = 'month';
  bsConfig: Partial<BsDatepickerConfig>
  targetCBU: boolean = false;
  years: any = []
  year: string;
  yearHTML: number

  ngOnInit() {

    this.getYears()
    console.log(this.years)
  }
  getYears() {
    this.dealerTargetCbu.getYearList().subscribe((res) => {
      let result = []
      result.push(res)
      for (const item of result[0]) {
        this.years.push(item)
      }
    }, (err) => {
      console.error(err)
    })
  }

  getDataTarget() {
    if (this.year) {
      if (this.year == "No data target yet.")
        alert("No data target yet.")
      else {
        this.yearHTML = parseInt(this.year)
        this.dealerTargetCbu.getDealerTargetCBU(this.year).subscribe((res) => {
          let resultTarget = []
          let resultActual = []
          let result = []

          resultTarget.push(res["target"])
          resultActual.push(res["actual"])
          let index = 0
          for (const item of resultTarget[0]) {
            result.push(item)
            result[index].totalTargetRow = item.may + item.june + item.july + item.august + item.september + item.october + item.november + item.december + item.january + item.february + item.march

            for (const iterator of resultActual[0].filter(val => val.category == item.material_group_description)) {
              result[index].aprilActual = iterator.april
              result[index].mayActual = iterator.may
              result[index].juneActual = iterator.june
              result[index].julyActual = iterator.july
              result[index].augustActual = iterator.august
              result[index].septemberActual = iterator.september
              result[index].octoberActual = iterator.october
              result[index].novemberActual = iterator.november
              result[index].decemberActual = iterator.december
              result[index].januaryActual = iterator.january
              result[index].februaryActual = iterator.february
              result[index].marchActual = iterator.march

              result[index].aprilActualPercent = iterator.april / item.april * 100
              result[index].mayActualPercent = iterator.may / item.may * 100
              result[index].juneActualPercent = iterator.june / item.june * 100
              result[index].julyActualPercent = iterator.july / item.july * 100
              result[index].augustActualPercent = iterator.august / item.august * 100
              result[index].septemberActualPercent = iterator.september / item.september * 100
              result[index].octoberActualPercent = iterator.october / item.october * 100
              result[index].novemberActualPercent = iterator.november / item.november * 100
              result[index].decemberActualPercent = iterator.december / item.december * 100
              result[index].januaryActualPercent = iterator.january / item.january * 100
              result[index].februaryActualPercent = iterator.february / item.february * 100
              result[index].marchActualPercent = iterator.march / item.march * 100

              result[index].totalActualRow = iterator.may + iterator.june + iterator.july + iterator.august + iterator.september + iterator.october + iterator.november + iterator.december + iterator.january + iterator.february + iterator.march
              result[index].totalPercentRow = iterator.april / item.april * 100 + iterator.may / item.may * 100 + iterator.june / item.june * 100 + iterator.july / item.july * 100 + iterator.august / item.august * 100 + iterator.september / item.september * 100 + iterator.october / item.october * 100 + iterator.november / item.november * 100 + iterator.december / item.december * 100 + iterator.january / item.january * 100 + iterator.february / item.february * 100 + iterator.march / item.march * 100
            }

            result[index].aprilActual = result[index].aprilActual ? result[index].aprilActual : 0
            result[index].mayActual = result[index].mayActual ? result[index].mayActual : 0
            result[index].juneActual = result[index].juneActual ? result[index].juneActual : 0
            result[index].julyActual = result[index].julyActual ? result[index].julyActual : 0
            result[index].augustActual = result[index].augustActual ? result[index].augustActual : 0
            result[index].septemberActual = result[index].septemberActual ? result[index].septemberActual : 0
            result[index].octoberActual = result[index].octoberActual ? result[index].octoberActual : 0
            result[index].novemberActual = result[index].novemberActual ? result[index].novemberActual : 0
            result[index].decemberActual = result[index].decemberActual ? result[index].decemberActual : 0
            result[index].januaryActual = result[index].januaryActual ? result[index].januaryActual : 0
            result[index].februaryActual = result[index].februaryActual ? result[index].februaryActual : 0
            result[index].marchActual = result[index].marchActual ? result[index].marchActual : 0

            result[index].aprilActualPercent = result[index].aprilActualPercent ? result[index].aprilActualPercent.toFixed(2) : 0
            result[index].mayActualPercent = result[index].mayActualPercent ? result[index].mayActualPercent.toFixed(2) : 0
            result[index].juneActualPercent = result[index].juneActualPercent ? result[index].juneActualPercent.toFixed(2) : 0
            result[index].julyActualPercent = result[index].julyActualPercent ? result[index].julyActualPercent.toFixed(2) : 0
            result[index].augustActualPercent = result[index].augustActualPercent ? result[index].augustActualPercent.toFixed(2) : 0
            result[index].septemberActualPercent = result[index].septemberActualPercent ? result[index].septemberActualPercent.toFixed(2) : 0
            result[index].octoberActualPercent = result[index].octoberActualPercent ? result[index].octoberActualPercent.toFixed(2) : 0
            result[index].novemberActualPercent = result[index].novemberActualPercent ? result[index].novemberActualPercent.toFixed(2) : 0
            result[index].decemberActualPercent = result[index].decemberActualPercent ? result[index].decemberActualPercent.toFixed(2) : 0
            result[index].januaryActualPercent = result[index].januaryActualPercent ? result[index].januaryActualPercent.toFixed(2) : 0
            result[index].februaryActualPercent = result[index].februaryActualPercent ? result[index].februaryActualPercent.toFixed(2) : 0
            result[index].marchActualPercent = result[index].marchActualPercent ? result[index].marchActualPercent.toFixed(2) : 0

            result[index].totalActualRow = result[index].totalActualRow ? result[index].totalActualRow : 0
            result[index].totalPercentRow = result[index].totalPercentRow ? result[index].totalPercentRow.toFixed(2) : 0

            index++
          }
          console.log(result)
          this.dealerTargetCBU = result
          this.targetCBU = true
        }, (err) => {
          console.error(err)
        })
      }
    } else {
      alert('Choose target periode first.')
    }
  }

  //Total Target
  totalTarget() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.totalTargetRow)
    }
    return total
  }

  totalTargetApril() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.april)
    }
    return total
  }

  totalTargetMay() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.may)
    }
    return total
  }

  totalTargetJune() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.june)
    }
    return total
  }

  totalTargetJuly() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.july)
    }
    return total
  }

  totalTargetAugust() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.august)
    }
    return total
  }

  totalTargetSeptember() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.september)
    }
    return total
  }

  totalTargetOctober() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.october)
    }
    return total
  }

  totalTargetNovember() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.november)
    }
    return total
  }

  totalTargetDecember() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.december)
    }
    return total
  }

  totalTargetJanuary() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.january)
    }
    return total
  }

  totalTargetFebruary() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.february)
    }
    return total
  }

  totalTargetMarch() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.march)
    }
    return total
  }

  //Total Actual
  totalActual() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.totalActualRow)
    }
    return total
  }

  totalActualApril() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.aprilActual)
    }
    return total
  }

  totalActualMay() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.mayActual)
    }
    return total
  }

  totalActualJune() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.juneActual)
    }
    return total
  }

  totalActualJuly() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.julyActual)
    }
    return total
  }

  totalActualAugust() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.augustActual)
    }
    return total
  }

  totalActualSeptember() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.septemberActual)
    }
    return total
  }

  totalActualOctober() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.octoberActual)
    }
    return total
  }

  totalActualNovember() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.novemberActual)
    }
    return total
  }

  totalActualDecember() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.decemberActual)
    }
    return total
  }

  totalActualJanuary() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.januaryActual)
    }
    return total
  }

  totalActualFebruary() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.februaryActual)
    }
    return total
  }

  totalActualMarch() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += parseInt(item.marchActual)
    }
    return total
  }

  //Total Percent
  totalPercent() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.totalPercentRow
    }
    return total
  }

  totalPercentApril() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.aprilActualPercent
    }
    return total
  }

  totalPercentMay() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.mayActualPercent
    }
    return total
  }

  totalPercentJune() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.juneActualPercent
    }
    return total
  }

  totalPercentJuly() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.julyActualPercent
    }
    return total
  }

  totalPercentAugust() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.augustActualPercent
    }
    return total
  }

  totalPercentSeptember() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.septemberActualPercent
    }
    return total
  }

  totalPercentOctober() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.octoberActualPercent
    }
    return total
  }

  totalPercentNovember() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.novemberActualPercent
    }
    return total
  }

  totalPercentDecember() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.decemberActualPercent
    }
    return total
  }

  totalPercentJanuary() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.januaryActualPercent
    }
    return total
  }

  totalPercentFebruary() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.februaryActualPercent
    }
    return total
  }

  totalPercentMarch() {
    let total = 0
    for (const item of this.dealerTargetCBU) {
      total += item.marchActualPercent
    }
    return total
  }

}
