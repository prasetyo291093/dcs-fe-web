import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'


@Injectable({
  providedIn: 'root'
})
export class DeliveryInformationService {

  constructor(
    private http: HttpClient
  ) { }

  baseUrl = environment.baseUrl

  getSONumber() {
    let sap_code = JSON.parse(localStorage.getItem('user')).sap_code
    return this.http.get(`${this.baseUrl}/api/delivery-information/list/${sap_code}`);
  }

  search(val) {
    return this.http.post(`${this.baseUrl}/api/delivery-information/search`, val);
  }

  saveDelInformation(val, so_number, dn_number, mat_number) {
    return this.http.post(`${this.baseUrl}/api/delivery-information/save/${so_number}/${dn_number}/${mat_number}`, val);
  }

  downloadInvoice(data) {
    return this.http.post(`${this.baseUrl}/api/download-dn`, data, {responseType: "blob"});
  }

  downloadOrderConfirmation(data) {
    return this.http.post(`${this.baseUrl}/api/download-so-number`, data, {responseType: "blob"});
  }
}
