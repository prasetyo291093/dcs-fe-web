import { Component, OnInit } from '@angular/core';
import { UserManagementService } from '../../../services/user-management.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  userManage1: boolean = false;
  userManage2: boolean = false;
  userManage3: boolean = false;

  userList: any = [];

  number: number;

  username: string
  sap_code: string
  role: string
  dealer_name: string
  email: string
  id_number: string
  mobile_number: string
  address: string
  dealerName: string;
  userId: number

  addEditUser: boolean = false
  previewDealerProfileMenu: boolean = false
  updateDealerInformation: boolean = false
  createOrder: boolean = false
  downloadOrderDocument: boolean = false
  simulateOrder: boolean = false
  previewPriceSimulate: boolean = false
  doingPayment: boolean = false
  previewOrderReport: boolean = false
  downloadSalesOrderDocument: boolean = false
  previewDownloadDeliveryReport: boolean = false
  previewAvailability: boolean = false
  previewPriceInq: boolean = false
  previewDownloadOutstandingReport: boolean = false
  previewAPAging: boolean = false
  previewInvoiceTimeliness: boolean = false
  inputUploadDealerForecast: boolean = false
  previewPromoDetail: boolean = false
  inputUploadDealerStock: boolean = false
  inputWarrantyCard: boolean = false
  claimServiceStatus: boolean = false
  claimLogisticStatus: boolean = false
  downloadInformationSharing: boolean = false

  canAddEditUser: boolean = false


  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private Users: UserManagementService
  ) {

  }

  ngOnInit() {
    this.userManage1 = true;
    this.dealerName = localStorage.getItem("dealer_name")
    this.canAddEditUser = JSON.parse(localStorage.getItem("user")).permissions["Add, edit and terminate user"]
    this.getListUser()
  }
  getListUser() {
    this.spinner.show()
    this.userList = []
    this.Users.getUser().subscribe(result => {
      for (const key in result) {
        if (result.hasOwnProperty(key)) {
          const element = result[key];
          this.userList.push(element)
        }
      }
      // console.log(this.userList)
      this.spinner.hide()
    }, error => {
      this.spinner.hide()
      console.error(error)
    })
  }

  disAcc() {
    this.userManage2 = false;
    this.userManage1 = true;
    this.addEditUser = false
    this.previewDealerProfileMenu = false
    this.updateDealerInformation = false
    this.createOrder = false
    this.downloadOrderDocument = false
    this.simulateOrder = false
    this.previewPriceSimulate = false
    this.doingPayment = false
    this.previewOrderReport = false
    this.downloadSalesOrderDocument = false
    this.previewDownloadDeliveryReport = false
    this.previewAvailability = false
    this.previewPriceInq = false
    this.previewDownloadOutstandingReport = false
    this.previewAPAging = false
    this.previewInvoiceTimeliness = false
    this.inputUploadDealerForecast = false
    this.previewPromoDetail = false
    this.inputUploadDealerStock = false
    this.inputWarrantyCard = false
    this.claimServiceStatus = false
    this.claimLogisticStatus = false
    this.downloadInformationSharing = false
    this.getListUser()
  }
  addUser() {
    this.userManage3 = true;
    this.userManage1 = false;
    this.userManage2 = false;
  }

  back() {
    this.userManage1 = true;
    this.userManage3 = false;
    this.userManage2 = false;
  }

  editDealer(id) {
    this.userManage2 = true;
    this.userManage3 = false;
    this.userManage1 = false;
    this.userId = id
    this.spinner.show()
    this.Users.getDetailUser(id).subscribe((result) => {
      this.username = result["value"]["first_name"]
      this.email = result["value"]["email"]
      this.role = result["role"][0]

      console.log(result)

      this.addEditUser = result["value"]["permissions"] ? result["value"]["permissions"]["Add, edit and terminate user"] : false
      this.previewDealerProfileMenu = result["value"]["permissions"] ? result["value"]["permissions"]["Preview Dealer Profile menu"] : false
      this.updateDealerInformation = result["value"]["permissions"] ? result["value"]["permissions"]["Update Dealer Information"] : false
      this.createOrder = result["value"]["permissions"] ? result["value"]["permissions"]["Create Order"] : false
      this.downloadOrderDocument = result["value"]["permissions"] ? result["value"]["permissions"]["Download Simulate Order and Order Confirmation Document"] : false
      this.simulateOrder = result["value"]["permissions"] ? result["value"]["permissions"]["Simulate Order"] : false
      this.previewPriceSimulate = result["value"]["permissions"] ? result["value"]["permissions"]["Preview Price in Simulate Order"] : false
      this.doingPayment = result["value"]["permissions"] ? result["value"]["permissions"]["Doing Payment"] : false
      this.previewOrderReport = result["value"]["permissions"] ? result["value"]["permissions"]["Preview Order Report"] : false
      this.downloadSalesOrderDocument = result["value"]["permissions"] ? result["value"]["permissions"]["Preview price and Download Sales Order Document, Invoice Document and Tax Invoice Document"] : false
      this.previewDownloadDeliveryReport = result["value"]["permissions"] ? result["value"]["permissions"]["Preview, update delivery report and download document"] : false
      this.previewAvailability = result["value"]["permissions"] ? result["value"]["permissions"]["Preview availability"] : false
      this.previewPriceInq = result["value"]["permissions"] ? result["value"]["permissions"]["Preview price"] : false
      this.previewDownloadOutstandingReport = result["value"]["permissions"] ? result["value"]["permissions"]["Preview, Download, Preview invoice information and do payment in Outstanding Invoice Report"] : false
      this.previewAPAging = result["value"]["permissions"] ? result["value"]["permissions"]["Preview A/P Aging"] : false
      this.previewInvoiceTimeliness = result["value"]["permissions"] ? result["value"]["permissions"]["Preview Invoice Timeliness"] : false
      this.inputUploadDealerForecast = result["value"]["permissions"] ? result["value"]["permissions"]["Input or Upload and preview Dealer Forecast"] : false
      this.previewPromoDetail = result["value"]["permissions"] ? result["value"]["permissions"]["Preview all Promo Detail"] : false
      this.inputUploadDealerStock = result["value"]["permissions"] ? result["value"]["permissions"]["Input or Upload and preview Dealer Stock"] : false
      this.inputWarrantyCard = result["value"]["permissions"] ? result["value"]["permissions"]["Input Warranty Card"] : false
      this.claimServiceStatus = result["value"]["permissions"] ? result["value"]["permissions"]["Service Claim Status"] : false
      this.claimLogisticStatus = result["value"]["permissions"] ? result["value"]["permissions"]["Logistic Claim Status"] : false
      this.downloadInformationSharing = result["value"]["permissions"] ? result["value"]["permissions"]["Download Information Sharing"] : false
      this.spinner.hide()
      
    }, (error) => {
      console.log(error)
      this.spinner.hide()
    })
  }

  saveEditUser() {
    let data = {
      role : this.role,
      permissions: [
        `"Add, edit and terminate user": ${this.addEditUser}`,
        `"Preview Dealer Profile menu": ${this.previewDealerProfileMenu}`,
        `"Update Dealer Information": ${this.updateDealerInformation}`,
        `"Create Order": ${this.createOrder}`,
        `"Download Simulate Order and Order Confirmation Document": ${this.downloadOrderDocument}`,
        `"Simulate Order": ${this.simulateOrder}`,
        `"Preview Price in Simulate Order": ${this.previewPriceSimulate}`,
        `"Doing Payment": ${this.doingPayment}`,
        `"Preview Order Report": ${this.previewOrderReport}`,
        `"Preview price and Download Sales Order Document, Invoice Document and Tax Invoice Document": ${this.downloadSalesOrderDocument}`,
        `"Preview, update delivery report and download document": ${this.previewDownloadDeliveryReport}`,
        `"Preview availability": ${this.previewAvailability}`,
        `"Preview price": ${this.previewPriceInq}`,
        `"Preview, Download, Preview invoice information and do payment in Outstanding Invoice Report": ${this.previewDownloadOutstandingReport}`,
        `"Preview A/P Aging": ${this.previewAPAging}`,
        `"Preview Invoice Timeliness": ${this.previewInvoiceTimeliness}`,
        `"Input or Upload and preview Dealer Forecast": ${this.inputUploadDealerForecast}`,
        `"Preview all Promo Detail": ${this.previewPromoDetail}`,
        `"Input or Upload and preview Dealer Stock": ${this.inputUploadDealerStock}`,
        `"Input Warranty Card": ${this.inputWarrantyCard}`,
        `"Service Claim Status": ${this.claimServiceStatus}`,
        `"Logistic Claim Status": ${this.claimLogisticStatus}`,
        `"Download Information Sharing": ${this.downloadInformationSharing}`
      ]
    }
    console.log(data)
    this.spinner.show()
    this.Users.saveDetailUser(this.userId, data).subscribe((result) => {
      this.addEditUser = false
      this.previewDealerProfileMenu = false
      this.updateDealerInformation = false
      this.createOrder = false
      this.downloadOrderDocument = false
      this.simulateOrder = false
      this.previewPriceSimulate = false
      this.doingPayment = false
      this.previewOrderReport = false
      this.downloadSalesOrderDocument = false
      this.previewDownloadDeliveryReport = false
      this.previewAvailability = false
      this.previewPriceInq = false
      this.previewDownloadOutstandingReport = false
      this.previewAPAging = false
      this.previewInvoiceTimeliness = false
      this.inputUploadDealerForecast = false
      this.previewPromoDetail = false
      this.inputUploadDealerStock = false
      this.inputWarrantyCard = false
      this.claimServiceStatus = false
      this.claimLogisticStatus = false
      this.downloadInformationSharing = false
      this.spinner.hide()
      this.getListUser()
      this.userManage1 = true;
      this.userManage3 = false;
      this.userManage2 = false;
    }, (error) => {
      console.error(error)
      this.spinner.hide()
    })
  }

  saveAcc() {
    var data = {
      name: this.username,
      sap_code: JSON.parse(localStorage.getItem("user")).sap_code,
      dealer_name: this.dealerName,
      email: this.email,
      id_number: this.id_number,
      mobile_number: this.mobile_number,
      address: this.address
    }
    this.spinner.show()
    this.Users.addUser(data).subscribe(result => {
      console.log(result)
      if (result["message"] == "Success") {
        this.getListUser()
        this.userManage3 = false;
        this.userManage1 = true
      }
      this.spinner.hide()
    }, error => {
      console.error(error)
      this.spinner.hide()
    })
  }

  deleteAcc(id) {
    var data = {
      user_id: id
    }
    this.spinner.show()
    this.Users.deleteUserRequest(data).subscribe(result => {
      this.getListUser()
        this.userManage3 = false;
        this.userManage1 = true
    }, error => {
      console.error(error)
      this.spinner.hide()
    })
  }
}
