import { TestBed } from '@angular/core/testing';

import { OutstandingAndOverdueReportService } from './outstanding-and-overdue-report.service';

describe('OutstandingAndOverdueReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutstandingAndOverdueReportService = TestBed.get(OutstandingAndOverdueReportService);
    expect(service).toBeTruthy();
  });
});
