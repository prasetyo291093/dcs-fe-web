import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutstandingInvoiceCbuComponent } from './outstanding-invoice-cbu.component';

describe('OutstandingInvoiceCbuComponent', () => {
  let component: OutstandingInvoiceCbuComponent;
  let fixture: ComponentFixture<OutstandingInvoiceCbuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutstandingInvoiceCbuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutstandingInvoiceCbuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
