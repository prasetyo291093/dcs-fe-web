import { TestBed } from '@angular/core/testing';

import { SparepartsPurchaseOrderService } from './spareparts-purchase-order.service';

describe('SparepartsPurchaseOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SparepartsPurchaseOrderService = TestBed.get(SparepartsPurchaseOrderService);
    expect(service).toBeTruthy();
  });
});
