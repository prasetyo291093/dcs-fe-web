import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarrantyClaimFormComponent } from './warranty-claim-form.component';

describe('WarrantyClaimFormComponent', () => {
  let component: WarrantyClaimFormComponent;
  let fixture: ComponentFixture<WarrantyClaimFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarrantyClaimFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrantyClaimFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
