import { Component, OnInit } from "@angular/core";
import { OrderStatusService } from "../../../../services/order-status.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";

declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-order-status",
  templateUrl: "./order-status.component.html",
  styleUrls: ["./order-status.component.css"]
})
export class OrderStatusComponent implements OnInit {
  btnFilter: boolean = false;
  resetFilter: boolean = false;
  filterDealerOrder: boolean = false;
  filterDate: boolean = false;
  filterNo: boolean = false;
  filterPurchaseNo: boolean = false;
  filterOrderType: boolean = false;
  filterBillCode: boolean = false;
  filterMaterialGroup: boolean = false;
  filterMaterialCode: boolean = false;
  filterSalesOrder: boolean = false;
  filterInvoiceDate: boolean = false;
  filterInvoiceNo: boolean = false;
  filterInvoiceStatus: boolean = false;

  baseUrl = environment.baseUrl;

  data: any;

  soNumber: any;
  matCode: any;

  so_number_from: any;
  so_number_to: any;
  so_date_from: any;
  so_date_to: any;
  material_number_from: any;
  material_number_to: any;
  orderStatusList: any[];
  freshData: boolean = true;
  filteredData: boolean = false;
  listFiltered: any[];
  dropDownSoNumberList: any = [];
  dropDownMaterialList: any = [];

  downloadSalesOrderDocument: boolean = false

  constructor(
    private OrderStatus: OrderStatusService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit() {
    this.btnFilter = true;
    this.downloadSalesOrderDocument = JSON.parse(localStorage.getItem("user")).permissions["Preview price and Download Sales Order Document, Invoice Document and Tax Invoice Document"]
    this.getListOrderStatus();
    this.filterDealerOrder = true;
  }
  getListOrderStatus(): any {
    this.spinner.show();
    this.OrderStatus.getSONumber().subscribe(
      res => {
        let list = [];
        let result = [];
        list.push(res);

        console.log(list)

        var sortByDate = function(a, b) {
          var dateA = new Date(b.order_date).getTime();
          var dateB = new Date(a.order_date).getTime();
          if (dateA > dateB) return 1;
          if (dateA < dateB) return -1;
          return 0;
        };

        for (const iterator of list[0]) {
          if (iterator.cancel_status == "0") 
            iterator.cancel_status_desc = "-";
          else if (iterator.cancel_status == "1")
            iterator.cancel_status_desc = "Canceled";
        }

        this.orderStatusList = list[0].sort(sortByDate);

        let map = new Map();
        for (const item of list[0]) {
          if (!map.has(item.sales_order_number)) {
            map.set(item.sales_order_number, true);
            this.dropDownSoNumberList.push(item.sales_order_number);
          }
        }
        let map2 = new Map();
        for (const item of list[0]) {
          if (!map2.has(item.matnr)) {
            map2.set(item.matnr, true);
            this.dropDownMaterialList.push(item.matnr);
          }
        }

        this.spinner.hide();
      },
      error => {
        console.log(error);
        this.spinner.hide();
      }
    );
  }

  search() {
    var data = {
      so_number_from: this.so_number_from,
      so_number_to: this.so_number_to,
      so_date_from: new Date(this.so_date_from),
      so_date_to: new Date(this.so_date_to),
      material_number_from: this.material_number_from
    };

    console.log(data);
    this.listFiltered = this.orderStatusList;
    this.listFiltered = this.listFiltered.filter((item: any) => {
      let order_date = new Date(item.order_date);
      order_date.setHours(0, 0, 0, 0);
      console.log(item.order_date);
      data.so_date_from.setHours(0, 0, 0, 0);
      data.so_date_to.setHours(0, 0, 0, 0);
      let result: any;

      if (!isNaN(data.so_date_from.getTime()))
        result = order_date.getTime() == data.so_date_from.getTime();
      if (!isNaN(data.so_date_to.getTime()))
        result = order_date.getTime() == data.so_date_to.getTime();
      if (
        !isNaN(data.so_date_from.getTime()) &&
        !isNaN(data.so_date_to.getTime())
      )
        result =
          order_date.getTime() >= data.so_date_from.getTime() &&
          order_date.getTime() <= data.so_date_to.getTime();
      if (data.so_number_from)
        result = item.sales_order_number == data.so_number_from;
      if (data.so_number_to)
        result = item.sales_order_number == data.so_number_to;
      if (data.so_number_from && data.so_number_to)
        result =
          item.sales_order_number >= data.so_number_from &&
          item.sales_order_number <= data.so_number_to;
      if (data.material_number_from)
        result = item.matnr == data.material_number_from;
      if (
        !isNaN(data.so_date_from.getTime()) &&
        !isNaN(data.so_date_to.getTime()) &&
        data.so_number_from &&
        data.so_number_to &&
        data.material_number_from
      )
        result =
          item.sales_order_number >= data.so_number_from &&
          item.sales_order_number <= data.so_number_to &&
          order_date.getTime() >= data.so_date_from.getTime() &&
          order_date.getTime() <= data.so_date_to.getTime() &&
          item.matnr == data.material_number_from;

      return result;
    });
    this.freshData = false;
    this.filteredData = true;
  }
  reset() {
    delete this.so_number_from;
    delete this.so_number_to;
    delete this.so_date_from;
    delete this.so_date_to;
    delete this.material_number_from;

    this.onRefresh();
    this.freshData = true;
    this.filteredData = false;
  }

  onRefresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };
    let currentUrl = this.router.url + "?";
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }

  filter() {
    this.btnFilter = false;
    this.resetFilter = true;
  }
  resetFil() {
    this.btnFilter = true;
    this.resetFilter = false;
  }

  selectfil(event) {
    var z = event.target.value;

    if (z == "purcahse order no") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = true;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = false;
    } else if (z == "order date") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = true;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = false;
    } else if (z == "order no") {
      this.filterDealerOrder = false;
      this.filterNo = true;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = false;
    } else if (z == "order type") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = true;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = false;
    } else if (z == "bill code") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = true;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = false;
    } else if (z == "material group") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = true;
      this.filterMaterialCode = false;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = false;
    } else if (z == "material code") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = true;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = false;
    } else if (z == "status sales order") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterSalesOrder = true;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = false;
    } else if (z == "invoice date") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = true;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = false;
    } else if (z == "invoice no") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = true;
      this.filterInvoiceStatus = false;
    } else if (z == "invoice status") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = true;
    } else {
      this.filterDealerOrder = true;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterSalesOrder = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceStatus = false;
    }

    console.log(z);
  }

  downloadSO(soNumber, confirmid) {
    // console.log(soNumber)
    // let item = [...this.orderStatusList]
    // item = item.filter(e => e.sales_order_number == soNumber)

    let data = {
      so_number: soNumber,
      confirmid: confirmid
    };
    // console.log(item)
    console.log(data);
    this.OrderStatus.downloadOrderConfirmation(data).subscribe(
      result => {
        this.downloadFile(result, "so-number");
      },
      error => console.error(error)
    );
  }

  downloadPDF(pathName,No) {
    window.open(`${this.baseUrl}/api/download-pdf/`+pathName+`/`+No+`.pdf`, "_blank");
  }

  downloadFile(data, filename) {
    filename = filename ? filename : "invoice";
    const blob = new Blob([data], { type: "application/pdf" });
    const url = window.URL.createObjectURL(blob);
    // window.location.href = url
    // window.open
    let link = document.createElement("a");
    link.style.display = "none";
    link.setAttribute("download", `${filename}.pdf`);
    link.setAttribute("href", url);
    document.body.appendChild(link);
    link.click();
  }
}
