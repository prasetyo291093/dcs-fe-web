import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  submitResetPassword(data, email, resetcode) {
    return this.http.post(`${this.baseUrl}/api/reset-password/${email}/${resetcode}`, data);
  }
}
