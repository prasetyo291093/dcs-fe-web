import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class InformationSharingService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getInformationSharing() {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(`${this.baseUrl}/api/information-sharing/${sap_code}`);
  }
}
