import { Component, OnInit } from "@angular/core";
import { LogisticClaimSparepartReportService } from "src/app/services/logistic-claim-sparepart-report.service";
import { NgxSpinnerService } from "ngx-spinner";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Router } from '@angular/router';

@Component({
  selector: "app-logistic-claim-report-spareparts",
  templateUrl: "./logistic-claim-report-spareparts.component.html",
  styleUrls: ["./logistic-claim-report-spareparts.component.css"]
})
export class LogisticClaimReportSparepartsComponent implements OnInit {
  claimReport: any = [];
  claimNoList: any = [];
  matNoList: any = [];
  dnNoList: any = [];
  claimDateFrom: any;
  claimDateTo: any;
  dnNumber: string;
  claimNumber: string;
  claimStatus: string;
  materialCode: string;
  listReportFiltered: any = [];
  freshData: boolean = true;
  filteredData: boolean = false;
  ChronoClaimMore: any;

  constructor(
    private logisticClaimReport: LogisticClaimSparepartReportService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private modalService: BsModalService
  ) {}

  btnFilter: boolean = false;
  resetFilter: boolean = false;
  modalRef: BsModalRef;

  ngOnInit() {
    this.btnFilter = true;
    this.spinner.show();
    this.logisticClaimReport.getLogisticClaimReportSparepart().subscribe(
      result => {
        if (Object.entries(result).length > 0) {
          let sortByDate = function(a, b) {
            let dateA = new Date(b.claim_date).getTime();
            let dateB = new Date(a.claim_date).getTime();
            if (dateA > dateB) return 1;
            if (dateA < dateB) return -1;
            return 0;
          };

          this.claimReport = result;
          this.claimReport.sort(sortByDate);

          for (const item of this.claimReport) {
            if(item.state == 0) {
              item.approval_state = "Rejected"
            } else if(item.state == 1) {
              item.approval_state = "Submitted"
            } else if(item.state == 2) {
              item.approval_state = "Revise"
            } else if(item.state == 9) {
              item.approval_state = "Approved"
            }
          }

          let mapClaimNo = new Map();
          let mapMatNo = new Map();
          let mapDnNo = new Map();
          for (const item of this.claimReport) {
            if (!mapClaimNo.has(item.claim_number)) {
              mapClaimNo.set(item.claim_number, true);
              this.claimNoList.push(item.claim_number);
            }
          }
          for (const item of this.claimReport) {
            if (!mapMatNo.has(item.material_code)) {
              mapMatNo.set(item.material_code, true);
              this.matNoList.push(item.material_code);
            }
          }
          for (const item of this.claimReport) {
            if (!mapDnNo.has(item.dn_number)) {
              mapDnNo.set(item.dn_number, true);
              this.dnNoList.push(item.dn_number);
            }
          }
        }
        this.spinner.hide();
      },
      error => {
        console.error(error);
        this.spinner.hide();
      }
    );
  }

  filter() {
    this.btnFilter = false;
    this.resetFilter = true;
  }
  resetFil() {
    this.btnFilter = true;
    this.resetFilter = false;
  }

  reset() {
    delete this.claimDateFrom;
    delete this.claimDateTo;
    delete this.claimNumber;
    delete this.dnNumber;
    delete this.materialCode;
    delete this.claimStatus;
    this.listReportFiltered.length = 0;

    this.freshData = true;
    this.filteredData = false;
  }

  submitFilter() {
    let data = {
      claim_date_from: new Date(this.claimDateFrom),
      claim_date_to: new Date(this.claimDateTo),
      claim_number: this.claimNumber,
      dn_number: this.dnNumber,
      material_code: this.materialCode,
      claim_status: this.claimStatus
    };
    this.listReportFiltered = this.claimReport;
    this.listReportFiltered = this.listReportFiltered.filter((item: any) => {
      let claim_date = new Date(item.claim_date);
      let claim_number = item.claim_number;
      let claim_status = item.approval_status;
      let material_code = item.material_code;
      let dn_number = item.dn_number;

      claim_date.setHours(0, 0, 0, 0);
      data.claim_date_from.setHours(0, 0, 0, 0);
      data.claim_date_to.setHours(0, 0, 0, 0);
      let result: any;

      if (!isNaN(data.claim_date_from.getTime())) {
        result = claim_date.getTime() == data.claim_date_from.getTime();
      }
      if (!isNaN(data.claim_date_to.getTime())) {
        result = claim_date.getTime() == data.claim_date_to.getTime();
      }
      if (
        !isNaN(data.claim_date_from.getTime()) &&
        !isNaN(data.claim_date_to.getTime())
      ) {
        result =
          claim_date.getTime() >= data.claim_date_from.getTime() &&
          claim_date.getTime() <= data.claim_date_to.getTime();
      }
      if (data.claim_number) {
        result = claim_number == data.claim_number;
      }
      if (data.claim_status) {
        result = claim_status == data.claim_status;
      }
      if (data.material_code) {
        result = material_code == data.material_code;
      }
      if (data.dn_number) {
        result = dn_number == data.dn_number;
      }
      if (
        !isNaN(data.claim_date_from.getTime()) &&
        !isNaN(data.claim_date_to.getTime()) &&
        data.claim_number &&
        data.claim_status &&
        data.material_code &&
        data.dn_number
      ) {
        result =
          claim_date.getTime() >= data.claim_date_from.getTime() &&
          claim_date.getTime() <= data.claim_date_to.getTime() &&
          claim_number == data.claim_number &&
          claim_status == data.claim_status &&
          material_code == data.material_code &&
          dn_number == data.dn_number;
      }

      return result;
    });
    this.freshData = false;
    this.filteredData = true;
  }

  openModalChrono(template, i) {
    this.spinner.show();
    if (this.freshData == true) {
      this.ChronoClaimMore = this.claimReport[i].claim_chronology
    } else {
      this.ChronoClaimMore = this.listReportFiltered[i].claim_chronology
    }

    this.modalRef = this.modalService.show(template);
    this.spinner.hide();
  }

  downloadReport() {
    let data, filename, link, outstandingData;

    outstandingData =
      this.filteredData == true ? this.listReportFiltered : this.claimReport;

    // console.log(this.outstanding);
    let csv = this.convertToCSV({
      data: outstandingData
    });

    if (csv == null) return;
    filename = "Report-Claim-Sparepart.csv";

    if (!csv.match(/^data:text\/csv/i)) {
      csv = "data:text/csv;charset=utf-8," + csv;
    }

    data = encodeURI(csv);
    link = document.createElement("a");
    link.setAttribute("href", data);
    link.setAttribute("download", filename);
    link.click();
  }

  convertToCSV(args) {
    let result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
      return null;
    }

    columnDelimiter = args.columnDelimiter || ",";
    lineDelimiter = args.lineDelimiter || "\n";

    keys = Object.keys(data[0]);

    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(item => {
      ctr = 0;
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter;

        result += item[key];
        ctr++;
      });
      result += lineDelimiter;
    });

    return result;
  }

  exportTableToExcel(tableId, filename = "") {
    // var uri = 'data:application/vnd.ms-excel;base64,'
    //   , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
    //   , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
    //   , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    // return function (table, name) {
    //   if (!table.nodeType) table = document.getElementById(table)
    //   var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
    //   window.location.href = uri + base64(format(template, ctx))
    // }
    let downloadLink;
    let dataType = "application/vnd.ms-excel";
    let tableSelect = document.getElementById(tableId);
    let tableHTML = tableSelect.outerHTML.replace(/ /g, "%20");

    filename = filename ? filename + ".xls" : "Report-Claim-Sparepart.xls";

    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
      let blob = new Blob(["\ufeff", tableHTML], {
        type: dataType
      });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      downloadLink.href = "data:" + dataType + ", " + tableHTML;

      downloadLink.download = filename;
      downloadLink.click();
    }
  }

  editClaim(claimNumber) {
    this.router.navigate(["claim/logistic-claim-form-spareparts"], {
      queryParams: { claimNumber: claimNumber }
    });
  }
}
