import { TestBed } from '@angular/core/testing';

import { WarrantyClaimService } from './warranty-claim.service';

describe('WarrantyClaimService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WarrantyClaimService = TestBed.get(WarrantyClaimService);
    expect(service).toBeTruthy();
  });
});
