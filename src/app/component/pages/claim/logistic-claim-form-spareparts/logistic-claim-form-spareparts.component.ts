import { Component, OnInit } from "@angular/core";
import { LogisticClaimSparepartsService } from "src/app/services/logistic-claim-spareparts.service";
import { formatDate } from "@angular/common";
import { NgxSpinnerService } from "ngx-spinner";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { environment } from "src/environments/environment";
declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-logistic-claim-form-spareparts",
  templateUrl: "./logistic-claim-form-spareparts.component.html",
  styleUrls: ["./logistic-claim-form-spareparts.component.css"]
})
export class LogisticClaimFormSparepartsComponent implements OnInit {
  listDN: any = [];
  dnNumber: string;
  listDNDetail: any = [];
  dateNow = "";
  today = new Date();
  claimDate: any;
  dnDate: any;
  claimChrono: any;
  claimNumb: string;

  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private activeRouter: ActivatedRoute,
    private logisticClaimSparepart: LogisticClaimSparepartsService
  ) {
    this.claimDate = formatDate(this.today, "yyyy-MM-dd", "en-US", "+0700");
  }

  urls = new Array<string>();
  urls2 = new Array<string>();
  urls3 = new Array<string>();
  urls4 = new Array<string>();
  addImage: boolean = false;
  addImage2: boolean = false;
  addImage3: boolean = false;
  addImage4: boolean = false;
  formData: FormData = new FormData();

  files: any = [];
  files2: any = [];
  files3: any = [];
  files4: any = [];

  baseUrl = environment.baseUrl;

  ngOnInit() {
    this.addImage = true;
    this.addImage2 = true;
    this.addImage3 = true;
    this.addImage4 = true;

    this.activeRouter.queryParams.subscribe((params: Params) => {
      this.claimNumb = params["claimNumber"];
    });

    this.spinner.show();
    if (this.claimNumb) {
      this.logisticClaimSparepart.getClaimSparepart(this.claimNumb).subscribe(
        result => {
          if (
            Object.entries(result["header"]).length > 0 &&
            Object.entries(result["detail"]).length > 0
          ) {
            let index = 0;
            for (const item of result["detail"]) {
              this.listDNDetail.push(item);
              this.listDNDetail[index]["material_number"] = item.material_code;
              this.listDNDetail[index]["delivery_notice_quantity"] =
                item.dn_qty;
              index++;
            }
            //img label
            for (const item of result["header"][0]["img_label"]) {
              this.getDataUri(
                `${this.baseUrl}/api/logistic-claim/sparepart/image/${item}`,
                data => {
                  this.urls.push(data);
                  if (this.urls.length == 10) this.addImage = false;
                }
              );
            }

            //img packaging
            for (const item of result["header"][0]["img_packaging"]) {
              this.getDataUri(
                `${this.baseUrl}/api/logistic-claim/sparepart/image/${item}`,
                data => {
                  this.urls3.push(data);
                  if (this.urls3.length == 10) this.addImage3 = false;
                }
              );
            }

            //img parts
            for (const item of result["header"][0]["img_parts"]) {
              this.getDataUri(
                `${this.baseUrl}/api/logistic-claim/sparepart/image/${item}`,
                data => {
                  this.urls2.push(data);
                  if (this.urls2.length == 10) this.addImage2 = false;
                }
              );
            }

            //img dn
            for (const item of result["header"][0]["img_dn"]) {
              this.getDataUri(
                `${this.baseUrl}/api/logistic-claim/sparepart/image/${item}`,
                data => {
                  this.urls4.push(data);
                  if (this.urls4.length == 10) this.addImage4 = false;
                }
              );
            }

            this.listDNDetail[0]["delivery_notice_date"] =
              result["header"][0]["dn_date"];
            this.dnNumber = result["header"][0]["dn_number"];
            this.claimDate = result["header"][0]["claim_date"];
            this.claimChrono = result["header"][0]["claim_chronology"];

            this.spinner.hide();
          }
        },
        error => {
          console.error(error);
          this.spinner.hide();
        }
      );
    } else {
      // this.spinner.show();
      this.logisticClaimSparepart.getListDNSparepart().subscribe(
        (result: []) => {
          console.log(result);
          for (const iterator of result) {
            this.listDN.push(iterator);
          }
          this.spinner.hide();
        },
        error => {
          console.error(error);
          this.spinner.hide();
        }
      );
    }
  }

  getDataUri(url, callback) {
    let httpRequest = new XMLHttpRequest();
    httpRequest.onloadend = () => {
      let fileReader = new FileReader();
      fileReader.onloadend = () => {
        callback(fileReader.result);
      };
      if (httpRequest.status == 200)
        fileReader.readAsDataURL(httpRequest.response);
    };
    httpRequest.open("GET", url);
    httpRequest.responseType = "blob";
    httpRequest.send();
  }

  getDetailDN() {
    if (this.dnNumber) {
      this.listDNDetail.length = 0;
      this.urls.length = 0;
      this.urls2.length = 0;
      this.urls3.length = 0;
      this.urls4.length = 0;
      this.files.length = 0;
      this.files2.length = 0;
      this.files3.length = 0;
      this.files4.length = 0;
      this.spinner.show();
      this.logisticClaimSparepart.getDetailDNSparepart(this.dnNumber).subscribe(
        (result: []) => {
          for (const iterator of result) {
            this.listDNDetail.push(iterator);
          }
          this.spinner.hide();
        },
        error => {
          console.error(error);
          this.spinner.hide();
        }
      );
    } else {
      alert("Please choose DN Number!");
    }
  }

  minusOne(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
      // Decrement one only if value is > 1
      $("input[name=" + val + "]").val(currentVal - 1);
      $(".qtyplus")
        .val("+")
        .removeAttr("style");
    } else {
      // Otherwise put a 0 there
      $(".qtyminus")
        .val("-")
        .css("color", "#aaa");
      $(".qtyminus")
        .val("-")
        .css("cursor", "not-allowed");
    }
  }

  plusONe(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If is not undefined
    if (!isNaN(currentVal)) {
      // Increment only if value is < 20
      let deliveryNoticeQuantity = parseInt(
        this.listDNDetail[value]["delivery_notice_quantity"]
      );
      if (currentVal < deliveryNoticeQuantity) {
        $("input[name=" + val + "]").val(currentVal + 1);
        $(".qtyminus")
          .val("-")
          .removeAttr("style");
      } else {
        alert("Claim Quantity can't greater than DN Quantity.");
      }
    } else {
      // Otherwise put a 0 there
      $("input[name=" + val + "]").val(1);
    }
  }

  QtyClaim(i, value) {
    let deliveryNoticeQuantity = parseInt(
      this.listDNDetail[i]["delivery_notice_quantity"]
    );
    if (parseInt(value) > deliveryNoticeQuantity) {
      alert("Claim Quantity can't greater than DN Quantity.");
      $("#pallet" + i).val(deliveryNoticeQuantity);
    }
  }

  detectFiles(event) {
    // this.urls = [];
    let files = event.target.files;

    if (this.urls.length >= 10) {
      alert("Only 10 image maximum");
      return;
    }
    if (files.length >= 11) {
      alert("only 10 image");
    } else if (files.length == 10) {
      this.addImage = false;

      // console.log(files)
      for (let file of files) {
        if (file.size / 1024 / 1024 >= 1) {
          alert("Each file size can't be more than 1 MB.");
        } else {
          let reader = new FileReader();
          reader.onload = (e: any) => {
            this.urls.push(e.target.result);
          };
          reader.readAsDataURL(file);
        }
      }
    } else if (files.length <= 9) {
      // if (this.urls.length >= 10) {
      this.addImage = true;

      for (let file of files) {
        if (file.size / 1024 / 1024 >= 1) {
          alert("Each file size can't be more than 1 MB.");
        } else {
          let reader = new FileReader();
          reader.onload = (e: any) => {
            this.urls.push(e.target.result);

            if (this.urls.length == 10) {
              this.addImage = false;
            }
          };
          reader.readAsDataURL(file);
        }
      }
    }
  }

  detectFiles2(event) {
    // this.urls2 = [];
    let files = event.target.files;

    if (this.urls2.length >= 10) {
      alert("Only 10 image maximum");
      return;
    }

    if (files.length >= 11) {
      alert("only 10 image");
    } else if (files.length == 10) {
      this.addImage2 = false;

      for (let file of files) {
        if (file.size / 1024 / 1024 >= 1) {
          alert("Each file size can't be more than 1 MB.");
        } else {
          let reader = new FileReader();
          reader.onload = (e: any) => {
            this.urls2.push(e.target.result);
          };
          reader.readAsDataURL(file);
        }
      }
    } else if (files.length <= 9) {
      this.addImage2 = true;

      for (let file of files) {
        if (file.size / 1024 / 1024 >= 1) {
          alert("Each file size can't be more than 1 MB.");
        } else {
          let reader = new FileReader();
          reader.onload = (e: any) => {
            this.urls2.push(e.target.result);
            if (this.urls2.length == 10) {
              this.addImage2 = false;
            }
          };
          reader.readAsDataURL(file);
        }
      }
    }
  }

  detectFiles3(event) {
    // this.urls3 = [];
    let files = event.target.files;

    if (this.urls3.length >= 10) {
      alert("Only 10 image maximum");
      return;
    }
    if (files.length >= 11) {
      alert("only 10 image");
    } else if (files.length == 10) {
      this.addImage3 = false;

      for (let file of files) {
        if (file.size / 1024 / 1024 >= 1) {
          alert("Each file size can't be more than 1 MB.");
        } else {
          let reader = new FileReader();
          reader.onload = (e: any) => {
            this.urls3.push(e.target.result);
          };
          reader.readAsDataURL(file);
        }
      }
    } else if (files.length <= 9) {
      this.addImage3 = true;

      for (let file of files) {
        if (file.size / 1024 / 1024 >= 1) {
          alert("Each file size can't be more than 1 MB.");
        } else {
          let reader = new FileReader();
          reader.onload = (e: any) => {
            this.urls3.push(e.target.result);

            if (this.urls3.length == 10) {
              this.addImage3 = false;
            }
          };
          reader.readAsDataURL(file);
        }
      }
    }
  }

  detectFiles4(event) {
    // this.urls4 = [];
    let files = event.target.files;

    if (this.urls4.length >= 10) {
      alert("Only 10 image maximum");
      return;
    }

    if (files.length >= 11) {
      alert("only 10 image");
    } else if (files.length == 10) {
      this.addImage4 = false;

      for (let file of files) {
        if (file.size / 1024 / 1024 >= 1) {
          alert("Each file size can't be more than 1 MB.");
        } else {
          let reader = new FileReader();
          reader.onload = (e: any) => {
            this.urls4.push(e.target.result);
          };
          reader.readAsDataURL(file);
        }
      }
    } else if (files.length <= 9) {
      this.addImage4 = true;

      for (let file of files) {
        if (file.size / 1024 / 1024 >= 1) {
          alert("Each file size can't be more than 1 MB.");
        } else {
          let reader = new FileReader();
          reader.onload = (e: any) => {
            this.urls4.push(e.target.result);
            if (this.urls4.length == 10) {
              this.addImage4 = false;
            }
          };
          reader.readAsDataURL(file);
        }
      }
    }
  }

  save() {
    if (this.dnNumber) {
      let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;

      let data = {
        dn_number: this.dnNumber,
        claim_date: this.claimDate,
        dn_date: this.listDNDetail[0].delivery_notice_date,
        claim_chronology: this.claimChrono,
        img_label: this.urls,
        img_parts: this.urls2,
        img_packaging: this.urls3,
        img_dn: this.urls4,
        sap_code: sap_code,
        material_code: [],
        material_description: [],
        dn_qty: [],
        claim_qty: [],
        user: JSON.parse(localStorage.getItem("user")).email
      };

      $("#tblClaimLogSparepart #material_number").each(function() {
        data.material_code.push($(this).text());
      });
      $("#tblClaimLogSparepart #material_desc").each(function() {
        data.material_description.push($(this).text());
      });
      $("#tblClaimLogSparepart #dn_qty").each(function() {
        data.dn_qty.push($(this).text());
      });
      $("#tblClaimLogSparepart #pallet").each(function() {
        data.claim_qty.push($(this).val());
      });

      for (let i = 0; i < data.claim_qty.length; i++) {
        if (data.claim_qty[i] == 0) {
          data.claim_qty.splice(i, 1);
          data.material_code.splice(i, 1);
          data.material_description.splice(i, 1);
          data.dn_qty.splice(i, 1);
        }
      }
      console.log(data);

      if (
        data.claim_chronology == undefined ||
        data.img_dn.length == 0 ||
        data.img_label.length == 0 ||
        data.img_packaging.length == 0 ||
        data.img_parts.length == 0 ||
        data.material_code.length == 0
      ) {
        alert("All mandatory field must be filled.");
        return;
      }
      this.spinner.show();
      this.logisticClaimSparepart.saveClaimSparepart(data).subscribe(
        result => {
          if (result["message"] == "Claim submitted.") {
            alert(result["message"]);
            this.router.navigate(["claim/logistic-claim-report-spareparts"]);
          } else {
            alert(result["message"]);
          }
          this.spinner.hide();
        },
        (error: Error) => {
          console.log(error.message);
          this.spinner.hide();
        }
      );
    } else {
      alert("Choose DN Number first.");
    }
  }
  deleteImgLabel(index: number) {
    // console.log(index);
    this.urls.splice(index, 1);
    this.files.splice(index, 1);
    if (this.urls.length <= 9) this.addImage = true;
  }
  deleteImgParts(index: number) {
    this.urls2.splice(index, 1);
    this.files2.splice(index, 1);
    if (this.urls2.length <= 9) this.addImage2 = true;
  }
  deleteImgPackaging(index: number) {
    this.urls3.splice(index, 1);
    this.files3.splice(index, 1);
    if (this.urls3.length <= 9) this.addImage3 = true;
  }
  deleteImgDn(index: number) {
    this.urls4.splice(index, 1);
    this.files4.splice(index, 1);
    if (this.urls4.length <= 9) this.addImage4 = true;
  }
  discard() {
    this.urls.length = 0;
    this.urls2.length = 0;
    this.urls3.length = 0;
    this.urls4.length = 0;
    delete this.claimNumb;
    this.listDNDetail.length = 0;
    delete this.dnNumber;
    delete this.claimDate;
    delete this.claimChrono;
    // this.router.routeReuseStrategy.shouldReuseRoute = function() {
    //   return false;
    // };
    // let currentUrl = this.router.url + "?";
    // this.router.navigateByUrl(currentUrl).then(() => {
    //   this.router.navigated = false;
    this.router.navigate([], {
      queryParams: { claimNumber: null },
      queryParamsHandling: "merge"
    });
    // });
  }

  submitClaim() {
    if (this.claimNumb) {
      this.edit();
    } else {
      this.save();
    }
  }
  edit() {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;

    let data = {
      dn_number: this.dnNumber,
      claim_date: this.claimDate,
      dn_date: this.listDNDetail[0].delivery_notice_date,
      claim_chronology: this.claimChrono,
      img_label: this.urls,
      img_parts: this.urls2,
      img_packaging: this.urls3,
      img_dn: this.urls4,
      sap_code: sap_code,
      material_code: [],
      material_description: [],
      dn_qty: [],
      claim_qty: [],
      user: JSON.parse(localStorage.getItem("user")).email
    };

    $("#tblClaimLogSparepart #material_number").each(function() {
      data.material_code.push($(this).text());
    });
    $("#tblClaimLogSparepart #material_desc").each(function() {
      data.material_description.push($(this).text());
    });
    $("#tblClaimLogSparepart #dn_qty").each(function() {
      data.dn_qty.push($(this).text());
    });
    $("#tblClaimLogSparepart #pallet").each(function() {
      data.claim_qty.push($(this).val());
    });

    for (let i = 0; i < data.claim_qty.length; i++) {
      if (data.claim_qty[i] == 0) {
        data.claim_qty.splice(i, 1);
        data.material_code.splice(i, 1);
        data.material_description.splice(i, 1);
        data.dn_qty.splice(i, 1);
      }
    }

    console.log(data);
    this.spinner.show();
    this.logisticClaimSparepart
      .updateClaimSparepart(this.claimNumb, data)
      .subscribe(
        result => {
          if (result["message"] == "Claim updated.") {
            alert(result["message"]);
            this.router.navigate(["claim/logistic-claim-report-spareparts"]);
          } else alert(result["message"]);
          this.spinner.hide();
        },
        error => {
          console.error(error);
          this.spinner.hide();
        }
      );
  }
}
