import { TestBed } from '@angular/core/testing';

import { InvoiceTimelinessService } from './invoice-timeliness.service';

describe('InvoiceTimelinessService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InvoiceTimelinessService = TestBed.get(InvoiceTimelinessService);
    expect(service).toBeTruthy();
  });
});
