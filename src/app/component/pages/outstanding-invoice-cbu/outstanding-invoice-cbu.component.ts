import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { formatDate, Location } from "@angular/common";
import { Router, ActivatedRoute,NavigationExtras } from "@angular/router";
import { CbuPurchaseOrderService } from "../../../services/cbu-purchase-order.service";
import { OutstandingAndOverdueReportService } from "src/app/services/outstanding-and-overdue-report.service";
import { environment } from "src/environments/environment";
declare var jquery: any;
declare var $: any;
declare var number: any;
import * as moment from 'moment';

@Component({
  selector: 'app-outstanding-invoice-cbu',
  templateUrl: './outstanding-invoice-cbu.component.html',
  styleUrls: ['./outstanding-invoice-cbu.component.css']
})
export class OutstandingInvoiceCbuComponent implements OnInit {

  data_: any[];

  disabledPay1: any
  disabledPay2: any
  disabledPay3: any
  disabledPay4: any
  so_number: string

  po_number: string
  order_type: string
  invoice_number: string
  invoice_date: any
  due_date: any
  delivery_notice_number: string
  delivery_notice_date: any
  data_sap_dcs_amount: any = [];
  data_sap_dcs: any[];
  total_discount: number;
  total_allocated: number;
  vat: number;
  total_allocated_with_vat: number;
  total_discount_simulate: number;
  total_allocated_simulate: number;
  total_vat_simulate: number;
  total_allocated_vat_simulate: number;
  pallet: any = [];
  total_allocated_before_discount_simulate: any;
  total_allocated_before_discount: any;
  payment_deadline: any;
  online_payment_type: string;
  customerAccount: string;
  userIDBCA: string;
  cash_payment_type: string = "Bank Transfer";

  is_tester: boolean = true;

  baseUrl = environment.baseUrl;
  appUrl = environment.appUrl;

  canPreviewPriceSimulate: boolean = false
  canDoingPayment: boolean = false
  confirmCBU: boolean = false;
  bankTF: boolean = false;

  lblConOrder = "Continue Order";
  payment_method: string;
  paymentMethods: any;
  name = "Get Date";
  today = new Date();
  jstoday = "";

  data: any[];
  constructor(
    private spinner: NgxSpinnerService
    ,private router: Router
    ,private activatedRoute: ActivatedRoute
    ,private outstandingReport: OutstandingAndOverdueReportService
    ,private cbuOrder: CbuPurchaseOrderService) {
      this.jstoday = formatDate(this.today, "dd-MM-yyyy", "en-US", "+0700");
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      let so_number = params['so_number'];
      let invoice_number = params['invoice_number'];
      this.getPaymentDetail(so_number,invoice_number);
    });
    this.canPreviewPriceSimulate = JSON.parse(localStorage.getItem("user")).permissions["Preview Price in Simulate Order"]
    this.canDoingPayment = JSON.parse(localStorage.getItem("user")).permissions["Doing Payment"]
  }

  getPaymentDetail(so_number,invoice_number) {
    this.spinner.show();
    this.customerAccount = localStorage.getItem("bank_transfer_va")
    this.outstandingReport.getPaymentDetail(so_number,invoice_number).subscribe(
      (_result: []) => {
        console.log(_result)
        if(_result['data_cbu'].length > 0){
          this.po_number = _result['data_cbu'][0]['po_number'] ? _result['data_cbu'][0]['po_number'] : _result['data_sparepart'][0]['po_number']
          this.so_number = so_number
          this.order_type = 'CBU'
          this.invoice_number = _result['data_cbu'][0]['invoice_number'] ? _result['data_cbu'][0]['invoice_number'] : _result['data_sparepart'][0]['invoice_number']
          this.invoice_date = _result['data_cbu'][0]['invoice_date'] ? _result['data_cbu'][0]['invoice_date'] : _result['data_sparepart'][0]['invoice_date']
          this.due_date = _result['data_cbu'][0]['due_date'] ? _result['data_cbu'][0]['due_date'] : _result['data_sparepart'][0]['due_date']
          this.delivery_notice_number = '-'
          if(_result['data_cbu'][0]['delivery_notice_number'] != null)
            this.delivery_notice_number = _result['data_cbu'][0]['delivery_notice_number'] ? _result['data_cbu'][0]['delivery_notice_number'] : _result['data_sparepart'][0]['delivery_notice_number']
          this.delivery_notice_date = '-'
          if(_result['data_cbu'][0]['delivery_notice_date'] != null)
            this.delivery_notice_date = _result['data_cbu'][0]['delivery_notice_date'] ? _result['data_cbu'][0]['delivery_notice_date'] : _result['data_sparepart'][0]['delivery_notice_date']
        

          this.cbuOrder.getTOPFromBillTo(_result['data_cbu'][0]['bill_to'], 10).subscribe(
            result => {
              if (result["message"] == "Success.")
                this.paymentMethods = result["top"];
              else this.paymentMethods = 0;
              console.log(`"${this.paymentMethods}"`);
            },
            error => {
              console.log(error);
            }
          );

          this.data_ = _result['list_material_data_cbu']
          // Total alloc before disc =  znetwr2
          // Total Disc = zdiscount
          // VAT = zmwsbp2
          // Total alloc after disc = znetwr2 - zdiscount
          // Total allocated with VAT = znetwr2 + zmwsbp2
          // var total_allocated_before_discount_ = 0
          // for (var i = 0; i < _result['list_material_data_cbu'].length; i++) {
          //   total_allocated_before_discount_ = total_allocated_before_discount_ + parseInt(_result['list_material_data_cbu'][i]['znetwr2'])
          // }
          // this.total_allocated_before_discount = total_allocated_before_discount_

          var total_discount_ = 0
          for (var i = 0; i < _result['list_material_data_cbu'].length; i++) {
            total_discount_ = total_discount_ + _result['list_material_data_cbu'][i]['zdiscount'] * 100
          }
          this.total_discount = total_discount_

          var vat_ = 0
          for (var i = 0; i < _result['list_material_data_cbu'].length; i++) {
            vat_ = vat_ + _result['list_material_data_cbu'][i]['zmwsbp2'] * 100
          }
          this.vat = vat_

        }else if(_result['data_sparepart'].length > 0){
          this.po_number = _result['data_sparepart'][0]['po_number']
          this.so_number = so_number
          this.order_type = 'SPAREPARTS'
          this.invoice_number = _result['data_sparepart'][0]['invoice_number']
          this.invoice_date = _result['data_sparepart'][0]['invoice_date']
          this.due_date = _result['data_sparepart'][0]['due_date']
          this.delivery_notice_number = '-'
          if(_result['data_sparepart'][0]['delivery_notice_number'] != null)
            this.delivery_notice_number =  _result['data_sparepart'][0]['delivery_notice_number']
          this.delivery_notice_date = '-'
          if(_result['data_sparepart'][0]['delivery_notice_date'] != null)
            this.delivery_notice_date = _result['data_sparepart'][0]['delivery_notice_date']
          
          this.cbuOrder.getTOPFromBillTo(_result['data_sparepart'][0]['bill_to'], 20).subscribe(
            result => {
              if (result["message"] == "Success.")
                this.paymentMethods = result["top"];
              else this.paymentMethods = 0;
              console.log(`"${this.paymentMethods}"`);
            },
            error => {
              console.log(error);
            }
          );
        }else{
          alert('Please Pay Outside DCS')
          this.router.navigate(["outstanding-and-overdue/report"])
        }
        
        var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;

        if (customer_code == '0010000024'){
          this.is_tester = true
        }

        var user_id = JSON.parse(localStorage.getItem("user")).id;

        var paymentMethod = `"${this.paymentMethods}"`;

        if (paymentMethod == '"T000"') {
          var payment_method = "Cash";
          this.payment_method = "Cash";
        } else {
          var payment_method = "Credit";
          this.payment_method = "Credit";
        }



          // var data = {
          //   customer_code: customer_code,
          //   po_number: this.po_number,
          //   bill_to: _result['data_cbu'][0]['bill_to'] ? _result['data_cbu'][0]['bill_to'] : _result['data_sparepart'][0]['bill_to'],
          //   ship_to: _result['data_cbu'][0]['ship_to'] ? _result['data_cbu'][0]['ship_to'] : _result['data_sparepart'][0]['ship_to'],
          //   remarks: _result['data_cbu'][0]['remarks'] ? _result['data_cbu'][0]['remarks'] : _result['data_sparepart'][0]['remarks'],
          //   material_number: [],
          //   qty: [],
          //   pallet: [],
          //   payment_method: payment_method,
          //   sales_order_number: "9999",
          //   user_id: user_id,
          //   unit_of_measure: [],
          //   sloc: [],
          //   sold_to: _result['data_cbu'][0]['bill_to'] ? _result['data_cbu'][0]['bill_to'] : _result['data_sparepart'][0]['bill_to'],
          //   posnr: [],
          //   order_date: formatDate(this.today, "yyyy-MM-dd", "en-US", "+0530"),
          //   payment_trigger: '-',
          //   sub_total: _result['data_cbu'][0]['total_ordered'] ? _result['data_cbu'][0]['total_ordered'] : _result['data_sparepart'][0]['total_ordered'],
          //   voucher: sessionStorage.getItem("voucherCode"),
          //   user: JSON.parse(localStorage.getItem("user")).email
          //   // so_number: so_number.toString()
          // };
          
          // if(_result['data_cbu']){
          //   for (var i = 0; i < _result['data_cbu'].length; i++) {
          //     data.material_number.push(_result['data_cbu'][i]['material_number']);
          //   }
          // }else{
          //   for (i = 0; i < _result['data_sparepart'].length; i++) {
          //     data.material_number.push(_result['data_sparepart'][i]['material_number']);
          //   }
          // }

          // if(_result['data_cbu']){
          //   for (var i = 0; i < _result['data_cbu'].length; i++) {
          //     data.unit_of_measure.push(_result['data_cbu'][i]['base_unit_of_measure']);
          //   }
          // }else{
          //   for (i = 0; i < _result['data_sparepart'].length; i++) {
          //     data.unit_of_measure.push(_result['data_sparepart'][i]['base_unit_of_measure']);
          //   }
          // }

          // if(_result['data_cbu']){
          //   for (var i = 0; i < _result['data_cbu'].length; i++) {
          //     data.sloc.push(_result['data_cbu'][i]['sloc']);
          //     // data.sloc.push('1500');
          //   }
          // }else{
          //   for (i = 0; i < _result['data_sparepart'].length; i++) {
          //     data.sloc.push(_result['data_sparepart'][i]['sloc']);
          //   }
          // }

          // if(_result['data_cbu']){
          //   for (var i = 0; i < _result['data_cbu'].length; i++) {
          //     var qty:number = _result['data_cbu'][i]['pallet'] * _result['data_cbu'][i]['numerator_for_conversion_to_base_units_of_measure']
          //     data.qty.push(qty.toString());
          //   }
          // }else{
          //   for (i = 0; i < _result['data_sparepart'].length; i++) {
          //     data.qty.push(parseInt(_result['data_sparepart'][i]['qty']).toString());
          //   }
          // }
  
          // this.pallet.length = 0;
          // if(_result['data_cbu']){
          //   for (var i = 0; i < _result['data_cbu'].length; i++) {
          //     data.pallet.push(parseInt(_result['data_cbu'][i]['pallet']).toString());
          //   }
          // }
    
          // var posnr = 10;
    
          // // if (this.voucherCode) {
          // //   let posnr2 = [];
          // //   $("#tableCBU #posnr").each(function () {
          // //     data.posnr.push($(this).text());
          // //     posnr2.push($(this).text());
          // //   });
          // //   this.posnr = posnr2;
          // //   console.log(this.posnr);
          // // } else {
          // if(_result['data_cbu'].length > 0)
          //   for (var i = 0; i < _result['data_cbu'].length; i++) {
          //     if (posnr.toString().length == 2) {
          //       data.posnr.push("0000" + posnr.toString()).toString();
          //       posnr += 10;
          //     } else if (posnr.toString().length == 3) {
          //       data.posnr.push("000" + posnr.toString()).toString();
          //       posnr += 10;
          //     } else if (posnr.toString().length == 4) {
          //       data.posnr.push("00" + posnr.toString()).toString();
          //       posnr += 10;
          //     } else if (posnr.toString().length == 5) {
          //       data.posnr.push("0" + posnr.toString()).toString();
          //       posnr += 10;
          //     }
          // }else{
          //   for (var i = 0; i < _result['data_sparepart'].length; i++) {
          //     if (posnr.toString().length == 2) {
          //       data.posnr.push("0000" + posnr.toString()).toString();
          //       posnr += 10;
          //     } else if (posnr.toString().length == 3) {
          //       data.posnr.push("000" + posnr.toString()).toString();
          //       posnr += 10;
          //     } else if (posnr.toString().length == 4) {
          //       data.posnr.push("00" + posnr.toString()).toString();
          //       posnr += 10;
          //     } else if (posnr.toString().length == 5) {
          //       data.posnr.push("0" + posnr.toString()).toString();
          //       posnr += 10;
          //     }
          //   }
          // }

          // console.log(data)
        
          // this.cbuOrder.submitOrder(data).subscribe(
          //   res => {
          //     var data_sap = [];
          //     data_sap.push(res);
          //     console.log(data_sap);
          //     this.data_sap_dcs = data_sap[0].result.output;
          //     let j = 1;
          //     for (let i = 0; i < this.data_sap_dcs.length; i++) {
          //       if (this.data_sap_dcs[i].posnr_so.split("").pop() == 0) {
          //         this.data_sap_dcs[i].number = j + ".";
          //         j++;
          //       }
          //       this.data_sap_dcs[i].kwmeng = Math.round(
          //         this.data_sap_dcs[i].kwmeng
          //       );
          //       this.data_sap_dcs[i].zkwmeng2 = Math.round(
          //         this.data_sap_dcs[i].zkwmeng2
          //       );
          //       this.data_sap_dcs[i].backorder = Math.round(
          //         this.data_sap_dcs[i].kwmeng - this.data_sap_dcs[i].zkwmeng2
          //       );
          //       this.data_sap_dcs[i].netpr_gros = this.data_sap_dcs[i].netpr_gros;
          //       this.data_sap_dcs[i].netwr_gros = this.data_sap_dcs[i].netwr_gros;
          //       this.data_sap_dcs[i].zdiscount_gros = this.data_sap_dcs[
          //         i
          //       ].zdiscount_gros.replace("-", "");
          //       if (this.data_sap_dcs[i].waerk == "IDR") {
          //         this.data_sap_dcs[i].netpr_gros =
          //           this.data_sap_dcs[i].netpr_gros * 100;
          //         this.data_sap_dcs[i].netwr_gros =
          //           this.data_sap_dcs[i].netwr_gros * 100;
          //         this.data_sap_dcs[i].zdiscount_gros =
          //           this.data_sap_dcs[i].zdiscount_gros.replace("-", "") * 100;
          //       }
          //     }
          //     if (data_sap) {
          //       if (
          //         data_sap[0].result.header[0].message ==
          //         "Please Confirm released Credit Limit to HPPI"
          //       ) {
          //         alert(`This order is blocked. Please contact HPPI.`);
          //       }
          //       this.cbuOrder.storeHeaderOrder(data_sap[0].result.header[0]);
          //       this.confirmCBU = true;
          //       // this.so_number = JSON.parse(
          //       //   localStorage.getItem("orderHeaderData")
          //       // ).vbeln;
          //       this.total_allocated_before_discount = JSON.parse(
          //         localStorage.getItem("orderHeaderData")
          //       ).znetwr2_gros;
          //       this.total_discount = JSON.parse(
          //         localStorage.getItem("orderHeaderData")
          //       ).zdiscount.replace("-", "");
          //       this.total_discount = this.total_discount ? this.total_discount : 0;
          //       this.total_allocated = JSON.parse(
          //         localStorage.getItem("orderHeaderData")
          //       ).znetwr2;
          //       this.total_allocated = this.total_allocated
          //         ? this.total_allocated
          //         : 0;
          //       this.vat = JSON.parse(
          //         localStorage.getItem("orderHeaderData")
          //       ).zmwsbp2;
          //       this.vat = this.vat ? this.vat : 0;
          //       console.log(data_sap);
          //       if (data_sap[0].result.header[0].waers === "IDR") {
          //         this.total_allocated_before_discount =
          //           JSON.parse(localStorage.getItem("orderHeaderData"))
          //             .znetwr2_gros * 100;
          //         this.total_discount =
          //           JSON.parse(
          //             localStorage.getItem("orderHeaderData")
          //           ).zdiscount.replace("-", "") * 100;
          //         this.total_allocated =
          //           JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
          //           100;
          //         this.vat =
          //           JSON.parse(localStorage.getItem("orderHeaderData")).zmwsbp2 *
          //           100;
          //       }
          //       this.total_allocated_with_vat = this.total_allocated + this.vat;
          //       // this.spinner.hide()
          //       this.confirmCBU = true;
          //       this.spinner.hide();
          //       alert("Order being process in back order!");
          //     }
          //   },
          //   err => {
          //     this.spinner.hide();
          //     console.log(err);
          //   }
          // );
        
        this.spinner.hide();
      },
      (_error: Error) => {
        this.spinner.hide();
        console.log(_error.message);
      }
    );
  }

  back(){
    this.router.navigate(["/outstanding-and-overdue/report"])
  }
  downloadSO() {
    
  }

  conOrder() {
    if (this.online_payment_type == "KlikBCA"){
      if(!this.userIDBCA){
        alert('Please Input UserID')
        return
      }
    }

    var zkwmeng2 = 0;
    for (let i = 0; i < this.data_.length; i++) {
      zkwmeng2 = zkwmeng2 + this.data_[i].kwmeng2;
    }
    // console.log(zkwmeng2);

    if (zkwmeng2 > 0) {
      this.spinner.show();
              let dateNow = moment()
              let transactionDateNow = dateNow.format("YYYY-MM-DD HH:mm:ss")
              let transactionDateExpire = dateNow.add('10', 'minutes').format("YYYY-MM-DD HH:mm:ss")

              let userEmail = JSON.parse(localStorage.getItem("dealer")).email1[0]
              // let userEmail = "arbhasyech@gmail.com"

              let additionalData = `no - no - no - ${userEmail}`

              let params = {
                type: this.online_payment_type,
                channelId: "",
                currency: "IDR",
                transactionNo: this.so_number,
                transactionAmount: this.totalAllocatedBeforeDisc() + this.vat,
                transactionDate: transactionDateNow,
                transactionExpire: transactionDateExpire,
                description: "DCSTRANSACTION",
                customerAccount: "",
                customerName: localStorage.getItem("dealer_name"),
                freeTexts: [{
                  indonesian: "",
                  english: ""
                }],
                additionalData: additionalData,
                payType: 'outstanding',
                callbackURL: `${this.appUrl}/redirect?so_number=` + this.so_number
              }

              let paramsNonVa = {
                channelId: "",
                transactionNo: this.so_number,
                transactionAmount: this.totalAllocatedBeforeDisc() + this.vat,
                transactionDate: transactionDateNow,
                transactionExpire: transactionDateExpire,
                description: "DCS DESC",
                customerName: localStorage.getItem("dealer_name"),
                customerEmail: JSON.parse(localStorage.getItem("dealer")).email1[0],
                customerPhone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0],
                customerAccount: "",
                type: this.online_payment_type,
                additionalData: additionalData,
                payType: 'outstanding',
                callbackURL: `${this.appUrl}/redirect?so_number=` + this.so_number
              }

              let paramsCreditCard = {
                channelId: "HondaPower",
                type: "Credit Card",
                transactionNo: this.so_number,
                transactionAmount: this.totalAllocatedBeforeDisc() + this.vat,
                transactionDate: transactionDateNow,
                transactionExpire: transactionDateExpire,
                description: "DCS DESC",
                additionalData: additionalData,
                payType: 'outstanding',
                callbackURL: `${this.appUrl}/redirect?so_number=` + this.so_number
              }

              if (this.online_payment_type == "BCA VA") {
                params.type = "BCA VA"
                params.channelId = "HONDAPOWERBVA01"
                params.customerAccount = "61222" + localStorage.getItem("bca_va")
                this.customerAccount = "61222" + localStorage.getItem("bca_va")
              } else if (this.online_payment_type == "KlikBCA") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKB01"
                let userID = {
                  userID: this.userIDBCA
                }
                Object.assign(paramsNonVa,userID)
                paramsNonVa.customerAccount = this.userIDBCA
                this.customerAccount = this.userIDBCA
              } else if (this.online_payment_type == "BCA KlikPay") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKP01"
                paramsNonVa.customerAccount = localStorage.getItem("bank_transfer_va")
                this.customerAccount = localStorage.getItem("bank_transfer_va")
              } else if (this.online_payment_type == "CIMB Click") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERCIMBCL01"
                paramsNonVa.customerAccount = localStorage.getItem("bank_transfer_va")
                this.customerAccount = localStorage.getItem("bank_transfer_va")
              } else if (this.online_payment_type == "iPay BNI") {
                paramsNonVa.type = "iPay BNI"
                paramsNonVa.channelId = "HONDAIPAY01"
                paramsNonVa.customerAccount = localStorage.getItem("bank_transfer_va")
                this.customerAccount = localStorage.getItem("bank_transfer_va")
              } else if (this.online_payment_type == "Permata VA") {
                params.type = "Permata VA"
                params.channelId = "HONDAPOWERPVA01"
                params.customerAccount = "873584" + localStorage.getItem("permata_va")
                this.customerAccount = "873584" + localStorage.getItem("permata_va")
              }

              console.log(paramsCreditCard)
              // END PARAMS
              // IF PAYMENT == PERMATA VA OR BCA VA
              if (this.online_payment_type == "Permata VA" || this.online_payment_type == "BCA VA") {
                this.cbuOrder.sendPayment(params).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    console.log(result)
                    
                    this.bankTF = true;
                    this.confirmCBU = false;

                    this.so_number = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).vbeln;
                    this.total_allocated = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).znetwr2;
                    if (
                      JSON.parse(localStorage.getItem("orderHeaderData")).waerk == "IDR"
                    ) {
                      this.total_allocated =
                        JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                        100;
                    }
                    this.payment_deadline = new Date();
                    this.payment_deadline.setDate(this.payment_deadline.getDate() + 9);

                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.log(error)
                  this.spinner.hide()
                })
              }
              // ELSE IF PAYMENT == CC
              else if (this.online_payment_type == "Credit Card") {
                console.log(paramsCreditCard)
                this.cbuOrder.sendPayment(paramsCreditCard).subscribe(result => {
                  console.log(result)
                  if (result["insertStatus"] == "00") {
                    console.log(result)
                    
                    if (result["redirectURL"] !== "") {
                      location.href = result["redirectURL"]
                    }
                    this.bankTF = true;
                    this.confirmCBU = false;

                    this.so_number = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).vbeln;
                    this.total_allocated = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).znetwr2;
                    if (
                      JSON.parse(localStorage.getItem("orderHeaderData")).waerk == "IDR"
                    ) {
                      this.total_allocated =
                        JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                        100;
                    }
                    this.payment_deadline = new Date();
                    this.payment_deadline.setDate(this.payment_deadline.getDate() + 9);
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.log(error)
                  this.spinner.hide()
                })
              } else if (this.online_payment_type == "KlikBCA" || this.online_payment_type == "BCA KlikPay" || this.online_payment_type == "CIMB Click" || this.online_payment_type == "iPay BNI") {
                this.cbuOrder.sendPayment(paramsNonVa).subscribe(result => {
                  console.log(result)
                  if (result["insertStatus"] == "00") {
                    
                    // console.log(res);
                    this.bankTF = true;
                    this.confirmCBU = false;
                    let redirectURL = result["redirectURL"]

                    if (redirectURL) {
                      let form = document.createElement("form")
                      form.setAttribute("method", "POST")
                      form.setAttribute("enctype", "application/x-www-form-urlencoded")
                      form.setAttribute("action", redirectURL)
                      for (const key in result["redirectData"]) {
                        if (result["redirectData"].hasOwnProperty(key)) {
                          const element = result["redirectData"][key];
                          let input = document.createElement("input")
                          input.setAttribute("type", "hidden")
                          input.setAttribute("name", key)
                          input.setAttribute("value", element)
                          form.appendChild(input)
                        }
                      }
                      document.body.appendChild(form)

                      form.submit()
                    }

                    this.so_number = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).vbeln;
                    this.total_allocated = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).znetwr2;
                    if (
                      JSON.parse(localStorage.getItem("orderHeaderData")).waerk == "IDR"
                    ) {
                      this.total_allocated =
                        JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                        100;
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.log(error)
                  this.spinner.hide()
                })
              } else {
                this.bankTF = true;
                this.confirmCBU = false;

                this.so_number = JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).vbeln;
                this.total_allocated = JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).znetwr2;
                if (
                  JSON.parse(localStorage.getItem("orderHeaderData")).waerk == "IDR"
                ) {
                  this.total_allocated =
                    JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                    100;
                }
                this.payment_deadline = new Date();
                this.payment_deadline.setDate(this.payment_deadline.getDate() + 9);
              }

            this.spinner.hide();
    } else {
      alert("Order being process in back order!");
      location.reload();
    }
    sessionStorage.removeItem("voucherCode");
    window.scroll(0, 0);
  }

  totalSimulateOrderQty() {
    var totalOrder = 0;
    $("#tableCBUOutstanding #zkwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalConfirmationOrderQty() {
    var totalOrder = 0;
    $("#tableCBUConfirmation #zkwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalSimulateOrderAmount() {
    var totalOrder = 0;
    $("#tableCBUOutstanding #ztotal").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalConfirmationOrderAmount() {
    var totalOrder = 0;
    $("#tableCBUConfirmation #znetwr").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalSimulateAmountAllocated() {
    var allocated = 0;
    $("#tableCBUSubmit #zznetwr2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    return allocated;
  }

  totalSimulateAllocated() {
    var allocated = 0;
    $("#tableCBUOutstanding #zzkwmeng2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    return allocated;
  }

  totalAllocatedBeforeDisc() {
    var allocated_before_disc = 0;
    $("#tableCBUOutstanding #ztotal").each(function () {
      // console.log(parseInt($(this).text()));
      allocated_before_disc = allocated_before_disc + parseInt($(this).text());
    });
    return allocated_before_disc;
  }

  totalAllocated() {
    var allocated = 0;
    $("#tableCBUConfirmation #zzkwmeng2").each(function () {
      console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    if (allocated == 0) this.lblConOrder = "Continue Order";
    return allocated;
  }

  totalBackorder() {
    var backorder = 0;
    $("#tableCBUOutstanding #zbackorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }

  totalSubmitBackorder() {
    var backorder = 0;
    $("#tableCBUConfirmation #zbackorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }

  totalDiscount() {
    var discount = 0;
    $("#tableCBUSubmit #zzdiscount").each(function () {
      // console.log(parseInt($(this).text()));
      discount = discount + parseInt($(this).text());
    });
    return discount;
  }

  totalVAT() {
    var backorder = 0;
    // for (var i = 0; i < this.prodSelected.length; i++) {
    //   if(this.prodSelected[i].backorder){
    //     backorder = backorder + this.prodSelected[i].backorder;
    //   }else{
    //     backorder = 0;
    //   }
    // }
    return backorder;
  }

  totalAllocatedVAT() {
    var backorder = 0;
    // for (var i = 0; i < this.prodSelected.length; i++) {
    //   if(this.prodSelected[i].backorder){
    //     backorder = backorder + this.prodSelected[i].backorder;
    //   }else{
    //     backorder = 0;
    //   }
    // }
    return backorder;
  }

  radioActive() {
    this.disabledPay1 = "";
    this.disabledPay2 = "";
    this.disabledPay3 = "";
    this.disabledPay4 = "";
  }
  jenisPay(val) {
    this.radioActive();
    if (val == 1) {
      this.disabledPay3 = "disable-radio";
      this.disabledPay4 = "disable-radio";
      this.cash_payment_type = "Bank Transfer";
    } else {
      this.disabledPay1 = "disable-radio";
      this.disabledPay2 = "disable-radio";
      this.cash_payment_type = "Payment Gateway";
    }
  }

  onlinePaymentType(e) {
    console.log(e)
    if (this.disabledPay3 != "disable-radio" && this.disabledPay4 != "disable-radio") {
      if (e == 'klik-bca') {
        this.online_payment_type = "KlikBCA"
      } else if (e == 'bca-klikpay') {
        this.online_payment_type = "BCA KlikPay"
      } else if (e == 'cimb-click') {
        this.online_payment_type = "CIMB Click"
      } else if (e == 'ipay-bni') {
        this.online_payment_type = "iPay BNI"
      } else if (e == 'permata-bankva') {
        this.online_payment_type = "Permata VA"
      } else if (e == 'bca-va') {
        this.online_payment_type = "BCA VA"
      } else if (e == 'credit-card') {
        this.online_payment_type = "Credit Card"
      }
    } else {
      this.online_payment_type = ""
    }
    console.log(this.online_payment_type)
  }

}