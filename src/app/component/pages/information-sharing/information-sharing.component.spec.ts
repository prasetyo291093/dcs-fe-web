import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationSharingComponent } from './information-sharing.component';

describe('InformationSharingComponent', () => {
  let component: InformationSharingComponent;
  let fixture: ComponentFixture<InformationSharingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationSharingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationSharingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
