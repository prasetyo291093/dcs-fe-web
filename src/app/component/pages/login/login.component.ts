import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../services/auth.service";
import { FormsModule } from "@angular/forms";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  title = "Honda";
  viewPasswordCon = "password";
  isActive: boolean = false;
  model: any = {};
  baseUrl = environment.baseUrl

  // images: any = [
  //   "/assets/img/L1.jpg",
  //   "/assets/img/L2.jpg",
  //   "/assets/img/L3.jpg",
  //   "/assets/img/L4.jpg",
  //   "/assets/img/L5.jpg"
  // ];
  images: any = []

  constructor(private router: Router, private auth: AuthService) { }

  seePassword(): void {
    this.isActive = !this.isActive;
    this.isActive
      ? (this.viewPasswordCon = "text")
      : (this.viewPasswordCon = "password");
  }
  onSubmit() {
    this.auth.login(this.model).subscribe(
      res => {
        var data = [];
        data.push(res);
        console.log(data);
        if (data[0].message == "Success") {
          this.router.navigate(["dealer-profile"]);
          $("body").removeClass("bg-body");
          this.auth.storeUser(data[0].value);
          this.auth.storeUserGroup(data[0].group);
          this.auth.storeUrgention(data[0].urgention);
          this.auth.storeSalesHistory(data[0].actual, data[0].target);
          this.auth.storeUserClassification(data[0].classification);
          this.auth.storeMonthlyTargetActual(
            data[0].monthlyCBU,
            data[0].monthlyPart
          );
          this.auth.storeVirtualAccount(data[0].va,data[0].bank_transfer_va,data[0].bca_va,data[0].permata_va);
        } else if (data[0].message == "Your account is inactive.") {
          alert(data[0].message);
        } else if (data[0].message == "You cannot access this site.") {
          alert(data[0].message);
        } else if (data[0].message == "Your account has been blocked.") {
          alert(data[0].message);
        } else if (data[0].message == "Username doesn't exist.") {
          alert(data[0].message);
        } else if (data[0].message == "System maintenance.") {
          // alert(data[0].message + ' Please comeback at : ' + data[0].end_time);
          this.router.navigate(["maintenance"]);
        } else {
          alert("Sorry, your password was incorrect");
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  forgotPass() {
    this.router.navigate(["forgot-password"]);
  }

  ngOnInit() {
    $("body").addClass("bg-body");
    this.auth.getImageLogin().subscribe(result => {
      for (const key in result) {
        if (result.hasOwnProperty(key)) {
          const element = this.baseUrl + result[key];
          console.log(element)
          this.images.push(element)
        }
      }

      console.log(this.viewPasswordCon)

      var num = Math.floor(Math.random() * this.images.length);
      var img = this.images[num];
      $(".bg-login").css({
        background: "url(" + img + ")no-repeat center",
        "background-position": "right"
      });
    }, error => console.error(error))

  }
}
