import { TestBed } from '@angular/core/testing';

import { WarrantyClaimReportService } from './warranty-claim-report.service';

describe('WarrantyClaimReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WarrantyClaimReportService = TestBed.get(WarrantyClaimReportService);
    expect(service).toBeTruthy();
  });
});
