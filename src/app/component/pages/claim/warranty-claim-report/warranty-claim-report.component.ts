import { Component, OnInit, OnDestroy } from '@angular/core';
import { WarrantyClaimReportService } from 'src/app/services/warranty-claim-report.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-warranty-claim-report',
  templateUrl: './warranty-claim-report.component.html',
  styleUrls: ['./warranty-claim-report.component.css']
})
export class WarrantyClaimReportComponent implements OnInit, OnDestroy {

  btnFilter = false;
  resetFilter = false;
  items: any;
  itemCounts: any;

  constructor(
    private warrantyClaimReport: WarrantyClaimReportService,
  ) { }

  private subscription: Subscription = new Subscription();

  ngOnInit() {
    this.btnFilter = true;
    this.fetchReportData();
  }

  fetchReportData() {
    const sap_code = JSON.parse(localStorage.getItem('user')).sap_code;

    this.subscription.add(this.warrantyClaimReport.fetchReportData(sap_code).subscribe(
      result => {
        // console.log(result["form"]);
        // this.items = result;
        // this.itemCounts = result['form'];
        let items = []
        let form = []
        let detail = []
        let repairment = []
        let other_bill = []

        for (const key in result["form"]) {
          if (result["form"].hasOwnProperty(key)) {
            const element = result["form"][key];
            form.push(element)
          }
        }
        for (const key in result["detail"]) {
          if (result["detail"].hasOwnProperty(key)) {
            const element = result["detail"][key];
            detail.push(element)
          }
        }
        for (const key in result["repairment"]) {
          if (result["repairment"].hasOwnProperty(key)) {
            const element = result["repairment"][key];
            repairment.push(element)
          }
        }
        for (const key in result["other_bill"]) {
          if (result["other_bill"].hasOwnProperty(key)) {
            const element = result["other_bill"][key];
            other_bill.push(element)
          }
        }

        for (let i = 0; i < form.length; i++) {
          const id = form[i]["id"];
          for (let j = 0; j < detail.length; j++) {
            const form_id = detail[j]["form_id"];
            if(form_id == id) {
              items.push(form[i])
              items[i]["total_part_cost"] = detail[j]["total_part_cost"]
              items[i]["total_frt"] = detail[j]["total_frt"]
              items[i]["service_rate"] = detail[j]["service_rate"]
              items[i]["transportation_cost"] = detail[j]["transportation_cost"]
              items[i]["total_allowance_repairment_on_customer_location"] = detail[j]["total_allowance_repairment_on_customer_location"]
              items[i]["allowance_to_remove_obe_from_boat_total_allowance"] = detail[j]["allowance_to_remove_obe_from_boat_total_allowance"]
              items[i]["allowance_to_install_obe_from_boat_total_allowance"] = detail[j]["allowance_to_install_obe_from_boat_total_allowance"]
              items[i]["allowance_transportation_install_obe_from_boat_total_allowance"] = detail[j]["allowance_transportation_install_obe_from_boat_total_allowance"]
            }
          }
        }
        for (let i = 0; i < repairment.length; i++) {
          for (let j = 0; j < form.length; j++) {
            const id = form[j]["id"];
            const form_id = repairment[i]["form_id"];
            if(id == form_id) {
              items[j]["unit_price"] = repairment[i]["unit_price"]
              items[j]["frt"] = repairment[i]["frt"]
            }
          }
        }

        for (const item of items) {
          if(item.state == 0) {
            item.status = "Rejected"
          } else if(item.state == 1) {
            item.status = "Submitted"
          } else if(item.state == 2) {
            item.status = "Revise"
          } else if(item.state == 9) {
            item.status = "Approved"
          }
        }

        console.log(items)
        this.items = items
      }, (error: Error) => {
        console.log(error.stack);
        console.log(error.message);
      }
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  filter() {
    this.btnFilter = false;
    this.resetFilter = true;
  }

  resetFil() {
    this.btnFilter = true;
    this.resetFilter = false;
  }

}
