import { Component }from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router';
import { Title }             from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  // public title = ""

  constructor(
    public _router: Router,
    public title: Title,
  ) {
    this._router.events.subscribe((data) => {
       if (data instanceof RoutesRecognized) {
        let newtitle = data.state.root.firstChild.data.title;
        this.title.setTitle(newtitle? 'Honda - '+newtitle:'Honda');
       }
     });
  }
}
