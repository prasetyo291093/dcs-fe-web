export interface MaterialSearchItem {
    material_number: string;
    material_description: string;
}