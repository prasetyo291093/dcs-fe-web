import { Component, OnInit } from "@angular/core";
import { environment } from "src/environments/environment";
import { ResetPasswordService } from "src/app/services/reset-password.service";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.css"]
})
export class ResetPasswordComponent implements OnInit {
  constructor(
    private spinner: NgxSpinnerService,
    private resetPassword: ResetPasswordService,
    private router: Router
  ) {}

  viewPasswordCon = "password";
  isActive: boolean = false;

  viewPasswordCon2 = "password";
  isActive2: boolean = false;
  newPassword: string;
  confirmPassword: string;
  baseUrl = environment.baseUrl;

  ngOnInit() {}

  seePassword(): void {
    this.isActive = !this.isActive;
    this.isActive
      ? (this.viewPasswordCon = "text")
      : (this.viewPasswordCon = "password");

    console.log(this.isActive);
  }

  seePassword2(): void {
    this.isActive2 = !this.isActive2;
    this.isActive2
      ? (this.viewPasswordCon2 = "text")
      : (this.viewPasswordCon2 = "password");
  }

  submit() {
    let url_string = window.location.href;
    let url = new URL(url_string);
    let email = url.searchParams.get("email");
    let resetCode = url.searchParams.get("resetcode");
    let regex = new RegExp(
      "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
    );
    if (this.newPassword || this.confirmPassword) {
      if (this.newPassword == this.confirmPassword) {
        if (
          this.newPassword.match(regex) &&
          this.confirmPassword.match(regex)
        ) {
          let data = {
            password: this.newPassword,
            password_confirmation: this.confirmPassword
          };
          // console.log(email, resetCode);
          this.spinner.show();
          this.resetPassword
            .submitResetPassword(data, email, resetCode)
            .subscribe(
              result => {
                // console.log(result);
                this.spinner.hide();
                if (result["message"] == "Login with your new password") {
                  alert("Password has been reset.");
                  this.router.navigate(["login"]);
                } else if(result["message"] == "failed") {
                  alert("Reset code has expired")
                }
              },
              error => {
                this.spinner.hide();
                console.log(error);
              }
            );
        } else {
          alert("Password doesn't match with the criteria");
        }
      } else {
        alert("New Password and Confirm Password doesn't match.");
      }
    } else {
      alert("Both New Password and Confirm Password can't be empty.");
    }
  }
}
