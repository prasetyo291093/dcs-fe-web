import { Component, OnInit } from "@angular/core";
import { LogisticClaimCbuService } from "src/app/services/logistic-claim-cbu.service";
import { ProductCheckingService } from "src/app/services/product-checking.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router, ActivatedRoute, Params } from "@angular/router";
import * as moment from "moment";

@Component({
  selector: "app-logistic-claim-form-cbu",
  templateUrl: "./logistic-claim-form-cbu.component.html",
  styleUrls: ["./logistic-claim-form-cbu.component.css"]
})
export class LogisticClaimFormCbuComponent implements OnInit {
  frontSide: string = "No File Chosen";
  rightSide: string = "No File Chosen";
  rearSide: string = "No File Chosen";
  leftSide: string = "No File Chosen";
  packaging: string = "No File Chosen";
  barcode: string = "No File Chosen";
  engineNumber: string;
  frameNumber: string;
  materialGroup: string;
  materialNumber: string;
  materialDesc: string;
  remarks: string;
  claimType: string;
  dnNumber: string;
  dnDate: string;
  chronologyClaim: string;
  imageFront: any;
  imageRear: any;
  imagePackaging: any;
  imageRight: any;
  imageLeft: any;
  imageBarcode: any;
  claimForm: boolean = false;
  claimNum: string;

  url = new Array<string>();
  url2 = new Array<string>();
  url3 = new Array<string>();
  url4 = new Array<string>();
  url5 = new Array<string>();
  url6 = new Array<string>();

  constructor(
    private logisticClaimCbu: LogisticClaimCbuService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private activeRouter: ActivatedRoute,
    private productChecking: ProductCheckingService
  ) { }

  ngOnInit() {
    // let claimNum;
    this.activeRouter.queryParams.subscribe((params: Params) => {
      this.claimNum = params["claimNumber"];
    });
    console.log(this.claimNum);
    if (this.claimNum) {
      this.spinner.show();
      this.logisticClaimCbu.getClaimCbu(this.claimNum).subscribe(
        result => {
          if (Object.entries(result).length > 0) {
            this.claimForm = true;
            this.engineNumber = result[0]["engine_number"];
            this.frameNumber = result[0]["frame_number"];
            this.materialGroup = result[0]["material_group"];
            this.materialNumber = result[0]["material_code"];
            this.materialDesc = result[0]["material_description"];
            this.remarks = result[0]["remarks"];
            this.claimType = result[0]["claim_type"];
            this.dnNumber = result[0]["dn_number"];
            this.dnDate = moment(result[0]["dn_date"]).format("DD-MM-YYYY");
            this.chronologyClaim = result[0]["chronology_claim"];
            if (result[0]["img_front"]) {
              this.imageFront = result[0]["img_front"];
              this.url.push(result[0]["img_front"])
              this.frontSide = "Uploaded"
            }
            if (result[0]["img_rear"]) {
              this.imageRear = result[0]["img_rear"];
              this.url3.push(result[0]["img_rear"])
              this.rearSide = "Uploaded"
            }
            if (result[0]["img_packaging"]) {
              this.imagePackaging = result[0]["img_packaging"];
              this.url5.push(result[0]["img_packaging"])
              this.packaging = "Uploaded"
            }
            if (result[0]["img_right"]) {
              this.imageRight = result[0]["img_right"];
              this.url2.push(result[0]["img_right"])
              this.rightSide = "Uploaded"
            }
            if (result[0]["img_left"]) {
              this.imageLeft = result[0]["img_left"];
              this.url4.push(result[0]["img_left"])
              this.leftSide = "Uploaded"
            }
            if (result[0]["img_barcode"]) {
              this.imageBarcode = result[0]["img_barcode"];
              this.url6.push(result[0]["img_barcode"])
              this.barcode = "Uploaded"
            }
          }
          this.spinner.hide();
        },
        error => {
          console.error(error);
          this.spinner.hide();
        }
      );
    }
  }

  // changeName(event) {
  //   var files = event.target.files[0].name;

  //   if (event.target.files[0].size / 1024 / 1024 >= 1) {
  //     alert("File size can't be more than 1 MB.");
  //     return;
  //   }

  //   let reader = new FileReader();
  //   reader.onload = (e: any) => {
  //     this.imageFront = e.target.result;
  //   };
  //   reader.readAsDataURL(event.target.files[0]);
  //   if (
  //     files.length != 0 ||
  //     files.length != null ||
  //     files.length != undefined
  //   ) {
  //     this.frontSide = files;
  //   }
  // }

  changeName(event) {
    this.url = [];
    let files = event.target.files;

    if (event.target.files[0].size / 1024 / 1024 >= 1) {
      alert("File size can't be more than 1 MB.");
      return;
    }

    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.url.push(e.target.result);
          this.imageFront = e.target.result
        }
        reader.readAsDataURL(file);
      }
    }
  }

  // changeName2(event) {
  //   var files = event.target.files[0].name;

  //   if (event.target.files[0].size / 1024 / 1024 >= 1) {
  //     alert("File size can't be more than 1 MB.");
  //     return;
  //   }

  //   let reader = new FileReader();
  //   reader.onload = (e: any) => {
  //     this.imageRight = e.target.result;
  //   };
  //   reader.readAsDataURL(event.target.files[0]);

  //   if (
  //     files.length != 0 ||
  //     files.length != null ||
  //     files.length != undefined
  //   ) {
  //     this.rightSide = files;
  //   }
  // }

  changeName2(event) {
    this.url2 = [];
    let files = event.target.files;

    if (event.target.files[0].size / 1024 / 1024 >= 1) {
      alert("File size can't be more than 1 MB.");
      return;
    }

    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.url2.push(e.target.result);
          this.imageRight = e.target.result
        }
        reader.readAsDataURL(file);
      }
    }
  }

  // changeName3(event) {
  //   var files = event.target.files[0].name;

  //   if (event.target.files[0].size / 1024 / 1024 >= 1) {
  //     alert("File size can't be more than 1 MB.");
  //     return;
  //   }

  //   let reader = new FileReader();
  //   reader.onload = (e: any) => {
  //     this.imageRear = e.target.result;
  //   };
  //   reader.readAsDataURL(event.target.files[0]);
  //   if (
  //     files.length != 0 ||
  //     files.length != null ||
  //     files.length != undefined
  //   ) {
  //     this.rearSide = files;
  //   }
  // }

  changeName3(event) {
    this.url3 = [];
    let files = event.target.files;
    if (event.target.files[0].size / 1024 / 1024 >= 1) {
      alert("File size can't be more than 1 MB.");
      return;
    }
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.url3.push(e.target.result);
          this.imageRear = e.target.result
        }
        reader.readAsDataURL(file);
      }
    }
  }

  // changeName4(event) {
  //   var files = event.target.files[0].name;

  //   if (event.target.files[0].size / 1024 / 1024 >= 1) {
  //     alert("File size can't be more than 1 MB.");
  //     return;
  //   }

  //   let reader = new FileReader();
  //   reader.onload = (e: any) => {
  //     this.imageLeft = e.target.result;
  //   };
  //   reader.readAsDataURL(event.target.files[0]);
  //   if (
  //     files.length != 0 ||
  //     files.length != null ||
  //     files.length != undefined
  //   ) {
  //     this.leftSide = files;
  //   }
  // }

  changeName4(event) {
    this.url4 = [];
    let files = event.target.files;
    if (event.target.files[0].size / 1024 / 1024 >= 1) {
      alert("File size can't be more than 1 MB.");
      return;
    }
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.url4.push(e.target.result);
          this.imageLeft = e.target.result
        }
        reader.readAsDataURL(file);
      }
    }
  }

  // changeName5(event) {
  //   var files = event.target.files[0].name;

  //   if (event.target.files[0].size / 1024 / 1024 >= 1) {
  //     alert("File size can't be more than 1 MB.");
  //     return;
  //   }

  //   let reader = new FileReader();
  //   reader.onload = (e: any) => {
  //     this.imagePackaging = e.target.result;
  //   };
  //   reader.readAsDataURL(event.target.files[0]);
  //   if (
  //     files.length != 0 ||
  //     files.length != null ||
  //     files.length != undefined
  //   ) {
  //     this.packaging = files;
  //   }
  // }

  changeName5(event) {
    this.url5 = [];
    let files = event.target.files;
    if (event.target.files[0].size / 1024 / 1024 >= 1) {
      alert("File size can't be more than 1 MB.");
      return;
    }
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.url5.push(e.target.result);
          this.imagePackaging = e.target.result
        }
        reader.readAsDataURL(file);
      }
    }
  }

  // changeName6(event) {
  //   var files = event.target.files[0].name;

  //   if (event.target.files[0].size / 1024 / 1024 >= 1) {
  //     alert("File size can't be more than 1 MB.");
  //     return;
  //   }

  //   let reader = new FileReader();
  //   reader.onload = (e: any) => {
  //     this.imageBarcode = e.target.result;
  //   };
  //   reader.readAsDataURL(event.target.files[0]);
  //   if (
  //     files.length != 0 ||
  //     files.length != null ||
  //     files.length != undefined
  //   ) {
  //     this.barcode = files;
  //   }
  // }

  changeName6(event) {
    this.url6 = [];
    let files = event.target.files;
    if (event.target.files[0].size / 1024 / 1024 >= 1) {
      alert("File size can't be more than 1 MB.");
      return;
    }
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.url6.push(e.target.result);
          this.imageBarcode = e.target.result
        }
        reader.readAsDataURL(file);
      }
    }
  }

  submitProduct() {
    this.spinner.show();
    if (this.engineNumber && this.frameNumber) {
      let data = { serial_number: this.engineNumber, user: JSON.parse(localStorage.getItem("user")).email };
      this.productChecking.checkProduct(data).subscribe(
        result => {
          if (result["status"] == "success") {
            this.claimForm = true;
            this.materialNumber = result["result"][0]["matnr"];
            this.materialGroup = result["result"][0]["bezei"];
            this.materialDesc = result["result"][0]["arktx"];
            this.dnDate = moment(result["result"][0]["lfdat"]).format("DD-MM-YYYY");
            this.dnNumber = result["result"][0]["lief_nr"];
          } else if (result["status"] == "error")
            alert("This product is not registered. \nPlease Contact HPPI");
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          console.log(error);
        }
      );
      // .console.log("kirim yang engine number");
    } else if (this.frameNumber) {
      let data = { serial_number: this.frameNumber, user: JSON.parse(localStorage.getItem("user")).email };
      this.productChecking.checkProduct(data).subscribe(
        result => {
          if (result["status"] == "success") {
            this.claimForm = true;
            this.materialNumber = result["result"][0]["matnr"];
            this.materialGroup = result["result"][0]["bezei"];
            this.materialDesc = result["result"][0]["arktx"];
            this.dnDate = moment(result["result"][0]["lfdat"]).format("DD-MM-YYYY");
            this.dnNumber = result["result"][0]["lief_nr"];
          } else if (result["status"] == "error")
            alert("This product is not registered. \nPlease Contact HPPI");
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          console.log(error);
        }
      );
      // console.log("kirim frame number doang");
    } else if (this.engineNumber) {
      let data = { serial_number: this.engineNumber, user: JSON.parse(localStorage.getItem("user")).email };
      this.productChecking.checkProduct(data).subscribe(
        result => {
          if (result["status"] == "success") {
            this.claimForm = true;
            this.materialNumber = result["result"][0]["matnr"];
            this.materialGroup = result["result"][0]["bezei"];
            this.materialDesc = result["result"][0]["arktx"];
            this.dnDate = moment(result["result"][0]["lfdat"]).format("DD-MM-YYYY");
            this.dnNumber = result["result"][0]["lief_nr"];
          } else if (result["status"] == "error")
            alert("This product is not registered. \nPlease Contact HPPI");
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          console.log(error);
        }
      );
      // console.log("kirim engine number doang");
    } else {
      this.spinner.hide();
      alert("Please input Engine Number or Frame Number before submit.");
    }
  }

  submitClaim() {
    if (this.claimNum) {
      this.editClaim()
    } else {
      this.saveClaim()
    }
  }
  editClaim() {
    let data = {
      engine_number: this.engineNumber,
      frame_number: this.frameNumber,
      material_group: this.materialGroup,
      material_code: this.materialNumber,
      material_description: this.materialDesc,
      remarks: this.remarks,
      claim_type: this.claimType,
      dn_number: this.dnNumber,
      dn_date: moment(this.dnDate, "YYYY-MM-DD"),
      chronology_claim: this.chronologyClaim,
      img_front: this.imageFront,
      img_rear: this.imageRear,
      img_packaging: this.imagePackaging,
      img_right: this.imageRight,
      img_left: this.imageLeft,
      img_barcode: this.imageBarcode,
      user: JSON.parse(localStorage.getItem("user")).email
    }
    this.spinner.show();

    if (
      this.chronologyClaim == undefined ||
      this.imageFront == undefined ||
      this.imageRear == undefined ||
      this.imagePackaging == undefined ||
      this.imageRight == undefined ||
      this.imageLeft == undefined ||
      this.imageBarcode == undefined
    ) {
      alert("All mandatory field must be filled.");
      this.spinner.hide();
      return;
    }

    this.logisticClaimCbu.updateClaimCbu(this.claimNum, data).subscribe((result) => {
      if (result["message"] == "Claim updated.") {
        alert(result["message"])
        this.router.navigate(["claim/logistic-claim-report-cbu"]);
      }
      else alert(result["message"])
      this.spinner.hide();
    }, (error) => {
      console.error(error)
      this.spinner.hide()
    })

  }

  saveClaim() {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    let data = {
      sap_code: sap_code,
      engine_number: this.engineNumber,
      frame_number: this.frameNumber,
      material_group: this.materialGroup,
      material_code: this.materialNumber,
      material_description: this.materialDesc,
      remarks: this.remarks,
      claim_type: this.claimType,
      dn_number: this.dnNumber,
      dn_date: moment(this.dnDate, "YYYY-MM-DD"),
      chronology_claim: this.chronologyClaim,
      img_front: this.imageFront,
      img_rear: this.imageRear,
      img_packaging: this.imagePackaging,
      img_right: this.imageRight,
      img_left: this.imageLeft,
      img_barcode: this.imageBarcode,
      user: JSON.parse(localStorage.getItem("user")).email
    };
    this.spinner.show();

    if (
      this.chronologyClaim == undefined ||
      this.imageFront == undefined ||
      this.imageRear == undefined ||
      this.imagePackaging == undefined ||
      this.imageRight == undefined ||
      this.imageLeft == undefined ||
      this.imageBarcode == undefined
    ) {
      alert("All mandatory field must be filled.");
      this.spinner.hide();
      return;
    }

    this.logisticClaimCbu.saveClaimCbu(data).subscribe(
      result => {
        if (result["message"] == "Claim submitted.") {
          alert(result["message"]);
          this.router.navigate(["claim/logistic-claim-report-cbu"]);
        } else alert(result["message"]);
        this.spinner.hide();
      },
      error => {
        console.log(error);
        this.spinner.hide();
      }
    );
    // this.logisticClaimCbu.saveClaimCbu(data).subscribe(
    //   result => {
    //     console.log(result);
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );
  }
}
