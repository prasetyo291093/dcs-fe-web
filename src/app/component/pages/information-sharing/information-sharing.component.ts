import { Component, OnInit, SecurityContext } from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { InformationSharingService } from "src/app/services/information-sharing.service";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner"
declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-information-sharing",
  templateUrl: "./information-sharing.component.html",
  styleUrls: ["./information-sharing.component.css"]
})
export class InformationSharingComponent implements OnInit {
  modalRef: BsModalRef;
  infoSharingList: any = [];
  baseFile64: any;
  infoSharingCBU: any = [];
  infoSharingSparepart: any = [];
  infoSharingService: any = [];
  infoSharingGeneral: any;
  informationTypeDrop: string;
  currPage: number = 1;
  numPages: number = 0;
  thePDF: any;
  constructor(
    private modalService: BsModalService,
    private sanitized: DomSanitizer,
    private spinner: NgxSpinnerService,
    private infoSharing: InformationSharingService
  ) { }

  checkGeneral: boolean = false;
  checkCbu: boolean = false;
  checkSparepart: boolean = false;
  checkService: boolean = false;
  informationType: Boolean = false;

  generalTab: boolean = false;
  cbuTab: boolean = false;
  sparepartsTab: boolean = false;
  serviceTab: boolean = false;
  canDownloadInformationSharing: boolean = false

  ngOnInit() {
    this.generalTab = true;
    this.spinner.show()
    this.canDownloadInformationSharing = JSON.parse(localStorage.getItem("user")).permissions["Download Information Sharing"]
    this.infoSharing.getInformationSharing().subscribe(
      result => {
        if (Object.entries(result["data"]).length > 0) {
          this.infoSharingList = result["data"];

          let sortByLatest = (a, b) => {
            var dateA = new Date(b.created_at).getTime();
            var dateB = new Date(a.created_at).getTime();
            if (dateA > dateB) return 1;
            if (dateA < dateB) return -1;
            return 0;
          }
          this.infoSharingList.sort(sortByLatest)
          for (let item of this.infoSharingList) {
            item.file_size_in_kb = Math.round(item.file_size / 1024);
            item.download = false
          }
          this.infoSharingGeneral = this.infoSharingList.filter((item) => item.category === "General Information")
          this.infoSharingCBU = this.infoSharingList.filter((item) => item.category === "CBU");
          this.infoSharingSparepart = this.infoSharingList.filter(item => item.category === "Spareparts");
          this.infoSharingService = this.infoSharingList.filter(item => item.category === "Service");
        }
        this.spinner.hide()
      },
      error => {
        console.log(error)
        this.spinner.hide()
      }
    );
  }

  cleanURL(oldURL: string): SafeResourceUrl {
    return this.sanitized.bypassSecurityTrustResourceUrl(oldURL);
    // return this.sanitized.sanitize(SecurityContext.URL, oldURL)
  }

  checkedAll(event, type: string) {
    var check = event.target.checked;

    switch (type) {
      case "general":
        if (check == true) {
          this.checkGeneral = true
          for (const item of this.infoSharingGeneral)
            item.download = true
        } else {
          this.checkGeneral = false
          for (const item of this.infoSharingGeneral)
            item.download = false
        }

        break;
      case "cbu":
        if (check == true) {
          this.checkCbu = true
          for (const item of this.infoSharingCBU)
            item.download = true
        } else {
          this.checkCbu = false
          for (const item of this.infoSharingCBU)
            item.download = false
        }
        break;
      case "sparepart":
        if (check == true) {
          this.checkSparepart = true
          for (const item of this.infoSharingSparepart)
            item.download = true
        } else {
          this.checkSparepart = false
          for (const item of this.infoSharingSparepart)
            item.download = false
        }
        break;
      case "service":
        if (check == true) {
          this.checkService = true
          for (const item of this.infoSharingService)
            item.download = true
        } else {
          this.checkService = false
          for (const item of this.infoSharingService)
            item.download = false
        }
        break;
    }
  }

  hideTable() {
    this.generalTab = false;
    this.cbuTab = false;
    this.sparepartsTab = false;
    this.serviceTab = false;
  }

  category(event) {
    var cat = event.target.value;
    this.hideTable();
    $("#informationType").prop("selectedIndex", -1);
    $("#sortBy").prop("selectedIndex", 0)
    if (cat == "general information") {
      this.informationType = false;
      this.generalTab = true;
    } else if (cat == "cbu") {
      this.informationType = true;
      this.cbuTab = true;
    } else if (cat == "spareparts") {

      this.informationType = true;
      this.sparepartsTab = true;
    } else {
      this.informationType = true;
      this.serviceTab = true;
    }
  }

  infoType(event) {
    let type = event.target.value;
    this.informationTypeDrop = event.target.value
    $("#sortBy").prop("selectedIndex", 0)
    if (this.serviceTab === true) {
      switch (type) {
        case "all":
          this.infoSharingService = this.infoSharingList.filter(
            item => item.category === "Service"
          );
          break;
        case "bulletin":
          this.infoSharingService = this.infoSharingList.filter(
            item => item.category === "Service"
          );
          this.infoSharingService = this.infoSharingService.filter(
            item => item.information_type === "Bulletin"
          );
          break;

        case "brochure":
          this.infoSharingService = this.infoSharingList.filter(
            item => item.category === "Service"
          );
          this.infoSharingService = this.infoSharingService.filter(
            item => item.information_type === "Brochure"
          );
          break;

        case "Promo":
          this.infoSharingService = this.infoSharingList.filter(
            item => item.category === "Service"
          );
          this.infoSharingService = this.infoSharingService.filter(
            item => item.information_type === "Promotion"
          );
          break;
        case "others":
          this.infoSharingService = this.infoSharingList.filter(
            item => item.category === "Service"
          );
          this.infoSharingService = this.infoSharingService.filter(
            item => item.information_type === "Others"
          );
          break;
      }
    } else if (this.cbuTab === true) {
      switch (type) {
        case "all":
          this.infoSharingCBU = this.infoSharingList.filter(
            item => item.category === "CBU"
          );
          break;
        case "bulletin":
          this.infoSharingCBU = this.infoSharingList.filter(
            item => item.category === "CBU"
          );
          this.infoSharingCBU = this.infoSharingCBU.filter(
            item => item.information_type === "Bulletin"
          );
          break;

        case "brochure":
          this.infoSharingCBU = this.infoSharingList.filter(
            item => item.category === "CBU"
          );
          this.infoSharingCBU = this.infoSharingCBU.filter(
            item => item.information_type === "Brochure"
          );
          break;

        case "Promo":
          this.infoSharingCBU = this.infoSharingList.filter(
            item => item.category === "CBU"
          );
          this.infoSharingCBU = this.infoSharingCBU.filter(
            item => item.information_type === "Promotion"
          );
          break;
        case "others":
          this.infoSharingCBU = this.infoSharingList.filter(
            item => item.category === "CBU"
          );
          this.infoSharingCBU = this.infoSharingCBU.filter(
            item => item.information_type === "Others"
          );
          break;
      }
    } else if (this.sparepartsTab === true) {
      switch (type) {
        case "all":
          this.infoSharingSparepart = this.infoSharingList.filter(
            item => item.category === "Spareparts"
          );
          break;
        case "bulletin":
          this.infoSharingSparepart = this.infoSharingList.filter(
            item => item.category === "Spareparts"
          );
          this.infoSharingSparepart = this.infoSharingSparepart.filter(
            item => item.information_type === "Bulletin"
          );
          break;

        case "brochure":
          this.infoSharingSparepart = this.infoSharingList.filter(
            item => item.category === "Spareparts"
          );
          this.infoSharingSparepart = this.infoSharingSparepart.filter(
            item => item.information_type === "Brochure"
          );
          break;

        case "Promo":
          this.infoSharingSparepart = this.infoSharingList.filter(
            item => item.category === "Spareparts"
          );
          this.infoSharingSparepart = this.infoSharingSparepart.filter(
            item => item.information_type === "Promotion"
          );
          break;
        case "others":
          this.infoSharingSparepart = this.infoSharingList.filter(
            item => item.category === "Spareparts"
          );
          this.infoSharingSparepart = this.infoSharingSparepart.filter(
            item => item.information_type === "Others"
          );
          break;
      }
    }
  }

  test(viewPdf, filePDF) {
    if (!filePDF.includes("data:application/pdf;base64"))
      this.baseFile64 = "data:application/pdf;base64, " + encodeURIComponent(filePDF);
    else
      this.baseFile64 = encodeURIComponent(filePDF)

    this.modalRef = this.modalService.show(viewPdf);
  }

  downloadFile(filePDF, fileName) {
    let file = "data:application/pdf;base64, " + filePDF;
    let link = document.createElement("a")
    link.setAttribute('href', "data:application/pdf;base64, " + encodeURIComponent(filePDF))
    link.setAttribute("download", `${fileName}.pdf`)
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  }

  sortBy(event) {
    let sort = event.target.value

    let sortByLatest = (a, b) => {
      var dateA = new Date(b.created_at).getTime();
      var dateB = new Date(a.created_at).getTime();
      if (dateA > dateB) return 1;
      if (dateA < dateB) return -1;
      return 0;
    }

    let sortByOldest = (a, b) => {
      var dateA = new Date(b.created_at).getTime();
      var dateB = new Date(a.created_at).getTime();
      if (dateA < dateB) return 1;
      if (dateA > dateB) return -1;
      return 0;
    }

    let sortByAZ = (a, b) => {
      if (a.information_name < b.information_name) return -1;
      if (a.information_name > b.information_name) return 1;
      return 0;
    }

    let sortByZA = (a, b) => {
      if (a.information_name > b.information_name) return -1;
      if (a.information_name < b.information_name) return 1;
      return 0;
    }
    if (this.generalTab === true) {
      switch (sort) {
        case "late":
          this.infoSharingGeneral.sort(sortByLatest)
          break;
        case "old":
          this.infoSharingGeneral.sort(sortByOldest)
          break;
        case "atoz":
          this.infoSharingGeneral.sort(sortByAZ)
          break;
        case "ztoa":
          this.infoSharingGeneral.sort(sortByZA)
          break;
      }
    } else if (this.cbuTab === true) {
      switch (sort) {
        case "late":
          this.infoSharingCBU.sort(sortByLatest)
          break;
        case "old":
          this.infoSharingCBU.sort(sortByOldest)
          break;
        case "atoz":
          this.infoSharingCBU.sort(sortByAZ)
          break;
        case "ztoa":
          this.infoSharingCBU.sort(sortByZA)
          break;
      }
    } else if (this.sparepartsTab === true) {
      switch (sort) {
        case "late":
          this.infoSharingSparepart.sort(sortByLatest)
          break;
        case "old":
          this.infoSharingSparepart.sort(sortByOldest)
          break;
        case "atoz":
          this.infoSharingSparepart.sort(sortByAZ)
          break;
        case "ztoa":
          this.infoSharingSparepart.sort(sortByZA)
          break;
      }
    } else if (this.serviceTab === true) {
      switch (sort) {
        case "late":
          this.infoSharingService.sort(sortByLatest)
          break;
        case "old":
          this.infoSharingService.sort(sortByOldest)
          break;
        case "atoz":
          this.infoSharingService.sort(sortByAZ)
          break;
        case "ztoa":
          this.infoSharingService.sort(sortByZA)
          break;
      }
    }
  }
  checkOneDownload(event: any, index: number, type: string) {
    let check = event.target.checked

    switch (type) {
      case "general":
        if (check == true)
          this.infoSharingGeneral[index].download = true
        else
          this.infoSharingGeneral[index].download = false

        if (this.infoSharingGeneral.length == this.infoSharingGeneral.filter(item => item.download == true).length) {
          this.checkGeneral = true
        } else this.checkGeneral = false
        break;
      case "cbu":
        if (check == true)
          this.infoSharingCBU[index].download = true
        else
          this.infoSharingCBU[index].download = false

        if (this.infoSharingCBU.length == this.infoSharingCBU.filter(item => item.download == true).length) {
          this.checkCbu = true
        } else this.checkCbu = false
        break;
      case "sparepart":
        if (check == true)
          this.infoSharingSparepart[index].download = true
        else
          this.infoSharingSparepart[index].download = false

        if (this.infoSharingSparepart.length == this.infoSharingSparepart.filter(item => item.download == true).length) {
          this.checkSparepart = true
        } else this.checkSparepart = false
        break;
      case "service":
        if (check == true)
          this.infoSharingService[index].download = true
        else
          this.infoSharingService[index].download = false

        if (this.infoSharingService.length == this.infoSharingService.filter(item => item.download == true).length) {
          this.checkService = true
        } else this.checkService = false
        break;
    }
  }

  downloadReport() {
    let data = [];
    if (this.generalTab == true) {
      if (this.checkGeneral == true) {
        data = this.infoSharingGeneral
        this.onDownloadReport(data)
      } else {
        // data = this.infoSharingGeneral.map(item => item.download == true)
        for (const item of this.infoSharingGeneral) {
          if (item.download == true)
            data.push(item)
        }
        this.onDownloadReport(data)
      }
    } else if (this.cbuTab == true) {
      switch (this.informationTypeDrop) {
        case "all":
          if (this.checkCbu == true) {
            data = this.infoSharingCBU
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingCBU.map(item => item.download == true)
            for (const item of this.infoSharingCBU) {
              if (item.download == true)
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;
        case "bulletin":
          if (this.checkCbu == true) {
            data = this.infoSharingCBU.map(item => item.information_type === "Bulletin")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingCBU.map(item => item.download == true && item.information_type === "Bulletin")
            for (const item of this.infoSharingCBU) {
              if (item.download == true && item.information_type === "Bulletin")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;

        case "brochure":
          if (this.checkCbu == true) {
            data = this.infoSharingCBU.map(item => item.information_type === "Brochure")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingCBU.map(item => item.download == true && item.information_type === "Brochure")
            for (const item of this.infoSharingCBU) {
              if (item.download == true && item.information_type === "Brochure")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;

        case "Promo":
          if (this.checkCbu == true) {
            data = this.infoSharingCBU.map(item => item.information_type === "Promotion")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingCBU.map(item => item.download == true && item.information_type === "Promotion")
            for (const item of this.infoSharingCBU) {
              if (item.download == true && item.information_type === "Promotion")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;
        case "others":
          if (this.checkCbu == true) {
            data = this.infoSharingCBU.map(item => item.information_type === "Others")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingCBU.map(item => item.download == true && item.information_type === "Others")
            for (const item of this.infoSharingCBU) {
              if (item.download == true && item.information_type === "Others")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;
      }

    } else if (this.sparepartsTab == true) {
      switch (this.informationTypeDrop) {
        case "all":
          if (this.checkSparepart == true) {
            data = this.infoSharingSparepart
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingService.map(item => item.download == true)
            for (const item of this.infoSharingSparepart) {
              if (item.download == true)
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;
        case "bulletin":
          if (this.checkSparepart == true) {
            data = this.infoSharingSparepart.map(item => item.information_type === "Bulletin")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingService.map(item => item.download == true && item.information_type === "Bulletin")
            for (const item of this.infoSharingSparepart) {
              if (item.download == true && item.information_type === "Bulletin")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;

        case "brochure":
          if (this.checkSparepart == true) {
            data = this.infoSharingSparepart.map(item => item.information_type === "Brochure")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingService.map(item => item.download == true && item.information_type === "Brochure")
            for (const item of this.infoSharingSparepart) {
              if (item.download == true && item.information_type === "Brochure")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;

        case "Promo":
          if (this.checkSparepart == true) {
            data = this.infoSharingSparepart.map(item => item.information_type === "Promotion")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingService.map(item => item.download == true && item.information_type === "Promotion")
            for (const item of this.infoSharingSparepart) {
              if (item.download == true && item.information_type === "Promotion")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;
        case "others":
          if (this.checkSparepart == true) {
            data = this.infoSharingSparepart.map(item => item.information_type === "Others")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingService.map(item => item.download == true && item.information_type === "Others")
            for (const item of this.infoSharingSparepart) {
              if (item.download == true && item.information_type === "Others")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;
      }
    } else if (this.serviceTab == true) {
      switch (this.informationTypeDrop) {
        case "all":
          if (this.checkService == true) {
            data = this.infoSharingService
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingService.map(item => item.download == true)
            for (const item of this.infoSharingService) {
              if (item.download == true)
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;
        case "bulletin":
          if (this.checkService == true) {
            data = this.infoSharingService.map(item => item.information_type === "Bulletin")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingService.map(item => item.download == true && item.information_type === "Bulletin")
            for (const item of this.infoSharingService) {
              if (item.download == true && item.information_type === "Bulletin")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;

        case "brochure":
          if (this.checkService == true) {
            data = this.infoSharingService.map(item => item.information_type === "Brochure")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingService.map(item => item.download == true && item.information_type === "Brochure")
            for (const item of this.infoSharingService) {
              if (item.download == true && item.information_type === "Brochure")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;

        case "Promo":
          if (this.checkService == true) {
            data = this.infoSharingService.map(item => item.information_type === "Promotion")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingService.map(item => item.download == true && item.information_type === "Promotion")
            for (const item of this.infoSharingService) {
              if (item.download == true && item.information_type === "Promotion")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;
        case "others":
          if (this.checkService == true) {
            data = this.infoSharingService.map(item => item.information_type === "Others")
            this.onDownloadReport(data)
          } else {
            // data = this.infoSharingService.map(item => item.download == true && item.information_type === "Others")
            for (const item of this.infoSharingService) {
              if (item.download == true && item.information_type === "Others")
                data.push(item)
            }
            this.onDownloadReport(data)
          }
          break;
      }

    }
    console.log(data)
  }

  onDownloadReport(data) {

    if (!data.length || data == null) {
      alert("No data")
      return
    }
    let link = document.createElement("a")
    link.style.display = 'none';
    for (const item of data) {
      let file: string
      if (!item.file.includes("data:application/pdf;base64")) {
        file = "data:application/pdf;base64, " + encodeURIComponent(item.file)
      } else file = item.file
      link.setAttribute("download", `${item.information_name}.pdf`)
      link.setAttribute('href', file)
      document.body.appendChild(link)
      link.click()
    }
    document.body.removeChild(link)
  }
}
