import { TestBed } from '@angular/core/testing';

import { BackorderConfirmationService } from './backorder-confirmation.service';

describe('BackorderConfirmationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackorderConfirmationService = TestBed.get(BackorderConfirmationService);
    expect(service).toBeTruthy();
  });
});
