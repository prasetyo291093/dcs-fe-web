import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutstandingInvoiceSparepartsComponent } from './outstanding-invoice-spareparts.component';

describe('OutstandingInvoiceSparepartsComponent', () => {
  let component: OutstandingInvoiceSparepartsComponent;
  let fixture: ComponentFixture<OutstandingInvoiceSparepartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutstandingInvoiceSparepartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutstandingInvoiceSparepartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
