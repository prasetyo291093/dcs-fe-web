import { TestBed } from '@angular/core/testing';

import { CbuPurchaseOrderService } from './cbu-purchase-order.service';

describe('CbuPurchaseOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CbuPurchaseOrderService = TestBed.get(CbuPurchaseOrderService);
    expect(service).toBeTruthy();
  });
});
