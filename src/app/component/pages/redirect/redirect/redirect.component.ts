import { Component, OnInit } from '@angular/core';
import { CbuPurchaseOrderService } from "../../../../services/cbu-purchase-order.service";
import {Router, ActivatedRoute} from "@angular/router"

@Component({
 selector: 'app-redirect',
 templateUrl: './redirect.component.html',
 styleUrls: ['./redirect.component.css']
})
export class RedirectComponent implements OnInit {
  its_mobile:boolean=false;
 so_number: string;
 message: string = "Your transaction has been success.";

 constructor(
   private router: Router,
   private cbuOrder: CbuPurchaseOrderService,
   private activatedRoute: ActivatedRoute
 ) { }

 ngOnInit() {
   this.its_mobile = (this.activatedRoute.snapshot.queryParamMap.get('itsm')=="yes")?true:false;
   console.log(this.activatedRoute.snapshot.queryParamMap.get('so_number'))
     this.cbuOrder.getPaymentMessage(this.activatedRoute.snapshot.queryParamMap.get('so_number')).subscribe((result) =>{
       if(result['message'] != 'Success')
         this.message = "Sorry your transaction is failed. Please Contact HPPI for further information."
   });
    
 }

 backToLogin() {
   this.router.navigate(["dealer-profile"]);
 }

}