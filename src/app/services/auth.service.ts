import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  storeVirtualAccount(va:string, bank_transfer_va: string, bca_va: string, permata_va: string) {
    localStorage.setItem("va", va);
    localStorage.setItem("bank_transfer_va", bank_transfer_va);
    localStorage.setItem("bca_va", bca_va);
    localStorage.setItem("permata_va", permata_va);
  }
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  getUser() {
    var userData = localStorage.getItem("user");
    return userData;
  }
  getPaymentMethod() {
    var paymentMethod = localStorage.getItem("payment");
    return paymentMethod;
  }
  storeUser(val) {
    var data = JSON.stringify(val);
    var userData = localStorage.setItem("user", data);
  }
  storeUserGroup(val) {
    var data = JSON.stringify(val);
    var userGroup = localStorage.setItem("group", data);
  }
  storeUrgention(val) {
    var data = JSON.stringify(val);
    var paymentMethod = localStorage.setItem("urgention", data);
  }
  login(val) {
    return this.http.post(this.baseUrl + "/api/login", val);
  }
  changePass(val) {
    var id = JSON.parse(localStorage.getItem("user")).id;
    return this.http.post(this.baseUrl + "/api/change-password/" + id, val);
  }
  forgotPass(val) {
    return this.http.post(this.baseUrl + "/api/forgot-password", val);
  }
  clearUser() {
    localStorage.clear();
    sessionStorage.clear();
  }
  storeSalesHistory(valActual, valTarget) {
    // let dataActual = JSON.stringify(valActual);
    // let dataTarget = JSON.stringify(valTarget);
    let salesHistory = JSON.stringify({
      actual: valActual,
      target: valTarget
    });
    localStorage.setItem("salesHistory", salesHistory);
  }
  storeUserClassification(data) {
    localStorage.setItem("classification", JSON.stringify(data));
  }
  storeMonthlyTargetActual(monthlyCBU, monthlyPart) {
    let monthly = JSON.stringify({
      monthlyCBU: monthlyCBU,
      monthlyPart: monthlyPart
    });
    localStorage.setItem("monthlyTargetActual", monthly);
  }

  getImageLogin() {
    return this.http.get(`${this.baseUrl}/api/image-login`);
  }

  getVirtualAccount(bill_to_code) {
    var sap_code = JSON.parse(localStorage.getItem('user')).sap_code;
    return this.http.get(this.baseUrl + '/api/virtual-account/' + sap_code + '/' + bill_to_code);
  }
}
