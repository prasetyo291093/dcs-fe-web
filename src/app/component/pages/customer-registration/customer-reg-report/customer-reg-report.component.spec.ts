import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerRegReportComponent } from './customer-reg-report.component';

describe('CustomerRegReportComponent', () => {
  let component: CustomerRegReportComponent;
  let fixture: ComponentFixture<CustomerRegReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerRegReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRegReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
