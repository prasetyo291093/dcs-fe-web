import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) { }

  baseUrl = environment.baseUrl;

  list() {
    let user_id = JSON.parse(localStorage.getItem("user")).id;
    // let user_id = 101;
    return this.http.get(
      `${this.baseUrl}/api/notification/${user_id}`
    ).pipe(
      timeout(10000) //5 seconds
    );
  }

  read(data) {
    return this.http.post(`${this.baseUrl}/api/notification/read`, data);
  }
}
