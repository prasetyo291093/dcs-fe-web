import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquiryCbuComponent } from './inquiry-cbu.component';

describe('InquiryCbuComponent', () => {
  let component: InquiryCbuComponent;
  let fixture: ComponentFixture<InquiryCbuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryCbuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryCbuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
