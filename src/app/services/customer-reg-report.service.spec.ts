import { TestBed } from '@angular/core/testing';

import { CustomerRegReportService } from './customer-reg-report.service';

describe('CustomerRegReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomerRegReportService = TestBed.get(CustomerRegReportService);
    expect(service).toBeTruthy();
  });
});
