import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../../../services/auth.service";
import { DealerProfileComponent } from "../../../component/pages/dealer-profile/dealer-profile.component";
import { DealerProfileService } from "../../../services/dealer-profile.service";
import { NotificationService } from "../../../services/notification.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DomSanitizer } from '@angular/platform-browser';
declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  photoProfile: string;
  modalRef: BsModalRef;
  canUpdateDealerInformation: boolean = false
  canAddEditUser: boolean = false;
  notificationList: Notification[];
  totalNotif: number;
  user_id : number;
  userID : number;

  constructor(
    private router: Router,
    private auth: AuthService,
    private accountinfo: DealerProfileService,
    private modalService: BsModalService,
    private dealprof: DealerProfileService,
    private notification: NotificationService,
    public sanitized: DomSanitizer
  ) {this.notificationList = []}

  ngOnInit() {
    // var photoProfile = JSON.parse(localStorage.getItem("user"))
    //   .profile_picture_path;
    this.canAddEditUser = JSON.parse(localStorage.getItem("user")).permissions["Add, edit and terminate user"]
    this.canUpdateDealerInformation = JSON.parse(localStorage.getItem("user")).permissions["Update Dealer Information"]
    var user = JSON.parse(localStorage.getItem("user")).sap_code;
    this.user_id = JSON.parse(localStorage.getItem("user")).id;

    this.dealprof.getInfoUser(user).subscribe((result) => {
      this.photoProfile = "data:image/jpeg;base64, " + result["profile_picture"].toString();
    }, (error) => {
      console.log(error);
    })
    //  = photoProfile;

    this.notification.list().subscribe((notificationList: Notification[]) => {
      console.log(notificationList);
      this.notificationList = notificationList;
      this.totalNotif = notificationList.length;
    });
  }

  logOut() {
    this.auth.clearUser();
    this.router.navigate(["login"]);
  }

  notif() {
    $("#popup1").toggle();
    $("#popup2").hide();
    $('.main-notif').hide();

  }

  popover() {
    $("#popup2").toggle();
    $("#popup1").hide();
  }

  redirectMenuBo($event, $id){
    this.router.navigate(["/order-entry/back-order"]);

    let data = {
      id: $id
    };

    this.notification.read(data).subscribe(result => {});

    $event.target.classList.remove('activeBackground')
  }

  redirectMenuPending($event, $id){
    this.router.navigate(["/order-entry/pending-transaction"]);

    let data = {
      id: $id
    };

    this.notification.read(data).subscribe(result => {});

    $event.target.classList.remove('activeBackground')
  }

  redirectMenuOutstanding($event, $id){
    this.router.navigate(["/outstanding-and-overdue/report"]);

    let data = {
      id: $id
    };

    this.notification.read(data).subscribe(result => {});

    $event.target.classList.remove('activeBackground')
  }

  redirectMenuInformationSharing($event, $id, $user_id){
    this.router.navigate(["/information-sharing"]);

    let data = {
      id: $id,
      userID: $user_id,
      type: 'Information Sharing'
    };

    console.log(data);

    this.notification.read(data).subscribe(result => {});

    $event.target.classList.remove('activeBackground')
  }

  redirectMenuOrderStatus($event, $id){
    this.router.navigate(["/order-entry/order-status"]);

    let data = {
      id: $id
    };

    this.notification.read(data).subscribe(result => {});

    $event.target.classList.remove('activeBackground')
  }

  redirectMenuDeliveryInformation($event, $id){
    this.router.navigate(["/order-entry/delivery-information"]);

    let data = {
      id: $id
    };

    this.notification.read(data).subscribe(result => {});

    $event.target.classList.remove('activeBackground')
  }
  
  account_infor() {
    this.router.navigate(["account-information"]);
  }
  home() {
    this.router.navigate(["dealer-profile"]);
  }
  changePass() {
    this.router.navigate(["change-password"]);
  }
  user() {
    this.router.navigate(["user-management"]);
  }

  termsCondition() {
    this.router.navigate(["terms-condition"]);
  }

  test(template1) {
    this.modalRef = this.modalService.show(template1);
  }
}
