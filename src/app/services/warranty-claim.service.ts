import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class WarrantyClaimService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  createWarrantyClaim(body) {
    return this.http.post(`${this.baseUrl}/api/warranty-claim/input`, body);
  }
}
