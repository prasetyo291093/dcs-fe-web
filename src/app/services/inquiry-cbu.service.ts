import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class InquiryCbuService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  getListMaterial(val) {
    var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${this.baseUrl}/api/inquiry-cbu/searchMaterial/${sap_code}?search=${val}`
    );
  }

  getListInquiryCBU(data) {
    return this.http.post(`${this.baseUrl}/api/input-atp`, data);
  }
  getMaterial(val) {
    return this.http.get(
      `${this.baseUrl}/api/inquiry-cbu/search-code?search=${val}`
    );
  }
}
