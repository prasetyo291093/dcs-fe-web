import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class CustomerRegReportService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  getCustomerReport() {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${this.baseUrl}/api/customer-registration/report/${sap_code}`
    );
  }

  editCustomerReport(number) {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${this.baseUrl}/api/customer-registration/edit-report/${sap_code}/${number}`
    );
  }

  deleteCustomerReport(number) {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${this.baseUrl}/api/customer-registration/delete-report/${sap_code}/${number}`
    );
  }

}
