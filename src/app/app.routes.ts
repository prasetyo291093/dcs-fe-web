import { AccountInformationComponent } from './component/pages/account-information/account-information.component';
import { ChangePasswordComponent } from './component/pages/change-password/change-password.component';
import { DealerProfileComponent } from './component/pages/dealer-profile/dealer-profile.component';
import { ForgotPasswordComponent } from './component/pages/forgot-password/forgot-password.component';
import { LoginComponent } from './component/pages/login/login.component';
import { CbuPurchaseOrderComponent } from './component/pages/order-entry/cbu-purchase-order/cbu-purchase-order.component';
import { BackOrderComponent } from './component/pages//order-entry/back-order/back-order.component';
import { SparepartsPurchaseOrderComponent } from './component/pages/order-entry/spareparts-purchase-order/spareparts-purchase-order.component';
import { OrderStatusComponent } from './component/pages/order-entry/order-status/order-status.component';
import { PendingTransactionComponent } from './component/pages/order-entry/pending-transaction/pending-transaction.component';
import { DeliveryInformationComponent } from './component/pages/order-entry/delivery-information/delivery-information.component';
import { InquiryCbuComponent } from './component/pages/inquiry/inquiry-cbu/inquiry-cbu.component';
import { InquirySparepartsComponent } from './component/pages/inquiry/inquiry-spareparts/inquiry-spareparts.component';
import { UserManagementComponent } from './component/pages/user-management/user-management.component';
import { ProductCheckingComponent } from './component/pages/product-checking/product-checking.component';
import { SalesPromoComponent } from './component/pages/sales-promo/sales-promo.component';
import { InformationSharingComponent } from './component/pages/information-sharing/information-sharing.component';
import { ApAgingComponent } from './component/pages/outstanding-and-overdue/ap-aging/ap-aging.component';
import { InvoiceTimelinessComponent } from './component/pages/outstanding-and-overdue/invoice-timeliness/invoice-timeliness.component';
import { ReportComponent } from './component/pages/outstanding-and-overdue/report/report.component';
import { ForecastEntryComponent } from './component/pages/forecast/forecast-entry/forecast-entry.component';
import { ForecastReportComponent } from './component/pages/forecast/forecast-report/forecast-report.component';
import { DealerTargetCbuComponent } from './component/pages/dealer-target/dealer-target-cbu/dealer-target-cbu.component';
import { DealerTargetSparepartsComponent } from './component/pages/dealer-target/dealer-target-spareparts/dealer-target-spareparts.component';
import { WarrantyClaimFormComponent } from './component/pages/claim/warranty-claim-form/warranty-claim-form.component';
import { WarrantyClaimReportComponent } from './component/pages/claim/warranty-claim-report/warranty-claim-report.component';
import { LogisticClaimFormCbuComponent } from './component/pages/claim/logistic-claim-form-cbu/logistic-claim-form-cbu.component';
import { LogisticClaimReportCbuComponent } from './component/pages/claim/logistic-claim-report-cbu/logistic-claim-report-cbu.component';
import { LogisticClaimFormSparepartsComponent } from './component/pages/claim/logistic-claim-form-spareparts/logistic-claim-form-spareparts.component';
import { LogisticClaimReportSparepartsComponent } from './component/pages/claim/logistic-claim-report-spareparts/logistic-claim-report-spareparts.component';
import { CustomerRegInputComponent } from './component/pages/customer-registration/customer-reg-input/customer-reg-input.component';
import { CustomerRegReportComponent } from './component/pages/customer-registration/customer-reg-report/customer-reg-report.component';
import { CbuRewardComponent } from './component/pages/dealer-reward/cbu-reward/cbu-reward.component';
import { SparepartsRewardComponent } from './component/pages/dealer-reward/spareparts-reward/spareparts-reward.component';
import { StockInputComponent } from './component/pages/dealer-stock/stock-input/stock-input.component';
import { StockReportComponent } from './component/pages/dealer-stock/stock-report/stock-report.component';
import { ResetPasswordComponent } from './component/pages/reset-password/reset-password.component';
import { HowToPayComponent } from './component/pages/terms-condition/how-to-pay/how-to-pay.component';
import { DeliveryPolicyComponent } from './component/pages/terms-condition/delivery-policy/delivery-policy.component';
import { ReturnPolicyComponent } from './component/pages/terms-condition/return-policy/return-policy.component';
import { CustomerDataComponent } from './component/pages/terms-condition/customer-data/customer-data.component';
import { TermsConditionComponent } from './component/pages/terms-condition/terms-condition/terms-condition.component';
import { RedirectComponent } from './component/pages/redirect/redirect/redirect.component';
import { OutstandingInvoiceCbuComponent } from './component/pages/outstanding-invoice-cbu/outstanding-invoice-cbu.component';
import { OutstandingInvoiceSparepartsComponent } from './component/pages/outstanding-invoice-spareparts/outstanding-invoice-spareparts.component';
import { MaintenanceComponent } from './component/pages/maintenance/maintenance.component';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import * as $ from "jquery";


const appRoutes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', data: { title: 'Login' }, component: LoginComponent },
  { path: 'maintenance', data: { title: 'Maintenance' }, component: MaintenanceComponent },
  { path: 'reset-password', data: { title: 'Reset Password' }, component: ResetPasswordComponent },
  { path: 'dealer-profile', data: { title: 'Dealer Profile' }, component: DealerProfileComponent },
  { path: 'account-information', data: { title: 'Account Information' }, component: AccountInformationComponent },
  { path: 'user-management', data: { title: 'User Management' }, component: UserManagementComponent },
  { path: 'change-password', data: { title: 'Change Password' }, component: ChangePasswordComponent },
  { path: 'forgot-password', data: { title: 'Forgot Password' }, component: ForgotPasswordComponent },
  { path: 'forgot-password', data: { title: 'Forgot Password' }, component: ForgotPasswordComponent },
  {
    path: 'order-entry', data: { title: 'Order Entry' }, children: [
      { path: 'cbu-order-entry', data: { title: 'CBU Order Entry' }, component: CbuPurchaseOrderComponent },
      { path: 'back-order', data: { title: 'Backorder Confirmation' }, component: BackOrderComponent },
      { path: 'spareparts-order-entry', data: { title: 'Spareparts Order Entry' }, component: SparepartsPurchaseOrderComponent },
      { path: 'order-status', data: { title: 'Order Status' }, component: OrderStatusComponent },
      { path: 'pending-transaction', data: { title: 'Pending Transaction' }, component: PendingTransactionComponent },
      { path: 'delivery-information', data: { title: 'Delivery Information' }, component: DeliveryInformationComponent },
    ]
  },
  {
    path: 'inquiry', data: { title: 'Inquiry' }, children: [
      { path: 'cbu', data: { title: 'Inquiry CBU' }, component: InquiryCbuComponent },
      { path: 'spareparts', data: { title: 'Inquiry Spareparts' }, component: InquirySparepartsComponent },
    ]
  },
  {
    path: 'outstanding-and-overdue', data: { title: 'Outstanding And Overdue' }, children: [
      { path: 'ap-aging', data: { title: 'A/P Aging' }, component: ApAgingComponent },
      { path: 'invoice', data: { title: 'Invoice Timeline' }, component: InvoiceTimelinessComponent },
      { path: 'report', data: { title: 'Report' }, component: ReportComponent }
    ]
  },
  {
    path: 'forecast', data: { title: 'Forecast' }, children: [
      { path: 'forecast-entry', data: { title: 'Forecast Entry' }, component: ForecastEntryComponent },
      { path: 'forecast-report', data: { title: 'Forecast Report' }, component: ForecastReportComponent },
    ]
  },
  {
    path: 'dealer-target', data: { title: 'Dealer Target' }, children: [
      { path: 'dealer-target-cbu', data: { title: 'Dealer Target CBU' }, component: DealerTargetCbuComponent },
      { path: 'dealer-target-sparepart', data: { title: 'Dealer Target Spareparts' }, component: DealerTargetSparepartsComponent },
    ]
  },
  {
    path: 'claim', data: { title: 'Claim' }, children: [
      { path: 'warranty-claim-form', data: { title: 'Warranty Claim Form' }, component: WarrantyClaimFormComponent },
      { path: 'warranty-claim-report', data: { title: 'Warranty Claim Report' }, component: WarrantyClaimReportComponent },
      { path: 'logistic-claim-form-cbu', data: { title: 'Logistic Claim Form CBU' }, component: LogisticClaimFormCbuComponent },
      { path: 'logistic-claim-report-cbu', data: { title: 'Logistic Claim Report CBU' }, component: LogisticClaimReportCbuComponent },
      { path: 'logistic-claim-form-spareparts', data: { title: 'Logistic Claim Form Spareparts' }, component: LogisticClaimFormSparepartsComponent },
      { path: 'logistic-claim-report-spareparts', data: { title: 'Logistic Claim Report Spareparts' }, component: LogisticClaimReportSparepartsComponent }
    ]
  },
  {
    path: 'custom-registration', data: { title: 'Custom Registration' }, children: [
      { path: 'custom-registration-input', data: { title: 'Custom Registration Input' }, component: CustomerRegInputComponent },
      { path: 'custom-registration-report', data: { title: 'Custom Registration Report' }, component: CustomerRegReportComponent },
    ]
  },
  { path: 'product-checking', data: { title: 'Product Checking' }, component: ProductCheckingComponent },
  { path: 'sales-promo', data: { title: 'Sales Promo' }, component: SalesPromoComponent },
  {
    path: 'dealer-reward', data: { title: 'Dealer Reward' }, children: [
      { path: 'cbu-reward', data: { title: 'CBU Reward' }, component: CbuRewardComponent },
      { path: 'sparepart-reward', data: { title: 'Sparepart Reward' }, component: SparepartsRewardComponent },
    ]
  },
  {
    path: 'dealer-stock', data: { title: 'Dealer Stock' }, children: [
      { path: 'stock-input', data: { title: 'Stock Input' }, component: StockInputComponent },
      { path: 'stock-report', data: { title: 'Stock Report' }, component: StockReportComponent },
    ]
  },
  { path: 'information-sharing', data: { title: 'Information Sharing' }, component: InformationSharingComponent },
  {
    path: 'terms-and-condition', data: { title: 'Terms And Condition' }, children: [
      { path: 'how-to-pay', data: { title: 'How To Pay' }, component: HowToPayComponent },
      { path: 'delivery-policy', data: { title: 'Delivery Policy' }, component: DeliveryPolicyComponent },
      { path: 'return-policy', data: { title: 'Return/Refund Policy' }, component: ReturnPolicyComponent },
      { path: 'customer-data', data: { title: 'Customer Data Privacy Policy' }, component: CustomerDataComponent },
    ]
  },
  { path: 'terms-condition', data: { title: 'Terms And Condition' }, component: TermsConditionComponent },
  { path: 'redirect', data: { title: 'Terms And Condition' }, component: RedirectComponent },
  { path: 'outstanding-invoice-cbu', data: { title: 'Outstanding Invoice CBU' }, component:  OutstandingInvoiceCbuComponent },
  { path: 'outstanding-invoice-spareparts', data: { title: 'Outstanding Invoice Spareparts' }, component: OutstandingInvoiceSparepartsComponent }

];

export const routes: ModuleWithProviders = RouterModule.forRoot(appRoutes);
