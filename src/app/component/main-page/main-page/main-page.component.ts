import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  constructor(
    private router    : Router,
    private auth      : AuthService
  ) { }

  ngOnInit() {
    if(!this.auth.getUser()){
      this.router.navigate(['login']);
    }
  }

}
