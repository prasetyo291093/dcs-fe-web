import { Component, OnInit } from "@angular/core";
import { CustomerRegInputService } from "src/app/services/customer-reg-input.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ProductCheckingService } from "src/app/services/product-checking.service";
import { Router } from '@angular/router';

@Component({
  selector: "app-customer-reg-input",
  templateUrl: "./customer-reg-input.component.html",
  styleUrls: ["./customer-reg-input.component.css"]
})
export class CustomerRegInputComponent implements OnInit {
  appliedForReq: string;
  appliedForInput: string;
  engineNumber: string;
  frameNumber: string;
  materialGroup: string;
  materialNumber: string;
  materialDesc: string;
  ownerName: string;
  dateOfPurchase: string;
  phone: string;
  address: string;
  district: string;
  province: string;
  provinceList: any = [];
  subdealerName: string;
  subdealerPhone: string;
  showFormCustomerRegis: boolean = false;
  listSubDealer: any = [];
  showAppliedFor: boolean = false;
  detail: boolean = false;

  constructor(
    private customerRegInput: CustomerRegInputService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private productChecking: ProductCheckingService
  ) { }

  ngOnInit() {
    console.log(this.detail);
    this.spinner.show()
    this.customerRegInput.getListProvince().subscribe(result => {
      if (Object.entries(result).length > 0) {
        // this.province = result[0]["province"]
        for (const item in result) {
          this.provinceList.push(result[item]["name"])
        }
      }
      this.spinner.hide()
    }, error => {
      this.spinner.hide()
      console.error(error)
    })

    this.listSubDealer = []
    this.subdealerName = ""
    this.customerRegInput.getListSubDealer(localStorage.getItem("dealer_name")).subscribe(result => {
      console.log(result)
      if (Object.entries(result).length > 0) {
        // this.province = result[0]["province"]
        for (const item in result) {
          this.listSubDealer.push(result[item])
        }
      }
      this.spinner.hide()
    }, error => {
      this.spinner.hide()
      console.error(error)
    })

  }

  // getSubDealer(province) {
  //   this.listSubDealer = []
  //   this.subdealerName = ""
  //   this.customerRegInput.getListSubDealer(province).subscribe((result) => {
  //     this.listSubDealer = result
  //   }, (error) => {
  //     this.spinner.hide()
  //     console.error(error)
  //   })
  // }

  appliedFor(type) {
    // if (type == "Others") this.appliedForReq = this.appliedForInput;
    // else
    this.appliedForReq = type;
    console.log(this.appliedForReq);
  }

  submitProduct() {
    this.spinner.show();
    // this.showFormCustomerRegis = false;
    if (this.engineNumber && this.frameNumber) {
      let data = { serial_number: this.engineNumber, user: JSON.parse(localStorage.getItem("user")).email };
      this.productChecking.checkProduct(data).subscribe(
        result => {
          if (result["status"] == "success") {
            this.showFormCustomerRegis = true;
            this.materialNumber = result["result"][0]["matnr"];
            this.materialGroup = result["result"][0]["bezei"];
            if (this.materialGroup == "Engine")
              this.showAppliedFor = true
            this.materialDesc = result["result"][0]["arktx"];
          } else if (result["status"] == "error") {
            alert("This product is not registered. \nPlease Contact HPPI");
            this.showFormCustomerRegis = false;
          }
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          console.log(error);
          alert("This Material Has Already Been Registered");
          this.showFormCustomerRegis = false;
        }
      );
      // .console.log("kirim yang engine number");
    } else if (this.frameNumber) {
      let data = { serial_number: this.frameNumber, user: JSON.parse(localStorage.getItem("user")).email };
      this.productChecking.checkProduct(data).subscribe(
        result => {
          if (result["status"] == "success") {
            this.showFormCustomerRegis = true;
            this.materialNumber = result["result"][0]["matnr"];
            this.materialGroup = result["result"][0]["bezei"];
            if (this.materialGroup == "Engine")
              this.showAppliedFor = true
            this.materialDesc = result["result"][0]["arktx"];
          } else if (result["status"] == "error") {
            alert("This product is not registered. \nPlease Contact HPPI");
            this.showFormCustomerRegis = false;
          }
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          console.log(error);
          alert("This Material Has Already Been Registered");
          this.showFormCustomerRegis = false;
        }
      );
      // console.log("kirim frame number doang");
    } else if (this.engineNumber) {
      let data = { serial_number: this.engineNumber, user: JSON.parse(localStorage.getItem("user")).email };
      this.productChecking.checkProduct(data).subscribe(
        result => {
          if (result["status"] == "success") {
            this.showFormCustomerRegis = true;
            this.materialNumber = result["result"][0]["matnr"];
            this.materialGroup = result["result"][0]["bezei"];
            if (this.materialGroup == "Engine")
              this.showAppliedFor = true
            this.materialDesc = result["result"][0]["arktx"];
          } else if (result["status"] == "error") {
            alert("This product is not registered. \nPlease Contact HPPI");
            this.showFormCustomerRegis = false;
          }
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          console.log(error);
          alert("This Material Has Already Been Registered");
          this.showFormCustomerRegis = false;
        }
      );
      // console.log("kirim engine number doang");
    } else {
      this.spinner.hide();
      alert("Please input Engine Number or Frame Number before submit.");
    }
  }

  saveCustomer() {
    let data = {
      engine_number: this.engineNumber,
      frame_number: this.frameNumber,
      material_group: this.materialGroup,
      material_code: this.materialNumber.slice(-6),
      material_description: this.materialDesc,
      applied_for: "",
      owner_name: this.ownerName,
      date_of_purchase: this.dateOfPurchase,
      phone: this.phone,
      address: this.address,
      district: this.district,
      province: this.province,
      sub_dealer_name: this.subdealerName,
      sub_dealer_phone: this.subdealerPhone,
      user: JSON.parse(localStorage.getItem("user")).email,
      first_name: JSON.parse(localStorage.getItem("user")).first_name
    };

    console.log(data)

    if (this.appliedForReq == "Others") data.applied_for = this.appliedForInput;
    else data.applied_for = this.appliedForReq;

    if (
      !this.ownerName ||
      !this.dateOfPurchase ||
      // !this.phone ||
      !this.address ||
      !this.district ||
      !this.province ||
      !this.subdealerName
      // !this.subdealerPhone
    ) {
      alert("All mandatory must be filled");
      return
    }
    this.spinner.show()
    console.log(data)
    this.customerRegInput.regisCustomer(data).subscribe(
      result => {
        if (result["message"] == "Success") {
          alert(result["message"]);
          this.router.navigate(["custom-registration/custom-registration-report"]);
        }
        else alert(result["message"]);
        this.spinner.hide()
      },
      error => {
        console.error(error)
        this.spinner.hide()
      }
    );
  }
}
