import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class ForecastReportService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  getListForecastReport(data) {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.post(
      `${this.baseUrl}/api/dealer-forecast/report/${sap_code}`,
      data
    );
  }
}
