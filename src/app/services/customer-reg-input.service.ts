import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class CustomerRegInputService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  regisCustomer(data) {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.post(
      `${this.baseUrl}/api/customer-registration/${sap_code}`,
      data
    );
  }

  updateCustomer(data) {
    return this.http.post(
      `${this.baseUrl}/api/customer-registration/update/update-report`,
      data
    );
  }

  getListProvince() {
    // let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(`${this.baseUrl}/api/customer-registration/province`)
  }

  getListSubDealer(dealer_name) {
    return this.http.get(`${this.baseUrl}/api/customer-registration/sub-dealer/${dealer_name}`)
  }
}
