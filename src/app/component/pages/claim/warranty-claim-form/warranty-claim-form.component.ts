import { Component, OnInit, OnDestroy, OnChanges, ɵConsole } from '@angular/core';
import { WarrantyClaimFormService } from 'src/app/services/warranty-claim-form.service';
import { ProductCheckingService } from 'src/app/services/product-checking.service';
import { Subscription, interval } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { MaterialSearchItem } from 'src/app/interfaces/MaterialSearchItem';
import * as moment from 'moment';

declare var jquery: any;
declare var $: any;
declare var number: any;

@Component({
  selector: 'app-warranty-claim-form',
  templateUrl: './warranty-claim-form.component.html',
  styleUrls: ['./warranty-claim-form.component.css']
})

export class WarrantyClaimFormComponent implements OnInit, OnDestroy {
  claimForm: boolean = false;
  validation_date: any;

  constructor(
    private warrantyClaimForm: WarrantyClaimFormService,
    private spinner: NgxSpinnerService,
    private productChecking: ProductCheckingService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  private subscription: Subscription = new Subscription();

  filename = 'No File Choosen';
  Rcl = false;
  Rdw = false;
  tabOnDealer: boolean;

  materialGroup: string;
  materialNumber: string;
  materialDesc: string;

  engineNumber: string;
  frameNumber: string;
  customerName: string;
  customerAddress: string;
  purchaseDate: Date;
  occuranceDate: Date;
  reportDate: Date;
  qicReportNumber: string;
  operationHour: string;
  failureDesc: string;
  transportCost: any;
  actualHours: number;
  totalStaff: number;
  allowanceRate = 0;
  totalAllowance: number;
  totalAllowanceOnDealer: number;
  totalAllowanceAll: number;

  actualHoursRemoveOBEBoat: number;
  staffRemoveOBEBoat: number;
  tripRemoveOBEBoat: number;
  // allowanceRateRemoveOBEBoat: number;
  totalAllowanceRateRemoveOBEBoat: number;

  actualHoursInstallOBEBoat: number;
  staffInstallOBEBoat: number;
  tripInstallOBEBoat: number;
  // allowanceRateInstallOBEBoat: number;
  totalAllowanceRateInstallOBEBoat: number;

  actualHoursTransportOBEBoat: number;
  staffTransportOBEBoat: number;
  tripTransportOBEBoat: number;
  // allowanceRateTransportOBEBoat: number;
  totalAllowanceRateTransportOBEBoat: number;

  // repairmentItem: RepairmentAdditional[];
  partsNo: any = [];
  adtParts: any = [];
  qtyRepairment: any = [];
  unitsPriceRepairment: any = [];
  totalPriceRepairment: any = [];
  frtRepairment: any = [];
  totalFrt: number;
  totalPartCostRepairment: number;
  totalDpaRepairment: number;
  DPAPercentage = 0;

  expenseOtherSpareparts: any = [];
  qtyOtherSpareparts: any = [];
  unitsPriceOtherSpareparts: any = [];
  totalPriceOtherSpareparts: any = [];
  totalOtherExpenses: number;
  serviceRate = 0;
  totalServiceCost: number;

  repairmentItem: any = [1];
  otherBillItem: any = [1];

  totalClaimCost: number;

  urls = new Array<string>();
  files: any = [];
  allowNewImage = true;
  maxImageUpload = 3;

  searchItemsByCode: MaterialSearchItem[];
  searchItemsByDesc: MaterialSearchItem[];
  searchItems: MaterialSearchItem[];
  searchItem: MaterialSearchItem;

  myValue: string;
  myValue2: string;
  myValue3: string;

  ngOnInit() {
    this.Rcl = true;

    // hardcode. ntar dihapus
    this.purchaseDate = this.occuranceDate;
    this.reportDate = this.occuranceDate;

    this.getAllRate();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAllRate() {
    this.subscription.add(this.warrantyClaimForm.getWarrantyClaimRate().subscribe(
      result => {
        for (const key in result) {
          if (result.hasOwnProperty(key)) {
            if (result[key]['category'] === 'Service Rate') {
              this.serviceRate = result[key]['rate'];
            }

            if (result[key]['category'] === 'DPA Percentage') {
              this.DPAPercentage = result[key]['rate'] / 100;
            }

            if (result[key]['category'] === 'Allowance Rate') {
              this.allowanceRate = result[key]['rate'];
            }
          }
        }

        console.log(this.DPAPercentage, this.serviceRate, this.allowanceRate);
      }, error => {
        console.log(error);
      }
    ));
  }

  detectFiles(event: any) {
    const files = event.target.files;

    if (this.urls.length > this.maxImageUpload - 1) {
      alert(`Maximum image upload is ${this.maxImageUpload.toString()} images`);
      this.allowNewImage = false;
      return;
    }

    if (files.length <= this.maxImageUpload) {
      for (const file of files) {
        if (file.size / 1024 / 1024 >= 1) {
          alert('Each file size cannot be more than 1 MB.');
          return;
        }

        const reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        };
        reader.readAsDataURL(file);
      }
    } else {
      this.allowNewImage = false;
      alert(`Maximum image upload is ${this.maxImageUpload.toString()} images`);
    }
  }

  deleteImgLabel(index: number) {
    this.urls.splice(index, 1);
    this.files.splice(index, 1);
    if (this.urls.length <= this.maxImageUpload - 1) {
      this.allowNewImage = true;
    }
  }

  changeName(event: any) {
    const files = event.target.files[0].name;
    if (files.length !== 0 || files.length != null || files.length !== undefined) {
      this.filename = files;
    }
  }

  plusRepairment(i: number) {
    if (isNaN(this.qtyRepairment[i])) {
      this.qtyRepairment[i] = 0;
    }

    this.qtyRepairment[i] += 1;
  }

  minusRepairment(i: number) {
    if (isNaN(this.qtyRepairment[i])) {
      this.qtyRepairment[i] = 0;
    }

    if (this.qtyRepairment[i] !== 1) {
      this.qtyRepairment[i] -= 1;
    }
  }

  plusOther(i: number) {
    if (isNaN(this.qtyOtherSpareparts[i])) {
      this.qtyOtherSpareparts[i] = 0;
    }

    this.qtyOtherSpareparts[i] += 1;
  }

  minusOther(i: number) {
    if (isNaN(this.qtyOtherSpareparts[i])) {
      this.qtyOtherSpareparts[i] = 0;
    }

    if (this.qtyOtherSpareparts[i] !== 1) {
      this.qtyOtherSpareparts[i] -= 1;
    }
  }

  Repairment(val: any) {
    if (val === 2) {
      this.Rdw = true;
      this.Rcl = false;
      this.totalAllowance = 0;
      this.totalAllowanceOnDealer = 0;
      this.transportCost = 0
      this.totalAllowanceAll = this.totalAllowanceOnDealer;
      this.tabOnDealer = true;
    } else {
      this.Rdw = false;
      this.Rcl = true;
      this.totalAllowance = 0;
      this.totalAllowanceOnDealer = 0;
      this.transportCost = 0
      this.totalAllowanceAll = this.totalAllowance;
      this.tabOnDealer = false;
    }
    console.log(val);
  }

  addRepairmentList() {
    this.repairmentItem.push(1);
  }

  removeRepairmentList(i: number) {
    this.repairmentItem.splice(i, 1);
  }

  addOtherBillList() {
    this.otherBillItem.push(1);
  }

  removeOtherBillList(i: number) {
    this.otherBillItem.splice(i, 1);
  }

  calculateTotalFrt() {
    this.totalFrt = 0;
    for (let j = 0; j <= this.frtRepairment.length; j++) {
      if (!isNaN(this.frtRepairment[j])) {
        this.totalFrt += this.frtRepairment[j];
      }
    }
  }

  calculateTotalPriceRepairement(i: number) {
    this.totalPriceRepairment[i] = this.unitsPriceRepairment[i] * this.qtyRepairment[i];
  }

  // processMyValue(i): void {
  //   var numberVal = this.unitsPriceRepairment[i];
  //   var x = numberVal.replace(/[^0-9\.]/g, '');

  //   if (x != "") {
  //     var valArr = x.split('.');
  //     valArr[0] = (parseInt(valArr[0], 10)).toLocaleString();
  //     x = valArr.join('.');
  //   }
  //   this.unitsPriceRepairment[i] = x;
  // }

  // processMyValue2(i): void {
  //   var numberVal2 = this.unitsPriceOtherSpareparts[i];
  //   var x2 = numberVal2.replace(/[^0-9\.]/g, '');

  //   if (x2 != "") {
  //     var valArr2 = x2.split('.');
  //     valArr2[0] = (parseInt(valArr2[0], 10)).toLocaleString();
  //     x2 = valArr2.join('.');
  //   }

  //   this.unitsPriceOtherSpareparts[i] = x2;
  // }

  // processMyValue3(): void {
  //   var numberVal3 = this.transportCost;
  //   var x3 = numberVal3.replace(/[^0-9\.]/g, '');

  //   if (x3 != "") {
  //     var valArr3 = x3.split('.');
  //     valArr3[0] = (parseInt(valArr3[0], 10)).toLocaleString();
  //     x3 = valArr3.join('.');
  //   }

  //   this.transportCost = x3;
  // }

  calculateTotalPriceOther(i: number) {
    this.totalPriceOtherSpareparts[i] = this.unitsPriceOtherSpareparts[i] * this.qtyOtherSpareparts[i];
  }

  calculateTotalPartCostRepairment() {
    this.totalPartCostRepairment = 0;
    for (let j = 0; j <= this.totalPriceRepairment.length; j++) {
      if (!isNaN(this.totalPriceRepairment[j])) {
        this.totalPartCostRepairment += this.totalPriceRepairment[j];
      }
    }

    this.totalDpaRepairment = this.DPAPercentage * this.totalPartCostRepairment;
  }

  calculateTotalOtherExpenses() {
    this.totalOtherExpenses = 0;
    for (let j = 0; j <= this.totalPriceOtherSpareparts.length; j++) {
      if (!isNaN(this.totalPriceOtherSpareparts[j])) {
        this.totalOtherExpenses += this.totalPriceOtherSpareparts[j];
      }
    }

    this.totalServiceCost = this.serviceRate * this.totalFrt;
  }

  selectChangeHandler(e) {
    switch (e.target.name) {
      case 'actualHours':
        this.actualHours = e.target.value;
        break;
      case 'staff':
        this.totalStaff = e.target.value;
        break;
      case 'actualHoursRemoveOBEBoat':
        this.actualHoursRemoveOBEBoat = e.target.value;
        break;
      case 'staffRemoveOBEBoat':
        this.staffRemoveOBEBoat = e.target.value;
        break;
      case 'actualHoursInstallOBEBoat':
        this.actualHoursInstallOBEBoat = e.target.value;
        break;
      case 'staffInstallOBEBoat':
        this.staffInstallOBEBoat = e.target.value;
        break;
      case 'actualHoursTransportOBEBoat':
        this.actualHoursTransportOBEBoat = e.target.value;
        break;
      case 'staffTransportOBEBoat':
        this.staffTransportOBEBoat = e.target.value;
        break;
      default:
        break;
    }

    this.totalAllowance = this.actualHours * this.allowanceRate * this.totalStaff;

    this.tripRemoveOBEBoat = 0;
    // this.allowanceRateRemoveOBEBoat = 0;
    const totalAllowanceRateRemoveOBEBoat = this.actualHoursRemoveOBEBoat * this.staffRemoveOBEBoat * this.allowanceRate;
    if (!isNaN(totalAllowanceRateRemoveOBEBoat)) {
      this.totalAllowanceRateRemoveOBEBoat = totalAllowanceRateRemoveOBEBoat;
    }

    this.tripInstallOBEBoat = 0;
    // this.allowanceRateInstallOBEBoat = 0;
    const totalAllowanceRateInstallOBEBoat = this.actualHoursInstallOBEBoat * this.staffInstallOBEBoat * this.allowanceRate;
    if (!isNaN(totalAllowanceRateInstallOBEBoat)) {
      this.totalAllowanceRateInstallOBEBoat = totalAllowanceRateInstallOBEBoat;
    }

    this.tripTransportOBEBoat = 0;
    // this.allowanceRateTransportOBEBoat = 0;
    const totalAllowanceRateTransportOBEBoat = this.actualHoursTransportOBEBoat * this.staffTransportOBEBoat * this.allowanceRate;
    if (!isNaN(totalAllowanceRateTransportOBEBoat)) {
      this.totalAllowanceRateTransportOBEBoat = totalAllowanceRateTransportOBEBoat;
      this.transportCost = totalAllowanceRateTransportOBEBoat;
    }

    const totalAllowanceOnDealer = totalAllowanceRateRemoveOBEBoat + totalAllowanceRateInstallOBEBoat + totalAllowanceRateTransportOBEBoat;
    if (!isNaN(totalAllowanceOnDealer)) {
      this.totalAllowanceOnDealer = totalAllowanceOnDealer;
    }

    if (this.tabOnDealer) {
      this.totalAllowanceAll = this.totalAllowanceOnDealer;
    } else {
      this.totalAllowanceAll = this.totalAllowance;
    }

    // tslint:disable-next-line: max-line-length
    this.totalClaimCost = this.totalPartCostRepairment + this.totalDpaRepairment + this.totalOtherExpenses + this.totalServiceCost + this.totalAllowanceAll;
  }

  submitProduct() {
    this.spinner.show();
    let data: any;

    if (this.engineNumber && this.frameNumber) {
      data = { serial_number: this.engineNumber, user: JSON.parse(localStorage.getItem("user")).email };
    } else if (this.frameNumber) {
      data = { serial_number: this.frameNumber, user: JSON.parse(localStorage.getItem("user")).email };
    } else if (this.engineNumber) {
      data = { serial_number: this.engineNumber, user: JSON.parse(localStorage.getItem("user")).email };
    } else {
      this.spinner.hide();
      alert('Please input Engine Number or Frame Number before submit.');
    }

    this.subscription.add(this.productChecking.checkProduct(data).subscribe(
      result => {
        if (result['status'] === 'success') {
          this.claimForm = true;
          this.materialNumber = result['result'][0]['matnr'];
          this.materialGroup = result['result'][0]['bezei'];
          this.materialDesc = result['result'][0]['arktx'];
          this.frameNumber = result['result'][0]['frame'];

          let valid_date = new Date(result['result'][0]['lfdat'])
          valid_date.setDate(valid_date.getDate() + 180)
          this.validation_date = valid_date

          this.getCustomer(result['result'][0]['matnr']);
        } else if (result['status'] === 'error') {
          alert('This product is not registered. \nPlease Contact HPPI');
        }
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        console.log(error);
      }
    ));
  }

  getCustomer(materialNumber: string) {
    if (materialNumber !== '') {
      this.subscription.add(this.warrantyClaimForm.getWarrantyClaimCustomer(materialNumber).subscribe(
        result => {
          this.customerName = result['owner_name'] !== '' ? result['owner_name'] : '-';
          this.customerAddress = result['address'] !== '' ? result['address'] : '-';
          this.spinner.hide();
        },
        error => {
          console.log(error);
          this.spinner.hide();
        }
      ));
    }
  }

  resetForm() {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    const currentUrl = this.router.url + '?';
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }

  searchPartByDesc(i: number) {
    if (this.adtParts[i] === 'undefined' || this.adtParts[i] === '') {
      return;
    }

    this.subscription.add(this.warrantyClaimForm.getMaterialsByDesc(this.adtParts[i]).subscribe(
      result => {
        const searchItems: MaterialSearchItem[] = Object.assign([], this.searchItems);

        for (const key in result) {
          if (result.hasOwnProperty(key)) {
            if (result[key]['material_number'] !== 'undefined') {
              const searchItem: MaterialSearchItem = Object.assign({}, this.searchItem);

              searchItem.material_number = result[key]['material_number'];
              searchItem.material_description = result[key]['material_description'];
              searchItems.push(searchItem);
            }
          }
        }

        this.searchItemsByDesc = Object.assign([], searchItems);
      }, (error: Error) => {
        console.log(error);
      }
    ));
  }

  searchPartByCode(i: number) {
    if (this.partsNo[i] !== '') {
      this.subscription.add(this.warrantyClaimForm.getMaterialsByCode(this.partsNo[i]).subscribe(
        result => {
          const searchItems: MaterialSearchItem[] = Object.assign([], this.searchItems);

          for (const key in result) {
            if (result.hasOwnProperty(key)) {
              if (result[key]['material_number'] !== 'undefined') {
                const searchItem: MaterialSearchItem = Object.assign({}, this.searchItem);

                searchItem.material_number = result[key]['material_number'];
                searchItem.material_description = result[key]['material_description'];
                searchItems.push(searchItem);
              }
            }
          }

          this.searchItemsByCode = Object.assign([], searchItems);
        }, (error: Error) => {
          console.log(error);
        }
      ));
    }
  }

  selectChangeSearchHandler(e: any, i: number) {
    if (e.target.value !== 'undefined') {
      switch (e.target.name) {
        case 'partsNo':
          this.adtParts[i] = this.searchItemsByCode.find(item => item.material_number === e.target.value).material_description;
          break;
        case 'adtParts':
          this.partsNo[i] = this.searchItemsByDesc.find(item => item.material_description === e.target.value).material_number;
          break;
        default:
          break;
      }
    }
  }

  saveClaim() {
    const sap_code = JSON.parse(localStorage.getItem('user')).sap_code;

    let date = new Date(this.occuranceDate)

    if (isNaN(date.getTime()) || this.operationHour == "" || this.failureDesc == "") {
      alert("All mandatory need to be filled!");
      return;
    }

    const data = {
      sap_code: '',
      engine_number: '',
      frame_number: '',
      occurance_date: this.occuranceDate,
      qic_report_number: '',
      opertion_hour: this.operationHour,
      description: this.failureDesc,
      transport_cost: 0,
      actual_hour: 0,
      qty_staff: 0,
      allowance_rate: 2,

      actual_hour_remove_obe: 0,
      qty_staff_remove_obe: 0,
      trip_remove_obe: 0,
      allowance_rate_remove_obe: 0,
      allowance_subtotal_remove_obe: 0,

      actual_hour_install_obe: 0,
      qty_staff_install_obe: 0,
      trip_install_obe: 0,
      allowance_rate_install_obe: 0,
      allowance_subtotal_install_obe: 0,

      actual_hour_transport_obe: 0,
      qty_staff_transport_obe: 0,
      trip_transport_obe: 0,
      allowance_rate_transport_obe: 0,
      allowance_subtotal_transport_obe: 0,

      repairment_description: '',
      part_number: 0,
      qty_repairment: 0,
      unit_price_repairment: 0,
      total_price_rapairment: 0,
      frt: 0,

      failure_description: '',
      total_frt: 0,
      total_other_purchase: 0,

      expense_other_than_spareparts: '',
      qty_other_bill: 0,
      unit_price_other_bill: 0,
      total_price_other_bill: 0,

      material_group: '',
      material_code: '',
      material_description: '',

      customer_name: '',
      customer_address: '',
      purchase_date: new Date(),
      report_date: new Date(),

      total_claim_cost: 0,
      total_allowance: 0,
      total_allowance_on_dealer: 0,
      total_dpa: 0,
      total_hour_on_customer: '',
      total_hour_on_dealer: '',
      total_other_expense: 0,
      total_part_cost: 0,
      total_service_cost: 0,

      dealer_name: localStorage.getItem("dealer_name"),
      validation_date: this.validation_date,
      user: JSON.parse(localStorage.getItem("user")).email
    };


    data.sap_code = sap_code ? sap_code : '';
    data.engine_number = this.engineNumber ? this.engineNumber : '';

    data.frame_number = this.frameNumber ? this.frameNumber : '';
    // data.occurance_date = this.occuranceDate ? this.occuranceDate : new Date();
    data.qic_report_number = this.qicReportNumber ? this.qicReportNumber : '';
    // data.opertion_hour = this.operationHour ? this.operationHour : '';
    // data.description = this.failureDesc ? this.failureDesc : '';
    data.transport_cost = this.transportCost ? this.transportCost : 0;
    data.actual_hour = this.actualHours ? this.actualHours : 0;
    data.qty_staff = this.totalStaff ? this.totalStaff : 0;
    data.allowance_rate = this.allowanceRate ? this.allowanceRate : 0;

    data.actual_hour_remove_obe = this.actualHoursRemoveOBEBoat ? this.actualHoursRemoveOBEBoat : 0;
    data.qty_staff_remove_obe = this.staffRemoveOBEBoat ? this.staffRemoveOBEBoat : 0;
    data.trip_remove_obe = this.tripRemoveOBEBoat ? this.tripRemoveOBEBoat : 0;
    data.allowance_rate_remove_obe = this.allowanceRate ? this.allowanceRate : 0;
    data.allowance_subtotal_remove_obe = this.totalAllowanceRateRemoveOBEBoat ? this.totalAllowanceRateRemoveOBEBoat : 0;

    data.actual_hour_install_obe = this.actualHoursInstallOBEBoat ? this.actualHoursInstallOBEBoat : 0;
    data.qty_staff_install_obe = this.staffInstallOBEBoat ? this.staffInstallOBEBoat : 0;
    data.trip_install_obe = this.tripInstallOBEBoat ? this.tripInstallOBEBoat : 0;
    data.allowance_rate_install_obe = this.allowanceRate ? this.allowanceRate : 0;
    data.allowance_subtotal_install_obe = this.totalAllowanceRateInstallOBEBoat ? this.totalAllowanceRateInstallOBEBoat : 0;

    data.actual_hour_transport_obe = this.actualHoursTransportOBEBoat ? this.actualHoursTransportOBEBoat : 0;
    data.qty_staff_transport_obe = this.staffTransportOBEBoat ? this.staffTransportOBEBoat : 0;
    data.trip_transport_obe = this.tripTransportOBEBoat ? this.tripTransportOBEBoat : 0;
    data.allowance_rate_transport_obe = this.allowanceRate ? this.allowanceRate : 0;
    data.allowance_subtotal_transport_obe = this.totalAllowanceRateTransportOBEBoat ? this.totalAllowanceRateTransportOBEBoat : 0;

    data.repairment_description = this.adtParts ? this.adtParts : '';
    data.part_number = this.partsNo ? this.partsNo : '';
    data.qty_repairment = this.qtyRepairment ? this.qtyRepairment : 0;
    data.unit_price_repairment = this.unitsPriceRepairment ? this.unitsPriceRepairment : 0;
    data.total_price_rapairment = this.totalPriceRepairment ? this.totalPriceRepairment : 0;
    data.frt = this.frtRepairment ? this.frtRepairment : 0;

    data.expense_other_than_spareparts = this.expenseOtherSpareparts ? this.expenseOtherSpareparts : '';
    data.qty_other_bill = this.qtyOtherSpareparts ? this.qtyOtherSpareparts : 0;
    data.unit_price_other_bill = this.unitsPriceOtherSpareparts ? this.unitsPriceOtherSpareparts : 0;
    data.total_price_other_bill = this.totalPriceOtherSpareparts ? this.totalPriceOtherSpareparts : 0;

    data.material_group = this.materialGroup ? this.materialGroup : '';
    data.material_code = this.materialNumber ? this.materialNumber : '';
    data.material_description = this.materialDesc ? this.materialDesc : '';

    data.customer_name = this.customerName ? this.customerName : '';
    data.customer_address = this.customerAddress ? this.customerAddress : '';
    data.purchase_date = this.purchaseDate ? this.purchaseDate : new Date();
    data.report_date = this.reportDate ? this.reportDate : new Date();

    data.total_claim_cost = this.totalClaimCost ? this.totalClaimCost : 0;
    data.total_allowance = this.totalAllowance ? this.totalAllowance : 0;
    data.total_allowance_on_dealer = this.totalAllowanceOnDealer ? this.totalAllowanceOnDealer : 0;
    data.total_dpa = this.totalDpaRepairment ? this.totalDpaRepairment : 0;
    data.total_hour_on_customer = this.operationHour ? this.operationHour : '';
    data.total_hour_on_dealer = this.operationHour ? this.operationHour : '';
    data.total_other_expense = this.totalOtherExpenses ? this.totalOtherExpenses : 0;
    data.total_part_cost = this.totalPartCostRepairment ? this.totalPartCostRepairment : 0;
    data.total_service_cost = this.totalServiceCost ? this.totalServiceCost : 0;

    data.failure_description = this.failureDesc ? this.failureDesc : '';
    data.total_frt = this.totalFrt ? this.totalFrt : 0;
    data.total_other_purchase = this.totalOtherExpenses ? this.totalOtherExpenses : 0;



    // const data = {
    //   sap_code: sap_code,
    //   engine_number: this.engineNumber,
    //   frame_number: this.frameNumber,
    //   occurance_date: this.occuranceDate,
    //   qic_report_number: this.qicReportNumber,
    //   opertion_hour: this.operationHour,
    //   description: this.failureDesc,
    //   transport_cost: this.transportCost,
    //   actual_hour: this.actualHours,
    //   qty_staff: this.totalStaff,
    //   allowance_rate: this.allowanceRate,

    //   actual_hour_remove_obe: this.actualHoursRemoveOBEBoat,
    //   qty_staff_remove_obe: this.staffRemoveOBEBoat,
    //   trip_remove_obe: this.tripRemoveOBEBoat,
    //   // allowance_rate_remove_obe: this.allowanceRateRemoveOBEBoat,
    //   allowance_rate_remove_obe: this.allowanceRate,
    //   allowance_subtotal_remove_obe: this.totalAllowanceRateRemoveOBEBoat,

    //   actual_hour_install_obe: this.actualHoursInstallOBEBoat,
    //   qty_staff_install_obe: this.staffInstallOBEBoat,
    //   trip_install_obe: this.tripInstallOBEBoat,
    //   // allowance_rate_install_obe: this.allowanceRateInstallOBEBoat,
    //   allowance_rate_install_obe: this.allowanceRate,
    //   allowance_subtotal_install_obe: this.totalAllowanceRateInstallOBEBoat,

    //   actual_hour_transport_obe: this.actualHoursTransportOBEBoat,
    //   qty_staff_transport_obe: this.staffTransportOBEBoat,
    //   trip_transport_obe: this.tripTransportOBEBoat,
    //   // allowance_rate_transport_obe: this.allowanceRateTransportOBEBoat,
    //   allowance_rate_transport_obe: this.allowanceRate,
    //   allowance_subtotal_transport_obe: this.totalAllowanceRateTransportOBEBoat,

    //   repairment_description: this.adtParts,
    //   part_number: this.partsNo,
    //   qty_repairment: this.qtyRepairment,
    //   unit_price_repairment: this.unitsPriceRepairment,
    //   total_price_rapairment: this.totalPriceRepairment,
    //   frt: this.frtRepairment,

    //   expense_other_than_spareparts: this.expenseOtherSpareparts,
    //   qty_other_bill: this.qtyOtherSpareparts,
    //   unit_price_other_bill: this.unitsPriceOtherSpareparts,
    //   total_price_other_bill: this.totalPriceOtherSpareparts,

    //   material_group: this.materialGroup,
    //   material_code: this.materialNumber,
    //   material_description: this.materialDesc,

    //   customer_name: this.customerName,
    //   customer_address: this.customerAddress,
    //   purchase_date: this.purchaseDate,
    //   report_date: this.reportDate,
    // };

    this.subscription.add(this.warrantyClaimForm.saveWarrantyClaimInput(data).subscribe(
      result => {
        console.log('Data submitted successfully');
        this.router.navigate(['/claim/warranty-claim-report'], { relativeTo: this.activatedRoute });
      }, (error: Error) => {
        console.log(error.stack);
        console.log(error.message);
      }
    ));
  }

}
