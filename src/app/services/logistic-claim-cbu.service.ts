import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class LogisticClaimCbuService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  saveClaimCbu(body) {
    return this.http.post(`${this.baseUrl}/api/logistic-claim/cbu/input`, body);
  }

  getClaimCbu(claimNumber) {
    return this.http.get(`${this.baseUrl}/api/logistic-claim/cbu/edit/${claimNumber}`);
  }

  updateClaimCbu(claimNumber, body) {
    return this.http.post(`${this.baseUrl}/api/logistic-claim/cbu/edit/${claimNumber}`, body);
  }
}
