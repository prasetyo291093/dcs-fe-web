import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { InvoiceTimelinessService } from "src/app/services/invoice-timeliness.service";

@Component({
  selector: "app-invoice-timeliness",
  templateUrl: "./invoice-timeliness.component.html",
  styleUrls: ["./invoice-timeliness.component.css"]
})
export class InvoiceTimelinessComponent implements OnInit {
  invoice1: boolean = false;
  invoice2: boolean = false;
  invoice: any = [];
  currentYear: number;
  nextYear: number;
  invoiceDetail: any = [];

  constructor(
    private spinner: NgxSpinnerService,
    private invoiceTimeline: InvoiceTimelinessService
  ) {}

  ngOnInit() {
    this.invoice1 = true;

    this.currentYear = new Date().getFullYear();
    this.nextYear = new Date().getFullYear() + 1;
    this.invoice.length = 0;
    this.spinner.show()
    this.invoiceTimeline.getInvoiceTimeline().subscribe(
      result => {
        if (Object.entries(result).length !== 0) this.invoice = result[0];
        console.log(typeof this.invoice)
        this.spinner.hide()
      },
      error => {
        console.error(error);
        this.spinner.hide()
      }
    );
  }

  average() {
    let total = 0;
    if (Object.entries(this.invoice).length > 0) {
      total += this.invoice.april;
      total += this.invoice.may;
      total += this.invoice.june;
      total += this.invoice.july;
      total += this.invoice.august;
      total += this.invoice.september;
      total += this.invoice.october;
      total += this.invoice.november;
      total += this.invoice.december;
      total += this.invoice.january;
      total += this.invoice.february;
      total += this.invoice.march;
      total = total / 12;
    }
    return total;
  }

  detailinvo(month) {
    this.spinner.show()
    switch (month) {
      case "april":
        this.invoiceTimeline.getInvoiceTimelineDetail(4).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "may":
        this.invoiceTimeline.getInvoiceTimelineDetail(5).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "june":
        this.invoiceTimeline.getInvoiceTimelineDetail(6).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "july":
        this.invoiceTimeline.getInvoiceTimelineDetail(7).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "august":
        this.invoiceTimeline.getInvoiceTimelineDetail(8).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "september":
        this.invoiceTimeline.getInvoiceTimelineDetail(9).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "october":
        this.invoiceTimeline.getInvoiceTimelineDetail(10).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "november":
        this.invoiceTimeline.getInvoiceTimelineDetail(11).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "december":
        this.invoiceTimeline.getInvoiceTimelineDetail(12).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "january":
        this.invoiceTimeline.getInvoiceTimelineDetail(1).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "february":
        this.invoiceTimeline.getInvoiceTimelineDetail(2).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      case "march":
        this.invoiceTimeline.getInvoiceTimelineDetail(3).subscribe(
          result => {
            this.invoiceDetail = result;
            this.invoice2 = true;
            this.invoice1 = false;
            this.spinner.hide()
          },
          error => {
            console.error(error);
            this.spinner.hide()
          }
        );
        break;

      default:
        this.invoiceDetail = [];
        this.invoice2 = true;
        this.invoice1 = false;
        this.spinner.hide()
        break;
    }
  }
  backinvo() {
    this.invoice1 = true;
    this.invoice2 = false;
  }
  search() {
    // Spinner
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  downloadReportInvoice() {
    let data, filename, link;

    // console.log(this.outstanding);
    let csv = this.convertToCSV({
      data: this.invoiceDetail
    });

    if (csv == null) return;
    filename = "Report Outstanding.csv";

    if (!csv.match(/^data:text\/csv/i)) {
      csv = "data:text/csv;charset=utf-8," + csv;
    }

    data = encodeURI(csv);
    link = document.createElement("a");
    link.setAttribute("href", data);
    link.setAttribute("download", filename);
    link.click();
  }
  convertToCSV(args) {
    let result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
      return null;
    }

    columnDelimiter = args.columnDelimiter || ",";
    lineDelimiter = args.lineDelimiter || "\n";

    keys = Object.keys(data[0]);

    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(item => {
      ctr = 0;
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter;

        result += item[key];
        ctr++;
      });
      result += lineDelimiter;
    });

    return result;
  }
}
