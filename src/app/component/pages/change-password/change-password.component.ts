import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  lastUpdate:boolean = true;
  viewUp_Pass:boolean = false;
  notif: boolean = false;

  viewPasswordCon1 = "password";
  viewPasswordCon2 = "password";
  viewPasswordCon3 = "password";
  isActive1 : boolean = false;
  isActive2 : boolean = false;
  isActive3 : boolean = false;
  password1: any;
  password2: any;
  password3: any;

  constructor(
    private router : Router,
    private auth: AuthService
  ) { }

  ngOnInit() {
  }

  updatePass(){
    this.viewUp_Pass = true;
    this.lastUpdate = false;
  }

  disAccPass(){
    this.viewUp_Pass = false;
    this.lastUpdate = true;
  }
  seePassword(val): void {
    if(val==1){
      this.isActive1=!this.isActive1;
      (this.isActive1)? this.viewPasswordCon1 = "text":this.viewPasswordCon1 = 'password';
    }else if(val==2){
      this.isActive2=!this.isActive2;
      (this.isActive2)? this.viewPasswordCon2 = "text":this.viewPasswordCon2 = 'password';
    }else{
      this.isActive3=!this.isActive3;
      (this.isActive3)? this.viewPasswordCon3 = "text":this.viewPasswordCon3 = 'password';
    }
  }
  savePassword(){
    if(this.password2!=this.password3){
      alert('Your password and confirmation password do not match');
    }else{
      var data = {
        curPassword: this.password1,
        newPassword: this.password2
      }
      this.auth.changePass(data).subscribe(res=>{
        var response=[];
        response.push(res);
        if(response[0].message=='Password changed.'){
          this.router.navigate(['change-password']);
          alert("Password Changed");
        }
      },err=>{
        alert("Data doesn't exists")
      });
    }
  }
  validatePass(val){
    if(val.match(/[a-z]/g) && val.match(/[A-Z]/g) && val.length > 8){
      this.notif=false;
    }else{
      this.notif=true;
    }
  }
}
