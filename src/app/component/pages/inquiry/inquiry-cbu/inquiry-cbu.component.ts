import { Component, OnInit } from '@angular/core';
import { InquiryCbuService } from '../../../../services/inquiry-cbu.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNull } from 'util';

@Component({
  selector: 'app-inquiry-cbu',
  templateUrl: './inquiry-cbu.component.html',
  styleUrls: ['./inquiry-cbu.component.css']
})
export class InquiryCbuComponent implements OnInit {

  matEng: string;
  materialCode: any;

  data: any;

  material_group: string;
  material_code: string;
  supersession_part: string;
  material_description: string;
  currency: string;
  base_unit_of_measure: string;
  dealer_net: string;
  recommended_retail_price: string;
  weight_unit: string;
  availability: string;
  remarks: string;
  net_price: number;
  suggested_retail_price: any;
  searchQuery: any
  storage_location: string
  InquiryCBU: string
  volume_unit: any;
  // statesComplex: any[] = [
  //   // { id: 1, matCode_Desk: '100108 - GX390T2-QN-R280', matEng: 'Engine', matCode: '100108' },
  //   // { id: 2, matCode_Desk: '100109 - GX390T2-QN-R280', matEng: 'Engine', matCode: '100109' },
  //   // { id: 3, matCode_Desk: '100111 - GX390T2-QN-R280', matEng: 'Engine', matCode: '100110' },
  //   // { id: 4, matCode_Desk: '100112 - GX390T2-QN-R280', matEng: 'Engine', matCode: '100111' },
  //   // { id: 5, matCode_Desk: '100113 - GX390T2-QN-R280', matEng: 'Engine', matCode: '100112' }
  // ];
  // statesComplex: any[];

  canPreviewAvailability: boolean = false
  canPreviewPriceInq: boolean = false

  constructor(
    private ListCBU: InquiryCbuService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.canPreviewAvailability = JSON.parse(localStorage.getItem("user")).permissions["Preview availability"]
    this.canPreviewPriceInq = JSON.parse(localStorage.getItem("user")).permissions["Preview price"]

  }

  trackByFn(index, item) {
    return index;
  }

  getMaterialCode(val) {
    if (val.length > 1) {
      // val = "000000000000" + val;
      this.ListCBU.getListMaterial(val).subscribe(res => {
        this.materialCode = res;
        // console.log(this.materialCode)
        // console.log(this.materialCode["storage_location"])
        for (var key in this.materialCode) {
          // this.storage_location = this.materialCode[key].storage_location
          if (this.materialCode[key].storage_location == "1500")
            this.materialCode[key].storage_location = 'BRT';
          else if (this.materialCode[key].storage_location = "2200") {
            this.materialCode[key].storage_location = 'TMR';
          }
        }

        this.searchQuery = val
      }, err => {
        console.log(err);
      });
    }
  }

  getListInquiryCBU(val) {
    if (val.length > 2) {
      val = "000000000000" + val
      this.searchQuery = val
      var customer_code = JSON.parse(localStorage.getItem('user')).sap_code;
      val = val.split(" - ");
      val = val[0];
      var data = {
        customer_number: customer_code,
        div: "10",
        material_number: val,
        sloc: "1500"
      };
      console.log(data);
      this.spinner.show();
      this.ListCBU.getListInquiryCBU(data).subscribe(res => {
        console.log(res);
        if (res["material_code"] !== null) {
          this.material_group = res[0].mg_description;
          this.material_code = res[0].material_number;
          this.supersession_part = res[0].supersession_part;
          this.material_description = res[0].material_description;
          this.currency = res[0].currency;
          this.base_unit_of_measure = res[0].base_unit_of_measure;
          this.dealer_net = res[0].net_price;
          // this.recommended_retail_price = res[0].suggested_retail_price;
          this.weight_unit = res[0].weight_unit;
          this.remarks = res[0].remarks;
          this.net_price = Math.round(res[0].net_price * 100 * 1.1);
          if (res[0].suggested_retail_price) {
            this.suggested_retail_price = Math.round(res[0].suggested_retail_price);
          } else {
            this.suggested_retail_price = null;
          }
          if (Math.round(parseInt(res[0].qty)) > 0) {
            this.availability = "Yes";
          } else {
            this.availability = "No";
          }
          if (res[0].minimum_stock >= res[0].qty) {
            this.availability = "Limited";
          }
          else if (res[0].is_available == 1) {
            this.availability = "Available";
          } else if (res[0].is_available == 0) {
            this.availability = "Not Available";
          }
          this.spinner.hide();
        } else {
          this.material_group = "";
          this.material_code = "";
          this.supersession_part = "";
          this.material_description = "";
          this.currency = "";
          this.base_unit_of_measure = "";
          this.dealer_net = "";
          this.recommended_retail_price = "";
          this.weight_unit = "";
          this.remarks = "";
          this.net_price = null;
          this.suggested_retail_price = null;
          this.availability = "";
          this.spinner.hide();
          alert("Item Not Found.");
        }
      }, err => {
        this.spinner.hide();
        console.log(err);

      });
    }
  }

  submitItem() {
    var val = this.searchQuery
    if (val) {
      var customer_code = JSON.parse(localStorage.getItem('user')).sap_code;
      val = val.split(" - ");
      console.log(val)
      if (val.length > 2) {
        var sloc = val[2];
        if (sloc == 'BRT')
          sloc = "1500"
        else if (sloc == 'TMR')
          sloc = "3300"
        val = "000000000000" + val[0];
        var data = {
          customer_number: customer_code,
          div: "10",
          material_number: val,
          sloc: sloc,
          user: JSON.parse(localStorage.getItem("user")).email
        };
        console.log(data);
        this.spinner.show();
        this.ListCBU.getListInquiryCBU(data).subscribe(res => {
          console.log(res);
          if (res["material_code"] !== null) {

            if (res[0].is_continue)
              if (res[0].is_continue == 1)
                alert("This Material is discontinued. Please contact HPPI.")

            this.material_group = res[0].mg_description;
            this.material_code = res[0].material_number;
            this.supersession_part = res[0].supersession_part;
            this.material_description = res[0].material_description;
            this.currency = res[0].currency;
            this.base_unit_of_measure = res[0].base_unit_of_measure;
            this.dealer_net = res[0].net_price;
            this.suggested_retail_price = res[0].suggested_retail_price;
            this.weight_unit = res[0].weight_unit;
            this.remarks = res[0].remarks;
            this.volume_unit = res[0].volume_unit
            this.net_price = Math.round(res[0].net_price * 100 * 1.1);
            if (res[0].suggested_retail_price) {
              this.suggested_retail_price = Math.round(res[0].suggested_retail_price);
            } else {
              this.suggested_retail_price = null;
            }
            let qty = parseInt(res[0].qty.toString().replace(',', ''))

            if (qty == 0)
              this.availability = "Not Available"
            else if (qty > 1 && qty > res[0].minimum_stock)
              this.availability = "Available"
            else if (qty > 1 && qty < res[0].minimum_stock)
              this.availability = "Limited"
            else if (qty > 1 && qty > res[0].minimum_stock || isNull(res[0].minimum_stock))
              this.availability = "Available"
            else if (res[0].is_continue == 1)
              this.availability = "Discontinue"
            else
              this.availability = "Available"
            // else if (qty > 1 && qty > res[0].minimum_stock)
            //   this.availability = "Limited"
            // else if (qty > 1 && isNull(res[0].minimum_stock))
            //   this.availability = "Available"
            // if (Math.round(parseInt(res[0].qty)) > 0) {
            //   this.availability = "Yes";
            // }
            // else if (res[0].is_continue == 0) {
            //   this.availability = "Discontinue"
            // }
            // else {
            //   this.availability = "No";
            // }
            // if (res[0].minimum_stock >= res[0].qty) {
            //   this.availability = "Limited";
            // }
            // else if (res[0].is_available == 1) {
            //   this.availability = "Available";
            // } else if (res[0].is_available == 0) {
            //   this.availability = "Not Available";
            // }
            this.spinner.hide();
          } else {
            this.material_group = "";
            this.material_code = "";
            this.supersession_part = "";
            this.material_description = "";
            this.currency = "";
            this.base_unit_of_measure = "";
            this.dealer_net = "";
            this.recommended_retail_price = "";
            this.weight_unit = "";
            this.remarks = "";
            this.net_price = null;
            this.suggested_retail_price = null;
            this.availability = "";
            this.spinner.hide();
            alert("Item Not Found.");
          }
          this.spinner.hide()
        }, err => {
          this.spinner.hide();
          console.log(err);
          alert('You dont have permission to check this material.')
        });
      } else {
        this.spinner.hide()
        alert("Please choose material first.")
      }
    }
  }
}
