import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { InquiryCbuService } from "src/app/services/inquiry-cbu.service";
import { CbuPurchaseOrderService } from "src/app/services/cbu-purchase-order.service";
import { ForecastEntryService } from "src/app/services/forecast-entry.service";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";

declare var jquery: any;
declare var $: any;
declare var number: any;

@Component({
  selector: "app-forecast-entry",
  templateUrl: "./forecast-entry.component.html",
  styleUrls: ["./forecast-entry.component.css"]
})
export class ForecastEntryComponent implements OnInit {
  forecast: boolean = false;
  materialCode: any;
  selectedMatCode: any;
  listMaterialForecast: any = [];
  palletFirstMonth: any = 0;
  palletSecondMonth: any = 0;
  palletThirdMonth: any = 0;
  startingYear: any;
  period: any;
  years: any[];
  month1: string;
  month2: string;
  month3: string;
  disableForm: boolean;
  dataCSV: any = [];
  canInputUploadDealerForecast: boolean = false;

  constructor(
    private ListCBU: InquiryCbuService,
    private cbuOrder: CbuPurchaseOrderService,
    private forecastEntry: ForecastEntryService,
    private cdref: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit() {
    let thisYear = new Date().getFullYear();
    let years = [];
    this.canInputUploadDealerForecast = JSON.parse(localStorage.getItem("user")).permissions["Input or Upload and preview Dealer Forecast"]
    console.log(thisYear);
    for (let i = 0; i < 2; i++) {
      years.push(thisYear++);
    }
    this.years = years;
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }
  trackByFn(index, item) {
    return index;
  }
  searchMaterial(val) {
    console.log(val);
    if (val.length > 1) {
      this.ListCBU.getListMaterial(val).subscribe(
        result => {
          console.log(result);
          this.materialCode = result;
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  addItemForecast() {
    if (this.selectedMatCode) {
      let matCode = this.selectedMatCode;
      // matCode = matCode.split(' - ')[0]
      // matCode = "00000000000"
      this.ListCBU.getMaterial(matCode).subscribe(
        result => {
          let data = [];
          data.push(result);
          if (
            this.listMaterialForecast.filter(
              val => val.material_code == data[0].material_code
            ).length == 0
          )
            this.listMaterialForecast.push(data[0]);
          else alert("Material Duplicate!");
          delete this.selectedMatCode;
        },
        error => {
          alert(Object.values(error).toString());
          console.log(error);
        }
      );
    } else {
      alert("Please select material code.");
    }
  }

  pallet1Month(index) {
    let month1 = $(`input[name=palletFirstMonth${index}]`).val();
    let month2 = $(`input[name=palletSecondMonth${index}]`).val();
    let month3 = $(`input[name=palletThirdMonth${index}]`).val();
    let a =
      parseInt(month1 ? month1 : 0) +
      parseInt(month2 ? month2 : 0) +
      parseInt(month3 ? month3 : 0);
    let total =
      this.listMaterialForecast[index]
        .numerator_for_conversion_to_base_units_of_measure * a;
    this.listMaterialForecast[index].total = total;
    this.listMaterialForecast[index].month1 = month1;
  }
  pallet2Month(index) {
    let month1 = $(`input[name=palletFirstMonth${index}]`).val();
    let month2 = $(`input[name=palletSecondMonth${index}]`).val();
    let month3 = $(`input[name=palletThirdMonth${index}]`).val();
    let a =
      parseInt(month1 ? month1 : 0) +
      parseInt(month2 ? month2 : 0) +
      parseInt(month3 ? month3 : 0);
    let total =
      this.listMaterialForecast[index]
        .numerator_for_conversion_to_base_units_of_measure * a;
    this.listMaterialForecast[index].total = total;
    this.listMaterialForecast[index].month2 = month2;
  }
  pallet3Month(index) {
    let month1 = $(`input[name=palletFirstMonth${index}]`).val();
    let month2 = $(`input[name=palletSecondMonth${index}]`).val();
    let month3 = $(`input[name=palletThirdMonth${index}]`).val();
    let a =
      parseInt(month1 ? month1 : 0) +
      parseInt(month2 ? month2 : 0) +
      parseInt(month3 ? month3 : 0);
    let total =
      this.listMaterialForecast[index]
        .numerator_for_conversion_to_base_units_of_measure * a;
    this.listMaterialForecast[index].total = total;
    this.listMaterialForecast[index].month3 = month3;
  }
  deleteMaterial(index) {
    this.listMaterialForecast.splice(index, 1);
    this.totalFirstMonth();
    this.totalSecondMonth();
    this.totalThirdMonth();
    this.totalUnit();
  }
  show() {
    this.listMaterialForecast = [];
    let data = {
      starting_year: this.startingYear,
      periode: this.period.substring(0, 2)
    };

    let dateToday = new Date();
    let endDate: Date;
    let startDate: Date;
    if (data.periode == "Q1") {
      // tanggal 3 april - 6 hari
      endDate = new Date(dateToday.getFullYear(), 3, 3);
      startDate = new Date(
        dateToday.getFullYear(),
        endDate.getMonth() - 1,
        endDate.getDate() - 6
      );
    }
    if (data.periode == "Q2") {
      // tanggal 3 july - 6  hari
      endDate = new Date(dateToday.getFullYear(), 6, 3);
      startDate = new Date(
        dateToday.getFullYear(),
        endDate.getMonth() - 1,
        endDate.getDate() - 6
      );
    }
    if (data.periode == "Q3") {
      // tanggal 3 oktober - 6 hari
      endDate = new Date(dateToday.getFullYear(), 9, 3);
      startDate = new Date(
        dateToday.getFullYear(),
        endDate.getMonth() - 1,
        endDate.getDate() - 6
      );
    }
    if (data.periode == "Q4") {
      // tanggal 3 januari - 6 hari
      endDate = new Date(dateToday.getFullYear(), 0, 3);
      startDate = new Date(
        dateToday.getFullYear(),
        endDate.getMonth() - 1,
        endDate.getDate() - 6
      );
    }

    if (
      dateToday.getTime() > startDate.getTime() &&
      dateToday.getTime() < endDate.getTime()
    ) {
      this.disableForm = false;
    } else {
      this.disableForm = true;
    }

    if (data.periode == "Q1") {
      this.month1 = "April"; // tanggal 3 - 6 hari
      this.month2 = "May";
      this.month3 = "June";
    }
    if (data.periode == "Q2") {
      this.month1 = "July"; // tanggal 3 - 6  hari
      this.month2 = "August";
      this.month3 = "September";
    }
    if (data.periode == "Q3") {
      this.month1 = "October"; // tanggal 3 - 6 hari
      this.month2 = "November";
      this.month3 = "December";
    }
    if (data.periode == "Q4") {
      this.month1 = "January"; // tanggal 3 - 6 hari
      this.month2 = "February";
      this.month3 = "March";
    }
    if (!this.startingYear) {
      alert("Choose Starting Year");
      this.forecast = false;
    } else if (!this.period) {
      alert("Choose Period");
      this.forecast = false;
    } else {
      this.forecastEntry.submitPeriode(data).subscribe(res => {
        if (res["message"] == "exist") {
          let result = [];
          result.push(res);
          let i = 0;
          for (const item of result[0].value) {
            this.listMaterialForecast.push(item);
            this.listMaterialForecast[
              i
            ].numerator_for_conversion_to_base_units_of_measure =
              item.qty_per_pallet;
            this.listMaterialForecast["status"] = result[0].status;
            i++;
          }
          console.log(this.listMaterialForecast);
        } else {
          this.listMaterialForecast = [];
          this.listMaterialForecast["status"] = 3;
          // this.onRefresh()
        }
        // if (result[0].status == 0) {
        // }
        this.forecast = true;
      });
    }
  }

  onRefresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };
    let currentUrl = this.router.url + "?";
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }

  hide() {
    this.forecast = false;
  }
  submitForecast() {
    let data = {
      starting_year: this.startingYear,
      periode: this.period.substring(0, 2),
      material_number: [],
      material_group: [],
      material_description: [],
      qty_per_pallet: [],
      month1: [],
      month2: [],
      month3: [],
      total: [],
      dealer_name: localStorage.getItem("dealer_name")
    };
    let dateToday = new Date();
    let endDate: Date;
    let startDate: Date;
    if (data.periode == "Q1") {
      // tanggal 3 april - 6 hari
      endDate = new Date(dateToday.getFullYear(), 3, 3);
      startDate = new Date(
        dateToday.getFullYear(),
        endDate.getMonth() - 1,
        endDate.getDate() - 6
      );
    }
    if (data.periode == "Q2") {
      // tanggal 3 july - 6  hari
      endDate = new Date(dateToday.getFullYear(), 6, 3);
      startDate = new Date(
        dateToday.getFullYear(),
        endDate.getMonth() - 1,
        endDate.getDate() - 6
      );
    }
    if (data.periode == "Q3") {
      // tanggal 3 oktober - 6 hari
      endDate = new Date(dateToday.getFullYear(), 9, 3);
      startDate = new Date(
        dateToday.getFullYear(),
        endDate.getMonth() - 1,
        endDate.getDate() - 6
      );
    }
    if (data.periode == "Q4") {
      // tanggal 3 januari - 6 hari
      endDate = new Date(dateToday.getFullYear(), 0, 3);
      startDate = new Date(
        dateToday.getFullYear(),
        endDate.getMonth() - 1,
        endDate.getDate() - 6
      );
    }

    // let startingDate = new Date(
    //   dateToday.getFullYear(),
    //   dateToday.getMonth(),
    //   27
    // );

    // if (
    //   dateToday.getTime() > startDate.getTime() &&
    //   dateToday.getTime() < endDate.getTime()
    // ) {
      // alert("Correct Date");
      //If Date is valid to input
      if (this.listMaterialForecast.length > 0) {
        $("#tableForecast #material_number").each(function() {
          data.material_number.push($(this).text());
        });

        $("#tableForecast #material_group").each(function() {
          data.material_group.push($(this).text());
        });

        $("#tableForecast #material_description").each(function() {
          data.material_description.push($(this).text());
        });

        $("#tableForecast #pallet").each(function() {
          data.qty_per_pallet.push($(this).text());
        });

        $("#tableForecast #month1").each(function() {
          data.month1.push($(this).val());
        });

        $("#tableForecast #month2").each(function() {
          data.month2.push($(this).val());
        });

        $("#tableForecast #month3").each(function() {
          data.month3.push($(this).val());
        });

        $("#tableForecast #total").each(function() {
          data.total.push($(this).text());
        });
        console.log(data);
        this.forecastEntry.submitForecast(data).subscribe(
          result => {
            console.log(result);
            if (result["message"] == "Success") {
              alert("Data submitted successfully.");
              location.reload();
            }
          },
          error => {
            console.log(error);
          }
        );
      } else {
        alert("Please add material.");
      }
    // } else {
    //   //alert for date input is invalid
    //   alert("Invalid input date!");
    // }
  }

  minusOne(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
      // Decrement one only if value is > 1
      $("input[name=" + val + "]").val(currentVal - 1);
      $(".qtyplus")
        .val("+")
        .removeAttr("style");
    } else {
      // Otherwise put a 0 there
      $(".qtyminus")
        .val("-")
        .css("color", "#aaa");
      $(".qtyminus")
        .val("-")
        .css("cursor", "not-allowed");
    }
  }
  totalFirstMonth() {
    let total1 = 0;
    for (let i = 0; i < this.listMaterialForecast.length; i++) {
      if (!isNaN(parseInt(this.listMaterialForecast[i].month1)))
        total1 = total1 + parseInt(this.listMaterialForecast[i].month1);
    }
    return total1;
  }
  totalSecondMonth() {
    let total2 = 0;
    for (let i = 0; i < this.listMaterialForecast.length; i++) {
      if (!isNaN(parseInt(this.listMaterialForecast[i].month2)))
        total2 = total2 + parseInt(this.listMaterialForecast[i].month2);
    }
    return total2;
  }
  totalThirdMonth() {
    let total3 = 0;
    for (let i = 0; i < this.listMaterialForecast.length; i++) {
      if (!isNaN(parseInt(this.listMaterialForecast[i].month3)))
        total3 = total3 + parseInt(this.listMaterialForecast[i].month3);
    }
    return total3;
  }
  totalUnit() {
    let total = 0;
    for (let i = 0; i < this.listMaterialForecast.length; i++) {
      if (!isNaN(parseInt(this.listMaterialForecast[i].total)))
        total = total + parseInt(this.listMaterialForecast[i].total);
    }
    return total;
  }

  plusONe(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If is not undefined
    if (!isNaN(currentVal)) {
      // Increment only if value is < 20
      if (currentVal < 100) {
        $("input[name=" + val + "]").val(currentVal + 1);
        $(".qtyminus")
          .val("-")
          .removeAttr("style");
      } else {
        $(".qtyplus")
          .val("+")
          .css("color", "#aaa");
        $(".qtyplus")
          .val("+")
          .css("cursor", "not-allowed");
      }
    } else {
      // Otherwise put a 0 there
      $("input[name=" + val + "]").val(1);
    }
  }

  handleFiles(event) {
    // console.log(event.target.files);
    if (event.target.files.length > 0) {
      this.spinner.show();
      let reader = new FileReader();
      reader.onload = (event: any) => {
        let csv = event.target.result;

        let allTextLines = csv.split(/\r\n|\n/);
        let lines = [];

        //first line on csv
        let keys = allTextLines.shift().split(",");
        while (allTextLines.length) {

          let arr = allTextLines.shift().split(",");
          let obj = {};

          for (let i = 0; i < keys.length; i++) {
            obj[keys[i]] = arr[i];
          }
          lines.push(obj);
        }

        if (lines.length > 0) {
          this.dataCSV = lines;
          this.importCSVToTable();
        } else {
          this.spinner.hide();
          alert("File is empty or Format is does not match");
        }
      };
      reader.onerror = this.onErrorHandler;
      reader.readAsBinaryString(event.target.files[0]);
    }
  }

  importCSVToTable() {

    //eliminate all duplicate
    let map = new Map();
    let data = [];
    for (const item of this.dataCSV) {
      if (!map.has(item.material_number)) {
        map.set(item.material_number, true);
        data.push(item);
      } else {
        alert(`Material ${item.material_number} is Duplicate!`);
      }
    }
    this.dataCSV.length = 0;
    let index = 0;
    let listMaterialForecast = [];
    for (const item of data) {
      if (item.material_number) {
        let matCode = "000000000000" + item.material_number;
        // console.log(matCode);
        this.ListCBU.getMaterial(matCode).subscribe(
          result => {
            if (Object.entries(result).length > 0) {
              listMaterialForecast.push(result);

              let month1 = parseInt(item.month_1);
              let month2 = parseInt(item.month_2);
              let month3 = parseInt(item.month_3);
              let totalPerMonth = month1 + month2 + month3;
              let total = listMaterialForecast[index]["numerator_for_conversion_to_base_units_of_measure"] * totalPerMonth;

              listMaterialForecast[index]["month1"] = month1;
              listMaterialForecast[index]["month2"] = month2;
              listMaterialForecast[index]["month3"] = month3;
              listMaterialForecast[index]["total"] = total;

              if(this.listMaterialForecast.filter(item => item.material_code == listMaterialForecast[index].material_code).length == 0) {
                this.listMaterialForecast.push(listMaterialForecast[index])
              } else {
                alert(`Material ${item.material_number} is already added!`);
              }


              index++

              if(data.length - 1 == index) {
                $("#uploadCBU").val("");
                this.spinner.hide()
              }
            }
            // this.spinner.hide();
            // $("#uploadCBU").val("");
          },
          error => {
            alert(Object.values(error).toString());
            console.log(error);
            this.spinner.hide();
            $("#uploadCBU").val("");
          }
        );
      }

    }
  }
  onErrorHandler(event) {
    if (event.target.error.name == "NotReadableError") {
      alert("Can't read the file!");
      $("#uploadCBU").val("");
    }
    this.spinner.hide();
  }
}
