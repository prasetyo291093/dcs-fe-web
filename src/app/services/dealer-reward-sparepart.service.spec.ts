import { TestBed } from '@angular/core/testing';

import { DealerRewardSparepartService } from './dealer-reward-sparepart.service';

describe('DealerRewardSparepartService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DealerRewardSparepartService = TestBed.get(DealerRewardSparepartService);
    expect(service).toBeTruthy();
  });
});
