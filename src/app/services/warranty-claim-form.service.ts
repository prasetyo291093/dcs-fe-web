import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WarrantyClaimFormService {

  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  saveWarrantyClaimInput(data) {
    return this.http.post(`${this.baseUrl}/api/warranty-claim/input`, data);
  }

  getWarrantyClaimCustomer(material_code: string) {
    return this.http.get(`${this.baseUrl}/api/warranty-claim/customer/${material_code}`);
  }

  getWarrantyClaimRate() {
    return this.http.get(`${this.baseUrl}/api/warranty-claim/rate`);
  }

  getMaterialsByDesc(desc: string) {
    return this.http.get(`${this.baseUrl}/api/warranty-claim/materials/desc?search=${desc}`);
  }

  getMaterialsByCode(code: string) {
    return this.http.get(`${this.baseUrl}/api/warranty-claim/materials/code?search=${code}`);
  }

}
