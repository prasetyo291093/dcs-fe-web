import { TestBed } from '@angular/core/testing';

import { LogisticClaimSparepartsService } from './logistic-claim-spareparts.service';

describe('LogisticClaimSparepartsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogisticClaimSparepartsService = TestBed.get(LogisticClaimSparepartsService);
    expect(service).toBeTruthy();
  });
});
