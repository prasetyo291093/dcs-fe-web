import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WarrantyClaimReportService {

  constructor(private http: HttpClient) { }

  baseUrl = environment.baseUrl;

  fetchReportData(sap_code: string) {
    return this.http.get(`${this.baseUrl}/api/warranty-claim/report/${sap_code}`);
  }
}
