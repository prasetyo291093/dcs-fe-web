import { TestBed } from '@angular/core/testing';

import { PendingTransactionService } from './pending-transaction.service';

describe('PendingTransactionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PendingTransactionService = TestBed.get(PendingTransactionService);
    expect(service).toBeTruthy();
  });
});
