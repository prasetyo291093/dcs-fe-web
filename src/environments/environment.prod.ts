export const environment = {
  production: true,
  baseUrl: 'https://api.dcs-hppi.co.id',
  appUrl: 'https://dcs-hppi.co.id'
};
