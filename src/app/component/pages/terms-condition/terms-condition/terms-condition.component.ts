import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.css']
})
export class TermsConditionComponent implements OnInit {

  constructor() { }
  tab1:string = " ";
  tab2:string = " ";
  tab3:string = " ";
  tab4:string = " ";

  menu1:boolean = false;
  menu2:boolean = false;
  menu3:boolean = false;
  menu4:boolean = false;

  show1:boolean = false;
  show2:boolean = false;
  virtualClass:boolean=false;
  paymentClass:boolean=false;

  show3:boolean=false;
  show4:boolean=false;
  show5:boolean=false;
  jabalClass:boolean=false;
  sumkalClass:boolean=false;
  daerahClass:boolean=false;

  ngOnInit() {
    this.tab1= "blueActive";
    this.menu1 = true;
  }

  active(){
    this.tab1 = " ";
    this.tab2 = " ";
    this.tab3 = " ";
    this.tab4 = " ";
  }

  menuActive(){
    this.menu1 = false;
    this.menu2 = false;
    this.menu3 = false;
    this.menu4 = false;
  }

  openInfo_tab(val){
    this.active();
    this.menuActive();
    if(val == 1){
      this.tab1= "blueActive";
      this.menu1 = true;
    } else if(val == 2){
      this.tab2= "blueActive";
      this.menu2 = true;
    } else if(val == 3){
      this.tab3= "blueActive";
      this.menu3 = true;
    } else{
      this.tab4= "blueActive";
      this.menu4 = true;
    }
  }

  accordion(val){

    if(val == 'virtual'){
      this.virtualClass = !this.virtualClass;
      this.show1 = !this.show1;
      this.show2 = false;
      this.paymentClass=false;
    } else{
      this.paymentClass = !this.paymentClass;
      this.show2 = !this.show2;
      this.show1 = false;
      this.virtualClass=false;
    }
    console.log(val)

  }

  accordion2(val){
    if(val == "jabal"){
      this.show3 = !this.show3;
      this.show4 = false;
      this.show5 = false;
      this.jabalClass = !this.jabalClass;
      this.sumkalClass =false;
      this.daerahClass = false;
    } else if(val == "sumkal"){
      this.show4 = !this.show4;
      this.show3 = false;
      this.show5 = false;
      this.sumkalClass = !this.sumkalClass;
      this.jabalClass = false;
      this.daerahClass = false
    } else{
      this.show5 = !this.show5;
      this.show3 = false;
      this.show4 = false;
      this.daerahClass = !this.daerahClass;
      this.jabalClass = false;
      this.sumkalClass = false;
    }
  }

}
