import { Component, OnInit } from "@angular/core";
import { DealerRewardCbuService } from "src/app/services/dealer-reward-cbu.service";

@Component({
  selector: "app-cbu-reward",
  templateUrl: "./cbu-reward.component.html",
  styleUrls: ["./cbu-reward.component.css"]
})
export class CbuRewardComponent implements OnInit {
  reward: any = [];
  category: any = [];
  point: any = [];
  grandValue: number = 0;
  constructor(private dealerRewardCbu: DealerRewardCbuService) {}

  ngOnInit() {
    this.dealerRewardCbu.getDealerRewardCbu().subscribe(
      (result: []) => {

        for (let i = 0; i < result["reward_category"].length; i++) {
          let item = {
            reward_category: result["reward_category"][i],
            point: result["point"][i]
          }
          this.reward.push(item)
          // this.reward["reward_category"].push(result["reward_category"][i])
          // this.reward["point"].push(result["point"][i])
        }
        console.log(this.reward)
        for (const item of result["point"]) {
          this.grandValue += parseInt(item);
        }
      },
      (error: Error) => {
        console.log(error.message);
      }
    );
  }
}
