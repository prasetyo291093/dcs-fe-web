import { TestBed } from '@angular/core/testing';

import { InquirySparepartsService } from './inquiry-spareparts.service';

describe('InquirySparepartsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InquirySparepartsService = TestBed.get(InquirySparepartsService);
    expect(service).toBeTruthy();
  });
});
