import { TestBed } from '@angular/core/testing';

import { DealerProfileService } from './dealer-profile.service';

describe('DealerProfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DealerProfileService = TestBed.get(DealerProfileService);
    expect(service).toBeTruthy();
  });
});
