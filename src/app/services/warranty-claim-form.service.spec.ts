import { TestBed } from '@angular/core/testing';

import { WarrantyClaimFormService } from './warranty-claim-form.service';

describe('WarrantyClaimFormService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WarrantyClaimFormService = TestBed.get(WarrantyClaimFormService);
    expect(service).toBeTruthy();
  });
});
