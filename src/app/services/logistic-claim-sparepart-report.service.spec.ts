import { TestBed } from '@angular/core/testing';

import { LogisticClaimSparepartReportService } from './logistic-claim-sparepart-report.service';

describe('LogisticClaimSparepartReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogisticClaimSparepartReportService = TestBed.get(LogisticClaimSparepartReportService);
    expect(service).toBeTruthy();
  });
});
