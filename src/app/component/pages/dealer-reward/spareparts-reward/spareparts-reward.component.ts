import { Component, OnInit } from "@angular/core";
import { DealerRewardSparepartService } from "src/app/services/dealer-reward-sparepart.service";

@Component({
  selector: "app-spareparts-reward",
  templateUrl: "./spareparts-reward.component.html",
  styleUrls: ["./spareparts-reward.component.css"]
})
export class SparepartsRewardComponent implements OnInit {
  reward: any = [];
  grandValue: number = 0;
  constructor(private dealerRewardSparepart: DealerRewardSparepartService) {}

  ngOnInit() {
    this.dealerRewardSparepart.getDealerRewardSparepart().subscribe(
      (result: []) => {
        // this.reward = result;
        for (let i = 0; i < result["reward_category"].length; i++) {
          let item = {
            reward_category: result["reward_category"][i],
            point: result["point"][i]
          };
          this.reward.push(item);
        }
        for (const item of result["point"]) {
          this.grandValue += parseInt(item);
        }
      },
      (error: Error) => {
        console.log(error.message);
      }
    );
  }
}
