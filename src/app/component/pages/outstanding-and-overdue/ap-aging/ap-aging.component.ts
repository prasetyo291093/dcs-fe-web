import { Component, OnInit } from "@angular/core";
import { ApAgingService } from "src/app/services/ap-aging.service";
import { OutstandingAndOverdueReportService } from "src/app/services/outstanding-and-overdue-report.service";
import * as moment from "moment";

@Component({
  selector: "app-ap-aging",
  templateUrl: "./ap-aging.component.html",
  styleUrls: ["./ap-aging.component.css"]
})
export class ApAgingComponent implements OnInit {
  table1: boolean = false;
  table2: boolean = false;
  apAgingData: any = [];
  currency: any;
  total_outstanding: string;
  credit_limit: string;
  remaining_credit: string;
  not_due_amount: string;
  not_due_percentage: string;
  day_amount_30: string;
  day_percentage_30: string;
  day_amount_45: string;
  day_percentage_45: string;
  day_amount_60: string;
  day_percentage_60: string;
  day_amount_90: string;
  day_percentage_90: string;
  day_amount_120: string;
  day_percentage_120: string;
  day_amount_180: string;
  day_percentage_180: string;
  day_amount_360: string;
  day_percentage_360: string;
  day_amount_720: string;
  day_percentage_720: string;
  day_amount_721: string;
  day_percentage_721: string;
  created_at: string;
  apAgingDetail: [];
  notDue: any = [];
  due_0_30: any = [];
  due_31_45: any = [];
  due_46_60: any = [];
  due_61_90: any = [];
  due_91_120: any = [];
  due_121_180: any = [];
  due_181_360: any = [];
  due_361_720: any = [];
  due_721: any = [];

  constructor(
    private apAging: ApAgingService,
    private outstandingReport: OutstandingAndOverdueReportService
  ) {}

  ngOnInit() {
    this.table1 = true;
    let today = moment();
    let due = new Date("2019-03-20");
    let diff = today.diff(due, "days");
    console.log(`Dif: ${diff}`);
    this.apAging.getApAgingData().subscribe(
      _result => {
        // if (_result.length > 0)
        // this.apAgingData = _result;
        console.log(_result);
        this.currency = _result[0]["currency"];
        this.total_outstanding = _result[0]["total_outstanding"];
        this.credit_limit = _result[0]["credit_limit"];
        this.remaining_credit = _result[0]["remaining_credit"];
        this.not_due_amount = _result[0]["not_due_amount"];
        this.not_due_percentage = _result[0]["not_due_percentage"];
        this.day_amount_30 = _result[0]["day_amount_30"];
        this.day_percentage_30 = _result[0]["day_percentage_30"];
        this.day_amount_45 = _result[0]["day_amount_45"];
        this.day_percentage_45 = _result[0]["day_percentage_45"];
        this.day_amount_60 = _result[0]["day_amount_60"];
        this.day_percentage_60 = _result[0]["day_percentage_60"];
        this.day_amount_90 = _result[0]["day_amount_90"];
        this.day_percentage_90 = _result[0]["day_percentage_90"];
        this.day_amount_120 = _result[0]["day_amount_120"];
        this.day_percentage_120 = _result[0]["day_percentage_120"];
        this.day_amount_180 = _result[0]["day_amount_180"];
        this.day_percentage_180 = _result[0]["day_percentage_180"];
        this.day_amount_360 = _result[0]["day_amount_360"];
        this.day_percentage_360 = _result[0]["day_percentage_360"];
        this.day_amount_720 = _result[0]["day_amount_720"];
        this.day_percentage_720 = _result[0]["day_percentage_720"];
        this.day_amount_721 = _result[0]["day_amount_721"];
        this.day_percentage_721 = _result[0]["day_percentage_721"];
        this.created_at = _result[0]["created_at"];

        // console.log(this.apAgingData);
      },
      (error: Error) => {
        console.log(error.message);
      }
    );

    this.apAgingDetails();
  }
  apAgingDetails() {
    this.outstandingReport.getOutstandingReport().subscribe(
      result => {
        let today = moment();
        for (const item of result["outstanding"]) {
          let dueDate = new Date(item.due_date);
          let diff = today.diff(dueDate, "day");
          if (diff < 0) {
            this.notDue.push(item);
          } else if (diff >= 0 && diff <= 30) {
            this.due_0_30.push(item);
          } else if (diff >= 31 && diff <= 45) {
            this.due_31_45.push(item);
          } else if (diff >= 46 && diff <= 60) {
            this.due_46_60.push(item);
          } else if (diff >= 61 && diff <= 90) {
            this.due_61_90.push(item);
          } else if (diff >= 91 && diff <= 120) {
            this.due_91_120.push(item);
          } else if (diff >= 121 && diff <= 180) {
            this.due_121_180.push(item);
          } else if (diff >= 181 && diff <= 360) {
            this.due_181_360.push(item);
          } else if (diff >= 361 && diff <= 720) {
            this.due_361_720.push(item);
          } else if (diff >= 721) {
            this.due_721.push(item);
          }
        }
      },
      (error: Error) => {
        console.error(error.message);
      }
    );
  }

  detail(status: string) {
    if (status == "Not Due") {
      this.apAgingDetail = this.notDue;
    } else if (status == "Due 1-30") {
      this.apAgingDetail = this.due_0_30;
    } else if (status == "Due 31-45") {
      this.apAgingDetail = this.due_31_45;
    } else if (status == "Due 46-60") {
      this.apAgingDetail = this.due_46_60;
    } else if (status == "Due 61-90") {
      this.apAgingDetail = this.due_61_90;
    } else if (status == "Due 91-120") {
      this.apAgingDetail = this.due_91_120;
    } else if (status == "Due 121-180") {
      this.apAgingDetail = this.due_121_180;
    } else if (status == "Due 181-360") {
      this.apAgingDetail = this.due_181_360;
    } else if (status == "Due 361-720") {
      this.apAgingDetail = this.due_361_720;
    } else if (status == "Due 721") {
      this.apAgingDetail = this.due_721;
    }
    this.table2 = true;
    this.table1 = false;
  }

  back() {
    this.table1 = true;
    this.table2 = false;
  }

  downloadReport() {
    let data, filename, link;

    // console.log(this.outstanding);

    let apAgingReport = [
      {
        customer: JSON.parse(localStorage.getItem("user")).sap_code,
        customer_name: "PANCA JAYA GEMILANG",
        total_outstanding: this.total_outstanding,
        currency: this.currency,
        day_amount_30: this.day_amount_30,
        day_percentage_30: this.day_percentage_30,
        day_amount_45: this.day_amount_45,
        day_percentage_45: this.day_percentage_45,
        day_amount_60: this.day_amount_60,
        day_percentage_60: this.day_percentage_60,
        day_amount_90: this.day_amount_90,
        day_percentage_90: this.day_percentage_90,
        day_amount_120: this.day_amount_120,
        day_percentage_120: this.day_percentage_120,
        day_amount_180: this.day_amount_180,
        day_percentage_180: this.day_percentage_180,
        day_amount_360: this.day_amount_360,
        day_percentage_360: this.day_percentage_360,
        day_amount_720: this.day_amount_720,
        day_percentage_720: this.day_percentage_720,
        day_amount_721: this.day_amount_721,
        day_percentage_721: this.day_percentage_721,
        not_due_amount: this.not_due_amount,
        not_due_percentage: this.not_due_percentage,
        credit_limit: this.credit_limit,
        remaining_credit: this.remaining_credit
      }
    ];

    let csv = this.convertToCSV({
      data: apAgingReport
    });

    if (csv == null) return;
    filename = "Report A/P Aging.csv";

    if (!csv.match(/^data:text\/csv/i)) {
      csv = "data:text/csv;charset=utf-8," + csv;
    }

    data = encodeURI(csv);
    link = document.createElement("a");
    link.setAttribute("href", data);
    link.setAttribute("download", filename);
    link.click();
  }

  convertToCSV(args) {
    let result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
      return null;
    }

    columnDelimiter = args.columnDelimiter || ",";
    lineDelimiter = args.lineDelimiter || "\n";

    keys = Object.keys(data[0]);

    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(item => {
      ctr = 0;
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter;

        result += item[key];
        ctr++;
      });
      result += lineDelimiter;
    });

    return result;
  }
}
