import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SalesPromoService } from 'src/app/services/sales-promo.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-sales-promo',
  templateUrl: './sales-promo.component.html',
  styleUrls: ['./sales-promo.component.css']
})
export class SalesPromoComponent implements OnInit {

  btnFilter: boolean = false
  resetFilter: boolean = false
  filterDate: boolean = false
  filterNo: boolean = false
  paymentMethod: boolean = false
  paymentDue: boolean = false
  salesOrderCash: any = []
  salesOrderForecast: any = []
  salesOrderProject: any = []
  salesOrderOther: any = []
  salesOrderAdvance: any = []
  aw1: boolean = false
  aw2: boolean = false

  salesOrderDetail: any = []
  salesOrderDetailItem: any = []
  salesOrderDetailItemTotal: any = []

  salesPro1: string;
  salesPro2: string;
  salesPro3: string;
  salesPro4: string;
  salesPro5: string;

  cashDiscount: boolean;
  forecastDiscount: boolean;
  projectDiscount: boolean;
  advancePayment: boolean;
  otherDiscount: boolean;

  canPreviewPromoDetail: boolean = false

  constructor(
    private router: Router,
    private salesPromoOrder: SalesPromoService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.btnFilter = true;
    this.filterDate = true;
    this.salesPro1 = 'sales-promo-active';
    this.cashDiscount = true;
    this.canPreviewPromoDetail = JSON.parse(localStorage.getItem("user")).permissions["Preview all Promo Detail"]
    this.getSalesPromoList()
    // console.log(this.salesOrderCash)
    this.aw1 = true
  }
  filter() {
    this.btnFilter = false;
    this.resetFilter = true;
  }
  resetFil() {
    this.btnFilter = true;
    this.resetFilter = false;
  }
  getSalesPromoList() {
    this.spinner.show()
    this.salesPromoOrder.getListSalesPromo().subscribe((result) => {
      let map = new Map()
      let salesOrderList = []
      salesOrderList.push(result)
      let salesOrderResult = []
      for (const item of salesOrderList[0]) {
        if (!map.has(item["discount_number"])) {
          map.set(item["discount_number"], true)
          salesOrderResult.push(item)
        }
      }
      // console.log(salesOrderList.filter((x) => { return x["discount_number"] == "1003000032" }))
      for (const key in salesOrderResult) {
        // let pallet = 0
        // for (const item in salesOrderList.filter((x) => { return x == salesOrderResult[key].discount_number })) {
        //   pallet += parseInt(salesOrderList[item].pallet)
        // }
        // console.log(pallet)
        // salesOrderResult[key].status =
        if (salesOrderResult[key].promo_type == "Z14")
          this.salesOrderCash.push(salesOrderResult[key])
        if (salesOrderResult[key].promo_type == "Z15")
          this.salesOrderForecast.push(salesOrderResult[key])
        if (salesOrderResult[key].promo_type == "Z16")
          this.salesOrderProject.push(salesOrderResult[key])
        if (salesOrderResult[key].promo_type == "Z17")
          this.salesOrderOther.push(salesOrderResult[key])
        if (salesOrderResult[key].promo_type == "Z18")
          this.salesOrderAdvance.push(salesOrderResult[key])
      }
      for (const key in this.salesOrderCash) {
        let balance_pallet = 0
        this.salesOrderCash[key].promo_from = new Date(this.salesOrderCash[key].promo_from)
        this.salesOrderCash[key].promo_end = new Date(this.salesOrderCash[key].promo_end)
        var today = new Date()
        var date_today = today.setHours(0, 0, 0, 0)

        var promo_from = this.salesOrderCash[key].promo_from.getTime()
        var promo_end = this.salesOrderCash[key].promo_end.getTime()
        // console.log(date_today == promo_end)
        if (date_today > promo_end)
          this.salesOrderCash[key].status = "Expired"
        else if (date_today < promo_from)
          this.salesOrderCash[key].status = "Not Ready Yet"
        else
          this.salesOrderCash[key].status = "Open"

        if (promo_from > date_today)
          this.salesOrderCash[key].disabled = "true"
        else if (date_today == promo_from)
          this.salesOrderCash[key].disabled = "false"
        else if (date_today > promo_end)
          this.salesOrderCash[key].disabled = "true"
        else if (date_today < promo_end)
          this.salesOrderCash[key].disabled = "false"
        // this.salesOrderCash[key].today = date_today
        // this.salesOrderCash[key].valid_date = promo_from

        for (const key1 in salesOrderList[0]) {
          if (salesOrderList[0][key1].discount_number == this.salesOrderCash[key].discount_number) {
            balance_pallet += parseInt(salesOrderList[0][key1].balance_pallet)
          }
        }
        if (balance_pallet <= 0)
          this.salesOrderCash[key].status = "Completed"
        // if(this.salesOrderCash[key].balance_pallet)
      }
      console.log(this.salesOrderCash)

      //Forecast
      for (const key in this.salesOrderForecast) {
        let balance_pallet = 0
        this.salesOrderForecast[key].promo_from = new Date(this.salesOrderForecast[key].promo_from)
        this.salesOrderForecast[key].promo_end = new Date(this.salesOrderForecast[key].promo_end)
        var today = new Date()
        var date_today = today.setHours(0, 0, 0, 0)

        var promo_from = this.salesOrderForecast[key].promo_from.getTime()
        var promo_end = this.salesOrderForecast[key].promo_end.getTime()
        //console.log(date_today, promo_from, promo_end)
        if (date_today > promo_end)
          this.salesOrderForecast[key].status = "Expired"
        else if (date_today < promo_from)
          this.salesOrderForecast[key].status = "Not Ready Yet"
        else
          this.salesOrderForecast[key].status = "Open"

        if (promo_from > date_today)
          this.salesOrderForecast[key].disabled = "true"
        else if (date_today == promo_from)
          this.salesOrderForecast[key].disabled = "false"
        else if (date_today > promo_end)
          this.salesOrderForecast[key].disabled = "true"
        else if (date_today < promo_end)
          this.salesOrderForecast[key].disabled = "false"

        for (const key1 in salesOrderList[0]) {
          if (salesOrderList[0][key1].discount_number == this.salesOrderForecast[key].discount_number) {
            balance_pallet += parseInt(salesOrderList[0][key1].balance_pallet)
          }
        }
        if (balance_pallet <= 0)
          this.salesOrderForecast[key].status = "Completed"
      }

      //Project Discount
      for (const key in this.salesOrderProject) {
        let balance_pallet = 0
        this.salesOrderProject[key].promo_from = new Date(this.salesOrderProject[key].promo_from)
        this.salesOrderProject[key].promo_end = new Date(this.salesOrderProject[key].promo_end)
        var today = new Date()
        var date_today = today.setHours(0, 0, 0, 0)

        var promo_from = this.salesOrderProject[key].promo_from.getTime()
        var promo_end = this.salesOrderProject[key].promo_end.getTime()
        //console.log(date_today, promo_from, promo_end)
        if (date_today > promo_end)
          this.salesOrderProject[key].status = "Expired"
        else if (date_today < promo_from)
          this.salesOrderProject[key].status = "Not Ready Yet"
        else
          this.salesOrderProject[key].status = "Open"

        if (promo_from > date_today)
          this.salesOrderProject[key].disabled = "true"
        else if (date_today == promo_from)
          this.salesOrderProject[key].disabled = "false"
        else if (date_today > promo_end)
          this.salesOrderProject[key].disabled = "true"
        else if (date_today < promo_end)
          this.salesOrderProject[key].disabled = "false"

        for (const key1 in salesOrderList[0]) {
          if (salesOrderList[0][key1].discount_number == this.salesOrderProject[key].discount_number) {
            balance_pallet += parseInt(salesOrderList[0][key1].balance_pallet)
          }
        }
        if (balance_pallet <= 0)
          this.salesOrderProject[key].status = "Completed"
      }

      //Other Discount
      for (const key in this.salesOrderOther) {
        let balance_pallet = 0
        this.salesOrderOther[key].promo_from = new Date(this.salesOrderOther[key].promo_from)
        this.salesOrderOther[key].promo_end = new Date(this.salesOrderOther[key].promo_end)
        var today = new Date()
        var date_today = today.setHours(0, 0, 0, 0)

        var promo_from = this.salesOrderOther[key].promo_from.getTime()
        var promo_end = this.salesOrderOther[key].promo_end.getTime()
        //console.log(date_today, promo_from, promo_end)
        if (date_today > promo_end)
          this.salesOrderOther[key].status = "Expired"
        else if (date_today < promo_from)
          this.salesOrderOther[key].status = "Not Ready Yet"
        else
          this.salesOrderOther[key].status = "Open"

        if (promo_from > date_today)
          this.salesOrderOther[key].disabled = "true"
        else if (date_today == promo_from)
          this.salesOrderOther[key].disabled = "false"
        else if (date_today > promo_end)
          this.salesOrderOther[key].disabled = "true"
        else if (date_today < promo_end)
          this.salesOrderOther[key].disabled = "false"

        for (const key1 in salesOrderList[0]) {
          if (salesOrderList[0][key1].discount_number == this.salesOrderOther[key].discount_number) {
            balance_pallet += parseInt(salesOrderList[0][key1].balance_pallet)
          }
        }
        if (balance_pallet <= 0)
          this.salesOrderOther[key].status = "Completed"
      }

      for (const key in this.salesOrderAdvance) {
        let balance_pallet = 0
        this.salesOrderAdvance[key].promo_from = new Date(this.salesOrderAdvance[key].promo_from)
        this.salesOrderAdvance[key].promo_end = new Date(this.salesOrderAdvance[key].promo_end)
        var today = new Date()
        var date_today = today.setHours(0, 0, 0, 0)

        var promo_from = this.salesOrderAdvance[key].promo_from.getTime()
        var promo_end = this.salesOrderAdvance[key].promo_end.getTime()
        //console.log(date_today, promo_from, promo_end)
        if (date_today > promo_end)
          this.salesOrderAdvance[key].status = "Expired"
        else if (date_today < promo_from)
          this.salesOrderAdvance[key].status = "Not Ready Yet"
        else
          this.salesOrderAdvance[key].status = "Open"

        if (promo_from > date_today)
          this.salesOrderAdvance[key].disabled = "true"
        else if (date_today == promo_from)
          this.salesOrderAdvance[key].disabled = "false"
        else if (date_today > promo_end)
          this.salesOrderAdvance[key].disabled = "true"
        else if (date_today < promo_end)
          this.salesOrderAdvance[key].disabled = "false"

        for (const key1 in salesOrderList[0]) {
          if (salesOrderList[0][key1].discount_number == this.salesOrderAdvance[key].discount_number) {
            balance_pallet += parseInt(salesOrderList[0][key1].balance_pallet)
          }
        }
        if (balance_pallet <= 0)
          this.salesOrderAdvance[key].status = "Completed"
      }
      this.spinner.hide()
      // console.log(this.salesOrderCash, this.salesOrderForecast, this.salesOrderProject, this.salesOrderOther)
    }, (error) => { console.log(error); this.spinner.hide() })
  }
  classActive() {
    this.salesPro1 = '';
    this.salesPro2 = '';
    this.salesPro3 = '';
    this.salesPro4 = '';
    this.salesPro5 = '';
    this.cashDiscount = false;
    this.forecastDiscount = false;
    this.projectDiscount = false;
    this.advancePayment = false;
    this.otherDiscount = false;
  }
  tab_salesPro(val) {
    this.classActive();
    if (val == 1) {
      this.salesPro1 = 'sales-promo-active';
      this.cashDiscount = true;
    } else if (val == 2) {
      this.salesPro2 = 'sales-promo-active';
      this.forecastDiscount = true;
    } else if (val == 3) {
      this.salesPro3 = 'sales-promo-active';
      this.projectDiscount = true;
    } else if (val == 4) {
      this.salesPro4 = 'sales-promo-active';
      this.advancePayment = true;
    } else {
      this.salesPro5 = 'sales-promo-active';
      this.otherDiscount = true;
    }
  }
  useVoucher(index, promoType) {
    console.log(index, promoType)
    if (promoType == "cash-discount") {
      // alert(this.salesOrderCash[index].discount_number)
      sessionStorage.setItem("voucherCode", this.salesOrderCash[index].discount_number)
      sessionStorage.setItem("promoType", this.salesOrderCash[index].promo_type)
    }
    else if (promoType == "forecast-discount") {
      sessionStorage.setItem("voucherCode", this.salesOrderForecast[index].discount_number)
      sessionStorage.setItem("promoType", this.salesOrderForecast[index].promo_type)
    }
    else if (promoType == "project-discount") {
      sessionStorage.setItem("voucherCode", this.salesOrderProject[index].discount_number)
      sessionStorage.setItem("promoType", this.salesOrderProject[index].promo_type)
    }
    else if (promoType == "advance-discount") {
      sessionStorage.setItem("voucherCode", this.salesOrderAdvance[index].discount_number)
      sessionStorage.setItem("promoType", this.salesOrderAdvance[index].promo_type)
    }
    else if (promoType == "other-discount") {
      sessionStorage.setItem("voucherCode", this.salesOrderOther[index].discount_number)
      sessionStorage.setItem("promoType", this.salesOrderOther[index].promo_type)
    }
    // localStorage.setItem("voucherCode", "00000000001")
    this.router.navigate(['/order-entry/cbu-order-entry']);
  }

  detail(discount_number) {
    if (discount_number) {
      this.spinner.show()
      this.salesPromoOrder.getDetailPromoVoucher(discount_number).subscribe((result) => {
        // console.log(result)
        if (result[0].promo_type == "Z14")
          this.salesOrderDetail.discount_name = "Cash Discount"
        else if (result[0].promo_type == "Z15")
          this.salesOrderDetail.discount_name = "Forecast Discount"
        else if (result[0].promo_type == "Z16")
          this.salesOrderDetail.discount_name = "Project Discount"
        else if (result[0].promo_type == "Z17")
          this.salesOrderDetail.discount_name = "Other Discount"

        this.salesOrderDetail.voucher_code = result[0].discount_number
        this.salesOrderDetail.valid_date = result[0].promo_from
        this.salesOrderDetail.expire_date = result[0].promo_end

        this.salesOrderDetailItem = result

        this.salesOrderDetailItemTotal.total_pallet = 0
        this.salesOrderDetailItemTotal.total_qty = 0
        this.salesOrderDetailItemTotal.used_pallet = 0
        this.salesOrderDetailItemTotal.used_qty = 0
        this.salesOrderDetailItemTotal.balance_pallet = 0
        this.salesOrderDetailItemTotal.balance_qty = 0

        for (const key in this.salesOrderDetailItem) {
          this.salesOrderDetailItemTotal.total_pallet += parseInt(this.salesOrderDetailItem[key].pallet)
          this.salesOrderDetailItemTotal.total_qty += parseInt(this.salesOrderDetailItem[key].qty)
          this.salesOrderDetailItemTotal.used_pallet += parseInt(this.salesOrderDetailItem[key].used_pallet)
          this.salesOrderDetailItemTotal.used_qty += parseInt(this.salesOrderDetailItem[key].used_qty)
          this.salesOrderDetailItemTotal.balance_pallet += parseInt(this.salesOrderDetailItem[key].balance_pallet)
          this.salesOrderDetailItemTotal.balance_qty += parseInt(this.salesOrderDetailItem[key].balance_qty)
        }

        this.aw1 = false;
        this.aw2 = true;
        this.spinner.hide()
      }, (error) => { console.log(error); this.spinner.hide() })
    }
  }

  back() {
    this.aw2 = false;
    this.aw1 = true;
  }

}
