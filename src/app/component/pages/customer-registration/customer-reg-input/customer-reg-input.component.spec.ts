import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerRegInputComponent } from './customer-reg-input.component';

describe('CustomerRegInputComponent', () => {
  let component: CustomerRegInputComponent;
  let fixture: ComponentFixture<CustomerRegInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerRegInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRegInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
