import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class OutstandingAndOverdueReportService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getOutstandingReport() {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${this.baseUrl}/api/outstanding-and-overdue/report/${sap_code}`
    );
  }

  getPaymentDetail(so_number,invoice_number) {
    return this.http.get(
      `${this.baseUrl}/api/outstanding-and-overdue/pay/${so_number}/${invoice_number}`
    );
  }
  
}
