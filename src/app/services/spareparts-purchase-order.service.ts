import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class SparepartsPurchaseOrderService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  download() {
    return this.http.get(
      `${this.baseUrl}/api/order-sparepart/download-template`
    );
  }

  getBillToCode() {
    var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(`${this.baseUrl}/api/bill-to-list/${sap_code}`);
  }

  getShipToCode() {
    var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(`${this.baseUrl}/api/ship-to-list/${sap_code}`);
  }

  getListMaterial(val) {
    var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${
        this.baseUrl
      }/api/order-sparepart/search-code/${sap_code}?search=${val}`
    );
  }

  getSupersession(val) {
    return this.http.get(
      `${this.baseUrl}/api/order-sparepart/search-super?search=${val}`
    );
  }

  getMaterial(val) {
    var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${
        this.baseUrl
      }/api/order-sparepart/search-code2/${sap_code}?search=${val}`
    );
  }

  getMaterialUpload(idtrs) {
    var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${this.baseUrl}/api/order-sparepart/upload-fix/${sap_code}/${idtrs}`
    );
  }

  getPaymentMessage(soNumber) {
    return this.http.get(`${this.baseUrl}/api/payment-message/${soNumber}`);
  }

  updateDraft(param, val) {
    return this.http.post(
      `${this.baseUrl}/api/order-sparepart/updateDraft/${param}`,
      val
    );
  }
  submitOrder(val) {
    // console.log(val);
    return this.http.post(`${this.baseUrl}/api/order-sparepart/confirm`, val);
  }

  storeHeaderOrder(val) {
    var data = JSON.stringify(val);
    var orderHeaderData = localStorage.setItem("orderHeaderData", data);
  }

  storeOutputOrder(val) {
    var data = JSON.stringify(val);
    var orderOutputData = localStorage.setItem("orderOutputData", data);
  }

  paymentOrder(val) {
    console.log(val);
    return this.http.post(`${this.baseUrl}/api/dpr`, val);
  }

  simulateOrder(val) {
    return this.http.post(`${this.baseUrl}/api/order-sparepart/simulate`, val);
  }

  saveAsDraft(val) {
    return this.http.post(`${this.baseUrl}/api/order-sparepart/draft`, val);
  }

  getDraftList() {
    var id = JSON.parse(localStorage.getItem("user")).id;
    return this.http.get(`${this.baseUrl}/api/order-sparepart/draftList/${id}`);
  }
  getDraft(tr_id) {
    return this.http.get(
      `${this.baseUrl}/api/order-sparepart/contDraft/${tr_id}`
    );
  }
}
