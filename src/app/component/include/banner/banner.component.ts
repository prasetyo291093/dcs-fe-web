import { Component, OnInit } from '@angular/core';
import { DealerProfileService } from 'src/app/services/dealer-profile.service';
import { environment } from "src/environments/environment";

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  imageBanner: any = []
  baseUrl = environment.baseUrl

  constructor(
    private dealer: DealerProfileService
  ) { }

  ngOnInit() {
    this.dealer.getDealerImage().subscribe(result => {
      for (const key in result) {
        if (result.hasOwnProperty(key)) {
          const element = result[key];
          this.imageBanner.push(this.baseUrl + element)
        }
      }
      console.log(this.imageBanner)
    })
  }

}
