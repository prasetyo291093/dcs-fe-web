import { TestBed } from '@angular/core/testing';

import { StockReportService } from './stock-report.service';

describe('StockReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StockReportService = TestBed.get(StockReportService);
    expect(service).toBeTruthy();
  });
});
