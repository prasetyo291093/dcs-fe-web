import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class DealerTargetCbuService {

  constructor(
    private http: HttpClient
  ) { }

  baseUrl = environment.baseUrl

  getDealerTargetCBU(year) {
  let sap_code: string = JSON.parse(localStorage.getItem('user')).sap_code
    return this.http.get(`${this.baseUrl}/api/dealer-target/report-cbu/${sap_code}/${year}`)
  }

  getYearList() {
  let sap_code: string = JSON.parse(localStorage.getItem('user')).sap_code
    return this.http.get(`${this.baseUrl}/api/dealer-target/get-year-cbu/${sap_code}`)
  }
}
