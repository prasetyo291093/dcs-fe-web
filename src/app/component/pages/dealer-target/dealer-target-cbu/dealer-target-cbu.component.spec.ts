import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealerTargetCbuComponent } from './dealer-target-cbu.component';

describe('DealerTargetCbuComponent', () => {
  let component: DealerTargetCbuComponent;
  let fixture: ComponentFixture<DealerTargetCbuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealerTargetCbuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealerTargetCbuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
