import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogisticClaimFormSparepartsComponent } from './logistic-claim-form-spareparts.component';

describe('LogisticClaimFormSparepartsComponent', () => {
  let component: LogisticClaimFormSparepartsComponent;
  let fixture: ComponentFixture<LogisticClaimFormSparepartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogisticClaimFormSparepartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogisticClaimFormSparepartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
