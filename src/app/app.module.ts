import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { DealerProfileComponent } from './component/pages/dealer-profile/dealer-profile.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatButtonModule} from '@angular/material';
import { routes } from './app.routes';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './component/pages/login/login.component';
import { HeaderComponent } from './component/include/header/header.component';
import { SidebarComponent } from './component/include/sidebar/sidebar.component';
import { BannerComponent } from './component/include/banner/banner.component';
import { OrderReviewComponent } from './component/include/order-review/order-review.component';
import { AuthService } from './services/auth.service';
import { CbuPurchaseOrderService } from './services/cbu-purchase-order.service';
import { SparepartsPurchaseOrderService } from './services/spareparts-purchase-order.service';
import { InquiryCbuService } from './services/inquiry-cbu.service';
import { DealerProfileService } from './services/dealer-profile.service';
import { UserManagementService } from './services/user-management.service';
import { MainPageComponent } from './component/main-page/main-page/main-page.component';
import { AccountInformationComponent } from './component/pages/account-information/account-information.component';
import { ForgotPasswordComponent } from './component/pages/forgot-password/forgot-password.component';
import { CbuPurchaseOrderComponent } from './component/pages/order-entry/cbu-purchase-order/cbu-purchase-order.component';
import { SparepartsPurchaseOrderComponent } from './component/pages/order-entry/spareparts-purchase-order/spareparts-purchase-order.component';
import { PendingTransactionComponent } from './component/pages/order-entry/pending-transaction/pending-transaction.component';
import { BackOrderComponent } from './component/pages/order-entry/back-order/back-order.component';
import { OrderStatusComponent } from './component/pages/order-entry/order-status/order-status.component';
import { DeliveryInformationComponent } from './component/pages/order-entry/delivery-information/delivery-information.component';
import { InquiryCbuComponent } from './component/pages/inquiry/inquiry-cbu/inquiry-cbu.component';
import { InquirySparepartsComponent } from './component/pages/inquiry/inquiry-spareparts/inquiry-spareparts.component';
import { UserManagementComponent } from './component/pages/user-management/user-management.component';
import { ChangePasswordComponent } from './component/pages/change-password/change-password.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { PopoverModule } from 'ngx-bootstrap/popover';
import {NgxWebstorageModule} from 'ngx-webstorage';
import { FusionChartsModule } from 'angular-fusioncharts';
import { NgxSpinnerModule } from 'ngx-spinner';
// Load FusionCharts
import * as FusionCharts from 'fusioncharts';
// Load Charts module
import * as Charts from 'fusioncharts/fusioncharts.charts';
// Load fusion theme
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import { ProductCheckingComponent } from './component/pages/product-checking/product-checking.component';
import { SalesPromoComponent } from './component/pages/sales-promo/sales-promo.component';
import { InformationSharingComponent } from './component/pages/information-sharing/information-sharing.component';
import { ApAgingComponent } from './component/pages/outstanding-and-overdue/ap-aging/ap-aging.component';
import { ReportComponent } from './component/pages/outstanding-and-overdue/report/report.component';
import { InvoiceTimelinessComponent } from './component/pages/outstanding-and-overdue/invoice-timeliness/invoice-timeliness.component';
import { ForecastEntryComponent } from './component/pages/forecast/forecast-entry/forecast-entry.component';
import { ForecastReportComponent } from './component/pages/forecast/forecast-report/forecast-report.component';
import { DealerTargetCbuComponent } from './component/pages/dealer-target/dealer-target-cbu/dealer-target-cbu.component';
import { DealerTargetSparepartsComponent } from './component/pages/dealer-target/dealer-target-spareparts/dealer-target-spareparts.component';
import { CustomerRegInputComponent } from './component/pages/customer-registration/customer-reg-input/customer-reg-input.component';
import { CustomerRegReportComponent } from './component/pages/customer-registration/customer-reg-report/customer-reg-report.component';
import { CbuRewardComponent } from './component/pages/dealer-reward/cbu-reward/cbu-reward.component';
import { SparepartsRewardComponent } from './component/pages/dealer-reward/spareparts-reward/spareparts-reward.component';
import { StockInputComponent } from './component/pages/dealer-stock/stock-input/stock-input.component';
import { StockReportComponent } from './component/pages/dealer-stock/stock-report/stock-report.component';
import { WarrantyClaimFormComponent } from './component/pages/claim/warranty-claim-form/warranty-claim-form.component';
import { WarrantyClaimReportComponent } from './component/pages/claim/warranty-claim-report/warranty-claim-report.component';
import { LogisticClaimFormCbuComponent } from './component/pages/claim/logistic-claim-form-cbu/logistic-claim-form-cbu.component';
import { LogisticClaimReportCbuComponent } from './component/pages/claim/logistic-claim-report-cbu/logistic-claim-report-cbu.component';
import { LogisticClaimFormSparepartsComponent } from './component/pages/claim/logistic-claim-form-spareparts/logistic-claim-form-spareparts.component';
import { LogisticClaimReportSparepartsComponent } from './component/pages/claim/logistic-claim-report-spareparts/logistic-claim-report-spareparts.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxGalleryModule } from 'ngx-gallery';
import { ResetPasswordComponent } from './component/pages/reset-password/reset-password.component';
import { HowToPayComponent } from './component/pages/terms-condition/how-to-pay/how-to-pay.component';
import { DeliveryPolicyComponent } from './component/pages/terms-condition/delivery-policy/delivery-policy.component';
import { ReturnPolicyComponent } from './component/pages/terms-condition/return-policy/return-policy.component';
import { CustomerDataComponent } from './component/pages/terms-condition/customer-data/customer-data.component';
import { TermsConditionComponent } from './component/pages/terms-condition/terms-condition/terms-condition.component';
import { RedirectComponent } from './component/pages/redirect/redirect/redirect.component';
import { OutstandingInvoiceCbuComponent } from './component/pages/outstanding-invoice-cbu/outstanding-invoice-cbu.component';
import { OutstandingInvoiceSparepartsComponent } from './component/pages/outstanding-invoice-spareparts/outstanding-invoice-spareparts.component';
import { MaintenanceComponent } from './component/pages/maintenance/maintenance.component';

// Add dependencies to FusionChartsModule
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme)

@NgModule({
  declarations: [
    AppComponent,
    DealerProfileComponent,
    LoginComponent,
    HeaderComponent,
    SidebarComponent,
    BannerComponent,
    OrderReviewComponent,
    MainPageComponent,
    AccountInformationComponent,
    ForgotPasswordComponent,
    CbuPurchaseOrderComponent,
    SparepartsPurchaseOrderComponent,
    PendingTransactionComponent,
    BackOrderComponent,
    OrderStatusComponent,
    DeliveryInformationComponent,
    InquiryCbuComponent,
    InquirySparepartsComponent,
    UserManagementComponent,
    ChangePasswordComponent,
    ProductCheckingComponent,
    SalesPromoComponent,
    InformationSharingComponent,
    ApAgingComponent,
    ReportComponent,
    InvoiceTimelinessComponent,
    ForecastEntryComponent,
    ForecastReportComponent,
    DealerTargetCbuComponent,
    DealerTargetSparepartsComponent,
    CustomerRegInputComponent,
    CustomerRegReportComponent,
    CbuRewardComponent,
    SparepartsRewardComponent,
    StockInputComponent,
    StockReportComponent,
    WarrantyClaimFormComponent,
    WarrantyClaimReportComponent,
    LogisticClaimFormCbuComponent,
    LogisticClaimReportCbuComponent,
    LogisticClaimFormSparepartsComponent,
    LogisticClaimReportSparepartsComponent,
    ResetPasswordComponent,
    HowToPayComponent,
    DeliveryPolicyComponent,
    ReturnPolicyComponent,
    CustomerDataComponent,
    TermsConditionComponent,
    RedirectComponent,
    OutstandingInvoiceCbuComponent,
    OutstandingInvoiceSparepartsComponent,
    MaintenanceComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatButtonModule,
    ReactiveFormsModule,
    routes,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    TypeaheadModule.forRoot(),
    PopoverModule.forRoot(),
    NgxWebstorageModule.forRoot(),
    FusionChartsModule,
    NgxSpinnerModule,
    ModalModule.forRoot(),
    NgxGalleryModule
  ],
  providers: [
    AuthService,
    CbuPurchaseOrderService,
    SparepartsPurchaseOrderService,
    DealerProfileService,
    UserManagementService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class AppRoutingModule { }
