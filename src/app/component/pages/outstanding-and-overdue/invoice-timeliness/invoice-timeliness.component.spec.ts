import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceTimelinessComponent } from './invoice-timeliness.component';

describe('InvoiceTimelinessComponent', () => {
  let component: InvoiceTimelinessComponent;
  let fixture: ComponentFixture<InvoiceTimelinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceTimelinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceTimelinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
