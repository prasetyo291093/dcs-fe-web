import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CbuRewardComponent } from './cbu-reward.component';

describe('CbuRewardComponent', () => {
  let component: CbuRewardComponent;
  let fixture: ComponentFixture<CbuRewardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CbuRewardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CbuRewardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
