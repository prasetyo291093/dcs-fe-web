import { TestBed } from '@angular/core/testing';

import { InquiryCbuService } from './inquiry-cbu.service';

describe('InquiryCbuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InquiryCbuService = TestBed.get(InquiryCbuService);
    expect(service).toBeTruthy();
  });
});
