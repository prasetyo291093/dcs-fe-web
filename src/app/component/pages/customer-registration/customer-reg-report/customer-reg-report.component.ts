import { Component, OnInit } from "@angular/core";
import { CustomerRegReportService } from "src/app/services/customer-reg-report.service";
import { CustomerRegInputService } from "src/app/services/customer-reg-input.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import * as moment from "moment";

@Component({
  selector: "app-customer-reg-report",
  templateUrl: "./customer-reg-report.component.html",
  styleUrls: ["./customer-reg-report.component.css"]
})
export class CustomerRegReportComponent implements OnInit {
  modalRef: BsModalRef;
  customerReport: any = [];
  listMatCode: any = [];
  listFiltered: any = [];
  regisDateFrom: any;
  regisDateTo: any;
  sold_to_code: string;
  engineNumber: string;
  materialGroup: string;
  materialDesc: string;
  frameNumber: string;
  materialCode: string;
  freshData: boolean = true;
  filteredData: boolean = false;
  listMatGroup: any = [];
  listEngineNumber: any = [];
  listFrameNumber: any = [];
  report: boolean = true;
  detail: boolean = false;
  number: number;
  appliedForReq: string;
  appliedForInput: string;
  showAppliedFor: boolean = false;
  ownerName: string;
  dateOfPurchase: string;
  phone: string;
  address: string;
  district: string;
  provinceList: any = [];
  province: string;
  subdealerName: string;
  listSubDealer: any = [];
  subdealerPhone: string;

  constructor(
    private customerRegInput: CustomerRegInputService,
    private customerRegisReport: CustomerRegReportService,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    private router: Router,
  ) {}

  btnFilter: boolean = false;
  resetFilter: boolean = false;

  ngOnInit() {
    this.btnFilter = true;
    this.spinner.show();
    this.customerRegisReport.getCustomerReport().subscribe(
      result => {
        if (Object.entries(result).length > 0) {
          this.customerReport = result;
          let user = JSON.parse(localStorage.getItem("user"));
          for (const item of this.customerReport) {
            let input_date = moment(item.created_at);
            item.input_date = input_date.format("DD MMMM YYYY");
            item.waranty_valid = input_date.add(1, "y").format("DD MMMM YYYY");
            item.input_by = user.first_name;
          }

          let mapMatCode = new Map();
          for (const item of this.customerReport) {
            if (!mapMatCode.has(item.material_code)) {
              mapMatCode.set(item.material_code, true);
              this.listMatCode.push(item.material_code);
            }
          }

          let mapMatGroup = new Map();
          for (const item of this.customerReport) {
            if (!mapMatGroup.has(item.material_group)) {
              mapMatGroup.set(item.material_group, true);
              this.listMatGroup.push(item.material_group);
            }
          }

          let mapEngineNumber = new Map();
          for (const item of this.customerReport) {
            if (item.engine_number) {
              if (!mapEngineNumber.has(item.engine_number)) {
                mapEngineNumber.set(item.engine_number, true);
                this.listEngineNumber.push(item.engine_number);
              }
            }
          }

          let mapFrameNumber = new Map();
          for (const item of this.customerReport) {
            if (item.frame_number) {
              if (!mapFrameNumber.has(item.frame_number)) {
                mapFrameNumber.set(item.frame_number, true);
                this.listFrameNumber.push(item.frame_number);
              }
            }
          }

        }
        this.spinner.hide();
      },
      error => {
        console.error(error);
        this.spinner.hide();
      }
    );
  }

  confirm(confirmModal) {
    this.modalRef = this.modalService.show(confirmModal);
  }

  deleteModal(confirmModal,number){
    this.number = number;
    this.confirm(confirmModal)
  }

  delete(number) {
    this.spinner.show()
    this.customerRegisReport.deleteCustomerReport(number).subscribe(result => {
      location.reload()
    }, error => {
      console.error(error)
      this.spinner.hide()
    })
  }

  filter() {
    this.btnFilter = false;
    this.resetFilter = true;
  }
  resetFil() {
    delete this.regisDateFrom;
    delete this.regisDateTo;
    delete this.materialGroup;
    delete this.materialCode;
    delete this.engineNumber;
    delete this.frameNumber;

    this.freshData = true
    this.filteredData = false
  }

  refresh(): void {
    window.location.reload();
  }

  submitFilter() {
    this.listFiltered = this.customerReport;

    this.listFiltered = this.listFiltered.filter(item => {
      let result: any;

      let input = new Date(item.created_at);
      let from = new Date(this.regisDateFrom);
      let to = new Date(this.regisDateTo);

      input.setHours(0, 0, 0, 0);
      from.setHours(0, 0, 0, 0);
      to.setHours(0, 0, 0, 0);

      if (!isNaN(from.getTime())) {
        result = input.getTime() == from.getTime();
      }

      if (!isNaN(to.getTime())) {
        result = input.getTime() == to.getTime();
      }

      if (this.materialGroup) {
        result = item.material_group == this.materialGroup;
      }

      if (this.materialCode) {
        result = item.material_code == this.materialCode;
      }

      if (this.engineNumber) {
        result = item.engine_number == this.engineNumber;
      }

      if (this.frameNumber) {
        result = item.frame_number == this.frameNumber;
      }

      if (!isNaN(from.getTime()) && !isNaN(to.getTime())) {
        result =
          input.getTime() >= from.getTime() && input.getTime() <= to.getTime();
      }

      if (
        !isNaN(from.getTime()) &&
        !isNaN(to.getTime()) &&
        this.materialGroup &&
        this.materialCode &&
        this.engineNumber &&
        this.frameNumber
      ) {
        result =
          input.getTime() >= from.getTime() &&
          input.getTime() <= to.getTime() &&
          item.material_group == this.materialGroup &&
          item.material_code == this.materialCode &&
          item.engine_number == this.engineNumber &&
          item.frame_number == this.frameNumber;
      }

      return result;
    });

    this.freshData = false;
    this.filteredData = true;
  }

  downloadReport() {
    let data, filename, link, customerRegisData;

    customerRegisData =
      this.filteredData == true ? this.listFiltered : this.customerReport;

    for (const item of customerRegisData) {
      if (item.frame_number == null) item.frame_number = "";
      delete item.created_at;
      delete item.updated_at;
      delete item.deleted_at;
    }

    let csv = this.convertToCSV({
      data: customerRegisData
    });

    if (csv == null) return;
    filename = "Report Customer Registration.csv";

    if (!csv.match(/^data:text\/csv/i)) {
      csv = "data:text/csv;charset=utf-8," + csv;
    }

    data = encodeURI(csv);
    link = document.createElement("a");
    link.setAttribute("href", data);
    link.setAttribute("download", filename);
    link.click();

    customerRegisData =
      this.filteredData == true ? this.listFiltered : this.customerReport;
  }
  convertToCSV(args) {
    let result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
      return null;
    }

    columnDelimiter = args.columnDelimiter || ",";
    lineDelimiter = args.lineDelimiter || "\n";

    keys = Object.keys(data[0]);

    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(item => {
      ctr = 0;
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter;

        result += item[key];
        ctr++;
      });
      result += lineDelimiter;
    });

    return result;
  }

  edit(number) {
    console.log(number)
    this.spinner.show()
    this.customerRegisReport.editCustomerReport(number).subscribe(result => {
      this.report = false;
      this.detail = true;
      this.sold_to_code = result['sold_to_code']
      this.engineNumber = result['engine_number']
      this.frameNumber = result['frame_number']
      this.materialGroup = result['material_group']
      this.materialGroup = result['material_group']
      if (this.materialGroup == "Engine")
          this.showAppliedFor = true
      this.materialCode = result['material_code']
      this.materialDesc = result['material_description']
      this.ownerName = result['owner_name']
      this.dateOfPurchase = result['date_of_purchase']
      this.phone = result['phone']
      this.address = result['address']
      this.district = result['district']
      this.province = result['province']
      this.subdealerPhone = result['sub_dealer_phone']
      console.log(result)
      this.customerRegInput.getListProvince().subscribe(result => {
        if (Object.entries(result).length > 0) {
          // this.province = result[0]["province"]
          for (const item in result) {
            this.provinceList.push(result[item]["name"])
          }
        }
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        console.error(error)
      })
  
      this.listSubDealer = []
      this.subdealerName = result['sub_dealer_name']
      this.customerRegInput.getListSubDealer(localStorage.getItem("dealer_name")).subscribe(result => {
        console.log(result)
        if (Object.entries(result).length > 0) {
          // this.province = result[0]["province"]
          for (const item in result) {
            this.listSubDealer.push(result[item])
          }
        }
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        console.error(error)
      })

    }, error => {
      console.log(error)
      this.spinner.hide()
    })
    // sessionStorage.setItem("promoType", this.salesOrderCash[index].promo_type)
  }

  appliedFor(type) {
    // if (type == "Others") this.appliedForReq = this.appliedForInput;
    // else
    this.appliedForReq = type;
    console.log(this.appliedForReq);
  }

  updateCustomer() {
    let data = {
      sold_to_code: JSON.parse(localStorage.getItem("user")).sap_code,
      engine_number: this.engineNumber,
      frame_number: this.frameNumber,
      material_group: this.materialGroup,
      material_code: this.materialCode.slice(-6),
      material_description: this.materialDesc,
      applied_for: "",
      owner_name: this.ownerName,
      date_of_purchase: this.dateOfPurchase,
      phone: this.phone,
      address: this.address,
      district: this.district,
      province: this.province,
      sub_dealer_name: this.subdealerName,
      sub_dealer_phone: this.subdealerPhone,
      user: JSON.parse(localStorage.getItem("user")).email
    };

    if (this.appliedForReq == "Others") data.applied_for = this.appliedForInput;
    else data.applied_for = this.appliedForReq;

    if (
      !this.ownerName ||
      !this.dateOfPurchase ||
      // !this.phone ||
      !this.address ||
      !this.district ||
      !this.province ||
      !this.subdealerName
      // !this.subdealerPhone
    ) {
      alert("All mandatory must be filled");
      return
    }
    this.spinner.show()
    console.log(data)
    this.customerRegInput.updateCustomer(data).subscribe(
      result => {
        console.log(result)
        if (result["message"] == "Success") {
          alert(result["message"]);
          // this.router.navigate(["custom-registration/custom-registration-report"]);
          this.refresh()

        }
        else alert(result["message"]);
      },
      error => {
        console.error(error)
        this.spinner.hide()
      }
    );
  }
  
}
