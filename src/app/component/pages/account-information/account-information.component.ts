import { Component, OnInit, Sanitizer } from "@angular/core";
import { DealerProfileService } from "../../../services/dealer-profile.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
import { NgxSpinnerService } from 'ngx-spinner';

declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-account-information",
  templateUrl: "./account-information.component.html",
  styleUrls: ["./account-information.component.css"]
})
export class AccountInformationComponent implements OnInit {
  // information: any;

  model: any = {};

  tabAcc1: string;
  tabAcc2: string;
  tabAcc3: string;
  imageSrc: any;
  UpSatu: any;

  tabAccount1: boolean;
  tabAccount2: boolean;
  tabAccount3: boolean;

  branch1: string;
  branch2: string;
  branch3: string;

  number_of_technician: string;
  number_of_sales: string;
  number_of_employee: string;
  branch_name1: string;
  branch_desc1: string;
  branch_name2: string;
  branch_desc2: string;
  branch_name3: string;
  branch_desc3: string;
  website: string;
  additional_information: string;
  product_handling: string;
  pProfile: string;
  imgGal1: string;
  imgGal2: string;
  imgGal3: string;
  imgGal4: string;

  public acc_info_1: boolean = true;
  public acc_info_2: boolean = false;

  public account_info_edit: boolean = true;
  public uploadGal: boolean = false;
  public accountBranch: boolean = false;

  public inputBranch1: boolean = true;
  public inputBranch2: boolean = false;
  canUpdateDealerInformation: boolean = false;

  constructor(
    private router: Router,
    private accountinfo: DealerProfileService,
    private spinner: NgxSpinnerService,
    private sanitized: DomSanitizer
  ) {}

  ngOnInit() {
    var user = JSON.parse(localStorage.getItem("user")).sap_code;
    // this.canUpdateDealerInformation = JSON.parse(localStorage.getItem("user")).permissions["Update Dealer Information"]
    var photoProfile = JSON.parse(localStorage.getItem("user"))
      .profile_picture_path;
      this.spinner.show()
    this.accountinfo.getInfoUser(user).subscribe(
      res => {
        console.log(res);
        this.pProfile =
          "data:image/jpeg;base64," + res["profile_picture"].toString();
        this.number_of_technician = res["number_of_technician"];
        this.number_of_sales = res["number_of_sales"];
        this.number_of_employee = res["number_of_employee"];
        this.branch_name1 = res["branch_name1"];
        this.branch_desc1 = res["branch_desc1"];
        this.branch_name2 = res["branch_name2"];
        this.branch_desc2 = res["branch_desc2"];
        this.branch_name3 = res["branch_name3"];
        this.branch_desc3 = res["branch_desc3"];
        this.website = res["website"];
        this.additional_information = res["additional_information"];
        this.product_handling = res["product_handling"];

        this.imgGal1 = this.imgURL1 =
          "data:image/jpeg;base64, " + res["store_photo1"].toString();
        this.imgGal2 = this.imgURL2 =
          "data:image/jpeg;base64, " + res["store_photo2"].toString();
        this.imgGal3 = this.imgURL3 =
          "data:image/jpeg;base64, " + res["store_photo3"].toString();
        this.imgGal4 = this.imgURL4 =
          "data:image/jpeg;base64, " + res["store_photo4"].toString();
          this.spinner.hide()
      },
      err => {
        alert("Data doesn't exists");
        this.spinner.hide()
      }
    );

    this.tabAcc1 = "tab-account-active";
    this.tabAccount1 = true;
    this.UpSatu = true;
  }

  classActive() {
    this.tabAcc1 = "";
    this.tabAcc2 = "";
    this.tabAcc3 = "";
    this.tabAccount1 = false;
    this.tabAccount2 = false;
    this.tabAccount3 = false;
  }

  tab_account(val) {
    this.classActive();
    if (val == 1) {
      this.tabAcc1 = "tab-account-active";
      this.tabAccount1 = true;
      this.account_info_edit = true;
      this.uploadGal = false;
      this.accountBranch = false;
    } else if (val == 2) {
      this.tabAcc2 = "tab-account-active";
      this.tabAccount2 = true;
      this.account_info_edit = false;
      this.uploadGal = true;
      this.accountBranch = false;
    } else {
      this.tabAcc3 = "tab-account-active";
      this.tabAccount3 = true;
      this.account_info_edit = false;
      this.uploadGal = false;
      this.accountBranch = true;
    }
  }

  updateAcc() {
    this.acc_info_1 = !this.acc_info_1;
    this.acc_info_1 ? (this.acc_info_2 = false) : (this.acc_info_2 = true);
  }

  disAcc() {
    this.acc_info_1 = !this.acc_info_1;
    this.acc_info_1 ? (this.acc_info_2 = false) : (this.acc_info_2 = true);
  }

  updateAcc2() {
    this.inputBranch1 = !this.inputBranch1;
    this.inputBranch1
      ? (this.inputBranch2 = false)
      : (this.inputBranch2 = true);
  }

  disAcc2() {
    this.inputBranch1 = !this.inputBranch1;
    this.inputBranch1
      ? (this.inputBranch2 = false)
      : (this.inputBranch2 = true);
  }

  saveDealerInformation() {
    var data = {
      number_of_technician: this.number_of_technician,
      number_of_sales: this.number_of_sales,
      number_of_employee: this.number_of_employee,
      product_handling: this.product_handling,
      website: this.website,
      additional_information: this.additional_information
    };
    data.number_of_technician = data.number_of_technician.toString();
    data.number_of_sales = data.number_of_sales.toString();
    data.number_of_employee = data.number_of_employee.toString();
    data.product_handling = data.product_handling.toString();
    data.website = data.website.toString();
    data.additional_information = data.additional_information.toString();
    this.accountinfo.setDealerInformation(data).subscribe(
      res => {
        var data = [];
        data.push(res);
        if (data[0].message == "Success") {
          this.router.navigate(["dealer-profile"]);
        } else {
          alert("Please Review Again.");
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  saveBranchInformation() {
    var data = {
      branch_name1: this.branch_name1,
      branch1: this.branch_desc1,
      branch_name2: this.branch_name2,
      branch2: this.branch_desc2,
      branch_name3: this.branch_name3,
      branch3: this.branch_desc3
    };
    data.branch_name1 = data.branch_name1.toString();
    data.branch1 = data.branch1.toString();
    data.branch_name2 = data.branch_name2.toString();
    data.branch2 = data.branch2.toString();
    data.branch_name3 = data.branch_name3.toString();
    data.branch3 = data.branch3.toString();
    this.accountinfo.setBranchInformation(data).subscribe(
      res => {
        var data = [];
        data.push(res);
        if (data[0].message == "Success") {
          this.router.navigate(["dealer-profile"]);
        } else {
          alert("Please Review Again.");
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  readURL(event: any) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = e => {
        $(".borPP").css({
          background: "url(" + reader.result + ")no-repeat center",
          "background-size": "cover!important"
        });
        let base64data = reader.result.toString();
        let data = {
          base64: base64data.split(",")[1]
        };
        this.accountinfo.updateDealerPhotoProfile(data).subscribe(
          result => {
            console.log(result);
            alert("Success!");
          },
          error => {
            console.log(error);
          }
        );
      };
      reader.readAsDataURL(file);
    }
  }

  handleIdCard(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = e => {
        let base64data = reader.result.toString();
        console.log(event.target.files);
        let data = {
          base64: base64data.split(",")[1],
          file_name: event.target.files[0].name
        };
        this.accountinfo.setDealerIdCard(data).subscribe(
          result => {
            if (result["message"] == "Success") alert("ID Card saved.");
          },
          error => {
            console.error(error);
          }
        );
      };
      reader.readAsDataURL(file);
    }
  }

  // urls = new Array<string>();
  // detectFiles(event) {
  //   this.urls = [];
  //   let files = event.target.files;
  //   if (files) {
  //     for (let file of files) {
  //       let reader = new FileReader();
  //       reader.onload = (e: any) => {
  //         this.urls.push(e.target.result);
  //       }
  //       reader.readAsDataURL(file);
  //     }
  //   }
  // }
  public imagePath;
  imgURL1: any;
  imgURL2: any;
  imgURL3: any;
  imgURL4: any;
  deleteImg: any;
  inputUpSatu: any;

  preview1(event: any) {
    console.log(event.target.files);
    if (event.target.files.length === 0) return;

    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = _event => {
      this.imgURL1 = this.imgGal1 = reader.result.toString();
      //   let data = {
      //     sap_code: JSON.parse(localStorage.getItem("user")).sap_code,
      //     path: event.target.files[0].name,
      //     base64: reader.result.toString()
      //   };
      //   console.log(data);
      //   this.accountinfo.setDealerStorePhoto(data, 1).subscribe(
      //     result => {
      //       console.log(result);
      //     },
      //     error => {
      //       console.log(error);
      //     }
      //   );
    };
  }

  preview2(event: any) {
    var files = event.target.files;
    if (files.length === 0) return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = _event => {
      this.imgURL2 = this.imgGal2 = reader.result.toString();

      // let data = {
      //   sap_code: JSON.parse(localStorage.getItem("user")).sap_code,
      //   path: event.target.files[0].name,
      //   base64: reader.result.toString()
      // };
      // this.accountinfo.setDealerStorePhoto(data, 2).subscribe(
      //   result => {
      //     console.log(result);
      //   },
      //   error => {
      //     console.log(error);
      //   }
      // );
    };
  }
  preview3(event: any) {
    var files = event.target.files;
    if (files.length === 0) return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = _event => {
      this.imgURL3 = this.imgGal3 = reader.result.toString();

      // let data = {
      //   sap_code: JSON.parse(localStorage.getItem("user")).sap_code,
      //   path: event.target.files[0].name,
      //   base64: reader.result.toString()
      // };
      // this.accountinfo.setDealerStorePhoto(data, 3).subscribe(
      //   result => {
      //     console.log(result);
      //   },
      //   error => {
      //     console.log(error);
      //   }
      // );
    };
  }
  preview4(event: any) {
    var files = event.target.files;
    if (files.length === 0) return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = _event => {
      this.imgURL4 = this.imgGal4 = reader.result.toString();

      // let data = {
      //   sap_code: JSON.parse(localStorage.getItem("user")).sap_code,
      //   path: event.target.files[0].name,
      //   base64: reader.result.toString()
      // };
      // this.accountinfo.setDealerStorePhoto(data, 4).subscribe(
      //   result => {
      //     console.log(result);
      //   },
      //   error => {
      //     console.log(error);
      //   }
      // );
    };
  }

  updateStorePhoto() {
    let data = {
      store_photo1: this.imgURL1.split(",")[1],
      store_photo2: this.imgURL2.split(",")[1],
      store_photo3: this.imgURL3.split(",")[1],
      store_photo4: this.imgURL4.split(",")[1]
    };
    console.log(data);
    this.accountinfo.updateStorePhoto(data).subscribe(
      result => {
        if(result["msg"] == "Success") alert("Store Photos successfully saved.")
      },
      (error: Error) => {
        console.error(error.message);
      }
    );
  }
}
