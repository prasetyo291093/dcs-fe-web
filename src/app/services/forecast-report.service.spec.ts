import { TestBed } from '@angular/core/testing';

import { ForecastReportService } from './forecast-report.service';

describe('ForecastReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ForecastReportService = TestBed.get(ForecastReportService);
    expect(service).toBeTruthy();
  });
});
