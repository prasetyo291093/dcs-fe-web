import { Component, OnInit } from "@angular/core";
import { StockInputService } from "src/app/services/stock-input.service";
import { formatDate } from "@angular/common";
import { NgxSpinnerService } from "ngx-spinner";
declare var jquery: any;
declare var $: any;
declare var number: any;

declare var jquery: any;
declare var $: any;
declare var number: any;

@Component({
  selector: "app-stock-input",
  templateUrl: "./stock-input.component.html",
  styleUrls: ["./stock-input.component.css"]
})
export class StockInputComponent implements OnInit {
  userGroup: string;

  marineGen: any[];
  marineOutboard: any[];
  powerProductGenerator: any[];
  powerProductBlower: any;
  powerProductBrushCutter: any[];
  powerProductBrushMower: any[];
  powerProductLawnmower: any[];
  powerProductMonpal: any[];
  powerProductPump: any[];
  powerProductRidingMower: any[];
  powerProductBackpackSprayer: any[];
  powerProductTiller: any[];
  powerProductWaterPump: any[];
  powerProductLawnGarden: any[];
  powerProductAgreeculture: any[];
  powerProductAccessories: any[];
  powerProductEnginePP: any[];

  jstoday: any;
  disableForm: boolean;
  disableButton: boolean;
  allMarine: any = [];
  allPowerProduct: any = [];

  canInputUploadDealerStock: boolean = false

  constructor(
    private dealerStock: StockInputService,
    private spinner: NgxSpinnerService
  ) {
    this.jstoday = formatDate(Date.now(), "dd-MM-yyyy", "en-US", "+0700");
  }

  salesPro1: string;
  salesPro2: string;
  salesPro3: string;
  salesPro4: string;
  salesPro5: string;
  salesPro6: string;
  salesPro7: string;
  salesPro8: string;
  salesPro9: string;
  salesPro10: string;
  salesPro11: string;
  salesPro12: string;
  salesPro13: string;
  salesPro14: string;
  salesPro15: string;
  salesPro16: string;
  salesPro17: string;

  generator: boolean;
  blower: boolean;
  brushCutter: boolean;
  brushMower: boolean;
  lawnmower: boolean;
  monpal: boolean;
  pump: boolean;
  ridingMower: boolean;
  backpackSlayer: boolean;
  tiller: boolean;
  waterPump: boolean;
  lawnGarden: boolean;
  agreeculture: boolean;
  accecories: boolean;
  engine: boolean;

  outboard: boolean;
  gen: boolean;

  cat1: boolean;
  cat2: boolean;

  ngOnInit() {
    // this.spinner.show()
    this.salesPro1 = "sales-promo-active";
    this.generator = true;
    this.salesPro16 = "sales-promo-active";
    this.outboard = true;
    this.cat1 = true;

    this.canInputUploadDealerStock = JSON.parse(localStorage.getItem("user")).permissions["Input or Upload and preview Dealer Stock"]

    this.userGroup = JSON.parse(localStorage.getItem("group"));
    this.onDealerStockInit(this.userGroup);

    let dateToday = new Date();
    let dateT = dateToday.setHours(0, 0, 0, 0);

    var dd = String(dateToday.getDate()).padStart(2, '0');
    var mm = String(dateToday.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = dateToday.getFullYear();

    let endDate = new Date(
      yyyy
      ,parseInt(dd) < 5 ? parseInt(mm) + 1 : parseInt(mm)
      ,4);

    let startingDate = new Date(
      yyyy
      ,parseInt(dd) < 5 ? parseInt(mm) - 2 : parseInt(mm) - 1
      ,28
    );

    console.log(startingDate)
    console.log(endDate)
    
    if (
      dateT >= startingDate.getTime() &&
      dateT <= endDate.getTime()
    ) {
    this.disableForm = false;
    this.disableButton = false;
    } else {
      this.disableForm = true;
      this.disableButton = true;
    }

    // console.log(this.userGroup)
    // this.spinner.hide()
  }

  onDealerStockInit(userGroup) {
    if (userGroup == "001") {
      this.cat1 = false;
      this.cat2 = true;
      this.getDealerStockMarine(userGroup);
    } else if (userGroup == "002") {
      this.cat1 = true;
      this.cat2 = false;
      this.getDealerStockPowerProduct(userGroup);
    } else {
      this.getDealerStockMarine("001");
      this.getDealerStockPowerProduct("002");
    }
  }
  getDealerStockPowerProduct(userGroup: any): any {
    this.spinner.show();
    this.dealerStock.getMaterialDealerStock(userGroup).subscribe(
      result => {
        let results = [];
        results.push(result);
        this.allPowerProduct = result
        this.powerProductGenerator = results[0].filter(
          val => val.material_group == "001"
        );
        this.powerProductBlower = results[0].filter(
          val => val.material_group == "003"
        );
        this.powerProductBrushCutter = results[0].filter(
          val => val.material_group == "004"
        );
        this.powerProductBrushMower = results[0].filter(
          val => val.material_group == "005"
        );
        this.powerProductLawnmower = results[0].filter(
          val => val.material_group == "006"
        );
        this.powerProductMonpal = results[0].filter(
          val => val.material_group == "007"
        );
        this.powerProductPump = results[0].filter(
          val => val.material_group == "009"
        );
        this.powerProductRidingMower = results[0].filter(
          val => val.material_group == "010"
        );
        this.powerProductBackpackSprayer = results[0].filter(
          val => val.material_group == "011"
        );
        this.powerProductTiller = results[0].filter(
          val => val.material_group == "012"
        );
        this.powerProductWaterPump = results[0].filter(
          val => val.material_group == "013"
        );
        this.powerProductLawnGarden = results[0].filter(
          val => val.material_group == "014"
        );
        this.powerProductAgreeculture = results[0].filter(
          val => val.material_group == "015"
        );
        this.powerProductAccessories = results[0].filter(
          val => val.material_group == "019"
        );
        this.powerProductEnginePP = results[0].filter(
          val => val.material_group == "016"
        );
        this.spinner.hide();
      },
      error => {
        console.log(error);
        this.spinner.hide();
      }
    );
  }
  getDealerStockMarine(userGroup: any): any {
    this.spinner.show();
    this.dealerStock.getMaterialDealerStock(userGroup).subscribe(
      result => {
        let results = [];
        results.push(result);
        this.allMarine = result
        this.marineGen = results[0].filter(val => val.material_group == "001");
        this.marineOutboard = results[0].filter(
          val => val.material_group == "002"
        );
        this.spinner.hide();
      },
      error => {
        console.log(error);
        this.spinner.hide();
      }
    );
  }

  classActive() {
    this.salesPro1 = "";
    this.salesPro2 = "";
    this.salesPro3 = "";
    this.salesPro4 = "";
    this.salesPro5 = "";
    this.salesPro6 = "";
    this.salesPro7 = "";
    this.salesPro8 = "";
    this.salesPro9 = "";
    this.salesPro10 = "";
    this.salesPro11 = "";
    this.salesPro12 = "";
    this.salesPro13 = "";
    this.salesPro14 = "";
    this.salesPro15 = "";
    this.generator = false;
    this.blower = false;
    this.brushCutter = false;
    this.brushMower = false;
    this.lawnmower = false;
    this.monpal = false;
    this.pump = false;
    this.ridingMower = false;
    this.backpackSlayer = false;
    this.tiller = false;
    this.waterPump = false;
    this.lawnGarden = false;
    this.agreeculture = false;
    this.accecories = false;
    this.engine = false;
  }

  tab_salesPro(val) {
    this.classActive();
    if (val == 1) {
      this.salesPro1 = "sales-promo-active";
      this.generator = true;
    } else if (val == 2) {
      this.salesPro2 = "sales-promo-active";
      this.blower = true;
    } else if (val == 3) {
      this.salesPro3 = "sales-promo-active";
      this.brushCutter = true;
    } else if (val == 4) {
      this.salesPro4 = "sales-promo-active";
      this.brushMower = true;
    } else if (val == 5) {
      this.salesPro5 = "sales-promo-active";
      this.lawnmower = true;
    } else if (val == 6) {
      this.salesPro6 = "sales-promo-active";
      this.monpal = true;
    } else if (val == 7) {
      this.salesPro7 = "sales-promo-active";
      this.pump = true;
    } else if (val == 8) {
      this.salesPro8 = "sales-promo-active";
      this.ridingMower = true;
    } else if (val == 9) {
      this.salesPro9 = "sales-promo-active";
      this.backpackSlayer = true;
    } else if (val == 10) {
      this.salesPro10 = "sales-promo-active";
      this.tiller = true;
    } else if (val == 11) {
      this.salesPro11 = "sales-promo-active";
      this.waterPump = true;
    } else if (val == 12) {
      this.salesPro12 = "sales-promo-active";
      this.lawnGarden = true;
    } else if (val == 13) {
      this.salesPro13 = "sales-promo-active";
      this.agreeculture = true;
    } else if (val == 14) {
      this.salesPro14 = "sales-promo-active";
      this.accecories = true;
    } else {
      this.salesPro15 = "sales-promo-active";
      this.engine = true;
    }
  }

  next(val) {
    this.classActive();
    window.scrollTo(0, 0);
    if (val == 1) {
      this.salesPro2 = "sales-promo-active";
      this.blower = true;
    } else if (val == 2) {
      this.salesPro3 = "sales-promo-active";
      this.brushCutter = true;
    } else if (val == 3) {
      this.salesPro4 = "sales-promo-active";
      this.brushMower = true;
    } else if (val == 4) {
      this.salesPro5 = "sales-promo-active";
      this.lawnmower = true;
    } else if (val == 5) {
      this.salesPro6 = "sales-promo-active";
      this.monpal = true;
    } else if (val == 6) {
      this.salesPro7 = "sales-promo-active";
      this.pump = true;
    } else if (val == 7) {
      this.salesPro8 = "sales-promo-active";
      this.ridingMower = true;
    } else if (val == 8) {
      this.salesPro9 = "sales-promo-active";
      this.backpackSlayer = true;
    } else if (val == 9) {
      this.salesPro10 = "sales-promo-active";
      this.tiller = true;
    } else if (val == 10) {
      this.salesPro11 = "sales-promo-active";
      this.waterPump = true;
    } else if (val == 11) {
      this.salesPro12 = "sales-promo-active";
      this.lawnGarden = true;
    } else if (val == 12) {
      this.salesPro13 = "sales-promo-active";
      this.agreeculture = true;
    } else if (val == 13) {
      this.salesPro14 = "sales-promo-active";
      this.accecories = true;
    } else {
      this.salesPro15 = "sales-promo-active";
      this.engine = true;
    }
  }

  back(val) {
    this.classActive();
    window.scrollTo(0, 0);
    if (val == 1) {
      this.salesPro1 = "sales-promo-active";
      this.generator = true;
    } else if (val == 2) {
      this.salesPro2 = "sales-promo-active";
      this.blower = true;
    } else if (val == 3) {
      this.salesPro3 = "sales-promo-active";
      this.brushCutter = true;
    } else if (val == 4) {
      this.salesPro4 = "sales-promo-active";
      this.brushMower = true;
    } else if (val == 5) {
      this.salesPro5 = "sales-promo-active";
      this.lawnmower = true;
    } else if (val == 6) {
      this.salesPro6 = "sales-promo-active";
      this.monpal = true;
    } else if (val == 7) {
      this.salesPro7 = "sales-promo-active";
      this.pump = true;
    } else if (val == 8) {
      this.salesPro8 = "sales-promo-active";
      this.ridingMower = true;
    } else if (val == 9) {
      this.salesPro9 = "sales-promo-active";
      this.backpackSlayer = true;
    } else if (val == 10) {
      this.salesPro10 = "sales-promo-active";
      this.tiller = true;
    } else if (val == 11) {
      this.salesPro11 = "sales-promo-active";
      this.waterPump = true;
    } else if (val == 12) {
      this.salesPro12 = "sales-promo-active";
      this.lawnGarden = true;
    } else if (val == 13) {
      this.salesPro13 = "sales-promo-active";
      this.agreeculture = true;
    } else {
      this.salesPro14 = "sales-promo-active";
      this.accecories = true;
    }
  }

  hide() {
    this.cat1 = false;
    this.cat2 = false;
  }

  category(event) {
    var cat = event.target.value;
    this.hide();

    if (cat == "power product") {
      this.cat1 = true;
    } else {
      this.cat2 = true;
    }
  }

  classActive2() {
    this.salesPro16 = "";
    this.salesPro17 = "";
    this.outboard = false;
    this.gen = false;
  }

  marine(val) {
    this.classActive2();
    if (val == 1) {
      this.salesPro16 = "sales-promo-active";
      this.outboard = true;
    } else {
      this.salesPro17 = "sales-promo-active";
      this.gen = true;
    }
  }

  back2() {
    this.classActive2();
    window.scrollTo(0, 0);
    this.salesPro16 = "sales-promo-active";
    this.outboard = true;
  }

  next2() {
    this.classActive2();
    window.scrollTo(0, 0);
    this.salesPro17 = "sales-promo-active";
    this.gen = true;
  }

  submitMarine() {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    let data = {
      sap_code: sap_code,
      material_number: [],
      material_description: [],
      material_group: [],
      beginning_stock: [],
      selling_to_retail: [],
      selling_to_subdealer: [],
      selling_to_tender: [],
      selling_to_sum: [],
      end_month_stock_store: [],
      end_month_stock_warehouse: [],
      end_month_stock_sum: []
    };

    let dateToday = new Date();

    let endDate = new Date(
      dateToday.getFullYear(),
      dateToday.getMonth() + 1,
      3
    );
    let startingDate = new Date(
      endDate.getFullYear(),
      endDate.getMonth() - 1,
      endDate.getDate() - 6
    );
    // if (
    //   dateToday.getTime() > startingDate.getTime() &&
    //   dateToday.getTime() < endDate.getTime()
    // ) {
    // alert("Correct Date");
    //If Date is valid to input
    //Material Number
    $("#tblOutboardMarine #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblGeneratorMarine #material_number").each(function () {
      data.material_number.push($(this).text());
    });

    //Material Group
    $("#tblOutboardMarine #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblGeneratorMarine #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    let rows = data.material_number.length;

    //Material Desctiprion
    $("#tblOutboardMarine #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblGeneratorMarine #material_description").each(function () {
      data.material_description.push($(this).text());
    });

    //Beginning Stock
    $("#tblOutboardMarine #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblGeneratorMarine #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });

    //Selling to Retail
    $("#tblOutboardMarine #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblGeneratorMarine #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });

    //Selling to Subdealer
    $("#tblOutboardMarine #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblGeneratorMarine #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });

    //Selling to Tender
    $("#tblOutboardMarine #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblGeneratorMarine #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });

    //Selling to Sum
    for (let i = 0; i < rows; i++) {
      let selling_to_retail = parseInt(data.selling_to_retail[i]);
      let selling_to_subdealer = parseInt(data.selling_to_subdealer[i]);
      let selling_to_tender = parseInt(data.selling_to_tender[i]);
      let sum = selling_to_retail + selling_to_subdealer + selling_to_tender;
      data.selling_to_sum.push(sum);
    }

    //End month stock store
    $("#tblOutboardMarine #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblGeneratorMarine #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });

    //End month stock warehouse
    $("#tblOutboardMarine #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblGeneratorMarine #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });

    //Selling to Sum
    for (let i = 0; i < rows; i++) {
      let end_month_stock_store = parseInt(data.end_month_stock_store[i]);
      let end_month_stock_warehouse = parseInt(
        data.end_month_stock_warehouse[i]
      );
      let sum = end_month_stock_store + end_month_stock_warehouse;
      data.end_month_stock_sum.push(sum);
    }
    // console.log(data)
    this.dealerStock.submitStockInput(data).subscribe(
      result => {
        alert("Input Stock Success!");
      },
      error => console.log(error)
    );
    // } else {
    //   //alert for if date input is invalid
    //   alert("Out Side range !!");
    // }
  }

  submitPP() {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    let data = {
      sap_code: sap_code,
      material_number: [],
      material_description: [],
      material_group: [],
      beginning_stock: [],
      selling_to_retail: [],
      selling_to_subdealer: [],
      selling_to_tender: [],
      selling_to_sum: [],
      end_month_stock_store: [],
      end_month_stock_warehouse: [],
      end_month_stock_sum: []
    };
    let dateToday = new Date();

    let endDate = new Date(dateToday.getFullYear(), dateToday.getMonth(), 3);
    let startingDate = new Date(
      endDate.getFullYear(),
      endDate.getMonth() - 1,
      endDate.getDate() - 6
    );
    // if (
    //   dateToday.getTime() > startingDate.getTime() &&
    //   dateToday.getTime() < endDate.getTime()
    // ) {
    //alert("Correct Date");
    //If Date is valid to input
    //Material Number
    $("#tblGeneratorPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblBlowerPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblBrushCutterPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblBrushMowerPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblLawnMowerPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblMonpalPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblPumpPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblRidingMowerPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblBackpackSprayerPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblTillerPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblWaterPumpPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblLawnGardenPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblAgreeCulturePP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblAccessoriesPP #material_number").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tblEnginePP #material_number").each(function () {
      data.material_number.push($(this).text());
    });

    //Material Desc
    $("#tblGeneratorPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblBlowerPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblBrushCutterPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblBrushMowerPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblLawnMowerPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblMonpalPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblPumpPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblRidingMowerPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblBackpackSprayerPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblTillerPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblWaterPumpPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblLawnGardenPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblAgreeCulturePP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblAccessoriesPP #material_description").each(function () {
      data.material_description.push($(this).text());
    });
    $("#tblEnginePP #material_description").each(function () {
      data.material_description.push($(this).text());
    });

    //Material Group
    $("#tblGeneratorPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblBlowerPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblBrushCutterPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblBrushMowerPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblLawnMowerPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblMonpalPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblPumpPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblRidingMowerPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblBackpackSprayerPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblTillerPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblWaterPumpPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblLawnGardenPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblAgreeCulturePP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblAccessoriesPP #material_group").each(function () {
      data.material_group.push($(this).text());
    });
    $("#tblEnginePP #material_group").each(function () {
      data.material_group.push($(this).text());
    });

    //Beginning Stock
    $("#tblGeneratorPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblBlowerPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblBrushCutterPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblBrushMowerPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblLawnMowerPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblMonpalPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblPumpPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblRidingMowerPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblBackpackSprayerPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblTillerPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblWaterPumpPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblLawnGardenPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblAgreeCulturePP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblAccessoriesPP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });
    $("#tblEnginePP #beginning_stock").each(function () {
      data.beginning_stock.push(parseInt($(this).val()));
    });

    //Selling to Retail
    $("#tblGeneratorPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblBlowerPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblBrushCutterPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblBrushMowerPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblLawnMowerPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblMonpalPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblPumpPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblRidingMowerPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblBackpackSprayerPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblTillerPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblWaterPumpPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblLawnGardenPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblAgreeCulturePP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblAccessoriesPP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });
    $("#tblEnginePP #selling_to_retail").each(function () {
      data.selling_to_retail.push(parseInt($(this).val()));
    });

    //Selling to Subdealer
    $("#tblGeneratorPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblBlowerPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblBrushCutterPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblBrushMowerPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblLawnMowerPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblMonpalPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblPumpPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblRidingMowerPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblBackpackSprayerPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblTillerPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblWaterPumpPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblLawnGardenPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblAgreeCulturePP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblAccessoriesPP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });
    $("#tblEnginePP #selling_to_subdealer").each(function () {
      data.selling_to_subdealer.push(parseInt($(this).val()));
    });

    //Selling to Tender
    $("#tblGeneratorPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblBlowerPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblBrushCutterPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblBrushMowerPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblLawnMowerPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblMonpalPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblPumpPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblRidingMowerPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblBackpackSprayerPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblTillerPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblWaterPumpPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblLawnGardenPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblAgreeCulturePP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblAccessoriesPP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });
    $("#tblEnginePP #selling_to_tender").each(function () {
      data.selling_to_tender.push(parseInt($(this).val()));
    });

    //End Month Stock Store
    $("#tblGeneratorPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblBlowerPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblBrushCutterPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblBrushMowerPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblLawnMowerPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblMonpalPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblPumpPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblRidingMowerPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblBackpackSprayerPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblTillerPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblWaterPumpPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblLawnGardenPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblAgreeCulturePP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblAccessoriesPP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });
    $("#tblEnginePP #end_month_stock_store").each(function () {
      data.end_month_stock_store.push(parseInt($(this).val()));
    });

    //End Month Stock Warehouse
    $("#tblGeneratorPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblBlowerPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblBrushCutterPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblBrushMowerPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblLawnMowerPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblMonpalPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblPumpPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblRidingMowerPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblBackpackSprayerPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblTillerPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblWaterPumpPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblLawnGardenPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblAgreeCulturePP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblAccessoriesPP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });
    $("#tblEnginePP #end_month_stock_warehouse").each(function () {
      data.end_month_stock_warehouse.push(parseInt($(this).val()));
    });

    let rows = data.material_number.length;

    //End Month Stock Sum
    for (let i = 0; i < rows; i++) {
      let end_month_stock_store = parseInt(data.end_month_stock_store[i]);
      let end_month_stock_warehouse = parseInt(
        data.end_month_stock_warehouse[i]
      );
      let sum = end_month_stock_store + end_month_stock_warehouse;
      data.end_month_stock_sum.push(sum);
    }

    //Selling to Sum
    for (let i = 0; i < rows; i++) {
      let selling_to_retail = parseInt(data.selling_to_retail[i]);
      let selling_to_subdealer = parseInt(data.selling_to_subdealer[i]);
      let selling_to_tender = parseInt(data.selling_to_tender[i]);
      let sum = selling_to_retail + selling_to_subdealer + selling_to_tender;
      data.selling_to_sum.push(sum);
    }

    this.dealerStock.submitStockInput(data).subscribe(
      result => {
        alert("Input Stock Success!");
      },
      error => console.log(error)
    );
    // } else {
    //   //alert for if date input is invalid
    //   alert("Out Side range !!");
    // }
  }

  handleFiles(event) {

    if (event.target.files.length > 0) {
      this.spinner.show()
      let reader = new FileReader()
      reader.onload = (e: any) => {
        let csv = e.target.result

        let allLineText = csv.split(/\r\n|\n/)

        let lines = []
        let keys = allLineText.shift().split(",")
        while (allLineText.length) {
          let arr = allLineText.shift().split(",")
          let obj = {}
          for (let i = 0; i < keys.length; i++) {
            obj[keys[i]] = arr[i]
          }
          lines.push(obj)
        }
        console.log(lines)
        if (lines.length > 0) {

          //matching csv to form
          for (const item of lines) {
            let material_code = `000000000000${item.material_number}`;
            if (this.cat1 == true) {
              for (const iterator of this.allPowerProduct) {
                if (iterator.material_number == material_code) {
                  if (iterator.material_group == "001") {
                    // pp gen
                    let index = this.powerProductGenerator.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductGenerator[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductGenerator[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductGenerator[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductGenerator[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductGenerator[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "003") {
                    // pp blower
                    let index = this.powerProductBlower.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductBlower[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductBlower[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductBlower[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductBlower[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductBlower[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "004") {
                    // pp brush cutter
                    let index = this.powerProductBrushCutter.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductBrushCutter[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductBrushCutter[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductBrushCutter[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductBrushCutter[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductBrushCutter[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "005") {
                    // pp brush mower
                    let index = this.powerProductBrushMower.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductBrushMower[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductBrushMower[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductBrushMower[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductBrushMower[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductBrushMower[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "006") {
                    // pp lawnmower
                    let index = this.powerProductLawnmower.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductLawnmower[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductLawnmower[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductLawnmower[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductLawnmower[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductLawnmower[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "007") {
                    // pp monpal
                    let index = this.powerProductMonpal.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductMonpal[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductMonpal[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductMonpal[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductMonpal[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductMonpal[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "009") {
                    // pp pump
                    let index = this.powerProductPump.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductPump[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductPump[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductPump[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductPump[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductPump[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "010") {
                    // pp riding mower
                    let index = this.powerProductRidingMower.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductRidingMower[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductRidingMower[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductRidingMower[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductRidingMower[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductRidingMower[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "011") {
                    // pp backpack sprayer
                    let index = this.powerProductBackpackSprayer.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductBackpackSprayer[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductBackpackSprayer[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductBackpackSprayer[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductBackpackSprayer[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductBackpackSprayer[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "012") {
                    // pp tiller
                    let index = this.powerProductTiller.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductTiller[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductTiller[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductTiller[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductTiller[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductTiller[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "013") {
                    // pp water pump
                    let index = this.powerProductWaterPump.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductWaterPump[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductWaterPump[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductWaterPump[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductWaterPump[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductWaterPump[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "014") {
                    // pp lawn garden
                    let index = this.powerProductLawnGarden.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductLawnGarden[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductLawnGarden[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductLawnGarden[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductLawnGarden[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductLawnGarden[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "015") {
                    // pp agreeculture
                    let index = this.powerProductAgreeculture.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductAgreeculture[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductAgreeculture[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductAgreeculture[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductAgreeculture[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductAgreeculture[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "019") {
                    // pp accessories
                    let index = this.powerProductAccessories.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductAccessories[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductAccessories[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductAccessories[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductAccessories[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductAccessories[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "016") {
                    // pp enginge PP
                    let index = this.powerProductEnginePP.findIndex(
                      val => val.material_number == material_code
                    );
                    this.powerProductEnginePP[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.powerProductEnginePP[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.powerProductEnginePP[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.powerProductEnginePP[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.powerProductEnginePP[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  }
                }
              }
              // index++
            } else {
              for (const iterator of this.allMarine) {
                if (iterator.material_number == material_code) {
                  if (iterator.material_group == "001") {
                    // marine gen
                    let index = this.marineGen.findIndex(
                      val => val.material_number == material_code
                    );
                    this.marineGen[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.marineGen[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.marineGen[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.marineGen[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.marineGen[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  } else if (iterator.material_group == "002") {
                    // marine outboard
                    let index = this.marineOutboard.findIndex(
                      val => val.material_number == material_code
                    );
                    this.marineOutboard[index]["selling_to_retail"] =
                      item.selling_to_retail || 0;
                    this.marineOutboard[index]["selling_to_subdealer"] =
                      item.selling_to_subdealer || 0;
                    this.marineOutboard[index]["selling_to_tender"] =
                      item.selling_to_tender || 0;
                    this.marineOutboard[index]["end_month_stock_store"] =
                      item.end_month_stock_store || 0;
                    this.marineOutboard[index]["end_month_stock_warehouse"] =
                      item.end_month_stock_warehouse || 0;
                  }
                }
              }
              // index++
            }
          }
          this.spinner.hide()
        }
      }
      reader.onerror = (e: any) => {
        this.spinner.hide()
        if (e.target.error.name == "NotReadableError") {
          alert("Can't read the file")
        }
      }
      reader.readAsBinaryString(event.target.files[0])
    }
  }


  downloadTemplate() {
    let data, filename, link;
    const templateData = [];
    let userGroup = localStorage.getItem("group")
    // templateData =
    //   this.cat1 == true ? this.allPowerProduct : this.allMarine;
    console.log(userGroup)
    if (this.userGroup == "001") {
      // templateData = this.allMarine
      for (const item of this.allMarine) {
        templateData.push(item)
      }
    } else if (this.userGroup == "002") {
      // templateData = this.allPowerProduct
      for (const item of this.allPowerProduct) {
        templateData.push(item)
      }
    } else {
      // templateData = this.allMarine
      // templateData.concat(this.allPowerProduct)
      for (const item of this.allMarine) {
        templateData.push(item)
      }
      for (const item of this.allPowerProduct) {
        templateData.push(item)
      }
    }


    // console.log(templateData)
    const ayy = JSON.parse(JSON.stringify(templateData))
    console.log(ayy)
    for (const v of ayy) {
      delete v.material_group
      delete v.beginning_stock
      delete v.selling_to_sum
      delete v.end_month_stock_sum
      v.selling_to_retail = 0
      v.selling_to_subdealer = 0
      v.selling_to_tender = 0
      v.end_month_stock_store = 0
      v.end_month_stock_warehouse = 0
      v.material_number = v.material_number.slice(12, v.material_number.length)
    }
    // console.log(ayy)

    let csv = this.convertToCSV({
      data: ayy
    });

    if (csv == null) return;
    filename = "template_dealer_stock.csv";

    if (!csv.match(/^data:text\/csv/i)) {
      csv = "data:text/csv;charset=utf-8," + csv;
    }

    data = encodeURI(csv);
    link = document.createElement("a");
    link.setAttribute("href", data);
    link.setAttribute("download", filename);
    document.body.appendChild(link)
    link.click();
    document.body.removeChild(link)
  }
  convertToCSV(args) {
    let result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
      return null;
    }

    columnDelimiter = args.columnDelimiter || ",";
    lineDelimiter = args.lineDelimiter || "\n";

    keys = Object.keys(data[0]);

    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(item => {
      ctr = 0;
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter;

        result += item[key];
        ctr++;
      });
      result += lineDelimiter;
    });

    return result;
  }
}
