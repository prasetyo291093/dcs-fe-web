import { Component, OnInit, TemplateRef } from "@angular/core";
import { formatDate } from "@angular/common";
import { SparepartsPurchaseOrderService } from "../../../../services/spareparts-purchase-order.service";
import { LocalStorageService } from "ngx-webstorage";
import { NgxSpinnerService } from "ngx-spinner";
import { CbuPurchaseOrderService } from "src/app/services/cbu-purchase-order.service";
import { environment } from "src/environments/environment";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import * as moment from 'moment';

declare var jquery: any;
declare var $: any;
declare var number: any;

@Component({
  selector: "app-spareparts-purchase-order",
  templateUrl: "./spareparts-purchase-order.component.html",
  styleUrls: ["./spareparts-purchase-order.component.css"]
})
export class SparepartsPurchaseOrderComponent implements OnInit {
  materialCode_Spareparts: any;
  materialCode_Spareparts_add: any;
  prodSpareSelec: any = [];
  simulate: any = [];
  simOrderSpareparts: any = [];
  matSpare_selec: string;
  data_sap_dcs_amount: any = [];
  data_sap_dcs: any = [];
  total_discount: number;
  total_allocated: number;
  vat: number;
  total_allocated_with_vat: number;
  so_number: string;
  qty: number;
  qtyDraft: any = [];
  item_no_exist: any = [];
  item_duplicate: any = [];
  item_supersession: any = [];
  item_discontinued: any = [];
  // item_supersession1: any = []
  // item_supersession2: any = []
  // item_supersession3: any
  paramDraft: string;
  is_draft: string = "NO";
  paymentType: string;
  customerAccount: string;
  userIDBCA: string;

  uploadQty: any = [];

  name = "Get Date";
  today = new Date();
  jstoday = "";

  cbuOrder1: string;
  cbuOrder2: string;

  billtocode: string;
  shiptocode: string;

  disabledPay1: string;
  disabledPay2: string;
  disabledPay3: string;
  disabledPay4: string;

  entrySparepartform: boolean = true;
  simulateSpare: boolean = false;
  confirmSpare: boolean = false;
  bankTF: boolean = false;
  Cash: boolean = true;
  upload: boolean = false;
  notUpload: boolean = true;
  upload1: boolean = true;
  upload2: boolean = false;
  isNotDupe: boolean = true;
  isDraft: boolean = false;

  sparepartDraft: any = [];

  total_discount_simulate: number;
  total_allocated_simulate: number;
  total_vat_simulate: number;
  total_allocated_vat_simulate: number;
  payment_deadline: any;

  rows: any = [];

  is_tester: boolean = true;

  SpareorderEntry: boolean;
  Sparedraft: boolean;

  lblConOrder = "Continue Order";
  total_allocated_before_discount_simulate: any;
  total_allocated_before_discount: number;
  btnUpload: string = "false";
  cash_payment_type: string = "Bank Transfer";
  paymentMethods: any;
  urgention: any;
  virtualAccount: string;

  baseUrl = environment.baseUrl
  appUrl = environment.appUrl

  modalRef: BsModalRef;
  online_payment_type: string;
  canDownloadOrderDocument: boolean = false;
  canSimulateOrder: boolean = false;
  canPreviewPriceSimulate: boolean = false;
  canDoingPayment: boolean = false;
  canCreateOrder: boolean = false;

  constructor(
    private sparepartsOrder: SparepartsPurchaseOrderService,
    private storage: LocalStorageService,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    private cbuOrder: CbuPurchaseOrderService
  ) {
    this.jstoday = formatDate(this.today, "dd-MM-yyyy", "en-US", "+0700");
  }

  ngOnInit() {
    this.cbuOrder1 = "cbu-order-active";
    this.SpareorderEntry = true;
    // this.qty = 0;
    this.canCreateOrder = JSON.parse(localStorage.getItem("user")).permissions["Create Order"]
    this.canDownloadOrderDocument = JSON.parse(localStorage.getItem("user")).permissions["Download Simulate Order and Order Confirmation Document"]
    this.canSimulateOrder = JSON.parse(localStorage.getItem("user")).permissions["Simulate Order"]
    this.canPreviewPriceSimulate = JSON.parse(localStorage.getItem("user")).permissions["Preview Price in Simulate Order"]
    this.canDoingPayment = JSON.parse(localStorage.getItem("user")).permissions["Doing Payment"]

    let va = localStorage.getItem("virtualAccount");
    this.virtualAccount = va;
    console.log(va);

    this.urgention = localStorage.getItem("urgention");
    if (this.urgention == "0" || this.urgention == "null")
      this.simulate.delpri = "Normal";
    console.log(this.urgention);

    $(document).ready(function () {
      $('[data-toggle="popover"]').popover();
    });

    this.disabledPay3 = "disable-radio";
    this.disabledPay4 = "disable-radio";
    // console.log(this.prodSpareSelec);
    this.storage.observe("sparepartsOrder").subscribe(newValue => {
      // console.log(newValue);
    });

    this.sparepartsOrder.getBillToCode().subscribe(res => {
      var billtocode = [];
      billtocode.push(res);
      this.billtocode = billtocode[0].filter(item => item.division == "20");
    });
    this.sparepartsOrder.getShipToCode().subscribe(res => {
      var shiptocode = [];
      shiptocode.push(res);
      this.shiptocode = shiptocode[0].filter(item => item.division == "20");
    });
    this.spinner.show();
    this.sparepartsOrder.getDraftList().subscribe(res => {
      var result = [];
      result.push(res);
      var sparepartDraft = [];
      var map = new Map();

      var sortByDate = function (a, b) {
        var dateA = new Date(b.updated_at).getTime();
        var dateB = new Date(a.updated_at).getTime();
        // console.log(`Date A: ${dateA}`, `Date B: ${dateB}`)
        return dateA > dateB ? 1 : -1;
      };
      result[0].sort(sortByDate);

      for (const item of result[0]) {
        if (!map.has(item.param)) {
          map.set(item.param, true);
          this.sparepartDraft.push(item);
        }
      }
      // sparepartDraft.push(res);
      console.log(this.sparepartDraft);
      // this.sparepartDraft = sparepartDraft;
      this.spinner.hide();
    });
  }

  modalSpr1(spr1: TemplateRef<any>) {
    this.modalRef = this.modalService.show(spr1);
  }

  modalSpr2(spr2: TemplateRef<any>) {
    this.modalRef = this.modalService.show(spr2);
  }

  //REQ upload
  sendFileRequest(urllinks, formData, cb) {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 5000);
    $.ajax({
      url: urllinks,
      type: "POST",
      xhr: function () {
        var myXhr = $.ajaxSettings.xhr();
        if (myXhr.upload) {
        }
        return myXhr;
      },
      beforeSend: function (stuff) {
        console.log(stuff);
      },
      success: function (data) {
        cb({ result: data });
      },
      error: function (error) {
        cb({ result: error });
      },
      data: formData,
      cache: false,
      contentType: false,
      processData: false
    });
  }
  onfilecsvupload(event) {
    if (this.simulate.delpri) {
      if (localStorage.getItem("uploadPart"))
        localStorage.removeItem("uploadPart");
      let fileList: FileList = event.target.files;
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append("file", file, file.name);
      // console.log(formData);
      var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
      this.btnUpload = "true";
      this.spinner.show();
      this.sendFileRequest(
        this.baseUrl + "/api/order-sparepart/import/" + sap_code,
        formData,
        function (resp) {
          $("#uploadPart").val("");
          // uploadPart.push(resp)
          console.log(typeof JSON.stringify(resp));
          // localStorage.setItem('uploadPart', JSON.stringify(resp))
          // if (JSON.parse(localStorage.getItem('uploadPart'))["result"]) {
          uploadPart(resp);
          // }
        }
      );
      let uploadPart = resp => {
        localStorage.setItem("uploadPart", JSON.stringify(resp));
        this.upload1 = false;
        this.upload2 = true;
        this.btnUpload = "false";
        this.spinner.hide();
      };
      // if (JSON.parse(localStorage.getItem('uploadPart'))) {
      // this.upload1 = false;
      // this.upload2 = true;
      // } else {
      //   alert('Please re-upload. Connection have been lost.')
      // }
      this.spinner.hide();
    } else {
      alert("Please Choose Delivery Priority First !");
      $("#uploadPart").val("");
    }
  }

  submitUpload() {
    this.upload = true;
    this.notUpload = false;
    var result = JSON.parse(localStorage.getItem("uploadPart"));
    this.item_no_exist.length = 0;
    this.item_duplicate.length = 0;
    this.item_supersession.length = 0;
    this.item_discontinued.length = 0;
    // console.log(result);
    this.spinner.show();
    var data = [];
    console.log(result["result"].status);
    if (result) {
      for (var key in result["result"]["is_duplicate"]) {
        this.item_duplicate.push(
          result["result"]["is_duplicate"][key]["item1"]
        );
      }

      if (this.item_duplicate.length > 0) {
        alert(
          "The material number \n" +
          this.item_duplicate.join("\n") +
          "\nIs Duplicate. Please add another Item.\n\n" +
          "Total duplicate : " +
          this.item_duplicate.length
        );
      }

      if (
        result["result"]["exist"].length != result["result"]["is_oil"].length
      ) {
        if (result["result"]["is_oil"].length > 0) {
          alert(
            "The material number : \n" +
            result["result"]["is_oil"].join("\n") +
            "\nIs An Oil. Please remove them before simulate.\n\n" +
            "Total oil : " +
            result["result"]["is_oil"].length
          );
        }
      }

      for (var key in result["result"]["no_exist"]) {
        this.item_no_exist.push(result["result"]["no_exist"][key]["item1"]);
      }

      if (this.item_no_exist.length > 0) {
        alert(
          "File is rejected, you don't have permission to order this material : \n" +
          this.item_no_exist.join("\n") +
          "\n\nTotal rejected : " +
          this.item_no_exist.length
        );
      }

      for (var key in result["result"]["all"]) {
        if (result["result"]["all"][key]["supersession_part"] !== "-") {
          this.item_supersession.push(
            result["result"]["all"][key]["material_code"] +
            " - " +
            result["result"]["all"][key]["supersession_part"]
          );
        }
      }

      if (this.item_supersession.length > 0) {
        alert(
          "This item have supersession : \n" +
          this.item_supersession.join("\n") +
          "\n\nTotal supersession : " +
          this.item_supersession.length
        );
      }

      this.sparepartsOrder
        .getMaterialUpload(result["result"]["idtrs"])
        .subscribe(
          res => {
            // console.log(res[0]);
            let res2 = [];
            // if (parseInt(res[0]["is_continue"]) !== 0)
            res2.push(res);
            for (let i = 0; i < res2[0].length; i++) {
              if (parseInt(res2[0][i]["is_continue"]) !== 1)
                data.push(res2[0][i]);
            }
            // data.push(res);
            // console.log(data[0].length);
            for (var i = 0; i < data.length; i++) {
              data[i].check_duplicate = "no";
            }
            for (var i = 0; i < data.length; i++) {
              data[i].check_oil = "no";
              for (var l = 0; l < result["result"]["is_oil"].length; l++) {
                if (
                  result["result"]["exist"].length ==
                  result["result"]["is_oil"].length
                ) {
                  data[i].all_oil = "yes";
                } else {
                  if (
                    data[i].material_number == result["result"]["is_oil"][l]
                  ) {
                    data[i].check_oil = "yes";
                  }
                }
              }
            }

            for (var i = 0; i < data.length; i++) {
              data[i].check_supersession = "no";
              for (var l = 0; l < result["result"]["all"].length; l++) {
                if (
                  data[i].material_number ==
                  result["result"]["all"][l]["material_code"] &&
                  result["result"]["all"][l]["supersession_part"] != "-"
                ) {
                  data[i].check_supersession = "yes";
                }
              }
            }
            console.log(data);
            this.prodSpareSelec = data;
          },
          err => {
            console.log(err);
            this.spinner.hide();
          }
        );

      // check if material discontinued
      for (let key = 0; key < result["result"]["exist"].length; key++) {
        let material_number = result["result"]["exist"][key]["item1"];
        let lastIndex: number = result["result"]["exist"].length - 1;

        this.sparepartsOrder.getMaterial(material_number).subscribe(res => {
          // console.log(key)
          if (parseInt(res["is_continue"]) == 1)
            this.item_discontinued.push(res["material_number"] + "\n");

          if (key == lastIndex)
            if (this.item_discontinued.length > 0)
              alert(
                "The material number is discontinue. Please contact HPPI. \n" +
                this.item_discontinued.join("") +
                "\n Total material discontinued: " +
                this.item_discontinued.length
              );
        });
      }
      // alert(this.item_discontinued);

      // console.log(data)

      localStorage.removeItem("uploadPart");

      // console.log(this.prodSpareSelec)
      this.spinner.hide();

      this.upload1 = true;
      this.upload2 = false;
    } else {
      alert("File is empty. Please input material and quantity before upload.");
    }
  }

  downloadTemplate() {
    window.location.href =
      this.baseUrl + "/api/order-sparepart/download-template";
  }

  classActive() {
    this.cbuOrder1 = "";
    this.cbuOrder2 = "";
    this.SpareorderEntry = false;
    this.Sparedraft = false;
  }

  tab_cbuOrder(val) {
    this.spinner.show();
    this.classActive();
    if (val == 1) {
      this.cbuOrder1 = "cbu-order-active";
      this.SpareorderEntry = true;
    } else if (val == 2) {
      this.cbuOrder2 = "cbu-order-active";
      this.Sparedraft = true;
    } else {
      this.cbuOrder2 = "cbu-order-active";
      this.Sparedraft = true;
    }
    this.spinner.hide();
  }

  saveAsDraft(val) {
    var user_id = JSON.parse(localStorage.getItem("user")).id;
    var rows = document
      .getElementById("tableSparepart")
      .getElementsByTagName("tbody")[0]
      .getElementsByTagName("tr").length;
    var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;

    var data = {
      user_id: user_id,
      po_number: this.simulate.punchOrnumb,
      bill_to: this.simulate.billpay,
      ship_to: this.simulate.shipto,
      delivery_priority: this.simulate.delpri,
      remarks: this.simulate.remarks,
      material_number: [],
      qty: [],
      payment_status: "Cash"
    };
    $("#tableSparepart #material_code").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tableSparepart #quantity").each(function () {
      data.qty.push(parseInt(this.value).toString());
    });
    if (data.po_number) {
      data.po_number = data.po_number.toString();
    } else {
      data.po_number = "PO";
    }
    if (data.remarks) {
      data.remarks = data.remarks;
    } else {
      data.remarks = "remarks";
    }
    if (data.bill_to) {
      data.bill_to = data.bill_to.toString().split(" - ");
      data.bill_to = data.bill_to[0];
    } else {
      data.bill_to = "";
    }
    if (data.ship_to) {
      data.ship_to = data.ship_to.toString().split(" - ");
      data.ship_to = data.ship_to[0];
    } else {
      data.ship_to = "";
    }
    data.delivery_priority = data.delivery_priority.toString();
    // data.remarks = data.remarks.toString();
    data.user_id = data.user_id.toString();
    console.log(data);
    this.spinner.show();
    if (this.isDraft == false) {
      this.sparepartsOrder.saveAsDraft(data).subscribe(
        res => {
          var data = [];
          data.push(res);
          console.log(data);

          this.sparepartsOrder.getDraftList().subscribe(res => {
            var result = [];
            result.push(res);
            var sparepartDraft = [];
            var map = new Map();

            var sortByDate = function (a, b) {
              var dateA = new Date(b.updated_at).getTime();
              var dateB = new Date(a.updated_at).getTime();
              console.log(`Date A: ${dateA}`, `Date B: ${dateB}`);
              return dateA > dateB ? 1 : -1;
            };
            result[0].sort(sortByDate);

            for (const item of result[0]) {
              if (!map.has(item.param)) {
                map.set(item.param, true);
                sparepartDraft.push(item);
              }
            }
            this.sparepartDraft = sparepartDraft;

            // if (data[0].message == 'Drafted.') {
            //   this.classActive();
            //   if (val == 1) {
            //     this.cbuOrder1 = 'cbu-order-active';
            //     this.SpareorderEntry = true;
            //   } else if (val == 2) {
            //     this.cbuOrder2 = 'cbu-order-active';
            //     this.Sparedraft = true;
            //   }
            //   else {
            //     this.cbuOrder2 = 'cbu-order-active';
            //     this.Sparedraft = true;
            //   }
            //   this.spinner.hide()
            // } else {
            //   this.spinner.hide()
            //   alert('heho')
            // }
            this.spinner.hide();
            alert("Draft saved.");
            location.reload();
          });
        },
        err => {
          this.spinner.hide();
          console.log(err);
          // alert('error')
        }
      );
    } else {
      this.sparepartsOrder.updateDraft(this.paramDraft, data).subscribe(
        res => {
          console.log(res);

          this.sparepartsOrder.getDraftList().subscribe(
            res => {
              var result = [];
              result.push(res);
              var sparepartDraft = [];
              var map = new Map();

              var sortByDate = function (a, b) {
                var dateA = new Date(b.updated_at).getTime();
                var dateB = new Date(a.updated_at).getTime();
                console.log(`Date A: ${dateA}`, `Date B: ${dateB}`);
                return dateA > dateB ? 1 : -1;
              };
              result[0].sort(sortByDate);

              for (const item of result[0]) {
                if (!map.has(item.param)) {
                  map.set(item.param, true);
                  sparepartDraft.push(item);
                }
              }
              this.sparepartDraft = sparepartDraft;
              this.spinner.hide();
              alert("Draft updated.");
              location.reload();
            },
            err => {
              this.spinner.hide();
              console.log(err);
            }
          );
        },
        err => {
          this.spinner.hide();
        }
      );
    }
  }

  getBillTo() {
    console.log(this.simulate.billpay);
    this.cbuOrder.getTOPFromBillTo(this.simulate.billpay, 20).subscribe(
      result => {
        if (result["message"] == "Success.")
          this.paymentMethods = result["top"];
        else this.paymentMethods = 0;
        console.log(`"${this.paymentMethods}"`);
      },
      error => {
        console.log(error);
      }
    );
  }

  simOrder() {
    this.spinner.show();
    // var po_number: string = this.simulate.punchOrnumb;
    // var remarks: string = this.simulate.remarks;
    // if(po_number && remarks) {
    this.simulate.items = this.prodSpareSelec;
    this.storage.store("sparepartsOrder", this.simulate);
    this.getData("sparepartsOrder");
    var check_duplicate = 0;
    var check_oil = 0;
    var check_supersession = 0;
    var user_id = JSON.parse(localStorage.getItem("user")).id;
    var rows: any = document
      .getElementById("tableSparepart")
      .getElementsByTagName("tbody")[0]
      .getElementsByTagName("tr").length;
    var paymentMethod = `"${this.paymentMethods}"`;
    var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;
    // var so_number = JSON.parse(localStorage.getItem('orderHeaderData')).vbeln;

    if (paymentMethod == '"T000"') {
      var payment_method = "Cash";
    } else {
      var payment_method = "Credit";
    }

    var data = {
      customer_code: customer_code,
      po_number: this.simulate.punchOrnumb,
      bill_to: this.simulate.billpay,
      ship_to: this.simulate.shipto,
      delivery_priority: this.simulate.delpri,
      remarks: this.simulate.remarks,
      material_number: [],
      qty: [],
      payment_status: "Cash",
      sales_order_number: "9999",
      user_id: user_id,
      unit_of_measure: [],
      sloc: [],
      sold_to: this.simulate.billpay,
      posnr: [],
      order_date: formatDate(this.today, "yyyy-MM-dd", "en-US", "+0530"),
      payment_trigger: paymentMethod,
      sub_order: this.totalOrders().toString(),
      user: JSON.parse(localStorage.getItem("user")).email,
      payment_method: payment_method
      // so_number: so_number.toString()
    };

    ////!!!DELETE THIS
    if (this.simulate.punchOrnumb) {
      data.po_number = data.po_number.toString();
    } else {
      data.po_number = "-";
      this.simulate.punchOrnumb = "-";
    }

    if (this.simulate.remarks) {
      data.remarks = data.remarks.toString();
    } else {
      data.remarks = "-";
      this.simulate.remarks = "-";
    }

    //
    data.po_number = data.po_number.toString();
    data.bill_to = data.bill_to.toString().split(" - ");
    data.bill_to = data.bill_to[0];
    data.ship_to = data.ship_to.toString().split(" - ");
    data.ship_to = data.ship_to[0];
    data.sold_to = data.sold_to.toString().split(" - ");
    data.sold_to = data.sold_to[0];
    data.delivery_priority = data.delivery_priority.toString();
    data.remarks = data.remarks.toString();
    if (data.remarks.toString().toLowerCase() === "cash") {
      data.payment_trigger = '"T000"';
    }

    $("#tableSparepart #material_code").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tableSparepart #base_unit_of_measure").each(function () {
      data.unit_of_measure.push($(this).text());
    });
    $("#tableSparepart #sloc").each(function () {
      data.sloc.push($(this).text());
    });
    $("#tableSparepart input#quantity").each(function () {
      data.qty.push(parseInt(this.value).toString());
    });

    var posnr = 10;

    for (var i = 0; i < rows; i++) {
      if (posnr.toString().length == 2) {
        data.posnr.push("0000" + posnr.toString()).toString();
        posnr += 10;
      } else if (posnr.toString().length == 3) {
        data.posnr.push("000" + posnr.toString()).toString();
        posnr += 10;
      } else if (posnr.toString().length == 4) {
        data.posnr.push("00" + posnr.toString()).toString();
        posnr += 10;
      } else if (posnr.toString().length == 5) {
        data.posnr.push("0" + posnr.toString()).toString();
        posnr += 10;
      }
    }

    data.user_id = data.user_id.toString();
    //
    console.log(data);

    $("#tableSparepart #check_duplicate").each(function () {
      console.log($(this).text());
      if ($(this).text() == "yes") {
        check_duplicate++;
      }
    });

    $("#tableSparepart #check_oil").each(function () {
      console.log($(this).text());
      if ($(this).text() == "yes") {
        check_oil++;
      }
    });

    $("#tableSparepart #check_supersession").each(function () {
      console.log($(this).text());
      if ($(this).text() == "yes") {
        check_supersession++;
      }
    });
    if (paymentMethod == '"T000"') {
      this.paymentType = "Cash";
    } else {
      this.paymentType = "Credit";
    }
    //
    if (!data.qty.includes("0")) {
      if (check_duplicate > 1) {
        this.spinner.hide();
        alert("Please Remove Duplicate / Reupload File Before Simulate ");
        check_duplicate = 0;
        this.spinner.hide();
      } else if (check_oil > 0 && check_oil !== data.material_number.length) {
        alert("Please Not Combine Oil Before Simulate ");
        check_oil = 0;
        this.spinner.hide();
      } else if (check_supersession > 0) {
        alert("This part is changed, please input the latest part number");
        check_supersession = 0;
        this.spinner.hide();
      } else {
        this.sparepartsOrder.simulateOrder(data).subscribe(
          res => {
            console.log(res);
            var data_sap = [];
            data_sap.push(res);
            if (data_sap) {
              if (data_sap[0].status_item.includes("Unknown")) {
                this.spinner.hide();
                alert(data_sap[0].messages);
              } else {
                this.data_sap_dcs_amount = data_sap[0].result.header[1];
                this.data_sap_dcs = data_sap[0].result.output;
                this.total_allocated_before_discount_simulate =
                  this.data_sap_dcs_amount.znetwr2_gros * 100;
                this.total_discount_simulate =
                  this.data_sap_dcs_amount.zdiscount.replace("-", "") * 100;
                this.total_allocated_simulate =
                  this.data_sap_dcs_amount.znetwr2 * 100;
                this.total_vat_simulate =
                  this.data_sap_dcs_amount.zmwsbp2 * 100;
                this.total_allocated_vat_simulate =
                  this.total_allocated_simulate + this.total_vat_simulate;
                // console.log(this.data_sap_dcs.length);

                if (data_sap[0].status == "success") {
                  this.entrySparepartform = false;
                  this.simulateSpare = true;
                  let j = 1;
                  for (let i = 0; i < this.data_sap_dcs.length; i++) {
                    if (this.data_sap_dcs[i].posnr_so.split("").pop() == 0) {
                      this.data_sap_dcs[i].number = j + ".";
                      j++;
                    }
                    this.data_sap_dcs[i].kwmeng = Math.round(
                      this.data_sap_dcs[i].kwmeng
                    );
                    this.data_sap_dcs[i].zkwmeng2 = Math.round(
                      this.data_sap_dcs[i].zkwmeng2
                    );
                    this.data_sap_dcs[i].backorder = Math.round(
                      this.data_sap_dcs[i].kwmeng -
                      this.data_sap_dcs[i].zkwmeng2
                    );
                    this.data_sap_dcs[i].netpr_gros = this.data_sap_dcs[
                      i
                    ].netpr_gros;
                    this.data_sap_dcs[i].netwr_gros = this.data_sap_dcs[
                      i
                    ].netwr_gros;
                    this.data_sap_dcs[i].zdiscount_gros = this.data_sap_dcs[
                      i
                    ].zdiscount_gros.replace("-", "");
                    this.data_sap_dcs[i].netpr_gros =
                      this.data_sap_dcs[i].netpr_gros * 100;
                    this.data_sap_dcs[i].netwr_gros =
                      this.data_sap_dcs[i].netwr_gros * 100;
                    this.data_sap_dcs[i].zdiscount_gros =
                      this.data_sap_dcs[i].zdiscount_gros.replace("-", "") *
                      100;
                    // console.log(this.data_sap_dcs[i].netpr * 100);
                    // console.log(this.data_sap_dcs[i].netwr * 100);
                    // console.log(this.data_sap_dcs[i].zdiscount * 100);
                    // if(this.data_sap_dcs[0].waerk == "IDR"){
                    //   this.data_sap_dcs[i].netpr = this.data_sap_dcs[i].netpr * 100 ;
                    //   this.data_sap_dcs[i].netwr = this.data_sap_dcs[i].netwr * 100;
                    //   this.data_sap_dcs[i].zdiscount = this.data_sap_dcs[i].zdiscount * 100;
                    // }
                    // console.log(this.data_sap_dcs[i].matnr + ' - ' + ' ');
                    // this.data_sap_dcs[i]["0"] =
                    // this.sparepartsOrder.getMaterial(this.data_sap_dcs[i].matnr + ' - ' + 'a').subscribe(res => {
                    //   // this.data_sap_dcs[i].matwa = res["material_description"];
                    //   // this.data_sap_dcs[i].model = res["model"];
                    // }, err => {
                    //   this.spinner.hide()
                    //   console.log(err);
                    // });
                  }
                } else {
                  this.spinner.hide();
                  alert("heho");
                }
                this.spinner.hide();
              }
            }
          },
          err => {
            this.spinner.hide();
            // alert('Please Add Material First!')
            console.log(err);
          }
        );
      }
    } else {
      this.spinner.hide();
      alert("Please Add Quantity");
    }
    // } else {
    //   alert('Purchase Order Number and/or Remarks can\'t be blank');
    // }
  }

  getData(val) {
    this.simOrderSpareparts = this.storage.retrieve(val);
  }

  backOrder() {
    this.entrySparepartform = true;
    this.simulateSpare = false;
  }

  subOrder() {
    this.spinner.show();
    this.modalRef.hide()
    var user_id = JSON.parse(localStorage.getItem("user")).id;
    this.customerAccount = localStorage.getItem("bank_transfer_va")
    var rows: any = document
      .getElementById("tableSparepart")
      .getElementsByTagName("tbody")[0]
      .getElementsByTagName("tr").length;
    var paymentMethod = `"${this.paymentMethods}"`;
    var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;

    if (customer_code == '0010000024'){
      this.is_tester = true
    }

    var check_duplicate = 0;
    var check_oil = 0;
    var check_supersession = 0;
    if (paymentMethod == '"T000"') {
      var payment_method = "Cash";
    } else {
      var payment_method = "Credit";
    }
    // var so_number = JSON.parse(localStorage.getItem('orderHeaderData')).vbeln;

    var data = {
      customer_code: customer_code,
      po_number: this.simulate.punchOrnumb,
      bill_to: this.simulate.billpay,
      ship_to: this.simulate.shipto,
      delivery_priority: this.simulate.delpri,
      remarks: this.simulate.remarks,
      material_number: [],
      qty: [],
      payment_status: "Cash",
      sales_order_number: "9999",
      user_id: user_id,
      unit_of_measure: [],
      sloc: [],
      sold_to: this.simulate.billpay,
      posnr: [],
      order_date: formatDate(this.today, "yyyy-MM-dd", "en-US", "+0530"),
      payment_trigger: paymentMethod,
      sub_order: this.totalOrders().toString(),
      ordered: [],
      allocated: [],
      backorder: [],
      unit_price: [],
      discount: [],
      total_amount: this.totalConfirmationOrderAmount(),
      total_discount: this.total_discount,
      total_allocated: this.total_allocated,
      vat: this.vat,
      total_allocated_with_vat: this.total_allocated_with_vat,
      is_draft: this.is_draft,
      param: this.paramDraft,
      user: JSON.parse(localStorage.getItem("user")).email,
      payment_method: payment_method
      // so_number: so_number.toString()
    };
    //
    console.log(paymentMethod);
    if (paymentMethod == '"T000"') this.lblConOrder = "Confirm Payment";
    ////!!!DELETE THIS
    if (this.simulate.punchOrnumb) {
      data.po_number = data.po_number.toString();
      this.simulate.punchOrnumb = data.po_number;
    } else {
      data.po_number = "-";
      this.simulate.punchOrnumb = "-";
    }

    if (this.simulate.remarks) {
      data.remarks = data.remarks.toString();
      this.simulate.remarks = data.remarks;
    } else {
      data.remarks = "-";
      this.simulate.remarks = "-";
    }

    data.po_number = data.po_number.toString();
    data.bill_to = data.bill_to.toString().split(" - ");
    data.bill_to = data.bill_to[0];
    data.ship_to = data.ship_to.toString().split(" - ");
    data.ship_to = data.ship_to[0];
    data.sold_to = data.sold_to.toString().split(" - ");
    data.sold_to = data.sold_to[0];
    data.delivery_priority = data.delivery_priority.toString();
    data.remarks = data.remarks.toString();
    if (data.remarks.toString().toLowerCase() == "cash") {
      data.payment_trigger = '"T000"';
    }

    $("#tableSparepart #material_code").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tableSparepart #base_unit_of_measure").each(function () {
      data.unit_of_measure.push($(this).text());
    });
    $("#tableSparepart #sloc").each(function () {
      data.sloc.push($(this).text());
    });
    $("#tableSparepart input#quantity").each(function () {
      data.qty.push(parseInt(this.value).toString());
    });

    $("#tableSparepartConfirm #kwmeng").each(function () {
      data.ordered.push($(this).text());
    });
    let allocated_material = 0;
    $("#tableSparepartConfirm #zkwmeng2").each(function () {
      data.allocated.push($(this).text());
      allocated_material = allocated_material + parseInt($(this).text());
    });
    $("#tableSparepartConfirm #netpr").each(function () {
      data.unit_price.push($(this).text());
    });
    $("#tableSparepartConfirm #zdiscount").each(function () {
      data.discount.push($(this).text());
    });

    var posnr = 10;

    for (var i = 0; i < rows; i++) {
      if (posnr.toString().length == 2) {
        data.posnr.push("0000" + posnr.toString()).toString();
        posnr += 10;
      } else if (posnr.toString().length == 3) {
        data.posnr.push("000" + posnr.toString()).toString();
        posnr += 10;
      } else if (posnr.toString().length == 4) {
        data.posnr.push("00" + posnr.toString()).toString();
        posnr += 10;
      } else if (posnr.toString().length == 5) {
        data.posnr.push("0" + posnr.toString()).toString();
        posnr += 10;
      }
    }

    if (paymentMethod == '"T000"') {
      this.paymentType = "Cash";
    } else {
      this.paymentType = "Credit";
    }

    data.user_id = data.user_id.toString();
    //
    console.log(data);

    $("#tableSparepart #check_duplicate").each(function () {
      console.log($(this).text());
      if ($(this).text() == "yes") {
        check_duplicate++;
      }
    });

    $("#tableSparepart #check_oil").each(function () {
      console.log($(this).text());
      if ($(this).text() == "yes") {
        check_oil++;
      }
    });

    $("#tableSparepart #check_supersession").each(function () {
      console.log($(this).text());
      if ($(this).text() == "yes") {
        check_supersession++;
      }
    });

    // let allocated_material = 0;
    // for (let i = 0; i < data.allocated.length; i++) {
    //   allocated_material += parseInt(data.allocated[i])
    // }
    //
    if (!data.qty.includes("0")) {
      if (check_duplicate > 1) {
        alert("Please Remove Duplicate / Reupload File Before Simulate ");
        check_duplicate = 0;
        this.spinner.hide();
      } else if (check_oil > 0 && check_oil !== data.material_number.length) {
        alert("Please Not Combine Oil Before Submit ");
        check_oil = 0;
        this.spinner.hide();
      } else if (check_supersession > 0) {
        alert("This part is changed, please input the latest part number");
        check_supersession = 0;
        this.spinner.hide();
      } else {
        // if (allocated_material > 0) {
        this.sparepartsOrder.submitOrder(data).subscribe(
          res => {
            var data_sap = [];
            data_sap.push(res);
            console.log(data_sap);
            if (data_sap[0].status_item.includes("Unknown")) {
              this.spinner.hide();
              alert(data_sap[0].messages);
            } else {
              this.data_sap_dcs = data_sap[0].result.output;

              if (data_sap[0].status == "success") {
                if (paymentMethod == '"T000"') {
                  this.Cash = true;
                  // if (data.remarks.toString().toLowerCase() !== "cash") {
                  //   this.Cash = false;
                  // }
                  //
                  if (
                    data_sap[0].result.header[0].message ==
                    "Please Confirm released Credit Limit to HPPI"
                  ) {
                    alert(`This order is blocked. Please contact HPPI.`);
                  }
                  let zkwmeng2 = 0;
                  for (let i = 0; i < data_sap[0].result.output.length; i++) {
                    zkwmeng2 =
                      zkwmeng2 +
                      parseInt(data_sap[0].result.output[i].zkwmeng2);
                  }
                  if (zkwmeng2 == 0) this.Cash = false;
                  this.sparepartsOrder.storeHeaderOrder(
                    data_sap[0].result.header[0]
                  );
                  let j = 1;
                  for (let i = 0; i < this.data_sap_dcs.length; i++) {
                    if (this.data_sap_dcs[i].posnr_so.split("").pop() == 0) {
                      this.data_sap_dcs[i].number = j + ".";
                      j++;
                    }
                    this.data_sap_dcs[i].kwmeng = Math.round(
                      this.data_sap_dcs[i].kwmeng
                    );
                    this.data_sap_dcs[i].zkwmeng2 = Math.round(
                      this.data_sap_dcs[i].zkwmeng2
                    );
                    this.data_sap_dcs[i].backorder = Math.round(
                      this.data_sap_dcs[i].kwmeng -
                      this.data_sap_dcs[i].zkwmeng2
                    );
                    this.data_sap_dcs[i].netpr_gros =
                      this.data_sap_dcs[i].netpr_gros * 100;
                    this.data_sap_dcs[i].netwr_gros =
                      this.data_sap_dcs[i].netwr_gros * 100;
                    this.data_sap_dcs[i].zdiscount_gros =
                      this.data_sap_dcs[i].zdiscount_gros.replace("-", "") *
                      100;

                    // if(this.data_sap_dcs[i].waerk === "IDR"){
                    //   this.data_sap_dcs[i].netpr = this.data_sap_dcs[i].netpr * 100 ;
                    //   this.data_sap_dcs[i].netwr = this.data_sap_dcs[i].netwr * 100;
                    //   this.data_sap_dcs[i].zdiscount = this.data_sap_dcs[i].zdiscount * 100;
                    //
                    //   this.total_discount = Math.round(this.data_sap_dcs[i].zdiscount) * 100;
                    //   this.total_allocated = Math.round(this.data_sap_dcs[i].znetwr2) * 100;
                    //   this.vat = Math.round(this.data_sap_dcs[i].zmwsbp2) * 100;
                    // }
                    this.total_allocated_with_vat =
                      this.total_allocated + this.vat;
                    // console.log(this.data_sap_dcs[i].matnr + ' - ' + ' ');
                    // console.log(this.data_sap_dcs[0].netwr);
                    // this.sparepartsOrder.getMaterial(this.data_sap_dcs[i].matnr + ' - ' + 'a').subscribe(res => {
                    //   this.data_sap_dcs[i].matwa = res["material_description"];
                    //   this.data_sap_dcs[i].model = res["model"]
                    // }, err => {
                    //   console.log(err);
                    // });
                  }
                  this.confirmSpare = true;
                  this.simulateSpare = false;
                  this.entrySparepartform = false;
                  this.total_allocated_before_discount = parseInt(
                    JSON.parse(localStorage.getItem("orderHeaderData"))
                      .znetwr2_gros
                  ) * 100;
                  this.total_discount = parseInt(
                    JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).zdiscount.replace("-", "")
                  ) * 100;
                  this.total_allocated = parseInt(
                    JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2
                  ) * 100;
                  this.vat = parseInt(
                    JSON.parse(localStorage.getItem("orderHeaderData")).zmwsbp2
                  ) * 100;
                  this.total_allocated_with_vat =
                    this.total_allocated + this.vat;
                  this.so_number = JSON.parse(
                    localStorage.getItem("orderHeaderData")
                  ).vbeln;
                  if (data.allocated.length !== 0) this.Cash = false;
                } else {
                  this.sparepartsOrder.storeHeaderOrder(
                    data_sap[0].result.header[0]
                  );

                  let zkwmeng2 = 0;
                  for (let i = 0; i < data_sap[0].result.output.length; i++) {
                    zkwmeng2 =
                      zkwmeng2 +
                      parseInt(data_sap[0].result.output[i].zkwmeng2);
                  }
                  if (zkwmeng2 == 0) this.Cash = false;

                  let j = 1;
                  for (let i = 0; i < this.data_sap_dcs.length; i++) {
                    if (this.data_sap_dcs[i].posnr_so.split("").pop() == 0) {
                      this.data_sap_dcs[i].number = j + ".";
                      j++;
                    }
                    this.data_sap_dcs[i].kwmeng = Math.round(
                      this.data_sap_dcs[i].kwmeng
                    );
                    this.data_sap_dcs[i].zkwmeng2 = Math.round(
                      this.data_sap_dcs[i].zkwmeng2
                    );
                    this.data_sap_dcs[i].backorder = Math.round(
                      this.data_sap_dcs[i].kwmeng -
                      this.data_sap_dcs[i].zkwmeng2
                    );
                    this.data_sap_dcs[i].netpr_gros =
                      this.data_sap_dcs[i].netpr_gros * 100;
                    this.data_sap_dcs[i].netwr_gros =
                      this.data_sap_dcs[i].netwr_gros * 100;
                    this.data_sap_dcs[i].zdiscount_gros =
                      this.data_sap_dcs[i].zdiscount_gros.replace("-", "") *
                      100;
                    // if(this.data_sap_dcs[0].waerk === "IDR"){
                    //   this.data_sap_dcs[0].netpr = this.data_sap_dcs[0].netpr * 100 ;
                    //   this.data_sap_dcs[0].netwr = this.data_sap_dcs[0].netwr * 100;
                    //   this.data_sap_dcs[0].zdiscount = this.data_sap_dcs[0].zdiscount * 100;
                    // }
                    // console.log(this.data_sap_dcs[i].matnr + ' - ' + ' ');
                    // this.sparepartsOrder.getMaterial(this.data_sap_dcs[i].matnr + ' - ' + 'a').subscribe(res => {
                    //   this.data_sap_dcs[i].matwa = res["material_description"];
                    // }, err => {
                    //   console.log(err);
                    // });
                  }
                  this.confirmSpare = true;
                  this.simulateSpare = false;
                  this.entrySparepartform = false;
                  this.total_allocated_before_discount = parseInt(
                    JSON.parse(localStorage.getItem("orderHeaderData"))
                      .znetwr2_gros
                  );
                  this.total_discount = parseInt(
                    JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).zdiscount.replace("-", "")
                  );
                  this.total_allocated = parseInt(
                    JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2
                  );
                  this.vat = parseInt(
                    JSON.parse(localStorage.getItem("orderHeaderData")).zmwsbp2
                  );
                  if (this.data_sap_dcs[0].waerk == "IDR") {
                    this.total_allocated_before_discount =
                      parseInt(
                        JSON.parse(localStorage.getItem("orderHeaderData"))
                          .znetwr2_gros
                      ) * 100;
                    this.total_discount =
                      parseInt(
                        JSON.parse(
                          localStorage.getItem("orderHeaderData")
                        ).zdiscount.replace("-", "")
                      ) * 100;
                    this.total_allocated =
                      parseInt(
                        JSON.parse(localStorage.getItem("orderHeaderData"))
                          .znetwr2
                      ) * 100;
                    this.vat =
                      parseInt(
                        JSON.parse(localStorage.getItem("orderHeaderData"))
                          .zmwsbp2
                      ) * 100;
                  }
                  this.total_allocated_with_vat =
                    this.total_allocated + this.vat;
                  this.so_number = JSON.parse(
                    localStorage.getItem("orderHeaderData")
                  ).vbeln;
                  if (data.allocated.length !== 0) this.Cash = false;
                  // if ()
                }
              } else {
                this.spinner.hide();
                alert(data_sap[0].messages);
              }
              this.spinner.hide();
            }
          },
          err => {
            alert(Object.values(err));
            console.log(err);
            this.spinner.hide();
          }
        );
        // }
        // else {
        //   //
        //   alert('Order being process in back order!')
        //   this.sparepartsOrder.submitOrder(data).subscribe(res => {
        //     var data_sap = [];
        //     data_sap.push(res);
        //     this.sparepartsOrder.storeHeaderOrder(data_sap[0].result.header[0]);
        //     this.confirmSpare = true;
        //     this.simulateSpare = false;
        //     this.entrySparepartform = false;
        //     this.spinner.hide()
        //   }, err => {
        //     console.log(err)
        //     this.spinner.hide()
        //   })
        // }
      }
    } else {
      alert("Please Add Quantity");
    }
  }

  subOrderAfterSimulate() {
    var user_id = JSON.parse(localStorage.getItem("user")).id;
    this.customerAccount = localStorage.getItem("bank_transfer_va")
    var rows: any = document
      .getElementById("tableSparepartAfterSimulate")
      .getElementsByTagName("tbody")[0]
      .getElementsByClassName("ztr").length;
    var paymentMethod = `"${this.paymentMethods}"`;
    var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;
    this.modalRef.hide()
    if (paymentMethod == '"T000"') {
      var payment_method = "Cash";
    } else {
      var payment_method = "Credit";
    }
    var data = {
      customer_code: customer_code,
      po_number: this.simulate.punchOrnumb,
      bill_to: this.simulate.billpay,
      ship_to: this.simulate.shipto,
      delivery_priority: this.simulate.delpri,
      remarks: this.simulate.remarks,
      material_number: [],
      qty: [],
      payment_status: "Cash",
      sales_order_number: "9999",
      user_id: user_id,
      unit_of_measure: [],
      sloc: [],
      sold_to: this.simulate.billpay,
      posnr: [],
      order_date: formatDate(this.today, "yyyy-MM-dd", "en-US", "+0530"),
      payment_trigger: paymentMethod,
      ordered: [],
      allocated: [],
      backorder: [],
      unit_price: [],
      discount: [],
      total_amount: this.totalConfirmationOrderAmount(),
      total_discount: this.total_discount,
      total_allocated: this.total_allocated,
      vat: this.vat,
      total_allocated_with_vat: this.total_allocated_with_vat,
      is_draft: this.is_draft,
      param: this.paramDraft,
      user: JSON.parse(localStorage.getItem("user")).email,
      payment_method: payment_method
    };
    //

    if (paymentMethod == '"T000"') this.lblConOrder = "Confirm Payment";

    ////!!!DELETE THIS
    if (this.simulate.punchOrnumb) {
      data.po_number = data.po_number.toString();
      this.simulate.punchOrnumb = data.po_number;
    } else {
      data.po_number = "-";
      this.simulate.punchOrnumb = "-";
    }

    if (this.simulate.remarks) {
      data.remarks = data.remarks.toString();
      this.simulate.remarks = data.remarks;
    } else {
      data.remarks = "-";
      this.simulate.remarks = "-";
    }

    data.po_number = data.po_number.toString();
    data.bill_to = data.bill_to.toString().split(" - ");
    data.bill_to = data.bill_to[0];
    data.ship_to = data.ship_to.toString().split(" - ");
    data.ship_to = data.ship_to[0];
    data.sold_to = data.sold_to.toString().split(" - ");
    data.sold_to = data.sold_to[0];
    data.delivery_priority = data.delivery_priority.toString();
    data.remarks = data.remarks.toString();

    if (data.remarks.toString().toLowerCase() === "cash") {
      data.payment_trigger = '"T000"';
    }

    $("#tableSparepartAfterSimulate #zmatnr").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tableSparepartAfterSimulate #zbase_unit_of_measure").each(function () {
      data.unit_of_measure.push($(this).text());
    });
    $("#tableSparepartAfterSimulate #zsloc").each(function () {
      data.sloc.push($(this).text());
    });
    $("#tableSparepartAfterSimulate #zkwmeng").each(function () {
      data.qty.push($(this).text());
    });

    $("#tableSparepartAfterSimulate #zkwmeng").each(function () {
      data.ordered.push($(this).text());
    });
    var allocated_material = 0;
    $("#tableSparepartAfterSimulate #zzkwmeng2").each(function () {
      data.allocated.push($(this).text());
      allocated_material += parseInt($(this).text());
    });
    $("#tableSparepartAfterSimulate #znetpr").each(function () {
      data.unit_price.push($(this).text());
    });
    $("#tableSparepartAfterSimulate #zzdiscount").each(function () {
      data.discount.push($(this).text());
    });

    var posnr = 10;

    for (var i = 0; i < rows; i++) {
      if (posnr.toString().length == 2) {
        data.posnr.push("0000" + posnr.toString()).toString();
        posnr += 10;
      } else if (posnr.toString().length == 3) {
        data.posnr.push("000" + posnr.toString()).toString();
        posnr += 10;
      } else if (posnr.toString().length == 4) {
        data.posnr.push("00" + posnr.toString()).toString();
        posnr += 10;
      } else if (posnr.toString().length == 5) {
        data.posnr.push("0" + posnr.toString()).toString();
        posnr += 10;
      }
    }

    if (paymentMethod == '"T000"') {
      this.paymentType = "Cash";
    } else {
      this.paymentType = "Credit";
    }

    data.user_id = data.user_id.toString();
    //
    console.log(data);

    //
    if (!data.qty.includes("0")) {
      this.spinner.show();
      if (allocated_material > 0) {
        this.sparepartsOrder.submitOrder(data).subscribe(
          res => {
            console.log(res);
            var data_sap = [];
            data_sap.push(res);
            console.log(data_sap);
            this.data_sap_dcs = data_sap[0].result.output;
            console.log(this.data_sap_dcs.length);

            if (data_sap[0].status == "success") {
              if (paymentMethod == '"T030"') {
                if (data.remarks.toString().toLowerCase() !== "cash") {
                  this.Cash = false;
                }
                //
                if (
                  data_sap[0].result.header[0].message ==
                  "Please Confirm released Credit Limit to HPPI"
                ) {
                  alert(`This order is blocked. Please contact HPPI.`);
                }
                this.sparepartsOrder.storeHeaderOrder(
                  data_sap[0].result.header[0]
                );
                let j = 1;
                for (let i = 0; i < this.data_sap_dcs.length; i++) {
                  if (this.data_sap_dcs[i].posnr_so.split("").pop() == 0) {
                    this.data_sap_dcs[i].number = j + ".";
                    j++;
                  }
                  // console.log(this.data_sap_dcs);
                  this.data_sap_dcs[i].kwmeng = Math.round(
                    this.data_sap_dcs[i].kwmeng
                  );
                  this.data_sap_dcs[i].zkwmeng2 = Math.round(
                    this.data_sap_dcs[i].zkwmeng2
                  );
                  this.data_sap_dcs[i].backorder = Math.round(
                    this.data_sap_dcs[i].kwmeng - this.data_sap_dcs[i].zkwmeng2
                  );
                  this.data_sap_dcs[i].netpr_gros =
                    this.data_sap_dcs[i].netpr_gros * 100;
                  this.data_sap_dcs[i].netwr_gros =
                    this.data_sap_dcs[i].netwr_gros * 100;
                  this.data_sap_dcs[i].zdiscount_gros =
                    this.data_sap_dcs[i].zdiscount_gros.replace("-", "") * 100;
                  // if(this.data_sap_dcs[0].waerk == "IDR"){
                  //   this.data_sap_dcs[i].netpr = this.data_sap_dcs[i].netpr * 100 ;
                  //   this.data_sap_dcs[i].netwr = this.data_sap_dcs[i].netwr * 100;
                  //   this.data_sap_dcs[i].zdiscount = this.data_sap_dcs[i].zdiscount * 100;
                  // }
                  // console.log(this.data_sap_dcs[i].matnr + ' - ' + ' ');
                  this.sparepartsOrder
                    .getMaterial(this.data_sap_dcs[i].matnr + " - " + "a")
                    .subscribe(
                      res => {
                        this.data_sap_dcs[i].matwa =
                          res["material_description"];
                        this.data_sap_dcs[i].model = res["model"];
                      },
                      err => {
                        console.log(err);
                      }
                    );
                }
                this.confirmSpare = true;
                this.simulateSpare = false;
                this.entrySparepartform = false;
                this.total_allocated_before_discount = parseInt(
                  JSON.parse(localStorage.getItem("orderHeaderData"))
                    .znetwr2_gros
                );
                this.total_discount = JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).zdiscount.replace("-", "");
                this.total_allocated = JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).znetwr2;
                this.vat = JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).zmwsbp2;
                if (this.data_sap_dcs[0].waerk == "IDR") {
                  this.total_allocated_before_discount =
                    parseInt(
                      JSON.parse(localStorage.getItem("orderHeaderData"))
                        .znetwr2_gros
                    ) * 100;
                  this.total_discount =
                    JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).zdiscount.replace("-", "") * 100;
                  this.total_allocated =
                    JSON.parse(localStorage.getItem("orderHeaderData"))
                      .znetwr2 * 100;
                  this.vat =
                    JSON.parse(localStorage.getItem("orderHeaderData"))
                      .zmwsbp2 * 100;
                }
                this.total_allocated_with_vat = this.total_allocated + this.vat;
                this.so_number = JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).vbeln;
              } else {
                this.sparepartsOrder.storeHeaderOrder(
                  data_sap[0].result.header[0]
                );
                let j = 1;
                for (let i = 0; i < this.data_sap_dcs.length; i++) {
                  if (this.data_sap_dcs[i].posnr_so.split("").pop() == 0) {
                    this.data_sap_dcs[i].number = j + ".";
                    j++;
                  }
                  this.data_sap_dcs[i].kwmeng = Math.round(
                    this.data_sap_dcs[i].kwmeng
                  );
                  this.data_sap_dcs[i].zkwmeng2 = Math.round(
                    this.data_sap_dcs[i].zkwmeng2
                  );
                  this.data_sap_dcs[i].backorder = Math.round(
                    this.data_sap_dcs[i].kwmeng - this.data_sap_dcs[i].zkwmeng2
                  );
                  this.data_sap_dcs[i].netpr_gros =
                    this.data_sap_dcs[i].netpr_gros * 100;
                  this.data_sap_dcs[i].netwr_gros =
                    this.data_sap_dcs[i].netwr_gros * 100;
                  this.data_sap_dcs[i].zdiscount_gros =
                    this.data_sap_dcs[i].zdiscount_gros.replace("-", "") * 100;
                  // if(this.data_sap_dcs[0].waerk == "IDR"){
                  //   this.data_sap_dcs[i].netpr = this.data_sap_dcs[i].netpr * 100 ;
                  //   this.data_sap_dcs[i].netwr = this.data_sap_dcs[i].netwr * 100;
                  //   this.data_sap_dcs[i].zdiscount = this.data_sap_dcs[i].zdiscount * 100;
                  // }
                  // console.log(this.data_sap_dcs[i].matnr + ' - ' + ' ');
                  this.sparepartsOrder
                    .getMaterial(this.data_sap_dcs[i].matnr + " - " + "a")
                    .subscribe(
                      res => {
                        this.data_sap_dcs[i].matwa =
                          res["material_description"];
                      },
                      err => {
                        console.log(err);
                      }
                    );
                }
                this.confirmSpare = true;
                this.simulateSpare = false;
                this.entrySparepartform = false;
                this.total_allocated_before_discount =
                  parseInt(
                    JSON.parse(localStorage.getItem("orderHeaderData"))
                      .znetwr2_gros
                  ) * 100;
                this.total_discount =
                  JSON.parse(
                    localStorage.getItem("orderHeaderData")
                  ).zdiscount.replace("-", "") * 100;
                this.total_allocated =
                  JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                  100;
                this.vat =
                  JSON.parse(localStorage.getItem("orderHeaderData")).zmwsbp2 *
                  100;
                // if(JSON.parse(localStorage.getItem('orderHeaderData')).waerk == "IDR"){
                //   this.total_discount = Math.round(JSON.parse(localStorage.getItem('orderHeaderData')).zdiscount) * 100;
                //   this.total_allocated = Math.round(JSON.parse(localStorage.getItem('orderHeaderData')).znetwr2) * 100;
                //   this.vat = Math.round(JSON.parse(localStorage.getItem('orderHeaderData')).zmwsbp2) * 100;
                // }
                this.total_allocated_with_vat = this.total_allocated + this.vat;
                this.so_number = JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).vbeln;
              }
            } else {
              this.spinner.hide();
              alert("heho");
            }
            this.spinner.hide();
          },
          err => {
            this.spinner.hide();
            console.error(err);
            alert("error");
          }
        );
      } else {
        this.Cash = false;
        this.sparepartsOrder.submitOrder(data).subscribe(
          res => {
            console.log(res);
            var data_sap = [];
            data_sap.push(res);
            this.sparepartsOrder.storeHeaderOrder(data_sap[0].result.header[0]);
            if (
              data_sap[0].result.header[0].message ==
              "Please Confirm released Credit Limit to HPPI"
            ) {
              alert(`This order is blocked. Please contact HPPI.`);
            }
            let j = 1;
            for (let i = 0; i < this.data_sap_dcs.length; i++) {
              if (this.data_sap_dcs[i].posnr_so.split("").pop() == 0) {
                this.data_sap_dcs[i].number = j + ".";
                j++;
              }
              this.data_sap_dcs[i].kwmeng = this.data_sap_dcs[i].kwmeng;
              this.data_sap_dcs[i].zkwmeng2 = this.data_sap_dcs[i].zkwmeng2;
              this.data_sap_dcs[i].backorder =
                this.data_sap_dcs[i].kwmeng - this.data_sap_dcs[i].zkwmeng2;
              this.data_sap_dcs[i].netpr_gros = this.data_sap_dcs[i].netpr_gros;
              this.data_sap_dcs[i].netwr_gros = this.data_sap_dcs[i].netwr_gros;
              this.data_sap_dcs[i].zdiscount_gros = this.data_sap_dcs[
                i
              ].zdiscount_gros;
              console.log(i);
              this.sparepartsOrder
                .getMaterial(this.data_sap_dcs[i].matnr + " - " + "a")
                .subscribe(
                  res => {
                    this.data_sap_dcs[i].matwa = res["material_description"];
                  },
                  err => {
                    console.log(err);
                  }
                );
            }
            this.confirmSpare = true;
            this.simulateSpare = false;
            this.entrySparepartform = false;
            this.total_allocated_before_discount =
              parseInt(
                JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2_gros
              ) * 100;
            this.total_discount =
              JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).zdiscount.replace("-", "") * 100;
            this.total_allocated =
              JSON.parse(localStorage.getItem("orderHeaderData")).znetwr * 100;
            this.vat =
              JSON.parse(localStorage.getItem("orderHeaderData")).zmwsbp2 * 100;
            this.total_allocated_with_vat = this.total_allocated + this.vat;
            this.so_number = JSON.parse(
              localStorage.getItem("orderHeaderData")
            ).vbeln;

            this.confirmSpare = true;
            this.simulateSpare = false;
            this.entrySparepartform = false;
            alert("Order being process in back order!");
            this.spinner.hide();
          },
          err => {
            this.spinner.hide();
            console.log(err);
          }
        );
        // this.spinner.hide()
        // alert("Order being process in back order!");
        this.confirmSpare = true;
        this.simulateSpare = false;
        this.entrySparepartform = false;
      }
    } else {
      this.spinner.hide();
      alert("Please Add Quantity");
    }
  }

  checkTC(){
    console.log(this.online_payment_type)
    if(this.online_payment_type == "Credit Card"){
      if($('input[name=t_and_c]:checked').length > 0){
        this.conOrder()
      }else{
        alert('Please Check T&C before Submit.')
      }
    }
    else{
      this.conOrder()
    }
  }

  conOrder() {
    var zkwmeng2 = 0;
    for (let i = 0; i < this.data_sap_dcs.length; i++) {
      zkwmeng2 = zkwmeng2 + this.data_sap_dcs[i].zkwmeng2;
    }
    if (zkwmeng2 !== 0) {
      this.spinner.show();
      var data = {
        confirmid: JSON.parse(localStorage.getItem("orderHeaderData"))
          .confirmid,
        vbeln: JSON.parse(localStorage.getItem("orderHeaderData")).vbeln,
        idtrs: JSON.parse(localStorage.getItem("orderHeaderData")).idtrs,
        approve: "1",
        spart: "20",
        cash_payment_type: this.cash_payment_type,
        user: JSON.parse(localStorage.getItem("user")).email
      };

      var paymentMethod = `"${this.paymentMethods}"`;
      if (paymentMethod == '"T030"') {
        alert("Order Success");
        this.spinner.hide();
        location.reload();
        // if (!this.simulate.remarks) this.simulate.remarks = "remarks";
        // if (this.simulate.remarks.toString().toLowerCase() === "cash") {
        //   localStorage.setItem("payment", '"T000"');
        //   console.log(localStorage.getItem("payment"));
        //   this.sparepartsOrder.paymentOrder(data).subscribe(
        //     res => {
        //       console.log(res);
        //       localStorage.setItem("payment", '"T030"');
        //       this.so_number = JSON.parse(
        //         localStorage.getItem("orderHeaderData")
        //       ).vbeln;
        //       this.total_allocated_with_vat = this.total_allocated + this.vat;
        //       this.total_allocated =
        //         JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
        //         100;
        //       // if(JSON.parse(localStorage.getItem('orderHeaderData')).waers == 'IDR'){
        //       //   this.total_allocated = Math.round(JSON.parse(localStorage.getItem('orderHeaderData')).znetwr2) * 100;
        //       // }
        //       this.payment_deadline = new Date();
        //       this.payment_deadline.setDate(
        //         this.payment_deadline.getDate() + 9
        //       );
        //       this.bankTF = true;
        //       this.confirmSpare = false;
        //       this.spinner.hide();
        //     },
        //     err => {}
        //   );
        // } else {
        //   alert("Order Success");
        //   this.spinner.hide();
        //   location.reload();
        // }
      } else {

        this.sparepartsOrder.paymentOrder(data).subscribe(
          res => {
            console.log(res);
            this.so_number = JSON.parse(
              localStorage.getItem("orderHeaderData")
            ).vbeln;
            this.total_allocated =
              JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 * 100;
            this.payment_deadline = new Date();
            this.payment_deadline.setDate(this.payment_deadline.getDate() + 9);
            this.bankTF = true;
            this.confirmSpare = false;
            if (res["status"] == "success") {

              let dateNow = moment()
              let transactionDateNow = dateNow.format("YYYY-MM-DD HH:mm:ss")
              let transactionDateExpire = dateNow.add('10', 'minutes').format("YYYY-MM-DD HH:mm:ss")

              let userEmail = JSON.parse(localStorage.getItem("dealer")).email1[0]
              // let userEmail = "arbhasyech@gmail.com"

              let additionalData = `${res["result"][0]["gjahr"]} - ${res["result"][0]["belnr"]} - ${res["result"][0]["bukrs"]} - ${userEmail}`

              let params = {
                type: this.online_payment_type,
                channelId: "",
                currency: "IDR",
                transactionNo: JSON.parse(localStorage.getItem("orderHeaderData")).vbeln,
                transactionAmount: this.total_allocated_with_vat.toString(),
                transactionDate: transactionDateNow,
                transactionExpire: transactionDateExpire,
                description: "DCSTRANSACTION",
                customerAccount: "",
                customerName: localStorage.getItem("dealer_name"),
                freeTexts: [{
                  indonesian: "",
                  english: ""
                }],
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + JSON.parse(localStorage.getItem("orderHeaderData")).vbeln
              }

              let paramsNonVa = {
                channelId: "",
                transactionNo: JSON.parse(localStorage.getItem("orderHeaderData")).vbeln,
                transactionAmount: this.total_allocated_with_vat.toString(),
                transactionDate: transactionDateNow,
                transactionExpire: transactionDateExpire,
                description: "DCS TRANSACTION",
                customerName: localStorage.getItem("dealer_name"),
                customerEmail: userEmail,
                customerPhone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0],
                customerAccount: "",
                type: this.online_payment_type,
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + JSON.parse(localStorage.getItem("orderHeaderData")).vbeln
              }

              let paramsCreditCard = {
                channelId: "HONDAPOWER",
                type: "Credit Card",
                transactionNo: JSON.parse(localStorage.getItem("orderHeaderData")).vbeln,
                transactionAmount: this.total_allocated_with_vat.toString(),
                transactionDate: transactionDateNow,
                transactionExpire: transactionDateExpire,
                description: "DCS DESC",
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + JSON.parse(localStorage.getItem("orderHeaderData")).vbeln
              }

              if (this.online_payment_type == "BCA VA") {
                params.type = "BCA VA"
                params.channelId = "HONDAPOWERBVA01"
                params.customerAccount = "39252" + localStorage.getItem("bca_va")
                this.customerAccount = "39252" + localStorage.getItem("bca_va")
              } else if (this.online_payment_type == "KlikBCA") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKB01"
                let userID = {
                  userID: this.userIDBCA
                }
                Object.assign(paramsNonVa,userID)
                paramsNonVa.customerAccount = this.userIDBCA
                this.customerAccount = this.userIDBCA
              } else if (this.online_payment_type == "BCA KlikPay") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKP01"
                paramsNonVa.customerAccount = localStorage.getItem("bank_transfer_va")
                this.customerAccount = localStorage.getItem("bank_transfer_va")
              } else if (this.online_payment_type == "CIMB Click") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERCIMBCL01"
                paramsNonVa.customerAccount = localStorage.getItem("bank_transfer_va")
                this.customerAccount = localStorage.getItem("bank_transfer_va")
              } else if (this.online_payment_type == "iPay BNI") {
                paramsNonVa.type = "iPay BNI"
                paramsNonVa.channelId = "HONDAIPAY01"
                paramsNonVa.customerAccount = localStorage.getItem("bank_transfer_va")
                this.customerAccount = localStorage.getItem("bank_transfer_va")
              } else if (this.online_payment_type == "Permata VA") {
                params.type = "Permata VA"
                params.channelId = "HONDAPOWERPVA01"
                params.customerName = localStorage.getItem("dealer_name").substr(0,20)
                params.customerAccount = "873584" + localStorage.getItem("permata_va")
                this.customerAccount = "873584" + localStorage.getItem("permata_va")
              }

              if (this.online_payment_type == "Permata VA" || this.online_payment_type == "BCA VA") {
                this.cbuOrder.sendPayment(params).subscribe(result => {
                  if (result["insertStatus"] == "00") {

                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.error(error)
                  this.spinner.hide()
                })
              } else if (this.online_payment_type == "Credit Card") {
                console.log(paramsCreditCard)
                this.cbuOrder.sendPayment(paramsCreditCard).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    if (result["redirectURL"] !== "") {
                      location.href = result["redirectURL"]
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.error(error)
                  this.spinner.hide()
                })
              } else if (this.online_payment_type == "KlikBCA" ||
                this.online_payment_type == "BCA KlikPay" ||
                this.online_payment_type == "CIMB Click" ||
                this.online_payment_type == "iPay BNI") {
                this.cbuOrder.sendPayment(paramsNonVa).subscribe(result => {
                  if (result["insertStatus"] == "00") {

                    let redirectURL = result["redirectURL"]
                    if (redirectURL) {
                      let form = document.createElement("form")
                      form.setAttribute("method", "POST")
                      form.setAttribute("enctype", "application/x-www-form-urlencoded")
                      form.setAttribute("action", redirectURL)
                      for (const key in result["redirectData"]) {
                        if (result["redirectData"].hasOwnProperty(key)) {
                          const element = result["redirectData"][key];
                          let input = document.createElement("input")
                          input.setAttribute("type", "hidden")
                          input.setAttribute("name", key)
                          input.setAttribute("value", element)
                          form.appendChild(input)
                        }
                      }
                      document.body.appendChild(form)

                      form.submit()
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  this.spinner.hide()
                  console.error(error)
                })
              } else {
                console.log(res);
                this.so_number = JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).vbeln;
                this.total_allocated =
                  JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 * 100;
                this.payment_deadline = new Date();
                this.payment_deadline.setDate(this.payment_deadline.getDate() + 9);
                this.bankTF = true;
                this.confirmSpare = false;
              }
            }
            this.spinner.hide();
          },
          err => {
            this.spinner.hide()
            console.error(err)
          }
        );
      }
    } else {
      alert("Order being process in back order!");
      this.spinner.hide();
      location.reload();
    }
  }

  radioActive() {
    this.disabledPay1 = "";
    this.disabledPay2 = "";
    this.disabledPay3 = "";
    this.disabledPay4 = "";
  }

  downloadSO() {
    let data = {
      sold_to_code: this.simulate.shipto || this.simOrderSpareparts.shipto,
      bill_to_code: this.simulate.billpay || this.simOrderSpareparts.billpay,
      dealer_name: localStorage.getItem("dealer_name"),
      address: JSON.parse(localStorage.getItem("dealer")).address[0] || "-",
      phone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0] || "-",
      fax: JSON.parse(localStorage.getItem("dealer")).fax1[0] || "-",
      po_number: this.simulate.punchOrnumb || this.simOrderSpareparts.punchOrnumb,
      so_number: this.so_number || "-",
      confirm_date: this.jstoday || "-",
      order_type: this.simulate.delpri || this.simOrderSpareparts.delpri,
      currency: "IDR",
      payment: "-",
      email: JSON.parse(localStorage.getItem("dealer")).email1[0] || "-",
      contact: "-",
      material_code: [],
      material_description: [],
      qty: [],
      allocated: [],
      backorder: [],
      unit_price: [],
      total_amount: [],
      ttl_qty: 0,
      ttl_allocated: 0,
      ttl_backorder: 0,
      ttl_unit_price: 0,
      ttl_total_amount: 0,
      discount: [],
      total_discount: 0,
      total_without_vat: 0,
      vat: 0,
      grand_total: 0,
      total_with_vat: 0,
      title: ""
    }

    if (this.so_number == null) {
      data.title = "Order Simulation"
    } else {
      data.title = "Order Confirmation"
    }

    if (this.paymentMethods == "T000")
      data.payment = "Cash"
    else
      data.payment = "Credit"

    if (this.simulateSpare == true) {
      $("#tableSparepartAfterSimulate #zmatnr").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.material_code.push(item)
      })
      $("#tableSparepartAfterSimulate #zmatwa").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.material_description.push(item)
      })
      $("#tableSparepartAfterSimulate #zkwmeng").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.qty.push(item)
      })
      $("#tableSparepartAfterSimulate #zzkwmeng2").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.allocated.push(item)
      })
      $("#tableSparepartAfterSimulate #zbackorder").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.backorder.push(item)
      })
      $("#tableSparepartAfterSimulate #znetpr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.unit_price.push(item)
      })
      $("#tableSparepartAfterSimulate #zzdiscount").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.discount.push(item)
      })
      $("#tableSparepartAfterSimulate #znetwr2").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.total_amount.push(item)
      })

      data.ttl_qty = this.totalSimulateOrderQty()
      data.ttl_allocated = this.totalAllocatedQtySimulate()
      data.ttl_backorder = this.totalSimulateBackorder()

      for (const item of data.unit_price) {
        data.ttl_unit_price += parseInt(item)
      }
      for (const item of data.total_amount) {
        data.ttl_total_amount += parseInt(item)
      }
      for (const item of data.discount) {
        data.total_discount += parseInt(item)
      }
      data.total_without_vat = this.total_allocated_before_discount_simulate
      data.vat = this.total_vat_simulate
      data.grand_total = this.totalConfirmationOrderAmountSimulate()
      data.total_with_vat = this.total_allocated_vat_simulate
    } else {
      $("#tableSparepartConfirm #zmatnr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.material_code.push(item)
      })
      $("#tableSparepartConfirm #zmatwa").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.material_description.push(item)
      })
      $("#tableSparepartConfirm #zkwmeng").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.qty.push(item)
      })
      $("#tableSparepartConfirm #zzkwmeng2").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.allocated.push(item)
      })
      $("#tableSparepartConfirm #zbackorder").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.backorder.push(item)
      })
      $("#tableSparepartConfirm #znetpr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.unit_price.push(item)
      })
      $("#tableSparepartConfirm #zzdiscount").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.discount.push(item)
      })
      $("#tableSparepartConfirm #znetwr2").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.total_amount.push(item)
      })

      data.ttl_qty = this.totalConfirmationOrderQty()
      data.ttl_allocated = this.totalAllocatedQtySubmit()
      data.ttl_backorder = this.totalSubmitBackorder()

      for (const item of data.unit_price) {
        data.ttl_unit_price += parseInt(item)
      }
      for (const item of data.total_amount) {
        data.ttl_total_amount += parseInt(item)
      }
      for (const item of data.discount) {
        data.total_discount += parseInt(item)
      }

      data.total_without_vat = this.total_allocated_before_discount
      data.vat = this.vat
      data.grand_total = this.totalConfirmationOrderAmount()
      data.total_with_vat = this.total_allocated_with_vat
    }
    console.log(data);
    this.cbuOrder.downloadSO(data).subscribe(result => {
      this.downloadFile(result)
    })
  }

  downloadFile(data) {
    const blob = new Blob([data], { type: 'application/pdf' })
    const url = window.URL.createObjectURL(blob)
    // window.location.href = url
    // window.open
    let link = document.createElement('a')
    link.style.display = 'none';
    link.setAttribute("download", `order-confirm.pdf`)
    link.setAttribute('href', url)
    document.body.appendChild(link)
    link.click()
  }

  jenisPay(val) {
    this.radioActive();
    if (val == 1) {
      this.disabledPay3 = "disable-radio";
      this.disabledPay4 = "disable-radio";
      this.cash_payment_type = "Bank Transfer";
    } else {
      this.disabledPay1 = "disable-radio";
      this.disabledPay2 = "disable-radio";
      this.cash_payment_type = "Payment Gateway";
    }
  }

  onlinePaymentType(e) {
    console.log(e)
    
    if (this.disabledPay3 != "disable-radio" && this.disabledPay4 != "disable-radio") {
      if (e == 'klik-bca') {
        this.online_payment_type = "KlikBCA"
        $("input#userIDBCA").prop('disabled', false);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'bca-klikpay') {
        this.online_payment_type = "BCA KlikPay"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'cimb-click') {
        this.online_payment_type = "CIMB Click"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'ipay-bni') {
        this.online_payment_type = "iPay BNI"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'permata-bankva') {
        this.online_payment_type = "Permata VA"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'bca-va') {
        this.online_payment_type = "BCA VA"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'credit-card') {
        this.online_payment_type = "Credit Card"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").show();
        $('label[for="t_and_c"]').show();
      }
    } else {
      this.online_payment_type = "Bank transfer"
      $("input#userIDBCA").prop('disabled', true);
      $("#t_and_c").hide();
      $('label[for="t_and_c"]').hide();
    }
  }
  onQuantityChange(val) {
    var value = "quantity" + (val + 1);
    this.prodSpareSelec[val].qty = $("input[name=" + value + "]").val();
  }

  onQuantityChangeUpload(val) {
    var value = "quantity" + (val + 1);
    this.prodSpareSelec[val].item2 = $("input[name=" + value + "]").val();
  }

  minusOneUpload(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
      // Decrement one only if value is > 1
      $("input[name=" + val + "]").val(currentVal - 1);
      $(".qtyplus")
        .val("+")
        .removeAttr("style");
    } else {
      // Otherwise put a 0 there
      $(".qtyminus")
        .val("-")
        .css("color", "#aaa");
      $(".qtyminus")
        .val("-")
        .css("cursor", "not-allowed");
    }
    this.prodSpareSelec[value].item2 = $("input[name=" + val + "]").val();
  }
  plusONeUpload(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If is not undefined
    if (!isNaN(currentVal)) {
      // Increment only if value is < 20
      if (currentVal < 100) {
        $("input[name=" + val + "]").val(currentVal + 1);
        $(".qtyminus")
          .val("-")
          .removeAttr("style");
      } else {
        $(".qtyplus")
          .val("+")
          .css("color", "#aaa");
        $(".qtyplus")
          .val("+")
          .css("cursor", "not-allowed");
      }
    } else {
      // Otherwise put a 0 there
      $("input[name=" + val + "]").val(1);
    }
    this.prodSpareSelec[value].item2 = $("input[name=" + val + "]").val();
  }

  minusOne(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
      // Decrement one only if value is > 1
      $("input[name=" + val + "]").val(currentVal - 1);
      $(".qtyplus")
        .val("+")
        .removeAttr("style");
    } else {
      // Otherwise put a 0 there
      $(".qtyminus")
        .val("-")
        .css("color", "#aaa");
      $(".qtyminus")
        .val("-")
        .css("cursor", "not-allowed");
    }
    this.prodSpareSelec[value].qty = $("input[name=" + val + "]").val();
  }
  plusONe(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If is not undefined
    if (!isNaN(currentVal)) {
      // Increment only if value is < 20
      if (currentVal < 100) {
        $("input[name=" + val + "]").val(currentVal + 1);
        $(".qtyminus")
          .val("-")
          .removeAttr("style");
      } else {
        $(".qtyplus")
          .val("+")
          .css("color", "#aaa");
        $(".qtyplus")
          .val("+")
          .css("cursor", "not-allowed");
      }
    } else {
      // Otherwise put a 0 there
      $("input[name=" + val + "]").val(1);
    }
    this.prodSpareSelec[value].qty = $("input[name=" + val + "]").val();
  }
  searchSparepats(val) {
    if (this.simulate.delpri) {
      if (val.length > 4) {
        this.sparepartsOrder.getListMaterial(val).subscribe(
          res => {
            // alert(res);
            this.materialCode_Spareparts = res;
          },
          err => {
            console.log(err);
          }
        );
      } else {
        delete this.materialCode_Spareparts;
      }
    } else {
      alert("Please Choose Delivery Priority First !");
      this.matSpare_selec = "";
    }
  }

  addItemUpload() {
    if (this.matSpare_selec != "" && this.matSpare_selec != undefined) {
      this.sparepartsOrder.getSupersession(this.matSpare_selec).subscribe(
        res => {
          let supersession_material = []
          let not_supersession = ""
          for (const key in res["all"]) {
            if (res["all"][key]["supersession_part"] != "-") {
              supersession_material.push(res["all"][key]["material_code"] + " - " + res["all"][key]["supersession_part"])
            } else {
              not_supersession = res["all"][key]["material_code"]
            }
          }
          if (supersession_material.length > 0) {
            alert(
              "This item have supersession : \n" +
              supersession_material.join("\n")
            );
          }
          console.log(not_supersession)
          if (not_supersession == "") {
            alert("Material not found.")
            return
          }
          this.sparepartsOrder
            .getMaterial(not_supersession)
            .subscribe(
              res => {
                var data = [];
                data.push(res);
                console.log(data);
                var check = 0;
                var checkoil = 0;

                if (data[0]) {
                  data[0].item2 = 1;
                  if (document.getElementById("material_code")) {
                    $("#tableSparepart #material_code").each(function () {
                      // console.log(data[0]["material_number"]);
                      // console.log($(this).text());
                      if ($(this).text() == data[0]["material_number"]) {
                        check++;
                      } else {
                        data[0].check_duplicate = "no";
                      }
                    });
                    console.log(check);
                    if (
                      data[0]["material_group1"] ==
                      "005" /*&& data[0]["storage_location"] == '2300'*/
                    ) {
                      $("#tableSparepart #material_code").each(function () {
                        // alert($('#tableSparepart #material_group1').text() == '005' && $('#tableSparepart #sloc').text() != '2300')
                        $("#tableSparepart #material_group1").each(function () {
                          if ($(this).text() != "005") {
                            checkoil++;
                          } else {
                            data[0].check_oil = "no";
                          }
                        });
                        // if ($('#tableSparepart #material_group1').text() != '005' /*|| $('#tableSparepart #sloc').text() != '2300'*/) {
                        //   checkoil++;
                        // } else {
                        //   data[0].check_oil = "no";
                        // }
                      });
                    } else {
                      $("#tableSparepart #material_code").each(function () {
                        // alert($('#tableSparepart #material_group1').text() == '005' && $('#tableSparepart #sloc').text() == '2300')
                        $("#tableSparepart #material_group1").each(function () {
                          if ($(this).text() == "005") {
                            checkoil++;
                          } else {
                            data[0].check_oil = "no";
                          }
                        });
                        // if ($('#tableSparepart #material_group1').text() == '005' /*&& $('#tableSparepart #sloc').text() == '2300'*/) {
                        //   checkoil++;
                        // } else {
                        //   data[0].check_oil = "no";
                        // }
                      });
                    }
                    // console.log(check);
                    if (check > 0) {
                      alert("Please Add Another Item!");
                      check = 0;
                    } else if (checkoil > 0) {
                      alert("Oil cannot be combined with another Item!");
                      checkoil = 0;
                    } else {
                      if (data[0].is_continue !== 1)
                        this.prodSpareSelec.push(data[0]);
                      else
                        alert(
                          "This Material is discontinued. Please contact HPPI"
                        );
                      check = 0;
                    }
                  } else {
                    console.log(data);
                    if (data[0]) {
                      if (data[0].is_continue !== 1)
                        this.prodSpareSelec.push(data[0]);
                      else
                        alert(
                          "This Material is discontinued. Please contact HPPI"
                        );
                    } else {
                      alert(
                        "You don't have permission to order this material."
                      );
                    }
                  }
                } else {
                  alert("You don't have permission to order this material.");
                }
              },
              error => {
                console.log(error);
              }
            );
        },
        err => {
          console.log(err);
        }
      );
      // console.log(this.prodSpareSelec);
    } else {
      alert("Please add code or material code!");
    }
    this.matSpare_selec = "";
  }

  addItem() {
    if (this.matSpare_selec != "" && this.matSpare_selec != undefined) {
      this.sparepartsOrder.getSupersession(this.matSpare_selec).subscribe(
        res => {
          let supersession_material = []
          let not_supersession = ""
          for (const key in res["all"]) {
            if (res["all"][key]["supersession_part"] != "-") {
              supersession_material.push(res["all"][key]["material_code"] + " - " + res["all"][key]["supersession_part"])
            } else {
              not_supersession = res["all"][key]["material_code"]
            }
          }
          if (supersession_material.length > 0) {
            alert(
              "This item have supersession : \n" +
              supersession_material.join("\n")
            );
          }
          console.log(not_supersession)
          if (not_supersession == "") {
            alert("Material not found.")
            return
          }

          this.sparepartsOrder
            .getMaterial(not_supersession)
            .subscribe(
              res => {
                console.log(res);
                var data = [];
                data.push(res);
                console.log(data);
                var check = 0;
                var checkoil = 0;

                if (data[0]) {
                  if (document.getElementById("material_code")) {
                    $("#tableSparepart #material_code").each(function () {
                      // console.log(data[0]["material_number"]);
                      // console.log($(this).text());
                      if ($(this).text() == data[0]["material_number"]) {
                        check++;
                      }
                    });

                    if (data[0]["material_group1"] == "005") {
                      $("#tableSparepart #material_code").each(function () {
                        // let material_group = $('#tableSparepart #material_group1').text()
                        $("#tableSparepart #material_group1").each(function () {
                          if ($(this).text() != "005") {
                            checkoil++;
                          }
                        });
                      });
                    } else {
                      $("#tableSparepart #material_code").each(function () {
                        // let material_group = $('#tableSparepart #material_group1').text()
                        $("#tableSparepart #material_group1").each(function () {
                          if ($(this).text() == "005") {
                            checkoil++;
                          }
                        });
                      });
                    }
                    // console.log(check);
                    if (check > 0) {
                      alert("Please Add Another Item!");
                      check = 0;
                    } else if (checkoil > 0) {
                      alert("Oil cannot be combined with another Item!");
                      checkoil = 0;
                    } else {
                      if (data[0].is_continue !== 1)
                        this.prodSpareSelec.push(data[0]);
                      else
                        alert(
                          "This Material is discontinued. Please contact HPPI"
                        );
                      check = 0;
                    }
                  } else {
                    if (data[0]) {
                      if (data[0].is_continue !== 1)
                        this.prodSpareSelec.push(data[0]);
                      else
                        alert(
                          "This Material is discontinued. Please contact HPPI"
                        );
                    } else {
                      alert(
                        "You don't have permission to order this material."
                      );
                    }
                  }
                } else {
                  alert("You don't have permission to order this material.");
                }
              },
              err => {
                console.log(err);
              }
            );
        },
        err => {
          console.log(err);
        }
      );
      console.log(this.prodSpareSelec);
    } else {
      alert("Please add code or material code!");
    }
    this.matSpare_selec = "";
  }

  delete(val) {
    console.log(val);
    this.prodSpareSelec.splice(val, 1);
  }
  deleteUpload(val) {
    this.prodSpareSelec.splice(val, 1);
    // this.uploadQty.splice(val, 1);
    // console.log($('#tableSparepart #trUpload').text);
    // this.prodSpareSelec.splice(val, 1);
    if (this.prodSpareSelec.length == 0) {
      this.upload = false;
      this.notUpload = true;
    }
  }
  deleteAll() {
    this.prodSpareSelec = [];
    this.upload = false;
    this.notUpload = true;
  }

  // deleteDuplicate() {
  //   // alert('tes');
  //   // this.prodSpareSelec = [];
  //   var $tableRows = $("#tableSparepart tbody tr");
  //   var $rowsToMark = $();
  //
  //   $tableRows.each(function(n) {
  //       var id = this.id;
  //       var example = $(this).find('#material_code').html();
  //
  //       $tableRows.each(function(n) {
  //           var $this;
  //           if (this.id != id) {
  //               $this = $(this);
  //               if ($this.find('#material_code').html() == example) {
  //                   this.prodSelected[0].splice(id, 1)
  //               }
  //           }
  //       });
  //   });
  //
  //   $rowsToMark.css('backgroundColor', 'azure');
  // }

  // totalOrdersUpload() {
  //   var totalOrder = 0;
  //   for (var i = 0; i < this.prodSpareSelec.length; i++) {
  //     if (this.prodSpareSelec.check_duplicate == 'no') {
  //       totalOrder = totalOrder + parseInt(this.prodSpareSelec.uploadQty);
  //     }
  //   }
  //   return totalOrder;
  // }

  totalOrders() {
    var totalOrder = 0;
    for (var i = 0; i < this.prodSpareSelec.length; i++) {
      totalOrder =
        totalOrder + parseInt($("input[name=quantity" + (i + 1) + "]").val());
    }
    return totalOrder;
  }

  totalConfirmationOrderQty() {
    var totalOrder = 0;
    $("#tableSparepartConfirm #zkwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalConfirmationOrderAmountSimulate() {
    var totalOrder = 0;
    $("#tableSparepartAfterSimulate #znetwr").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalConfirmationOrderAmount() {
    var totalOrder = 0;
    $("#tableSparepartConfirm #znetwr").each(function () {
      // console.log(parseInt($(this).text()));
      // alert($(this).text())
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalAllocatedQtySubmit() {
    var allocated = 0;
    $("#tableSparepartConfirm #zzkwmeng2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    if (allocated == 0) this.lblConOrder = "Continue Order";
    return allocated;
  }

  totalAllocatedQtySimulate() {
    var allocated = 0;
    $("#tableSparepartAfterSimulate #zzkwmeng2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    return allocated;
  }

  totalAllocatedAmountSimulate() {
    var allocated = 0;
    $("#tableSparepartAfterSimulate #zkwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    return allocated;
  }

  totalSimulateBackorder() {
    var backorder = 0;
    $("#tableSparepartAfterSimulate #zbackorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }

  totalSubmitBackorder() {
    var backorder = 0;
    $("#tableSparepartConfirm #zbackorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }

  totalDiscountSimulate() {
    var discount = 0;
    $("#tableSparepartAfterSimulate #zdiscount").each(function () {
      // console.log(parseInt($(this).text()));
      discount = discount + parseInt($(this).text());
    });
    return discount;
  }

  totalVATSimulate() {
    var vat = 0;
    $("#tableSparepartAfterSimulate #zzmwsbp2").each(function () {
      // console.log(parseInt($(this).text()));
      vat = vat + parseInt($(this).text());
    });
    return vat;
  }

  totalAllocatedVAT() {
    var backorder = 0;
    // for (var i = 0; i < this.prodSelected.length; i++) {
    //   if(this.prodSelected[i].backorder){
    //     backorder = backorder + this.prodSelected[i].backorder;
    //   }else{
    //     backorder = 0;
    //   }
    // }
    return backorder;
  }

  dateNow() {
    var date = new Date();
    return date;
  }
  count(val) {
    var ttl = 0;
    for (var i = 0; i < val.length; i++) {
      ttl = ttl + parseInt(val[i].qty);
    }
    return ttl;
  }
  totalSimulateOrderQty() {
    var totalOrder = 0;
    $("#tableSparepartAfterSimulate #zkwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }
  shipToCodeValidation() {
    if (!this.simulate.billpay) {
      alert("Please Choose Bill to Name first!");
      this.simulate.shipto = "";
      $("#shiptocode").val($("#shiptocode option:first").val());
    }
  }
  deliveryPriorityValidation() {
    if (!this.simulate.shipto) {
      alert("Please Choose Ship to Name first!");
      this.simulate.delpri = "";
      $("#opDeli").val($("#opDeli option:first").val());
    }
  }
  billToCodeValidation() {
    if (!this.simulate.billpay) {
      this.simulate.shipto = "";
      this.matSpare_selec = "";
      $("#shiptocode").val($("#shiptocode option:first").val());
    }
    this.getBillTo();
  }
  getDraft(param) {
    this.spinner.show();
    this.paramDraft = param;
    this.isDraft = true;
    this.is_draft = "YES";
    console.log(this.paramDraft, this.isDraft);
    this.sparepartsOrder.getDraft(param).subscribe(
      res => {
        var sparepartDraft = [];
        // console.log(res);
        sparepartDraft.push(res);
        console.log(sparepartDraft[0][0].po_number);
        if (sparepartDraft) {
          var data = {
            po_number: sparepartDraft[0][0].po_number,
            bill_to: sparepartDraft[0][0].bill_to,
            ship_to: sparepartDraft[0][0].ship_to,
            remarks: sparepartDraft[0][0].remarks,
            material_number: [],
            qty: [],
            delivery_priority: sparepartDraft[0][0].is_priority,
            sales_order_number: sparepartDraft[0][0].sales_order_number,
            user_id: sparepartDraft[0][0].user_id,
            order_date: sparepartDraft[0][0].order_date
          };
          this.simulate.punchOrnumb = data.po_number;
          this.simulate.billpay = data.bill_to;
          this.simulate.shipto = data.ship_to;

          if (data.delivery_priority == 2) this.simulate.delpri = "Normal";
          else this.simulate.delpri = "Urgent";

          this.simulate.remarks = data.remarks;

          for (let index = 0; index < sparepartDraft[0].length; index++) {
            data.material_number.push(sparepartDraft[0][index].material_number);
            data.qty.push(sparepartDraft[0][index].qty);
          }
          console.log(data);
          this.prodSpareSelec = [];
          let i = 0;
          for (const item of sparepartDraft[0]) {
            this.sparepartsOrder.getMaterial(item.material_number).subscribe(
              res => {
                if (Object.entries(res).length > 0) {
                  // var dataMaterial = [];
                  // dataMaterial.push(res);
                  this.prodSpareSelec.push(res);
                  this.prodSpareSelec[i].qty = item.qty;
                  i++;
                }
              },
              err => {
                this.spinner.hide();
                console.log(err);
              }
            );
          }
          // for (let index = 0; index < this.prodSpareSelec.length; index++) {
          //   this.prodSpareSelec[index].qty = data.qty[index];
          // }
        }
        this.cbuOrder1 = "cbu-order-active";
        this.cbuOrder2 = "";
        this.SpareorderEntry = true;
        this.Sparedraft = false;
        this.spinner.hide();
      },
      err => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }
}
