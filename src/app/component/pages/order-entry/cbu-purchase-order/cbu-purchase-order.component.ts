import { Component, OnInit, ChangeDetectorRef, NgZone, TemplateRef } from "@angular/core";
import { formatDate, Location } from "@angular/common";
import { CbuPurchaseOrderService } from "../../../../services/cbu-purchase-order.service";
import { LocalStorageService } from "ngx-webstorage";
import { NgxSpinnerService } from "ngx-spinner";
import { SalesPromoService } from "src/app/services/sales-promo.service";
import { Router, NavigationEnd, NavigationStart } from "@angular/router";
import { filter } from "rxjs/operators";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
// import '../../../../../assets/js/number.js';
declare var jquery: any;
declare var $: any;
declare var number: any;
import { environment } from "src/environments/environment";
import * as moment from 'moment';
import { from } from 'rxjs';
import { HttpParams } from '@angular/common/http';
// import "rxjs/Rx";

@Component({
  selector: "app-cbu-purchase-order",
  templateUrl: "./cbu-purchase-order.component.html",
  styleUrls: ["./cbu-purchase-order.component.css"]
})
export class CbuPurchaseOrderComponent implements OnInit {
  materialCode: any;
  prodSelected: any = [];
  matSelcted: string;
  simulate: any = [];
  simOrderCBU: any[];
  data_sap_dcs_amount: any = [];
  data_sap_dcs: any[];
  total_discount: number;
  total_allocated: number;
  vat: number;
  total_allocated_with_vat: number;
  so_number: string;
  posnr = [];
  customerAccount: string;
  userIDBCA: string;

  cbuOrder1: string;
  cbuOrder2: string;
  disabledPay1: string;
  disabledPay2: string;
  disabledPay3: string;
  disabledPay4: string;

  billtocode: string;
  shiptocode: string;
  cbuDraft: any = [];
  payment_method: string;
  paramDraft: any;
  is_draft: string = "NO";
  voucherCode: string;

  item_duplicate: any = [];
  item_no_exist: any = [];

  total_discount_simulate: number;
  total_allocated_simulate: number;
  total_vat_simulate: number;
  total_allocated_vat_simulate: number;
  pallet: any = [];

  resp: any = [];

  rows: any = [];

  CBUorderEntry: boolean;
  CBUdraft: boolean;

  payment_deadline: any;

  entryCBUform: boolean = true;
  simulateCBU: boolean = false;
  confirmCBU: boolean = false;
  bankTF: boolean = false;
  Cash: boolean = true;
  upload1: boolean = true;
  upload2: boolean = false;

  upload: boolean = false;
  notUpload: boolean = true;
  isDrafted: boolean = false;
  Voucher: boolean = false;
  useVoucher: boolean = false;

  urlCBU: string = "order-entry/cbu-order-entry";
  baseUrl = environment.baseUrl;
  appUrl = environment.appUrl;

  is_tester: boolean = false;

  name = "Get Date";
  today = new Date();
  jstoday = "";
  lblConOrder = "Continue Order";
  total_allocated_before_discount_simulate: any;
  total_allocated_before_discount: any;
  btnUpload: string = "false";
  cash_payment_type: string = "Bank Transfer";
  paymentMethods: any;
  // virtualAccount: string;
  modalRef: BsModalRef;
  hashHex: string;
  online_payment_type: string;

  canDownloadOrderDocument: boolean = false
  canSimulateOrder: boolean = false
  canPreviewPriceSimulate: boolean = false
  canDoingPayment: boolean = false
  canCreateOrder: boolean = false;

  constructor(
    private cbuOrder: CbuPurchaseOrderService,
    private storage: LocalStorageService,
    private spinner: NgxSpinnerService,
    private cdref: ChangeDetectorRef,
    private salesPromoOrder: SalesPromoService,
    private router: Router,
    private modalService: BsModalService,
    private zone: NgZone
  ) {
    this.jstoday = formatDate(this.today, "dd-MM-yyyy", "en-US", "+0700");

    // $.getScript('../../../../../assets/js/number.js', function() {
    //   $('.inputUnit').each(function () {

    //     $(this).number();

    //   });
    // });
  }

  ngOnInit() {
    this.spinner.show();
    this.cbuOrder1 = "cbu-order-active";
    this.CBUorderEntry = true;
    this.disabledPay3 = "disable-radio";
    this.disabledPay4 = "disable-radio";

    // Permissions
    this.canCreateOrder = JSON.parse(localStorage.getItem("user")).permissions["Create Order"]
    this.canDownloadOrderDocument = JSON.parse(localStorage.getItem("user")).permissions["Download Simulate Order and Order Confirmation Document"]
    this.canSimulateOrder = JSON.parse(localStorage.getItem("user")).permissions["Simulate Order"]
    this.canPreviewPriceSimulate = JSON.parse(localStorage.getItem("user")).permissions["Preview Price in Simulate Order"]
    this.canDoingPayment = JSON.parse(localStorage.getItem("user")).permissions["Doing Payment"]

    this.storage.observe("cbuOrder").subscribe(newValue => {
      // console.log(newValue);
    });
    // let va = localStorage.getItem("virtualAccount");
    // this.virtualAccount = va;
    // console.log(va);
    if (sessionStorage.getItem("voucherCode")) {
      this.Voucher = true;
      this.voucherCode = sessionStorage.getItem("voucherCode");
      this.salesPromoOrder.useSalesPromoVoucher(this.voucherCode).subscribe(
        result => {
          let data = [];
          data.push(result);
          console.log(data);
          this.prodSelected = data[0];
          for (let i = 0; i < this.prodSelected.length; i++) {
            this.prodSelected[i].pallet = this.prodSelected[i].balance_pallet;
            this.prodSelected[i].qty_total = Math.round(
              this.prodSelected[i].balance_qty
            );
            this.prodSelected[i].base_unit_of_measure = this.prodSelected[
              i
            ].unit_measure;
          }
          this.useVoucher = true;
          this.notUpload = false;
          console.log(data[0]);
        },
        error => console.log(error)
      );
    }

    this.cbuOrder.getBillToCode().subscribe(res => {
      var billtocode = [];
      //
      billtocode.push(res);
      this.billtocode = billtocode[0].filter(item => item.division == "10");
    });
    this.cbuOrder.getShipToCode().subscribe(res => {
      var shiptocode = [];
      shiptocode.push(res);
      // console.log(res);
      this.shiptocode = shiptocode[0].filter(item => item.division == "10");
    });

    this.cbuOrder.getDraftList().subscribe(res => {
      var result = [];
      result.push(res);
      var cbuDraft = [];
      var map = new Map();
      var sortByDate = function (a, b) {
        var dateA = new Date(b.updated_at).getTime();
        var dateB = new Date(a.updated_at).getTime();
        // console.log(`Date A: ${dateA}`, `Date B: ${dateB}`)
        return dateA > dateB ? 1 : -1;
      };
      result[0].sort(sortByDate);
      for (const item of result[0]) {
        if (!map.has(item.param)) {
          map.set(item.param, true);
          this.cbuDraft.push(item);
        }
      }
      // this.cbuDraft.sort((a, b) => {
      //   return new Date(b.created_at) - new Date(a.created_at)
      // })
      console.log(this.cbuDraft);
      this.spinner.hide();
    });
  }
  ngAfterContentChecked() {
    // this.totalOrdersUpload()

    this.cdref.detectChanges();
  }

  //REQ upload
  sendFileRequest(urllinks, formData, cb) {
    $.ajax({
      url: urllinks,
      type: "POST",
      xhr: function () {
        var myXhr = $.ajaxSettings.xhr();
        if (myXhr.upload) {
        }
        return myXhr;
      },
      beforeSend: function (stuff) {
        console.log(stuff);
      },
      success: function (data) {
        cb({ result: data });
      },
      error: function (error) {
        cb({ result: error });
      },
      data: formData,
      cache: false,
      contentType: false,
      processData: false
    });
  }

  // orderPop(orderPop) {
  //   this.modalRef = this.modalService.show(orderPop);
  // }

  getPop(orderPop: TemplateRef<any>) {
    this.modalRef = this.modalService.show(orderPop);
  }

  onfilecsvupload(event) {
    if (this.simulate.shipto) {
      let fileList: FileList = event.target.files;
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append("file", file, file.name);
      //console.log(ship_to_code);
      var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
      var ship_to_code = this.simulate.shipto;
      console.log(sap_code);
      console.log(ship_to_code);
      this.btnUpload = "true";
      this.sendFileRequest(
        this.baseUrl + "/api/order-cbu/import/" + sap_code + "/" + ship_to_code,
        formData,
        function (resp) {
          $("#uploadPart").val("");
          console.log(typeof resp);
          // localStorage.setItem('uploadCBU', JSON.stringify(resp))
          uploadCBU(resp);
        }
      );
      let uploadCBU = resp => {
        localStorage.setItem("uploadCBU", JSON.stringify(resp));
        this.upload1 = false;
        (this.upload2 = true), (this.btnUpload = "false");
      };
      // this.upload1 = false;
      // this.upload2 = true;
    } else {
      alert("Choose Ship To First.");
    }
  }

  submitUpload() {
    this.upload = true;
    this.notUpload = false;
    var ship_to_code = this.simulate.shipto;

    this.item_duplicate.length = 0;
    this.item_no_exist.length = 0;
    // this.spinner.show()
    var result = JSON.parse(localStorage.getItem("uploadCBU"));
    var data = [];
    if (result) {
      // for (var i = 0; i < result["result"]["exist"].length; i++) {
      // this.pallet.push(result["result"]["exist"][i]["item2"]);
      this.cbuOrder
        .getMaterialUploadFix(ship_to_code, result["result"]["idtrs"])
        .subscribe(
          res => {
            if (res) {
              let res2 = [];
              res2.push(res);
              for (let i = 0; i < res2[0].length; i++) {
                data.push(res2[0][i]);
              }
            }
            // for (var j = 0; j < data.length; j++) {
            //   data[j].check_duplicate = "no";
            //   for (var l = 0; l < result["result"]["is_duplicate"].length; l++) {
            //     if (data[j].material_number == result["result"]["is_duplicate"][l]["item1"]) {
            //       data[j].check_duplicate = "yes";
            //     }
            //   }
            // }
            this.spinner.hide();
          },
          err => {
            console.log(err);
            this.spinner.hide();
          }
        );
      // }

      for (var key in result["result"]["is_duplicate"]) {
        this.item_duplicate.push(
          result["result"]["is_duplicate"][key]["item1"]
        );
      }

      if (this.item_duplicate.length > 0) {
        alert(
          "The material number \n" +
          this.item_duplicate.join("\n") +
          "\nIs Duplicate. Please add another Item.\n\n" +
          "Total duplicate : " +
          this.item_duplicate.length
        );
      }

      for (var key in result["result"]["no_exist"]) {
        this.item_no_exist.push(result["result"]["no_exist"][key]["item1"]);
      }

      if (this.item_no_exist.length > 0) {
        alert(
          "File is rejected, you don't have permission to order this material : \n" +
          this.item_no_exist.join("\n") +
          "\n\nTotal rejected : " +
          this.item_no_exist.length
        );
      }

      console.log(data);
      this.prodSelected = data;
      console.log(this.prodSelected);

      localStorage.removeItem("uploadCBU");

      this.upload1 = true;
      this.upload2 = false;
    } else {
      alert("File is empty. Please input material and quantity before upload.");
    }
  }

  download() {
    window.location.href = this.baseUrl + "/api/order-cbu/download-template";
  }

  downloadSO() {
    let data = {
      sold_to_code: this.simulate.shipto || "-",
      bill_to_code: this.simulate.billpay || "-",
      dealer_name: localStorage.getItem("dealer_name"),
      address: JSON.parse(localStorage.getItem("dealer")).address[0] || "-",
      phone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0] || "-",
      fax: JSON.parse(localStorage.getItem("dealer")).fax1[0] || "-",
      po_number: this.simulate.punchOrnumb || "-",
      so_number: this.so_number || "-",
      confirm_date: this.jstoday || "-",
      order_type: "-",
      currency: "IDR",
      payment: "-",
      email: JSON.parse(localStorage.getItem("dealer")).email1[0] || "-",
      contact: "-",
      material_code: [],
      material_description: [],
      qty: [],
      allocated: [],
      backorder: [],
      unit_price: [],
      total_amount: [],
      ttl_qty: 0,
      ttl_allocated: 0,
      ttl_backorder: 0,
      ttl_unit_price: 0,
      ttl_total_amount: 0,
      discount: [],
      total_discount: 0,
      total_without_vat: 0,
      vat: 0,
      grand_total: 0,
      total_with_vat: 0,
      title: ""
    }

    if (this.so_number == null) {
      data.title = "Order Simulation"
    } else {
      data.title = "Order Confirmation"
    }

    if (this.paymentMethods == "T000")
      data.payment = "Cash"
    else
      data.payment = "Credit"

    if (this.simulateCBU == true) {
      $("#tableCBUSubmit #zmatCode2").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.material_code.push(item)
      })
      $("#tableCBUSubmit #zmatDesc").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.material_description.push(item)
      })
      $("#tableCBUSubmit #zkwmeng").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.qty.push(item)
      })
      $("#tableCBUSubmit #zzkwmeng2").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.allocated.push(item)
      })
      $("#tableCBUSubmit #zbackorder").each(function () {
        let item = $(this).text()
        // item = item.replace(/[, ]+/g, "").trim()
        data.backorder.push(item)
      })
      $("#tableCBUSubmit #znetpr2").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.unit_price.push(item)
      })
      $("#tableCBUSubmit #zzdiscount").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.discount.push(item)
      })
      $("#tableCBUSubmit #znetwr2").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.total_amount.push(item)
      })

      data.ttl_qty = this.totalSimulateOrderQty()
      data.ttl_allocated = this.totalSimulateAllocated()
      data.ttl_backorder = this.totalBackorder()

      for (const item of data.unit_price) {
        data.ttl_unit_price += parseInt(item)
      }
      for (const item of data.total_amount) {
        data.ttl_total_amount += parseInt(item)
      }
      for (const item of data.discount) {
        data.total_discount += parseInt(item)
      }
      data.total_without_vat = this.total_allocated_simulate
      data.vat = this.total_vat_simulate
      data.grand_total = this.totalSimulateOrderAmount()
      data.total_with_vat = this.total_allocated_vat_simulate
    } else {
      $("#tableCBUConfirmation #zmatCode2").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.material_code.push(item)
      })
      $("#tableCBUConfirmation #zmatDesc").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.material_description.push(item)
      })
      $("#tableCBUConfirmation #zkwmeng").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.qty.push(item)
      })
      $("#tableCBUConfirmation #zzkwmeng2").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.allocated.push(item)
      })
      $("#tableCBUConfirmation #zbackorder").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.backorder.push(item)
      })
      $("#tableCBUConfirmation #znetpr").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.unit_price.push(item)
      })
      $("#tableCBUConfirmation #zzdiscount").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.discount.push(item)
      })
      $("#tableCBUConfirmation #znetwr2").each(function () {
        let item = $(this).text()
        item = item.replace(/[, ]+/g, "").trim()
        data.total_amount.push(item)
      })

      data.ttl_qty = this.totalConfirmationOrderQty()
      data.ttl_allocated = this.totalAllocated()
      data.ttl_backorder = this.totalSubmitBackorder()

      for (const item of data.unit_price) {
        data.ttl_unit_price += parseInt(item)
      }
      for (const item of data.total_amount) {
        data.ttl_total_amount += parseInt(item)
      }
      for (const item of data.discount) {
        data.total_discount += parseInt(item)
      }

      data.total_without_vat = this.total_allocated_before_discount
      data.vat = this.vat
      data.grand_total = this.totalConfirmationOrderAmount()
      data.total_with_vat = this.total_allocated_with_vat
    }
    console.log(data);
    this.cbuOrder.downloadSO(data).subscribe(result => {
      this.downloadFile(result)
    })
  }

  downloadFile(data) {
    const blob = new Blob([data], { type: 'application/pdf' })
    const url = window.URL.createObjectURL(blob)
    // window.location.href = url
    // window.open
    let link = document.createElement('a')
    link.style.display = 'none';
    link.setAttribute("download", `order-confirm.pdf`)
    link.setAttribute('href', url)
    document.body.appendChild(link)
    link.click()
  }

  classActive() {
    this.cbuOrder1 = "";
    this.cbuOrder2 = "";
    this.CBUorderEntry = false;
    this.CBUdraft = false;
  }
  tab_cbuOrder(val) {
    this.classActive();
    if (val == 1) {
      this.cbuOrder1 = "cbu-order-active";
      this.CBUorderEntry = true;
    } else {
      this.cbuOrder2 = "cbu-order-active";
      this.CBUdraft = true;
    }
  }

  saveAsDraft() {
    this.spinner.show();
    var user_id = JSON.parse(localStorage.getItem("user")).id;
    var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;
    var rows: any = document
      .getElementById("tableCBU")
      .getElementsByTagName("tbody")[0]
      .getElementsByTagName("tr").length;
    //
    var data = {
      user_id: user_id,
      po_number: this.simulate.punchOrnumb,
      bill_to: this.simulate.billpay,
      ship_to: this.simulate.shipto,
      remarks: this.simulate.remarks,
      material_number: [],
      pallet: [],
      payment_method: "Cash"
    };
    // //
    if (data.po_number) {
      data.po_number = data.po_number.toString();
    } else {
      data.po_number = "";
    }
    if (data.bill_to) {
      data.bill_to = data.bill_to.toString().split(" - ");
      data.bill_to = data.bill_to[0];
    } else {
      data.bill_to = "";
    }
    if (data.ship_to) {
      data.ship_to = data.ship_to.toString().split(" - ");
      data.ship_to = data.ship_to[0];
    } else {
      data.ship_to = "";
    }
    // if(data.sold_to){
    //   data.sold_to = data.sold_to.toString().split(" - ");
    //   data.sold_to = data.sold_to[0];
    // }
    if (data.remarks) {
      data.remarks = data.remarks.toString();
    } else {
      data.remarks = "";
    }

    $("#tableCBU #material_code").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tableCBU input#pallet").each(function () {
      data.pallet.push(parseInt(this.value).toString());
    });

    var posnr = 10;

    data.user_id = data.user_id.toString();

    console.log(data);

    if (this.isDrafted == false) {
      this.cbuOrder.saveAsDraft(data).subscribe(
        res => {
          // console.log(res);
          var data_sap = [];
          data_sap.push(res);
          console.log(data_sap[0].message);
          if (data_sap[0].message == "Success") {
            this.cbuOrder.getDraftList().subscribe(res => {
              var result = [];
              result.push(res);
              var cbuDraft = [];
              var map = new Map();

              var sortByDate = function (a, b) {
                var dateA = new Date(b.updated_at).getTime();
                var dateB = new Date(a.updated_at).getTime();
                return dateA > dateB ? 1 : -1;
              };
              result[0].sort(sortByDate);
              for (const item of result[0]) {
                if (!map.has(item.param)) {
                  map.set(item.param, true);
                  cbuDraft.push(item);
                }
              }
              this.cbuDraft = cbuDraft;
              // console.log(this.cbuDraft);
              // this.cbuOrder2 = 'cbu-order-active';
              // this.CBUdraft = true;
              this.spinner.hide();
              // alert("Draft saved.")
              // location.reload()
            });
          } else {
            this.spinner.hide();
            alert("heho");
          }
        },
        err => {
          this.spinner.hide();
          alert("error");
        }
      );
    } else {
      this.cbuOrder.updateDraft(this.paramDraft, data).subscribe(
        res => {
          console.log(res);
          this.cbuOrder.getDraftList().subscribe(res => {
            var result = [];
            result.push(res);
            var cbuDraft = [];
            var map = new Map();

            var sortByDate = function (a, b) {
              var dateA = new Date(b.updated_at).getTime();
              var dateB = new Date(a.updated_at).getTime();
              console.log(`Date A: ${dateA}`, `Date B: ${dateB}`);
              return dateA > dateB ? 1 : -1;
            };
            result[0].sort(sortByDate);

            for (const item of result[0]) {
              if (!map.has(item.param)) {
                map.set(item.param, true);
                cbuDraft.push(item);
              }
            }

            this.cbuDraft = cbuDraft;
            this.spinner.hide();
            // alert("Draft updated.")
            // location.reload()
            // this.cbuOrder2 = 'cbu-order-active';
            // this.CBUdraft = true;
          });
        },
        err => {
          this.spinner.hide();
          console.log(err);
        }
      );
    }
  }

  getBillTo() {
    console.log(this.simulate.billpay);
    this.cbuOrder.getTOPFromBillTo(this.simulate.billpay, 10).subscribe(
      result => {
        if (result["message"] == "Success.")
          this.paymentMethods = result["top"];
        else this.paymentMethods = 0;
        console.log(`"${this.paymentMethods}"`);
      },
      error => {
        console.log(error);
      }
    );
  }

  simOrder() {
    // var po_number: string = this.simulate.punchOrnumb;
    // var remarks: string = this.simulate.remarks;
    if (this.simulate.shipto) {
      this.spinner.show();
      // if(po_number && remarks) {
      this.simulate.items = this.prodSelected;
      this.storage.store("cbuOrder", this.simulate);
      this.getData("cbuOrder");
      var check_duplicate = 0;
      var paymentMethod = `"${this.paymentMethods}"`;

      if (paymentMethod == '"T000"') {
        var payment_method = "Cash";
        this.payment_method = "Cash";
      } else {
        var payment_method = "Credit";
        this.payment_method = "Credit";
      }

      if (
        sessionStorage.getItem("voucherCode") &&
        sessionStorage.getItem("promoType") == "Z14"
      ) {
        paymentMethod = '"T000"';
        var payment_method = "Cash";
        this.payment_method = "Cash";
      }

      var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;
      // var so_number = JSON.parse(localStorage.getItem('orderHeaderData')).vbeln;
      var user_id = JSON.parse(localStorage.getItem("user")).id;
      var rows: any = document
        .getElementById("tableCBU")
        .getElementsByTagName("tbody")[0]
        .getElementsByTagName("tr").length;
      //
      var data = {
        customer_code: customer_code,
        po_number: this.simulate.punchOrnumb,
        bill_to: this.simulate.billpay,
        ship_to: this.simulate.shipto,
        remarks: this.simulate.remarks,
        material_number: [],
        qty: [],
        pallet: [],
        payment_method: payment_method,
        sales_order_number: "9999",
        user_id: user_id,
        unit_of_measure: [],
        sloc: [],
        sold_to: this.simulate.billpay,
        posnr: [],
        order_date: formatDate(this.today, "yyyy-MM-dd", "en-US", "+0530"),
        payment_trigger: paymentMethod,
        sub_total: this.totalOrders().toString(),
        voucher: sessionStorage.getItem("voucherCode"),
        user: JSON.parse(localStorage.getItem("user")).email
        // so_number: so_number.toString()
      };
      // //
      if (this.simulate.punchOrnumb) {
        data.po_number = data.po_number.toString();
      } else {
        data.po_number = "-";
      }

      if (this.simulate.remarks) {
        data.remarks = data.remarks.toString();
      } else {
        data.remarks = "-";
      }
      data.bill_to = data.bill_to.toString().split(" - ");
      data.bill_to = data.bill_to[0];
      data.ship_to = data.ship_to.toString().split(" - ");
      data.ship_to = data.ship_to[0];
      data.sold_to = data.sold_to.toString().split(" - ");
      data.sold_to = data.sold_to[0];
      // if(this.simulate.remarks){
      //   data.remarks = data.remarks.toString();
      // }else{
      //   data.remarks = '';
      // }

      $("#tableCBU #material_code").each(function () {
        data.material_number.push($(this).text());
      });

      $("#tableCBU #base_unit_of_measure").each(function () {
        data.unit_of_measure.push($(this).text());
      });
      $("#tableCBU #sloc").each(function () {
        data.sloc.push($(this).text());
      });
      $("#tableCBU #qty").each(function () {
        data.qty.push($(this).text());
      });
      $("#tableCBU input#pallet").each(function () {
        data.pallet.push(parseInt(this.value).toString());
      });
      this.pallet.length = 0;
      for (let i = 0; i < data.pallet.length; i++) {
        this.pallet.push(data.pallet[i].toString());
      }

      var posnr = 10;

      console.log(typeof this.posnr);
      if (this.voucherCode) {
        let posnr2 = [];
        $("#tableCBU #posnr").each(function () {
          data.posnr.push($(this).text());
          posnr2.push($(this).text());
        });
        this.posnr = posnr2;
        console.log(this.posnr);
      } else {
        for (var i = 0; i < rows; i++) {
          if (posnr.toString().length == 2) {
            data.posnr.push("0000" + posnr.toString()).toString();
            posnr += 10;
          } else if (posnr.toString().length == 3) {
            data.posnr.push("000" + posnr.toString()).toString();
            posnr += 10;
          } else if (posnr.toString().length == 4) {
            data.posnr.push("00" + posnr.toString()).toString();
            posnr += 10;
          } else if (posnr.toString().length == 5) {
            data.posnr.push("0" + posnr.toString()).toString();
            posnr += 10;
          }
        }
      }

      data.user_id = data.user_id.toString();
      //
      console.log(data);
      //
      // alert(data.qty.includes("0"));
      $("#tableCBU #check_duplicate").each(function () {
        console.log($(this).text());
        if ($(this).text() == "yes") {
          check_duplicate++;
        }
      });

      if (!data.qty.includes("0")) {
        console.log(check_duplicate);
        if (check_duplicate > 1) {
          this.spinner.hide();
          alert("Please Remove Duplicate / Reupload File Before Simulate ");
          check_duplicate = 0;
        } else {
          this.spinner.show();
          this.cbuOrder.simulateOrder(data).subscribe(
            res => {
              var data_sap = [];
              data_sap.push(res);
              console.log(data_sap);
              if (data_sap[0].status != "error") {
                this.data_sap_dcs_amount = data_sap[0].result.header[1];
                this.data_sap_dcs = data_sap[0].result.output;
                this.total_allocated_before_discount_simulate =
                  this.data_sap_dcs_amount.znetwr2_gros * 100;
                this.total_discount_simulate =
                  this.data_sap_dcs_amount.zdiscount.replace("-", "") * 100;
                this.total_allocated_simulate =
                  this.data_sap_dcs_amount.znetwr2 * 100;
                this.total_vat_simulate =
                  this.data_sap_dcs_amount.zmwsbp2 * 100;
                this.total_allocated_vat_simulate =
                  this.total_allocated_simulate + this.total_vat_simulate;
                // console.log(this.data_sap_dcs);

                if (data_sap[0].status == "success") {
                  this.entryCBUform = false;
                  this.simulateCBU = true;
                  let j = 1;
                  for (let i = 0; i < this.data_sap_dcs.length; i++) {
                    if (this.data_sap_dcs[i].posnr_so.split("").pop() == 0) {
                      this.data_sap_dcs[i].number = j + ".";
                      j++;
                    }
                    this.data_sap_dcs[i].kwmeng = Math.round(
                      this.data_sap_dcs[i].kwmeng
                    );
                    this.data_sap_dcs[i].zkwmeng2 = Math.round(
                      this.data_sap_dcs[i].zkwmeng2
                    );
                    this.data_sap_dcs[i].backorder = Math.round(
                      this.data_sap_dcs[i].kwmeng -
                      this.data_sap_dcs[i].zkwmeng2
                    );
                    this.data_sap_dcs[i].netpr_gros = this.data_sap_dcs[
                      i
                    ].netpr_gros;
                    this.data_sap_dcs[i].netwr_gros = this.data_sap_dcs[
                      i
                    ].netwr_gros;
                    this.data_sap_dcs[i].zdiscount_gros = this.data_sap_dcs[
                      i
                    ].zdiscount_gros.replace("-", "");
                    if (this.data_sap_dcs[i].waerk == "IDR") {
                      this.data_sap_dcs[i].netpr_gros =
                        this.data_sap_dcs[i].netpr_gros * 100;
                      this.data_sap_dcs[i].netwr_gros =
                        this.data_sap_dcs[i].netwr_gros * 100;
                      this.data_sap_dcs[i].zdiscount_gros =
                        this.data_sap_dcs[i].zdiscount_gros.replace("-", "") *
                        100;
                    }
                    // this.data_sap_dcs[i]["0"] = this.data_sap_dcs[i]["0"]
                    // this.data_sap_dcs[i].matwa = this.data_sap_dcs[i].matwa;
                    // console.log(this.data_sap_dcs[i].matnr + ' - ' + ' ');
                    this.cbuOrder
                      .getMaterial(
                        this.data_sap_dcs[i].matnr + " - " + "a",
                        this.data_sap_dcs[i].zkunnr_sh
                      )
                      .subscribe(
                        res => {
                          // this.data_sap_dcs[i].material_group = res["mg_description"];
                        },
                        err => {
                          console.log(err);
                          this.spinner.hide();
                        }
                      );
                  }
                } else {
                  alert("heho");
                  this.spinner.hide();
                }
              } else {
                alert(data_sap[0].status);
                this.spinner.hide();
              }

              this.spinner.hide();
            },
            err => {
              alert("error");
              this.spinner.hide();
            }
          );
        }
      } else {
        this.spinner.hide();
        alert("Please Add Quantity");
      }
    } else if (!this.simulate.shipto) {
      alert("Choose Ship To First.");
    } else {
      alert("Choose Bill To and Ship To First.");
    }

    // } else {
    //   alert('Purchase Order Number and/or Remarks can\'t be blank');
    // }
  }

  getData(val) {
    this.simOrderCBU = this.storage.retrieve(val);
  }

  backOrder() {
    this.entryCBUform = true;
    this.simulateCBU = false;
    // console.log(this.prodSelected);
    // for (let i = 0; i < this.prodSelected.length; i++) {
    //   this.prodSelected[i].qty_total = 0;
    //   this.pallet[i] = 0;
    // }
  }

  subOrder() {
    //
    this.spinner.show();
    this.modalRef.hide()
    var user_id = JSON.parse(localStorage.getItem("user")).id;
    let paymentMethod = `"${this.paymentMethods}"`;
    this.customerAccount = localStorage.getItem("bank_transfer_va")

    if (paymentMethod == '"T000"') {
      var payment_method = "Cash";
      this.payment_method = "Cash";
    } else {
      var payment_method = "Credit";
      this.payment_method = "Credit";
    }

    if (
      sessionStorage.getItem("voucherCode") &&
      sessionStorage.getItem("promoType") == "Z14"
    ) {
      paymentMethod = '"T000"';
      var payment_method = "Cash";
      this.payment_method = "Cash";
    }

    var customer_code = JSON.parse(localStorage.getItem("user")).sap_code;

    if (customer_code == '0010000024'){
      this.is_tester = true
    }

    var rows: any = document
      .getElementById("tableCBUSubmit")
      .getElementsByTagName("tbody")[0]
      .getElementsByTagName("tr").length;
    var idrows: any = document
      .getElementById("tableCBUSubmit")
      .getElementsByTagName("tbody")[0]
      .getElementsByTagName("tr");

    var data = {
      customer_code: customer_code,
      po_number: this.simulate.punchOrnumb,
      bill_to: this.simulate.billpay,
      ship_to: this.simulate.shipto,
      remarks: this.simulate.remarks,
      material_number: [],
      qty: [],
      pallet: [],
      payment_method: payment_method,
      sales_order_number: "9999",
      user_id: user_id,
      unit_of_measure: [],
      sloc: [],
      sold_to: this.simulate.billpay,
      posnr: [],
      order_date: formatDate(this.today, "yyyy-MM-dd", "en-US", "+0530"),
      payment_trigger: paymentMethod,
      sub_total: this.totalOrders().toString(),
      ordered: [],
      allocated: [],
      backorder: [],
      unit_price: [],
      discount: [],
      total_amount: this.totalSimulateOrderAmount(),
      total_discount: this.total_discount_simulate,
      total_allocated: this.total_allocated_simulate,
      vat: this.total_vat_simulate,
      total_allocated_with_vat: this.total_allocated_vat_simulate,
      is_draft: this.is_draft,
      param: this.paramDraft,
      voucher: sessionStorage.getItem("voucherCode"),
      user: JSON.parse(localStorage.getItem("user")).email
      // so_number: so_number.toString()
    };

    if (paymentMethod == '"T000"') this.lblConOrder = "Confirm Payment";

    ////!!!DELETE THIS
    if (this.simulate.punchOrnumb) {
      data.po_number = data.po_number.toString();
      this.simulate.punchOrnumb = data.po_number;
    } else {
      data.po_number = "-";
      this.simulate.punchOrnumb = "-";
    }

    if (this.simulate.remarks) {
      data.remarks = data.remarks.toString();
      this.simulate.remarks = data.remarks;
    } else {
      data.remarks = "-";
      this.simulate.remarks = "-";
    }

    data.po_number = data.po_number.toString();
    data.bill_to = data.bill_to.toString().split(" - ");
    data.bill_to = data.bill_to[0];
    data.ship_to = data.ship_to.toString().split(" - ");
    data.ship_to = data.ship_to[0];
    data.sold_to = data.sold_to.toString().split(" - ");
    data.sold_to = data.sold_to[0];
    data.remarks = data.remarks.toString();

    $("#tableCBUSubmit #zmatCode").each(function () {
      data.material_number.push($(this).text());
    });
    $("#tableCBUSubmit #zbase_unit_of_measure").each(function () {
      data.unit_of_measure.push($(this).text());
    });
    $("#tableCBUSubmit #zsloc").each(function () {
      data.sloc.push($(this).text());
    });
    $("#tableCBUSubmit #zkwmeng").each(function () {
      data.qty.push($(this).text());
      data.ordered.push($(this).text());
    });
    var allocated_material = 0;
    $("#tableCBUSubmit #zzkwmeng2").each(function () {
      data.allocated.push($(this).text());
      allocated_material += parseInt($(this).text());
    });
    $("#tableCBUSubmit #zbackorder").each(function () {
      data.backorder.push($(this).text());
    });
    $("#tableCBUSubmit #znetpr").each(function () {
      data.unit_price.push($(this).text());
    });
    $("#tableCBUSubmit #zzdiscount").each(function () {
      data.discount.push($(this).text());
    });
    // console.log("Data Pallet: ", this.pallet.length)
    for (let i = 0; i < this.pallet.length; i++) {
      data.pallet.push(this.pallet[i].toString());
    }
    var posnr = 10;

    var zrows = 0;

    for (var i = 0; i < idrows.length; i++) {
      if (idrows[i].id == "ztr") {
        zrows++;
      }
    }

    if (this.voucherCode) {
      for (let i = 0; i < this.posnr.length; i++) {
        data.posnr.push(this.posnr[i]);
      }
    } else {
      for (var i = 0; i < zrows; i++) {
        if (posnr.toString().length == 2) {
          data.posnr.push("0000" + posnr.toString()).toString();
          posnr += 10;
        } else if (posnr.toString().length == 3) {
          data.posnr.push("000" + posnr.toString()).toString();
          posnr += 10;
        } else if (posnr.toString().length == 4) {
          data.posnr.push("00" + posnr.toString()).toString();
          posnr += 10;
        } else if (posnr.toString().length == 5) {
          data.posnr.push("0" + posnr.toString()).toString();
          posnr += 10;
        }
      }
    }

    data.user_id = data.user_id.toString();

    // var zkwmeng2 = 0;
    // $('#tableCBUSubmit #zzkwmeng2').each(function() {
    //   zkwmeng2 += $(this).text()
    // })
    // alert(allocated_material)
    console.log(data);
    if (allocated_material > 0) {
      this.cbuOrder.submitOrder(data).subscribe(
        res => {
          var data_sap = [];
          data_sap.push(res);
          console.log(data_sap);
          this.data_sap_dcs = data_sap[0].result.output;
          let j = 1;
          for (let i = 0; i < this.data_sap_dcs.length; i++) {
            if (this.data_sap_dcs[i].posnr_so.split("").pop() == 0) {
              this.data_sap_dcs[i].number = j + ".";
              j++;
            }
            this.data_sap_dcs[i].kwmeng = Math.round(
              this.data_sap_dcs[i].kwmeng
            );
            this.data_sap_dcs[i].zkwmeng2 = Math.round(
              this.data_sap_dcs[i].zkwmeng2
            );
            this.data_sap_dcs[i].backorder = Math.round(
              this.data_sap_dcs[i].kwmeng - this.data_sap_dcs[i].zkwmeng2
            );
            this.data_sap_dcs[i].netpr_gros = this.data_sap_dcs[i].netpr_gros;
            this.data_sap_dcs[i].netwr_gros = this.data_sap_dcs[i].netwr_gros;
            this.data_sap_dcs[i].zdiscount_gros = this.data_sap_dcs[
              i
            ].zdiscount_gros.replace("-", "");
            if (this.data_sap_dcs[i].waerk == "IDR") {
              this.data_sap_dcs[i].netpr_gros =
                this.data_sap_dcs[i].netpr_gros * 100;
              this.data_sap_dcs[i].netwr_gros =
                this.data_sap_dcs[i].netwr_gros * 100;
              this.data_sap_dcs[i].zdiscount_gros =
                this.data_sap_dcs[i].zdiscount_gros.replace("-", "") * 100;
            }
          }
          if (data_sap[0].status == "success") {
            if (paymentMethod == '"T030"') {
              if (data.remarks.toString().toLowerCase() !== "cash") {
                this.Cash = false;
              }
              if (
                data_sap[0].result.header[0].message ==
                "Please Confirm released Credit Limit to HPPI"
              ) {
                alert(`This order is blocked. Please contact HPPI.`);
              }
              this.cbuOrder.storeHeaderOrder(data_sap[0].result.header[0]);
              this.confirmCBU = true;
              this.simulateCBU = false;
              this.so_number = JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).vbeln;
              this.total_allocated_before_discount = JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).znetwr2_gros;
              this.total_discount = JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).zdiscount.replace("-", "");
              this.total_discount = this.total_discount
                ? this.total_discount
                : 0;
              this.total_allocated = JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).znetwr2;
              this.total_allocated = this.total_allocated
                ? this.total_allocated
                : 0;
              this.vat = JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).zmwsbp2;
              this.vat = this.vat ? this.vat : 0;
              console.log(data_sap);
              if (data_sap[0].result.header[0].waers === "IDR") {
                this.total_allocated_before_discount =
                  JSON.parse(localStorage.getItem("orderHeaderData"))
                    .znetwr2_gros * 100;
                this.total_discount =
                  JSON.parse(
                    localStorage.getItem("orderHeaderData")
                  ).zdiscount.replace("-", "") * 100;
                this.total_allocated =
                  JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                  100;
                this.vat =
                  JSON.parse(localStorage.getItem("orderHeaderData")).zmwsbp2 *
                  100;
              }
              this.total_allocated_with_vat = this.total_allocated + this.vat;
            } else {
              if (
                data_sap[0].messages == "SO Rejected for Credit Block Status"
              ) {
                alert("SO Rejected for Credit Block Status");
                this.Cash = false;
                this.lblConOrder = "Confirm";
                // location.reload();
              }

              this.cbuOrder.storeHeaderOrder(data_sap[0].result.header[0]);
              this.confirmCBU = true;
              this.simulateCBU = false;
              this.so_number = JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).vbeln;
              this.total_allocated_before_discount = JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).znetwr2_gros;
              this.total_discount = JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).zdiscount.replace("-", "");
              this.total_discount = this.total_discount
                ? this.total_discount
                : 0;
              this.total_allocated = JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).znetwr2;
              this.total_allocated = this.total_allocated
                ? this.total_allocated
                : 0;
              this.vat = JSON.parse(
                localStorage.getItem("orderHeaderData")
              ).zmwsbp2;
              this.vat = this.vat ? this.vat : 0;
              if (data_sap[0].result.header[0].waers === "IDR") {
                this.total_allocated_before_discount =
                  JSON.parse(localStorage.getItem("orderHeaderData"))
                    .znetwr2_gros * 100;
                this.total_discount =
                  JSON.parse(
                    localStorage.getItem("orderHeaderData")
                  ).zdiscount.replace("-", "") * 100;
                this.total_allocated =
                  JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                  100;
                this.vat =
                  JSON.parse(localStorage.getItem("orderHeaderData")).zmwsbp2 *
                  100;
              }
              this.total_allocated_with_vat = this.total_allocated + this.vat;
            }
          } else {
            alert("heho");
            this.spinner.hide();
          }
          this.spinner.hide();
        },
        err => {
          console.log(err);
          alert("error");
          this.spinner.hide();
        }
      );
    } else {
      this.spinner.show();

      this.cbuOrder.submitOrder(data).subscribe(
        res => {
          var data_sap = [];
          data_sap.push(res);
          console.log(data_sap);
          this.data_sap_dcs = data_sap[0].result.output;
          let j = 1;
          for (let i = 0; i < this.data_sap_dcs.length; i++) {
            if (this.data_sap_dcs[i].posnr_so.split("").pop() == 0) {
              this.data_sap_dcs[i].number = j + ".";
              j++;
            }
            this.data_sap_dcs[i].kwmeng = Math.round(
              this.data_sap_dcs[i].kwmeng
            );
            this.data_sap_dcs[i].zkwmeng2 = Math.round(
              this.data_sap_dcs[i].zkwmeng2
            );
            this.data_sap_dcs[i].backorder = Math.round(
              this.data_sap_dcs[i].kwmeng - this.data_sap_dcs[i].zkwmeng2
            );
            this.data_sap_dcs[i].netpr_gros = this.data_sap_dcs[i].netpr_gros;
            this.data_sap_dcs[i].netwr_gros = this.data_sap_dcs[i].netwr_gros;
            this.data_sap_dcs[i].zdiscount_gros = this.data_sap_dcs[
              i
            ].zdiscount_gros.replace("-", "");
            if (this.data_sap_dcs[i].waerk == "IDR") {
              this.data_sap_dcs[i].netpr_gros =
                this.data_sap_dcs[i].netpr_gros * 100;
              this.data_sap_dcs[i].netwr_gros =
                this.data_sap_dcs[i].netwr_gros * 100;
              this.data_sap_dcs[i].zdiscount_gros =
                this.data_sap_dcs[i].zdiscount_gros.replace("-", "") * 100;
            }
          }
          if (data_sap) {
            if (
              data_sap[0].result.header[0].message ==
              "Please Confirm released Credit Limit to HPPI"
            ) {
              alert(`This order is blocked. Please contact HPPI.`);
            }
            this.cbuOrder.storeHeaderOrder(data_sap[0].result.header[0]);
            this.confirmCBU = true;
            this.simulateCBU = false;
            this.Cash = false;
            this.so_number = JSON.parse(
              localStorage.getItem("orderHeaderData")
            ).vbeln;
            this.total_allocated_before_discount = JSON.parse(
              localStorage.getItem("orderHeaderData")
            ).znetwr2_gros;
            this.total_discount = JSON.parse(
              localStorage.getItem("orderHeaderData")
            ).zdiscount.replace("-", "");
            this.total_discount = this.total_discount ? this.total_discount : 0;
            this.total_allocated = JSON.parse(
              localStorage.getItem("orderHeaderData")
            ).znetwr2;
            this.total_allocated = this.total_allocated
              ? this.total_allocated
              : 0;
            this.vat = JSON.parse(
              localStorage.getItem("orderHeaderData")
            ).zmwsbp2;
            this.vat = this.vat ? this.vat : 0;
            console.log(data_sap);
            if (data_sap[0].result.header[0].waers === "IDR") {
              this.total_allocated_before_discount =
                JSON.parse(localStorage.getItem("orderHeaderData"))
                  .znetwr2_gros * 100;
              this.total_discount =
                JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).zdiscount.replace("-", "") * 100;
              this.total_allocated =
                JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                100;
              this.vat =
                JSON.parse(localStorage.getItem("orderHeaderData")).zmwsbp2 *
                100;
            }
            this.total_allocated_with_vat = this.total_allocated + this.vat;
            // this.spinner.hide()
            this.confirmCBU = true;
            this.simulateCBU = false;
            this.spinner.hide();
            alert("Order being process in back order!");
          }
        },
        err => {
          this.spinner.hide();
          console.log(err);
        }
      );
    }
    // sessionStorage.removeItem("voucherCode");
  }

  checkTC(){
    console.log(this.online_payment_type)
    if(this.online_payment_type == "Credit Card"){
      if($('input[name=t_and_c]:checked').length > 0){
        this.conOrder()
      }else{
        alert('Please Check T&C before Submit.')
      }
    }
    else{
      this.conOrder()
    }
  }

  conOrder() {
    if (this.online_payment_type == "KlikBCA"){
      if(!this.userIDBCA){
        alert('Please Input UserID')
        return
      }
    }
    var zkwmeng2 = 0;
    for (let i = 0; i < this.data_sap_dcs.length; i++) {
      zkwmeng2 = zkwmeng2 + this.data_sap_dcs[i].zkwmeng2;
    }
    // console.log(zkwmeng2);

    if (zkwmeng2 > 0) {
      var data = {
        confirmid: JSON.parse(localStorage.getItem("orderHeaderData"))
          .confirmid,
        vbeln: JSON.parse(localStorage.getItem("orderHeaderData")).vbeln,
        idtrs: JSON.parse(localStorage.getItem("orderHeaderData")).idtrs,
        approve: "1",
        spart: "10",
        cash_payment_type: this.cash_payment_type,
        user: JSON.parse(localStorage.getItem("user")).email
      };
      var paymentMethod = `"${this.paymentMethods}"`;

      // if (paymentMethod == '"T030"') {
      //   var paymentMethod = "Credit";
      //   this.payment_method = "Credit";
      // } else {
      //   var payment_method = "Cash";
      //   this.payment_method = "Cash";
      // }

      if (
        sessionStorage.getItem("voucherCode") &&
        sessionStorage.getItem("promoType") == "Z14"
      ) {
        paymentMethod = '"T000"';
        var payment_method = "Cash";
        this.payment_method = "Cash";
      }

      this.spinner.show();
      if (paymentMethod == '"T030"') {
        alert("order success");
        this.spinner.hide();
        location.reload();

      } else {
        // !!!!!!!!!!! Uncomment from here
        this.cbuOrder.paymentOrder(data).subscribe(
          res => {
            console.log(res);

            // PARAMS GOES HERE
            if (res["status"] == "success") {
              let dateNow = moment()
              let transactionDateNow = dateNow.format("YYYY-MM-DD HH:mm:ss")
              let transactionDateExpire = dateNow.add('10', 'minutes').format("YYYY-MM-DD HH:mm:ss")

              let userEmail = JSON.parse(localStorage.getItem("dealer")).email1[0]
              // let userEmail = "arbhasyech@gmail.com"

              let additionalData = `${res["result"][0]["gjahr"]} - ${res["result"][0]["belnr"]} - ${res["result"][0]["bukrs"]} - ${userEmail}`

              let params = {
                type: this.online_payment_type,
                channelId: "",
                currency: "IDR",
                transactionNo: JSON.parse(localStorage.getItem("orderHeaderData")).vbeln,
                transactionAmount: this.total_allocated_with_vat.toString(),
                transactionDate: transactionDateNow,
                transactionExpire: transactionDateExpire,
                description: "DCSTRANSACTION",
                customerAccount: "",
                customerName: localStorage.getItem("dealer_name"),
                freeTexts: [{
                  indonesian: "",
                  english: ""
                }],
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + JSON.parse(localStorage.getItem("orderHeaderData")).vbeln
              }

              let paramsNonVa = {
                channelId: "",
                transactionNo: JSON.parse(localStorage.getItem("orderHeaderData")).vbeln,
                transactionAmount: this.total_allocated_with_vat.toString(),
                transactionDate: transactionDateNow,
                transactionExpire: transactionDateExpire,
                description: "DCS DESC",
                customerName: localStorage.getItem("dealer_name"),
                customerEmail: JSON.parse(localStorage.getItem("dealer")).email1[0],
                customerPhone: JSON.parse(localStorage.getItem("dealer")).phone_number1[0],
                customerAccount: "",
                type: this.online_payment_type,
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + JSON.parse(localStorage.getItem("orderHeaderData")).vbeln
              }

              let paramsCreditCard = {
                channelId: "HONDAPOWER",
                type: "Credit Card",
                transactionNo: JSON.parse(localStorage.getItem("orderHeaderData")).vbeln,
                transactionAmount: this.total_allocated_with_vat.toString(),
                transactionDate: transactionDateNow,
                transactionExpire: transactionDateExpire,
                description: "DCS DESC",
                additionalData: additionalData,
                callbackURL: `${this.appUrl}/redirect?so_number=` + JSON.parse(localStorage.getItem("orderHeaderData")).vbeln
              }

              if (this.online_payment_type == "BCA VA") {
                params.type = "BCA VA"
                params.channelId = "HONDAPOWERBVA01"
                params.customerAccount = "61222" + localStorage.getItem("bca_va")
                this.customerAccount = "61222" + localStorage.getItem("bca_va")
              } else if (this.online_payment_type == "KlikBCA") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKB01"
                let userID = {
                  userID: this.userIDBCA
                }
                Object.assign(paramsNonVa,userID)
                paramsNonVa.customerAccount = this.userIDBCA
                this.customerAccount = this.userIDBCA
              } else if (this.online_payment_type == "BCA KlikPay") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERKP01"
                paramsNonVa.customerAccount = localStorage.getItem("bank_transfer_va")
                this.customerAccount = localStorage.getItem("bank_transfer_va")
              } else if (this.online_payment_type == "CIMB Click") {
                paramsNonVa.type = this.online_payment_type
                paramsNonVa.channelId = "HONDAPOWERCIMBCL01"
                paramsNonVa.customerAccount = localStorage.getItem("bank_transfer_va")
                this.customerAccount = localStorage.getItem("bank_transfer_va")
              } else if (this.online_payment_type == "iPay BNI") {
                paramsNonVa.type = "iPay BNI"
                paramsNonVa.channelId = "HONDAIPAY01"
                paramsNonVa.customerAccount = localStorage.getItem("bank_transfer_va")
                this.customerAccount = localStorage.getItem("bank_transfer_va")
              } else if (this.online_payment_type == "Permata VA") {
                params.type = "Permata VA"
                params.channelId = "HONDAPOWERPVA01"
                params.customerName = localStorage.getItem("dealer_name").substr(0,20),
                params.customerAccount = "873584" + localStorage.getItem("permata_va")
                this.customerAccount = "873584" + localStorage.getItem("permata_va")
              }

              console.log(paramsCreditCard)
              // END PARAMS
              // IF PAYMENT == PERMATA VA OR BCA VA
              if (this.online_payment_type == "Permata VA" || this.online_payment_type == "BCA VA") {
                this.cbuOrder.sendPayment(params).subscribe(result => {
                  if (result["insertStatus"] == "00") {
                    console.log(result)
                    if (paymentMethod == '"T000"' && payment_method == "Cash") {
                      location.reload();
                    }
                    this.bankTF = true;
                    this.confirmCBU = false;

                    this.so_number = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).vbeln;
                    this.total_allocated = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).znetwr2;
                    if (
                      JSON.parse(localStorage.getItem("orderHeaderData")).waerk == "IDR"
                    ) {
                      this.total_allocated =
                        JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                        100;
                    }
                    this.payment_deadline = new Date();
                    this.payment_deadline.setDate(this.payment_deadline.getDate() + 9);

                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.log(error)
                  this.spinner.hide()
                })
              }
              // ELSE IF PAYMENT == CC
              else if (this.online_payment_type == "Credit Card") {
                console.log(paramsCreditCard)
                this.cbuOrder.sendPayment(paramsCreditCard).subscribe(result => {
                  console.log(result)
                  if (result["insertStatus"] == "00") {
                    console.log(result)
                    if (paymentMethod == '"T000"' && payment_method == "Cash") {
                      location.reload();
                    }
                    if (result["redirectURL"] !== "") {
                      location.href = result["redirectURL"]
                    }
                    this.bankTF = true;
                    this.confirmCBU = false;

                    this.so_number = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).vbeln;
                    this.total_allocated = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).znetwr2;
                    if (
                      JSON.parse(localStorage.getItem("orderHeaderData")).waerk == "IDR"
                    ) {
                      this.total_allocated =
                        JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                        100;
                    }
                    this.payment_deadline = new Date();
                    this.payment_deadline.setDate(this.payment_deadline.getDate() + 9);
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.log(error)
                  this.spinner.hide()
                })
              } else if (this.online_payment_type == "KlikBCA" || this.online_payment_type == "BCA KlikPay" || this.online_payment_type == "CIMB Click" || this.online_payment_type == "iPay BNI") {
                this.cbuOrder.sendPayment(paramsNonVa).subscribe(result => {
                  console.log(result)
                  if (result["insertStatus"] == "00") {
                    if (paymentMethod == '"T000"' && payment_method == "Cash") {
                      location.reload();
                    }
                    // console.log(res);
                    this.bankTF = true;
                    this.confirmCBU = false;
                    let redirectURL = result["redirectURL"]

                    if (redirectURL) {
                      let form = document.createElement("form")
                      form.setAttribute("method", "POST")
                      form.setAttribute("enctype", "application/x-www-form-urlencoded")
                      form.setAttribute("action", redirectURL)
                      for (const key in result["redirectData"]) {
                        if (result["redirectData"].hasOwnProperty(key)) {
                          const element = result["redirectData"][key];
                          let input = document.createElement("input")
                          input.setAttribute("type", "hidden")
                          input.setAttribute("name", key)
                          input.setAttribute("value", element)
                          form.appendChild(input)
                        }
                      }
                      document.body.appendChild(form)

                      form.submit()
                    }

                    this.so_number = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).vbeln;
                    this.total_allocated = JSON.parse(
                      localStorage.getItem("orderHeaderData")
                    ).znetwr2;
                    if (
                      JSON.parse(localStorage.getItem("orderHeaderData")).waerk == "IDR"
                    ) {
                      this.total_allocated =
                        JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                        100;
                    }
                  } else {
                    alert(result["insertMessage"])
                  }
                }, error => {
                  console.log(error)
                  this.spinner.hide()
                })
              } else {
                this.bankTF = true;
                this.confirmCBU = false;

                this.so_number = JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).vbeln;
                this.total_allocated = JSON.parse(
                  localStorage.getItem("orderHeaderData")
                ).znetwr2;
                if (
                  JSON.parse(localStorage.getItem("orderHeaderData")).waerk == "IDR"
                ) {
                  this.total_allocated =
                    JSON.parse(localStorage.getItem("orderHeaderData")).znetwr2 *
                    100;
                }
                this.payment_deadline = new Date();
                this.payment_deadline.setDate(this.payment_deadline.getDate() + 9);
              }
            } else {
              alert(res["messages"])
            }

            this.spinner.hide();
          },
          err => {
            this.spinner.hide();
          }
        );

      }
      // this.spinner.hide();
      // !!!!!!!!!!! Uncomment to here
    } else {
      alert("Order being process in back order!");
      location.reload();
    }
    sessionStorage.removeItem("voucherCode");
    window.scroll(0, 0);
  }

  radioActive() {
    this.disabledPay1 = "";
    this.disabledPay2 = "";
    this.disabledPay3 = "";
    this.disabledPay4 = "";
  }
  jenisPay(val) {
    this.radioActive();
    if (val == 1) {
      this.disabledPay3 = "disable-radio";
      this.disabledPay4 = "disable-radio";
      this.cash_payment_type = "Bank Transfer";
    } else {
      this.disabledPay1 = "disable-radio";
      this.disabledPay2 = "disable-radio";
      this.cash_payment_type = "Payment Gateway";
    }
  }

  onlinePaymentType(e) {
    console.log(e)
    
    if (this.disabledPay3 != "disable-radio" && this.disabledPay4 != "disable-radio") {
      if (e == 'klik-bca') {
        this.online_payment_type = "KlikBCA"
        $("input#userIDBCA").prop('disabled', false);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'bca-klikpay') {
        this.online_payment_type = "BCA KlikPay"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'cimb-click') {
        this.online_payment_type = "CIMB Click"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'ipay-bni') {
        this.online_payment_type = "iPay BNI"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'permata-bankva') {
        this.online_payment_type = "Permata VA"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'bca-va') {
        this.online_payment_type = "BCA VA"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").hide();
        $('label[for="t_and_c"]').hide();
      } else if (e == 'credit-card') {
        this.online_payment_type = "Credit Card"
        $("input#userIDBCA").prop('disabled', true);
        $("#t_and_c").show();
        $('label[for="t_and_c"]').show();
      }
    } else {
      this.online_payment_type = "Bank transfer"
      $("input#userIDBCA").prop('disabled', true);
      $("#t_and_c").hide();
      $('label[for="t_and_c"]').hide();
    }
  }

  onQuantityChange(val, max) {
    console.log(max);
    console.log(val);
    var value = "quantity" + (val + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If is not undefined
    if (!isNaN(currentVal)) {
      // Increment only if value is < 20
      if (currentVal < max) {
        $("input[name=" + val + "]").val(currentVal + 1);
        $(".qtyminus")
          .val("-")
          .removeAttr("style");
      } else {
        $(".qtyplus")
          .val("+")
          .css("color", "#aaa");
        $(".qtyplus")
          .val("+")
          .css("cursor", "not-allowed");
      }
    } else {
      // Otherwise put a 0 there
      $("input[name=" + val + "]").val(1);
    }
    this.prodSelected[val].qty_total =
      Number($("input[name=" + value + "]").val()) *
      Number(
        this.prodSelected[val].numerator_for_conversion_to_base_units_of_measure
      );
  }

  minusOneVoucher(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 1) {
      // Decrement one only if value is > 1
      $("input[name=" + val + "]").val(currentVal - 1);
      $(".qtyplus")
        .val("+")
        .removeAttr("style");
    } else {
      // Otherwise put a 0 there
      $(".qtyminus")
        .val("-")
        .css("color", "#aaa");
      $(".qtyminus")
        .val("-")
        .css("cursor", "not-allowed");
    }
    this.prodSelected[value].qty_total =
      Number($("input[name=" + val + "]").val()) *
      Number(
        this.prodSelected[value]
          .numerator_for_conversion_to_base_units_of_measure
      );
  }
  plusONeVoucher(value, max) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If is not undefined
    if (!isNaN(currentVal)) {
      // Increment only if value is < 20
      if (currentVal < max) {
        $("input[name=" + val + "]").val(currentVal + 1);
        $(".qtyminus")
          .val("-")
          .removeAttr("style");
      } else {
        $(".qtyplus")
          .val("+")
          .css("color", "#aaa");
        $(".qtyplus")
          .val("+")
          .css("cursor", "not-allowed");
      }
    } else {
      // Otherwise put a 0 there
      $("input[name=" + val + "]").val(1);
    }
    this.prodSelected[value].qty_total =
      Number($("input[name=" + val + "]").val()) *
      Number(
        this.prodSelected[value]
          .numerator_for_conversion_to_base_units_of_measure
      );
  }

  minusOne(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
      // Decrement one only if value is > 1
      $("input[name=" + val + "]").val(currentVal - 1);
      $(".qtyplus")
        .val("+")
        .removeAttr("style");
    } else {
      // Otherwise put a 0 there
      $(".qtyminus")
        .val("-")
        .css("color", "#aaa");
      $(".qtyminus")
        .val("-")
        .css("cursor", "not-allowed");
    }
    this.prodSelected[value].qty_total =
      Number($("input[name=" + val + "]").val()) *
      Number(
        this.prodSelected[value]
          .numerator_for_conversion_to_base_units_of_measure
      );
  }
  plusONe(value) {
    // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If is not undefined
    if (!isNaN(currentVal)) {
      // Increment only if value is < 20
      if (currentVal < 100000) {
        $("input[name=" + val + "]").val(currentVal + 1);
        $(".qtyminus")
          .val("-")
          .removeAttr("style");
      } else {
        $(".qtyplus")
          .val("+")
          .css("color", "#aaa");
        $(".qtyplus")
          .val("+")
          .css("cursor", "not-allowed");
      }
    } else {
      // Otherwise put a 0 there
      $("input[name=" + val + "]").val(1);
    }
    this.prodSelected[value].qty_total =
      Number($("input[name=" + val + "]").val()) *
      Number(
        this.prodSelected[value]
          .numerator_for_conversion_to_base_units_of_measure
      );
  }

  minusOneUpload(value) {
    // // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
      // Decrement one only if value is > 1
      $("input[name=" + val + "]").val(currentVal - 1);
      $(".qtyplus")
        .val("+")
        .removeAttr("style");
    } else {
      // Otherwise put a 0 there
      $(".qtyminus")
        .val("-")
        .css("color", "#aaa");
      $(".qtyminus")
        .val("-")
        .css("cursor", "not-allowed");
    }
    this.prodSelected[value].qty = Number($("input[name=" + val + "]").val());
    // this.prodSelected[value].qty_total = Number($('input[name=' + val + ']').val()) * Number(this.prodSelected[value].numerator_for_conversion_to_base_units_of_measure);
  }
  plusONeUpload(value) {
    // // Get its current value
    var val = "quantity" + (value + 1);
    var currentVal = parseInt($("input[name=" + val + "]").val());
    // If is not undefined
    if (!isNaN(currentVal)) {
      // Increment only if value is < 20
      if (currentVal < 1000000) {
        $("input[name=" + val + "]").val(currentVal + 1);
        $(".qtyminus")
          .val("-")
          .removeAttr("style");
      } else {
        $(".qtyplus")
          .val("+")
          .css("color", "#aaa");
        $(".qtyplus")
          .val("+")
          .css("cursor", "not-allowed");
      }
    } else {
      // Otherwise put a 0 there
      $("input[name=" + val + "]").val(1);
    }
    this.prodSelected[value].qty = Number($("input[name=" + val + "]").val());
    // this.prodSelected[value].qty_total = Number($('input[name=' + val + ']').val()) * Number(this.prodSelected[value].numerator_for_conversion_to_base_units_of_measure);
  }

  searchOrder(val) {
    if (val.length > 1) {
      // console.log(this.simulate.shipto);
      var ship_to_code = this.simulate.shipto;
      if (ship_to_code) {
        // var ship_to_code = ship_to_code.split(" - ");
        // ship_to_code = ship_to_code[0];
        this.cbuOrder.getListMaterial(val, ship_to_code).subscribe(
          res => {
            this.materialCode = res;
          },
          err => {
            console.log(err);
          }
        );
      } else {
        alert("Please Choose Ship To Party First !");
        this.matSelcted = "";
      }
    }
  }

  addItemUpload() {
    if (this.matSelcted != "" && this.matSelcted != undefined) {
      var ship_to_code = this.simulate.shipto;
      this.cbuOrder.getMaterial(this.matSelcted, ship_to_code).subscribe(
        res => {
          var data = [];
          data.push(res);
          console.log(data);
          data[0].qty = 0;
          data[0].check_duplicate = "no";
          // console.log(this.pallet);
          // console.log(data[0]["material_number"]);

          var check = 0;

          if (document.getElementById("material_code")) {
            $("#tableCBU #material_code").each(function () {
              // console.log(data[0]["material_number"]);
              // console.log($(this).text());
              if ($(this).text() == data[0]["material_number"]) {
                check++;
                data[0].check_duplicate = "yes";
              } else {
                data[0].check_duplicate = "no";
              }
            });

            // console.log(check);
            if (check > 0) {
              alert("Please Add Another Item!");
              check = 0;
            } else {
              if (data[0].is_continue !== 0) this.prodSelected.push(data[0]);
              else alert("This Material is discontinued. Please contact HPPI");

              check = 0;
            }
          } else {
            if (data[0]) {
              if (data[0].is_continue !== 0) this.prodSelected.push(data[0]);
              else alert("This Material is discontinued. Please contact HPPI");
            } else {
              alert("You don't have permission to order this material.");
            }
          }
        },
        err => {
          console.log(err);
        }
      );
      console.log(this.prodSelected);
      // alert(this.prodSpareSelec);
    } else {
      alert("Please add code or material code!");
    }
    this.matSelcted = "";
  }

  addItem() {
    if (this.matSelcted != "" && this.matSelcted != undefined) {
      var ship_to_code = this.simulate.shipto;
      this.cbuOrder.getMaterial(this.matSelcted, ship_to_code).subscribe(
        res => {
          var data = [];
          data.push(res);
          console.log(data);
          if (this.pallet.length == 0) this.pallet.push(0);
          // console.log(this.pallet);
          // console.log(data[0]["material_number"]);

          var check = 0;

          if (document.getElementById("material_code")) {
            $("#tableCBU #material_code").each(function () {
              // console.log(data[0]["material_number"]);
              // console.log($(this).text());
              if ($(this).text() == data[0]["material_number"]) {
                check++;
              }
            });

            // console.log(check);
            if (check > 0) {
              alert("Please Add Another Item!");
              check = 0;
            } else {
              if (data[0].is_continue !== 1) this.prodSelected.push(data[0]);
              else alert("This Material is discontinued. Please contact HPPI");
              check = 0;
            }
          } else {
            if (data[0]) {
              if (data[0].is_continue !== 1) this.prodSelected.push(data[0]);
              else alert("This Material is discontinued. Please contact HPPI");
            } else {
              alert("You don't have permission to order this material.");
            }
          }
        },
        err => {
          console.log(err);
        }
      );

      // alert(this.prodSpareSelec);
    } else {
      alert("Please add code or material code!");
    }
    this.matSelcted = "";
  }

  delete(val) {
    console.log(val);
    this.prodSelected.splice(val, 1);
    // this.totalOrdersUpload()
    this.pallet.splice(val, 1);
  }
  deleteItemVoucher(i) {
    if (this.prodSelected.length > 1) {
      this.prodSelected.splice(i, 1);
      // this.totalOrdersUpload()
      this.pallet.splice(i, 1);
    } else {
      alert("Please Remove Voucher Code");
    }
  }

  deleteUpload(val) {
    this.prodSelected.splice(val, 1);
  }

  deleteAll() {
    this.prodSelected = [];
    this.pallet = [];
  }

  totalOrdersUpload() {
    var totalOrder = 0;
    for (var i = 0; i < this.prodSelected.length; i++) {
      // console.log(this.prodSelected[i].item2)
      // console.log(this.prodSelected[i])
      if (this.prodSelected[i].qty) {
        totalOrder =
          totalOrder +
          this.prodSelected[i].qty *
          this.prodSelected[i]
            .numerator_for_conversion_to_base_units_of_measure;
      } else {
        totalOrder = totalOrder;
      }
    }
    return totalOrder;
  }

  totalOrders() {
    var totalOrder = 0;
    for (var i = 0; i < this.prodSelected.length; i++) {
      // console.log(this.prodSelected[i].qty_total)
      if (this.prodSelected[i].qty_total) {
        totalOrder = totalOrder + this.prodSelected[i].qty_total;
      } else {
        totalOrder = totalOrder;
      }
    }
    return totalOrder;
  }

  totalOrdersVoucher() {
    var totalOrder = 0;
    for (var i = 0; i < this.prodSelected.length; i++) {
      // console.log(this.prodSelected[i].qty_total)
      if (this.prodSelected[i].qty_total) {
        totalOrder = totalOrder + this.prodSelected[i].qty_total;
      } else {
        totalOrder = totalOrder;
      }
    }
    return totalOrder;
  }

  totalSimulateOrderQty() {
    var totalOrder = 0;
    $("#tableCBUSubmit #zkwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalConfirmationOrderQty() {
    var totalOrder = 0;
    $("#tableCBUConfirmation #zkwmeng").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalSimulateOrderAmount() {
    var totalOrder = 0;
    $("#tableCBUSubmit #znetwr").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalConfirmationOrderAmount() {
    var totalOrder = 0;
    $("#tableCBUConfirmation #znetwr").each(function () {
      // console.log(parseInt($(this).text()));
      totalOrder = totalOrder + parseInt($(this).text());
    });
    return totalOrder;
  }

  totalSimulateAmountAllocated() {
    var allocated = 0;
    $("#tableCBUSubmit #zznetwr2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    return allocated;
  }
  totalSimulateAllocated() {
    var allocated = 0;
    $("#tableCBUSubmit #zzkwmeng2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    return allocated;
  }
  totalAllocated() {
    var allocated = 0;
    $("#tableCBUConfirmation #zzkwmeng2").each(function () {
      // console.log(parseInt($(this).text()));
      allocated = allocated + parseInt($(this).text());
    });
    if (allocated == 0) this.lblConOrder = "Continue Order";
    return allocated;
  }

  totalBackorder() {
    var backorder = 0;
    $("#tableCBUSubmit #zbackorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }

  totalSubmitBackorder() {
    var backorder = 0;
    $("#tableCBUConfirmation #zbackorder").each(function () {
      // console.log(parseInt($(this).text()));
      backorder = backorder + parseInt($(this).text());
    });
    return backorder;
  }

  totalDiscount() {
    var discount = 0;
    $("#tableCBUSubmit #zzdiscount").each(function () {
      // console.log(parseInt($(this).text()));
      discount = discount + parseInt($(this).text());
    });
    return discount;
  }

  totalVAT() {
    var backorder = 0;
    // for (var i = 0; i < this.prodSelected.length; i++) {
    //   if(this.prodSelected[i].backorder){
    //     backorder = backorder + this.prodSelected[i].backorder;
    //   }else{
    //     backorder = 0;
    //   }
    // }
    return backorder;
  }

  totalAllocatedVAT() {
    var backorder = 0;
    // for (var i = 0; i < this.prodSelected.length; i++) {
    //   if(this.prodSelected[i].backorder){
    //     backorder = backorder + this.prodSelected[i].backorder;
    //   }else{
    //     backorder = 0;
    //   }
    // }
    return backorder;
  }
  removeVoucher() {
    this.Voucher = false;
    this.voucherCode = "";
    sessionStorage.removeItem("voucherCode");
    this.prodSelected = [];
    this.router.navigate(["/order-entry/cbu-order-entry?refresh=1"]);
    this.onRefresh();
  }
  onRefresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    let currentUrl = this.router.url + "?";
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }
  shipToCodeValidation() {
    if (!this.simulate.billpay) {
      alert("Please Choose Bill to Name first!");
      this.simulate.shipto = "";
      $("#shiptocode").val($("#shiptocode option:first").val());
    }
  }
  billToCodeValidation() {
    if (!this.simulate.billpay) {
      this.simulate.shipto = "";
      this.matSelcted = "";
      $("#billtocode").val($("#billtocode option:first").val());
    }
  }

  getDraft(param) {
    this.paramDraft = param;
    this.isDrafted = true;
    this.is_draft = "YES";
    this.spinner.show();
    this.cbuOrder.getDraft(param).subscribe(
      res => {
        var cbuDraft = [];
        // console.log(res);
        cbuDraft.push(res);
        // console.log(cbuDraft[0][0].material_number);
        if (cbuDraft) {
          var data = {
            po_number: cbuDraft[0][0].po_number,
            bill_to: cbuDraft[0][0].bill_to,
            ship_to: cbuDraft[0][0].ship_to,
            remarks: cbuDraft[0][0].remarks,
            material_number: [],
            pallet: [],
            payment_method: cbuDraft[0][0].payment_method,
            sales_order_number: cbuDraft[0][0].sales_order_number,
            user_id: cbuDraft[0][0].user_id,
            order_date: cbuDraft[0][0].order_date
          };
          this.simulate.punchOrnumb = data.po_number;
          this.simulate.billpay = data.bill_to;
          this.simulate.shipto = data.ship_to;
          this.simulate.remarks = data.remarks;

          // for (let index = 0; index < cbuDraft[0].length; index++) {
          //   data.material_number.push(cbuDraft[0][index].material_number);
          //   data.pallet.push(cbuDraft[0][index].pallet);
          // }
          // for (const item of cbuDraft[0]) {
          //   data.material_number.push(item.material_number);
          //   data.pallet.push(item.pallet);
          // }
          console.log(data);
          this.prodSelected = [];
          let i = 0;
          for (const item of cbuDraft[0]) {
            this.cbuOrder.getMaterial(item.material_number, data.ship_to).subscribe(
              res => {
                if (Object.entries(res).length > 0) {
                  // var dataMaterial = [];
                  // dataMaterial.push(res);
                  this.prodSelected.push(res);
                  this.pallet[i] = item.pallet;
                  var total =
                    item.pallet *
                    this.prodSelected[i]
                      .numerator_for_conversion_to_base_units_of_measure;
                  this.prodSelected[i].qty_total = total;
                  i++;
                }
              },
              err => {
                this.spinner.hide();
                console.log(err);
              }
            );
            // this.spinner.hide()
          }

          this.cbuOrder1 = "cbu-order-active";
          this.cbuOrder2 = "";
          this.CBUdraft = false;
          this.CBUorderEntry = true;
          this.spinner.hide();
        } else {
          this.spinner.hide();
          alert("Cannot Get Data !");
        }
      },
      err => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }
}
