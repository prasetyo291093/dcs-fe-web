import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class PendingTransactionService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  getSONumber() {
    var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${this.baseUrl}/api/pending-transaction-fix/${sap_code}`
    );
  }

  search(val) {
    return this.http.post(`{this.baseUrl}/api/pending-transaction/search`, val);
  }
  getPendingTransaction(so_number, order_type: number, idtrs, confirmid) {
    return this.http.get(
      `${
        this.baseUrl
      }/api/pending-transaction/continue/${so_number}/${confirmid}?spart=${order_type}&idtrs_detail=${idtrs}`
    );
  }
}
