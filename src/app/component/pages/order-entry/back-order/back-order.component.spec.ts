import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackOrderComponent } from './back-order.component';

describe('BackOrderComponent', () => {
  let component: BackOrderComponent;
  let fixture: ComponentFixture<BackOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
