import { Component, OnInit } from '@angular/core';
import { InquirySparepartsService } from '../../../../services/inquiry-spareparts.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-inquiry-spareparts',
  templateUrl: './inquiry-spareparts.component.html',
  styleUrls: ['./inquiry-spareparts.component.css']
})
export class InquirySparepartsComponent implements OnInit {

  infoSpare: boolean = false;
  GenOne: boolean = false;
  GenTwo: boolean = false;
  backButton: boolean = false;

  data: any;

  materialCode: any;

  material_code: string;
  supersession_part: string;
  material_description: string;
  currency: string;
  base_unit_of_measure: string;
  dealer_net: number;
  recommended_retail_price: string;
  model_unit: string;
  weight_unit: string;
  availability: string;

  customSelected: string;
  searchQuery: any
  storage_location: string

  canPreviewAvailability: boolean = false;
  canPreviewPriceInq: boolean = false;
  // statesComplex: any[] = [
  //   { id: 1, partNo: '30500-Z0T-800' },
  //   { id: 2, partNo: '30500-Z0T-801' },
  //   { id: 3, partNo: '30500-Z0T-802' },
  //   { id: 4, partNo: '30500-Z0T-803' },
  //   { id: 5, partNo: '30500-Z0T-804' }
  // ];

  constructor(
    private ListSpareparts: InquirySparepartsService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.canPreviewAvailability = JSON.parse(localStorage.getItem("user")).permissions["Preview availability"]
    this.canPreviewPriceInq = JSON.parse(localStorage.getItem("user")).permissions["Preview price"]
    
  }
  trackByFn(index, item) {
    return index;
  }
  getMaterialCode(val) {
    if (val.length > 4) {
      this.ListSpareparts.getListMaterial(val).subscribe(res => {
        this.materialCode = res;
        this.searchQuery = val
        for (var key in this.materialCode) {
          this.storage_location = this.materialCode[key].storage_location
        }
      }, err => {
        console.log(err);
      });
    } else {
      delete this.materialCode
      delete this.searchQuery
    }
  }

  nextGen(val) {
    // console.log(val);
    var customer_code = JSON.parse(localStorage.getItem('user')).sap_code;
    // var material_number = $("button").val();
    // console.log(this.supersession_part_);
    var data = {
      customer_number: customer_code,
      div: "20",
      material_number: val,
      sloc: this.storage_location
    };

    var previousGen = {
      availability: this.availability,
      dealer_net: this.dealer_net,
      currency: this.currency
    }
    console.log(data);
    this.spinner.show();

    this.ListSpareparts.getListInquirySpareparts(data).subscribe(res => {
      console.log(res);
      this.ListSpareparts.setPreviousGen(previousGen);

      if (res[0].material_code !== null) {
        this.infoSpare = true;
        this.GenOne = false;
        this.backButton = true;
        this.material_code = res[0].material_number;
        this.supersession_part = res[0].supersession_part;
        this.material_description = res[0].material_description;
        this.currency = res[0].currency;
        this.base_unit_of_measure = res[0].base_unit_of_measure;
        this.dealer_net = res[0].net_price * 100;
        this.recommended_retail_price = res[0].recommended_retail_price;
        this.weight_unit = res[0].weight_unit;
        if (res[0].qty > 0) {
          this.availability = "Yes";
        } else {
          this.availability = "No";
        }
        this.spinner.hide();
      } else {
        this.spinner.hide();
        alert("Item Not Found.");
        this.infoSpare = false;
        this.GenOne = false;
      }
      // this.availability = res["weight_unit"];
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
    this.GenOne = false;
    this.GenTwo = true;
  }

  backGen(val) {
    // var previousGen = JSON.parse(localStorage.getItem('previousPart'));
    // console.log(previousGen);
    this.spinner.show()
    this.ListSpareparts.getPreviousGen(val).subscribe(res => {
      console.log(res);
      // this.ListSpareparts.setPreviousGen(res);
      var dealer_net = JSON.parse(localStorage.getItem('previousPart')).dealer_net;
      var availability = JSON.parse(localStorage.getItem('previousPart')).availability;
      var currency = JSON.parse(localStorage.getItem('previousPart')).currency;

      if (res) {
        this.infoSpare = true;
        this.GenOne = false;
        this.material_code = res["material_code"];
        this.supersession_part = res["supersession_part"];
        this.material_description = res["material_description"];
        this.currency = currency;
        this.base_unit_of_measure = res["base_unit_of_measure"];
        this.dealer_net = dealer_net;
        this.recommended_retail_price = res["recommended_retail_price"];
        this.weight_unit = res["weight_unit"];
        this.availability = availability;
        // if(res[0].qty > 0){
        //   this.availability = "Yes";
        // }else {
        //   this.availability = "No";
        // }
        this.spinner.hide();
      } else {
        this.spinner.hide();
        alert("Item Not Found.");
        this.infoSpare = false;
        this.GenOne = false;
      }
    }, err => {
      this.spinner.hide();
      alert("Item Is The First Gen !");
      this.backButton = false;
      console.log(err);
    });
  }
  // console.log(val);
  // var customer_code = JSON.parse(localStorage.getItem('user')).sap_code;
  // // var material_number = $("button").val();
  // // console.log(this.supersession_part_);
  // var data = {
  //   customer_number: customer_code,
  //   div: "20",
  //   material_number: val,
  //   sloc: "2200"
  // };
  // console.log(data);
  // this.spinner.show();
  // this.ListSpareparts.getPreviousGen(val).subscribe(res=>{
  //   console.log(res);
  //
  //   if(res){
  //     // this.backButton = true;
  //     this.infoSpare = true;
  //     // this.GenOne = true;
  //     this.material_code = res["material_code"];
  //     this.supersession_part = res["supersession_part"];
  //     this.material_description = res["material_description"];
  //     this.currency = res["currency"];
  //     this.base_unit_of_measure = res["base_unit_of_measure"];
  //     this.dealer_net = Math.round(res["net_price"]);
  //     this.recommended_retail_price = res["recommended_retail_price"];
  //     this.weight_unit = res["weight_unit"];
  //     if(res["is_active"] == 1){
  //       this.availability = "Available";
  //     }else if(res["is_active"] == 0){
  //       this.availability = "Not Available";
  //     }
  //     this.spinner.hide();
  //   }else{
  //     this.ListSpareparts.getListInquirySpareparts(data).subscribe(res=>{
  //       console.log(res);
  //       // this.backButton = false;
  //       this.infoSpare = true;
  //       this.GenOne = true;
  //       this.material_code = res["material_code"];
  //       this.supersession_part = res["supersession_part"];
  //       this.material_description = res["material_description"];
  //       this.currency = res["currency"];
  //       this.base_unit_of_measure = res["base_unit_of_measure"];
  //       this.dealer_net = Math.round(res["net_price"]);
  //       this.recommended_retail_price = res["recommended_retail_price"];
  //       this.weight_unit = res["weight_unit"];
  //       if(res["is_active"] == 1){
  //         this.availability = "Available";
  //       }else if(res["is_active"] == 0){
  //         this.availability = "Not Available";
  //       }
  //       this.spinner.hide();
  //     });
  //     // this.spinner.hide();
  //     // alert("Item Not Found.");
  //     // this.infoSpare = false;
  //     // this.GenOne = true;
  // }
  //   // this.availability = res["weight_unit"];
  // },err=>{
  //   this.spinner.hide();
  //   console.log(err);
  // });
  // // this.GenOne = true;
  // this.GenTwo = false;
  // }

  getListInquirySpareparts(val) {

    if (val.length > 2) {
      this.searchQuery = val
      var customer_code = JSON.parse(localStorage.getItem('user')).sap_code;
      val = val.split(" - ");
      val = val[0];
      var data = {
        customer_number: customer_code,
        div: "20",
        material_number: val,
        sloc: "2200"
      };
      console.log(data);
      this.spinner.show();
      this.ListSpareparts.getListInquirySpareparts(data).subscribe(res => {
        console.log(res);
        if (res[0].is_continue !== 0) {
          this.ListSpareparts.setPreviousGen(res);
          if (res[0].material_code !== null) {
            this.infoSpare = true;
            this.GenOne = true;
            this.material_code = res[0].material_number;
            this.model_unit = res[0].model;
            this.supersession_part = res[0].supersession_part;
            this.material_description = res[0].material_description;
            this.currency = res[0].currency;
            this.base_unit_of_measure = res[0].base_unit_of_measure;
            this.dealer_net = Math.round(res[0].net_price) * 100;
            this.recommended_retail_price = res[0].recommended_retail_price;
            this.weight_unit = res[0].weight_unit;
            if (res[0].qty > 0) {
              this.availability = "Yes";
            } else {
              this.availability = "No";
            }
            this.spinner.hide();
          } else {
            this.spinner.hide();
            alert("Item Not Found.");
            this.infoSpare = false;
            this.GenOne = false;
          }
        } else {
          this.spinner.hide()
          alert("This part is discontinued. Please contact HPPI.")
        }
        // this.availability = res[0]["weight_unit"];
      }, err => {
        console.log(err);
      });
    }
  }

  submitItem() {
    var val = this.searchQuery
    localStorage.removeItem('previousPart')
    this.infoSpare = false;
    this.GenOne = false;
    this.GenTwo = false
    if (val) {
      var customer_code = JSON.parse(localStorage.getItem('user')).sap_code;
      val = val.split(" - ");
      val = val[0];
      var data = {
        customer_number: customer_code,
        div: "20",
        material_number: val,
        sloc: this.storage_location,
        user: JSON.parse(localStorage.getItem("user")).email
      };
      console.log(data);
      this.spinner.show();
      this.ListSpareparts.getListInquirySpareparts(data).subscribe(res => {
        console.log(res);
        // if (res[0].is_continue !== 0) {
        if (res[0]) {
          this.ListSpareparts.setPreviousGen(res);
          if (res[0].material_code !== null) {
            if (res[0].is_continue == 1)
              alert("This Material is discontinued. Please contact HPPI.")

            this.infoSpare = true;
            this.GenOne = true;
            this.material_code = res[0].material_number;
            this.model_unit = res[0].model;
            this.supersession_part = res[0].supersession_part;
            this.material_description = res[0].material_description;
            this.currency = res[0].currency;
            this.base_unit_of_measure = res[0].base_unit_of_measure;
            this.dealer_net = res[0].net_price * 100;
            this.recommended_retail_price = res[0].recommended_retail_price;
            this.weight_unit = res[0].weight_unit;
            if (parseInt(res[0].qty) > 0) {
              this.availability = "Yes";
            }
            else if (res[0].is_continue == 1) {
              this.availability = "Discontinue"
            }
            else {
              this.availability = "No";
            }
            this.spinner.hide();
          } else {
            this.spinner.hide();
            alert("Item Not Found.");
            this.infoSpare = false;
            this.GenOne = false;
          }
        } else {
          this.spinner.hide()
          alert("You don't have permission to check this material")
        }
        // } else {
        //   this.spinner.hide()
        //   alert("This part is discontinued. Please contact HPPI.")
        // }
        // this.availability = res[0]["weight_unit"];
      }, err => {
        this.spinner.hide()
        console.log(err);
        alert("You dont have permission to check this material.")
      });
    }
  }
}
