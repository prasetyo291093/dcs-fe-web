import { Component, OnInit } from '@angular/core';
import { DeliveryInformationService } from '../../../../services/delivery-information.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-delivery-information',
  templateUrl: './delivery-information.component.html',
  styleUrls: ['./delivery-information.component.css']
})
export class DeliveryInformationComponent implements OnInit {

  btnFilter: boolean = false
  resetFilter: boolean = false
  editNameDeli: boolean = false
  viewNameDeli: boolean = false
  filterDealerOrder: boolean = false
  filterDate: boolean = false
  filterNo: boolean = false
  filterPurchaseNo: boolean = false
  filterOrderType: boolean = false
  filterBillCode: boolean = false
  filterMaterialGroup: boolean = false
  filterMaterialCode: boolean = false
  filterDnDate: boolean = false
  filterDnNo: boolean = false
  filterDeliveryType: boolean = false
  filterExpedition: boolean = false
  filterBillNo: boolean = false
  filterVehicleNo: boolean = false
  filterShipmentStatus: boolean = false
  filterReceivedDate: boolean = false

  data: any;

  soNumber: any;
  matCode: any;
  dnNumber: any;

  so_number_from: any;
  so_number_to: any;
  so_date_from: any;
  so_date_to: any;
  dn_date_from: any;
  dn_date_to: any;
  dn_number_from: any;
  dn_number_to: any;
  material_number_from: any;
  material_number_to: any;
  freshData: boolean = true;
  filteredData: boolean = false;
  listFiltered: any;
  dropDownSO: any = [];
  dropDownDN: any = [];
  dropDownCode: any = [];

  canPreviewDownloadDeliveryReport: boolean = false


  constructor(
    private DeliveryInformation: DeliveryInformationService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) { }

  ngOnInit() {
    this.btnFilter = true;
    this.viewNameDeli = true
    this.canPreviewDownloadDeliveryReport = JSON.parse(localStorage.getItem("user")).permissions["Preview, update delivery report and download document"]
    this.getListDeliveryInformation()

    this.filterDealerOrder = true;
  }
  getListDeliveryInformation(): any {
    this.spinner.show()
    this.DeliveryInformation.getSONumber().subscribe(res => {
      var soNumber = [];
      soNumber.push(res);
      var sortByDate = function (a, b) {
        var dateA = new Date(b.sales_order_date).getTime();
        var dateB = new Date(a.sales_order_date).getTime();
        if (dateA > dateB) return 1;
        if (dateA < dateB) return -1;
        return 0;
      };

      this.data = soNumber[0].sort(sortByDate)

      let mapSo = new Map()
      let mapDN = new Map()
      let mapCode = new Map()



      for (const item of soNumber[0]) {
        if (!mapSo.has(item.sales_order_number)) {
          mapSo.set(item.sales_order_number, true)
          this.dropDownSO.push(item.sales_order_number)
        }
      }

      for (const item of soNumber[0]) {
        if (!mapDN.has(item.delivery_notice_number)) {
          mapDN.set(item.delivery_notice_number, true)
          this.dropDownDN.push(item.delivery_notice_number)
        }
      }

      for (const item of soNumber[0]) {
        if (!mapCode.has(item.material_number)) {
          mapCode.set(item.material_number, true)
          this.dropDownCode.push(item.material_number)
        }
      }

      this.spinner.hide()
    }, (err) => {
      this.spinner.hide()
      console.log(err)
    });
  }

  saveDelInformation(i, so_number, dn_number, mat_number) {
    var data = {
      confirm_by: $('#confBy' + i).val(),
      confirm_date: $('#confDate' + i + '').val()
    }

    if (data.confirm_by == '' || null) {
      alert('Please fill the Received Date Confirmation field');
    }
    else if (data.confirm_date == '' || null) {
      alert('Please fill the Received By Confirmation field');
    }
    else {
      this.DeliveryInformation.saveDelInformation(data, so_number, dn_number, mat_number).subscribe(res => {
        console.log(res)
        if (res['message'] == 'Success') {
          $('#confBy' + i).css({
            'display': 'none',
          })
          $('.nameTBLDelInfo' + i).css({
            'display': '',
          })
          $('#imgDel' + i).css({
            'display': '',
          })
          $('.nameTBLDelInfo' + i).text(data.confirm_by)
          alert('This delivery information has been confirmed by ' + res['name'] + '.');
        }
        else {
          alert('blablabla');
        }
      },
        error => {
          console.log(error)
        });
    }
  }

  search() {
    var data = {
      so_number_from: this.so_number_from,
      so_number_to: this.so_number_to,
      so_date_from: new Date(this.so_date_from),
      so_date_to: new Date(this.so_date_to),
      dn_date_from: new Date(this.dn_date_from),
      dn_date_to: new Date(this.dn_date_to),
      dn_number_from: this.dn_number_from,
      dn_number_to: this.dn_number_to,
      material_number_from: this.material_number_from
    }
    console.log(data)
    this.listFiltered = this.data
    this.listFiltered = this.listFiltered.filter((item: any) => {
      let order_date = new Date(item.sales_order_date)
      let delivery_notice_date = new Date(item.delivery_notice_date)
      console.log(order_date)
      order_date.setHours(0, 0, 0, 0)
      delivery_notice_date.setHours(0, 0, 0, 0)
      data.so_date_from.setHours(0, 0, 0, 0)
      data.so_date_to.setHours(0, 0, 0, 0)
      data.dn_date_from.setHours(0, 0, 0, 0)
      data.dn_date_to.setHours(0, 0, 0, 0)
      let result: any

      if (!isNaN(data.so_date_from.getTime()))
        result = order_date.getTime() == data.so_date_from.getTime()
      if (!isNaN(data.so_date_to.getTime()))
        result = order_date.getTime() == data.so_date_to.getTime()
      if (!isNaN(data.so_date_from.getTime()) && !isNaN(data.so_date_to.getTime()))
        result = order_date.getTime() >= data.so_date_from.getTime() && order_date.getTime() <= data.so_date_to.getTime()
      if (data.so_number_from)
        result = item.sales_order_number == data.so_number_from
      if (data.so_number_to)
        result = item.sales_order_number == data.so_number_to
      if (data.so_number_from && data.so_number_to)
        result = item.sales_order_number >= data.so_number_from && item.sales_order_number <= data.so_number_to
      if (data.material_number_from)
        result = item.material_number == data.material_number_from
      if (!isNaN(data.dn_date_from.getTime()))
        result = delivery_notice_date.getTime() == data.dn_date_from.getTime()
      if (!isNaN(data.dn_date_to.getTime()))
        result = delivery_notice_date.getTime() == data.dn_date_to.getTime()
      if (!isNaN(data.dn_date_from.getTime()) && !isNaN(data.dn_date_to.getTime()))
        result = delivery_notice_date.getTime() >= data.dn_date_from.getTime() && delivery_notice_date.getTime() <= data.dn_date_to.getTime()
      if (data.dn_number_from)
        result = item.delivery_notice_number == data.dn_number_from
      if (data.dn_number_to)
        result = item.delivery_notice_number == data.dn_number_to
      if (data.dn_number_from && data.dn_number_to)
        result = item.delivery_notice_number >= data.dn_number_from && item.delivery_notice_number <= data.dn_number_to
      if (!isNaN(data.so_date_from.getTime()) && !isNaN(data.so_date_to.getTime()) && data.so_number_from && data.so_number_to && data.material_number_from && !isNaN(data.dn_date_from.getTime()) && !isNaN(data.dn_date_to.getTime()) && data.dn_number_from && data.dn_number_to)
        result = order_date.getTime() >= data.so_date_from.getTime()
          && order_date.getTime() <= data.so_date_to.getTime()
          && item.matnr == data.material_number_from
          && item.sales_order_number >= data.so_number_from
          && item.sales_order_number <= data.so_number_to
          && delivery_notice_date.getTime() >= data.dn_date_from.getTime()
          && delivery_notice_date.getTime() <= data.dn_date_to.getTime()
          && item.delivery_notice_number >= data.dn_number_from
          && item.delivery_notice_number <= data.dn_number_to

      return result

    })
    this.freshData = false
    this.filteredData = true
  }
  reset() {
    delete this.so_number_from
    delete this.so_number_to
    delete this.so_date_from
    delete this.so_date_to
    delete this.material_number_from

    this.onRefresh()
    this.freshData = true
    this.filteredData = false
  }
  onRefresh(): any {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    }
    let currentUrl = this.router.url + '?'
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url])
    })
  }

  filter() {
    this.btnFilter = false;
    this.resetFilter = true;
  }
  resetFil() {
    this.btnFilter = true;
    this.resetFilter = false;
  }
  clickEditNameDeli(i) {
    $('#confBy' + i).css({
      'display': '',
    })
    $('.nameTBLDelInfo' + i).css({
      'display': 'none',
    })
    $('#imgDel' + i).css({
      'display': 'none',
    })
  }

  selectfil(event) {
    var z = event.target.value;

    if (z == "purcahse order no") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = true;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "order date") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = true;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "order no") {
      this.filterDealerOrder = false;
      this.filterNo = true;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "order type") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = true;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "bill code") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = true;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "material group") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = true;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "material code") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = true;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "dn date") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = true;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "dn no") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = true;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "delivery type") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = true;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "expedition") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = true;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "airway bill no") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = true;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "vehicle no") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = true;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    } else if (z == "Shipment Status") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = true;
      this.filterReceivedDate = false;
    } else if (z == "Received Date") {
      this.filterDealerOrder = false;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = true;
    } else {
      this.filterDealerOrder = true;
      this.filterNo = false;
      this.filterDate = false;
      this.filterPurchaseNo = false;
      this.filterOrderType = false;
      this.filterBillCode = false;
      this.filterMaterialGroup = false;
      this.filterMaterialCode = false;
      this.filterDnDate = false;
      this.filterDnNo = false;
      this.filterDeliveryType = false;
      this.filterExpedition = false;
      this.filterBillNo = false;
      this.filterVehicleNo = false;
      this.filterShipmentStatus = false;
      this.filterReceivedDate = false;
    }


    console.log(z)
  }

  downloadInvoice(dnNumber) {
    console.log(dnNumber)
    let item = [...this.data]
    item = item.filter(e => e.delivery_notice_number == dnNumber)

    let data = {
      material_code: [],
      so_number: [],
      dn_number: dnNumber,
      sold_to_code: JSON.parse(localStorage.getItem("user")).sap_code || "-",
      bill_to_code: item[0].bill_to_code,
      dealer_name: localStorage.getItem("dealer_name"),
      address: JSON.parse(localStorage.getItem("dealer")).address[0] || "-",
      currency: "IDR",
      dn_qty: [],
      material_description: [],
    }

    for (const iterator of item) {
      data.material_code.push(iterator.material_number)
      data.material_description.push(iterator.material_description)
      data.so_number.push(iterator.sales_order_number)
      data.dn_qty.push(iterator.delivery_notice_quantity)
    }

    // data.vat = parseInt(data.total_amount.reduce((a, b) => a + b, 0)) * 0.1
    // data.total_with_vat = parseInt(data.total_amount.reduce((a, b) => a + b, 0)) + data.vat

    console.log(data)
    this.DeliveryInformation.downloadInvoice(data).subscribe(result => {
      this.downloadFile(result, "invoice")
    }, error => console.error(error))
  }

  downloadSO(soNumber, confirmid) {
    // let item = [...this.data]
    // item = item.filter(e => e.sales_order_number == soNumber)

    let data = {
      so_number: soNumber,
      confirmid: confirmid
    }

    this.DeliveryInformation.downloadOrderConfirmation(data).subscribe(result => {
      this.downloadFile(result, "so-number")
    }, error => console.error(error))
  }

  downloadFile(data, filename) {
    filename = filename ? filename : "invoice"
    const blob = new Blob([data], { type: 'application/pdf' })
    const url = window.URL.createObjectURL(blob)
    // window.location.href = url
    // window.open
    let link = document.createElement('a')
    link.style.display = 'none';
    link.setAttribute("download", `${filename}.pdf`)
    link.setAttribute('href', url)
    document.body.appendChild(link)
    link.click()
  }
}
