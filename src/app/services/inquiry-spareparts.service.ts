import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class InquirySparepartsService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  getListMaterial(val) {
    var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      this.baseUrl +
        "/api/inquiry-sparepart/searchMaterial/" +
        sap_code +
        "?search=" +
        val
    );
  }

  getListInquirySpareparts(val) {
    return this.http.post(this.baseUrl + "/api/input-atp", val);
  }

  setPreviousGen(val) {
    var data = JSON.stringify(val);
    var previousPart = localStorage.setItem("previousPart", data);
    // return this.http.get(this.baseUrl + '/api/inquiry-sparepart/search-super?supersession='+val);
  }

  getPreviousGen(val) {
    return this.http.get(
      this.baseUrl + "/api/inquiry-sparepart/search-super?supersession=" + val
    );
  }
}
