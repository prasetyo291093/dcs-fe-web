import { TestBed } from '@angular/core/testing';

import { DealerTargetSparepartService } from './dealer-target-sparepart.service';

describe('DealerTargetSparepartService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DealerTargetSparepartService = TestBed.get(DealerTargetSparepartService);
    expect(service).toBeTruthy();
  });
});
