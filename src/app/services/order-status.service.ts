import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class OrderStatusService {
  constructor(private http: HttpClient) { }

  baseUrl = environment.baseUrl;

  getSONumber() {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${this.baseUrl}/api/order-status/list-web/${sap_code}`
    );
  }

  search(val) {
    return this.http.post(`${this.baseUrl}/api/order-status/search`, val);
  }

  downloadPDF(path_name,file_name) {
    return this.http.get(`${this.baseUrl}/api/download-pdf/`+path_name+`/`+file_name+`.pdf`);
  }

  downloadOrderConfirmation(data) {
    return this.http.post(`${this.baseUrl}/api/download-so-number`, data, { responseType: 'blob' })
  }
}
