import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InquirySparepartsComponent } from './inquiry-spareparts.component';

describe('InquirySparepartsComponent', () => {
  let component: InquirySparepartsComponent;
  let fixture: ComponentFixture<InquirySparepartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InquirySparepartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquirySparepartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
