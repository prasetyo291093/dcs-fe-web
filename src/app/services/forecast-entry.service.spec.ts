import { TestBed } from '@angular/core/testing';

import { ForecastEntryService } from './forecast-entry.service';

describe('ForecastEntryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ForecastEntryService = TestBed.get(ForecastEntryService);
    expect(service).toBeTruthy();
  });
});
