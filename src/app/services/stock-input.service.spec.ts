import { TestBed } from '@angular/core/testing';

import { StockInputService } from './stock-input.service';

describe('StockInputService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StockInputService = TestBed.get(StockInputService);
    expect(service).toBeTruthy();
  });
});
