import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class UserManagementService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  // getMaterial(val){
  //   return this.http.get('http://52.77.251.170/api/order-cbu/search-code?search='+val);
  // }
  addUser(val) {
    return this.http.post(`${this.baseUrl}/api/user-request`, val)
  }

  getUser() {
    var sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(`${this.baseUrl}/api/user-list/${sap_code}`);
  }
  
  getDetailUser(id) {
    return this.http.get(`${this.baseUrl}/api/users/detail/${id}`);
  }

  deleteUserRequest(data) {
    return this.http.post(`${this.baseUrl}/api/delete-user-request`, data);
  }

  saveDetailUser(id, data) {
    return this.http.post(`${this.baseUrl}/api/users/detail/${id}`, data);
  }
}
