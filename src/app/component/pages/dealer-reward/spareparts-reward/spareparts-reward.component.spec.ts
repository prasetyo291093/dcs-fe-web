import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SparepartsRewardComponent } from './spareparts-reward.component';

describe('SparepartsRewardComponent', () => {
  let component: SparepartsRewardComponent;
  let fixture: ComponentFixture<SparepartsRewardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparepartsRewardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparepartsRewardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
