import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { OutstandingAndOverdueReportService } from "src/app/services/outstanding-and-overdue-report.service";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { findLast } from "@angular/compiler/src/directive_resolver";
declare var jquery: any;
declare var $: any;
declare var number: any;

@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.css"]
})
export class ReportComponent implements OnInit {
  btnFilter: boolean = false;
  resetFilter: boolean = false;
  filterBillTo: boolean = false;
  filterInvoiceNo: boolean = false;
  filterInvoiceDate: boolean = false;
  filterInvoiceQty: boolean = false;
  filterSymbol: boolean = false;
  filterDueDate: boolean = false;
  filterTop: boolean = false;
  filterTaxtInvoice: boolean = false;
  filterSoNo: boolean = false;
  prodSelected: any = [];
  limit: any = [];
  outstanding: any = [];

  so_date_from: any;
  so_date_to: any;
  invoice_date_from: any;
  invoice_date_to: any;
  due_date_from: any;
  due_date_to: any;

  listFiltered: any = [];

  filteredData: boolean;
  remaining_credit_limit: any;
  color: string;
  colorTotalOverdue: string;

  constructor(
    private spinner: NgxSpinnerService,
    private outstandingReport: OutstandingAndOverdueReportService,
    private router: Router
  ) {}

  ngOnInit() {
    this.btnFilter = true;

    this.filterBillTo = true;
    this.getOutstandingOverdueReport();
  }
  getOutstandingOverdueReport() {
    this.spinner.show();
    this.outstandingReport.getOutstandingReport().subscribe(
      (_result: []) => {
        // console.log();
        if (_result["limit"].length > 0 && _result["outstanding"].length > 0) {
          this.limit = _result["limit"][0];
          if (Math.sign(_result["limit"][0]["remaining_credit_limit"]) == -1) {
            //negative
            let rcl: string = _result["limit"][0][
              "remaining_credit_limit"
            ].toString();
            this.remaining_credit_limit = rcl.split("-")[1];
            this.color = "red";
          } else {
            //positive
            this.remaining_credit_limit = _result["limit"][0][
              "remaining_credit_limit"
            ].toString();
            this.color = "black";
          }

          if (Math.sign(_result["limit"][0]["total_overdue"]) == 1) {
            this.colorTotalOverdue = "red";
          } else {
            this.colorTotalOverdue = "black";
          }
          // for (const item of _result["outstanding"]) {
          //   let dateNow = new Date();
          //   let dueDate = new Date(item.due_date);
          //   dateNow.setHours(0,0,0,0)
          //   dueDate.setHours(0,0,0,0)

          //   if (dateNow.getTime() <= dueDate.getTime())
          //     item.symbol_due = "./assets/img/calendar.png";
          //   if (dateNow.getTime() == dueDate.getTime())
          //     item.symbol_due = "./assets/img/lonceng.png";
          //   if (dateNow.getTime() >= dueDate.getTime())
          //     item.symbol_due = "./assets/img/petir.png";
          // }
          this.outstanding = _result["outstanding"];
          console.log(this.outstanding);
        }
        this.spinner.hide();
      },
      (_error: Error) => {
        this.spinner.hide();
        console.log(_error.message);
      }
    );
  }

  total() {
    let total = 0;
    let data: any;
    if (this.filteredData) {
      data = this.listFiltered;
    } else {
      data = this.outstanding;
    }

    for (const iterator of data) {
      // tslint:disable-next-line: radix
      total += parseInt(iterator.invoice_amount);
    }

    return total;
  }

  convertToCSV(args) {
    let result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
      return null;
    }

    columnDelimiter = args.columnDelimiter || ",";
    lineDelimiter = args.lineDelimiter || "\n";

    keys = Object.keys(data[0]);

    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(item => {
      ctr = 0;
      keys.forEach(key => {
        if (ctr > 0) result += columnDelimiter;

        result += item[key];
        ctr++;
      });
      result += lineDelimiter;
    });

    return result;
  }

  test() {
    let data, filename, link, outstandingData;

    outstandingData =
      this.filteredData == true ? this.listFiltered : this.outstanding;

    for (let i = 0; i < outstandingData.length; i++) {
      if (outstandingData[i]["symbol_due"] == "@OJ@")
        outstandingData[i]["symbol_due"] = "Over Due";
      if (outstandingData[i]["symbol_due"] == "@FR@")
        outstandingData[i]["symbol_due"] = "Not Due";
      if (outstandingData[i]["symbol_due"] == "@1V@")
        outstandingData[i]["symbol_due"] = "Due";

        outstandingData[i]["invoice_amount"] = outstandingData[i]["invoice_amount"] * 100
    }
    // for (const item of outstandingData) {
    //   if (item.symbol_due == "./assets/img/petir.png")
    //     item.symbol_due = "Over Due";
    //   if (item.symbol_due == "./assets/img/calendar.png")
    //     item.symbol_due = "Not Due";
    //   if (item.symbol_due == "./assets/img/lonceng.png")
    //     item.symbol_due = "Due";
    // }

    console.log(this.outstanding);
    let csv = this.convertToCSV({
      data: outstandingData
    });

    if (csv == null) return;
    filename = "Report Outstanding.csv";

    if (!csv.match(/^data:text\/csv/i)) {
      csv = "data:text/csv;charset=utf-8," + csv;
    }

    data = encodeURI(csv);
    link = document.createElement("a");
    link.setAttribute("href", data);
    link.setAttribute("download", filename);
    link.click();

    for (let i = 0; i < outstandingData.length; i++) {
      if (outstandingData[i]["symbol_due"] == "Over Due")
        outstandingData[i]["symbol_due"] = "@OJ@";
      if (outstandingData[i]["symbol_due"] == "Not Due")
        outstandingData[i]["symbol_due"] = "@FR@";
      if (outstandingData[i]["symbol_due"] == "Due")
        outstandingData[i]["symbol_due"] = "@1V@";
    }
    // console.log(csv);
  }

  search() {
    this.spinner.show();
    const data = {
      so_date_from: new Date(this.so_date_from),
      so_date_to: new Date(this.so_date_to),
      invoice_date_from: new Date(this.invoice_date_from),
      invoice_date_to: new Date(this.invoice_date_to),
      due_date_from: new Date(this.due_date_from),
      due_date_to: new Date(this.due_date_to)
    };

    console.log(this.due_date_from);

    this.listFiltered = this.outstanding;
    this.listFiltered = this.listFiltered.filter((item: any) => {
      const invoice_date = new Date(item.invoice_date);
      const due_date = new Date(item.due_date);
      let sales_order_date = new Date(item.sales_order_date)

      invoice_date.setHours(0, 0, 0, 0);
      due_date.setHours(0, 0, 0, 0);
      data.so_date_from.setHours(0, 0, 0, 0);
      data.so_date_to.setHours(0, 0, 0, 0);
      data.invoice_date_from.setHours(0, 0, 0, 0);
      data.invoice_date_to.setHours(0, 0, 0, 0);
      data.due_date_from.setHours(0, 0, 0, 0);
      data.due_date_to.setHours(0, 0, 0, 0);
      let result: any;

      // tslint:disable-next-line: max-line-length
      if (
        isNaN(data.due_date_from.getTime()) &&
        isNaN(data.due_date_to.getTime()) &&
        isNaN(data.invoice_date_from.getTime()) &&
        isNaN(data.invoice_date_to.getTime()) &&
        isNaN(data.so_date_from.getTime()) &&
        isNaN(data.so_date_to.getTime())
      ) {
        // result = item;
        // return result;
        this.filteredData = false;

        this.resetForm();
        this.onRefresh();

        return item;
      }

      if (!isNaN(data.invoice_date_from.getTime())) {
        result = invoice_date.getTime() === data.invoice_date_from.getTime();
      }

      if (!isNaN(data.invoice_date_to.getTime())) {
        result = invoice_date.getTime() === data.invoice_date_to.getTime();
      }

      if (!isNaN(data.due_date_from.getTime())) {
        result = due_date.getTime() === data.due_date_from.getTime();
      }

      if (!isNaN(data.due_date_to.getTime())) {
        result = due_date.getTime() === data.due_date_to.getTime();
      }

      if (
        !isNaN(data.invoice_date_from.getTime()) &&
        !isNaN(data.invoice_date_to.getTime())
      ) {
        result =
          invoice_date.getTime() >= data.invoice_date_from.getTime() &&
          invoice_date.getTime() <= data.invoice_date_to.getTime();
      }

      if (
        !isNaN(data.due_date_from.getTime()) &&
        !isNaN(data.due_date_to.getTime())
      ) {
        result =
          due_date.getTime() >= data.due_date_from.getTime() &&
          due_date.getTime() <= data.due_date_to.getTime();
      }

      if(!isNaN(data.so_date_from.getTime())) {
        result = sales_order_date.getTime() == data.so_date_from.getTime()
      }

      if(!isNaN(data.so_date_to.getTime())) {
        result = sales_order_date.getTime() == data.so_date_to.getTime()
      }

      if(!isNaN(data.so_date_from.getTime()) && !isNaN(data.so_date_to.getTime())) {
        result = sales_order_date.getTime() >= data.so_date_from.getTime() && sales_order_date.getTime() <= data.so_date_to.getTime()
      }
      this.resetForm();

      return result;
    });

    this.filteredData = true;
    this.spinner.hide();
  }

  resetForm() {
    delete this.so_date_from;
    delete this.so_date_to;
    delete this.invoice_date_from;
    delete this.invoice_date_to;
    delete this.due_date_from;
    delete this.due_date_to;
  }

  onRefresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };
    let currentUrl = this.router.url + "?";
    this.router.navigateByUrl(currentUrl).then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }

  pay(so_number,invoice_number,type){
    if(type == 'CBU')
      this.router.navigate(["/outstanding-invoice-cbu"], { queryParams: { so_number: so_number, invoice_number: invoice_number } })
    else
      this.router.navigate(["/outstanding-invoice-spareparts"], { queryParams: { so_number: so_number, invoice_number: invoice_number } })
  }

  filter() {
    this.btnFilter = false;
    this.resetFilter = true;
  }
  resetFil() {
    this.btnFilter = true;
    this.resetFilter = false;
  }

  selectfil(event) {
    var z = event.target.value;

    if (z == "invoice no") {
      this.filterBillTo = false;
      this.filterInvoiceNo = true;
      this.filterInvoiceDate = false;
      this.filterInvoiceQty = false;
      this.filterSymbol = false;
      this.filterDueDate = false;
      this.filterTop = false;
      this.filterTaxtInvoice = false;
      this.filterSoNo = false;
    } else if (z == "invoice date") {
      this.filterBillTo = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceDate = true;
      this.filterInvoiceQty = false;
      this.filterSymbol = false;
      this.filterDueDate = false;
      this.filterTop = false;
      this.filterTaxtInvoice = false;
      this.filterSoNo = false;
    } else if (z == "invoice qty") {
      this.filterBillTo = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceQty = true;
      this.filterSymbol = false;
      this.filterDueDate = false;
      this.filterTop = false;
      this.filterTaxtInvoice = false;
      this.filterSoNo = false;
    } else if (z == "symbol due") {
      this.filterBillTo = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceQty = false;
      this.filterSymbol = true;
      this.filterDueDate = false;
      this.filterTop = false;
      this.filterTaxtInvoice = false;
      this.filterSoNo = false;
    } else if (z == "due date") {
      this.filterBillTo = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceQty = false;
      this.filterSymbol = false;
      this.filterDueDate = true;
      this.filterTop = false;
      this.filterTaxtInvoice = false;
      this.filterSoNo = false;
    } else if (z == "top") {
      this.filterBillTo = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceQty = false;
      this.filterSymbol = false;
      this.filterDueDate = false;
      this.filterTop = true;
      this.filterTaxtInvoice = false;
      this.filterSoNo = false;
    } else if (z == "tax invoice") {
      this.filterBillTo = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceQty = false;
      this.filterSymbol = false;
      this.filterDueDate = false;
      this.filterTop = false;
      this.filterTaxtInvoice = true;
      this.filterSoNo = false;
    } else if (z == "so no") {
      this.filterBillTo = false;
      this.filterInvoiceNo = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceQty = false;
      this.filterSymbol = false;
      this.filterDueDate = false;
      this.filterTop = false;
      this.filterTaxtInvoice = false;
      this.filterSoNo = true;
    } else {
      this.filterBillTo = true;
      this.filterInvoiceNo = false;
      this.filterInvoiceDate = false;
      this.filterInvoiceQty = false;
      this.filterSymbol = false;
      this.filterDueDate = false;
      this.filterTop = false;
      this.filterTaxtInvoice = false;
      this.filterSoNo = false;
    }

    console.log(z);
  }
}
