import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class InvoiceTimelinessService {

  constructor(private http: HttpClient) {}

  baseUrl = environment.baseUrl;

  getInvoiceTimeline() {
    let sap_code = JSON.parse(localStorage.getItem('user')).sap_code
    return this.http.get(`${this.baseUrl}/api/invoice-timeline/get-data/${sap_code}`);
  }

  getInvoiceTimelineDetail(month: number) {
    let sap_code = JSON.parse(localStorage.getItem('user')).sap_code
    return this.http.get(`${this.baseUrl}/api/invoice-timeline/detail/${sap_code}/${month}`);
  }
}
