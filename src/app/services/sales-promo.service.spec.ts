import { TestBed } from '@angular/core/testing';

import { SalesPromoService } from './sales-promo.service';

describe('SalesPromoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SalesPromoService = TestBed.get(SalesPromoService);
    expect(service).toBeTruthy();
  });
});
