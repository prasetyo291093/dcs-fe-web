import { TestBed } from '@angular/core/testing';

import { DealerRewardCbuService } from './dealer-reward-cbu.service';

describe('DealerRewardCbuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DealerRewardCbuService = TestBed.get(DealerRewardCbuService);
    expect(service).toBeTruthy();
  });
});
