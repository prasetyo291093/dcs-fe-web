import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../services/auth.service";
import { Router } from "@angular/router";
import { Chart } from "chart.js";
import { DealerProfileService } from "../../../services/dealer-profile.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
declare var jquery: any;
declare var $: any;
import { BrowserModule, DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: "app-dealer-profile",
  templateUrl: "./dealer-profile.component.html",
  styleUrls: ["./dealer-profile.component.css"]
})
export class DealerProfileComponent implements OnInit {
  items: any = [];

  // information: any;

  modalRef: BsModalRef;

  tab1: string;
  tab2: string;
  tab3: string;
  tab4: string;

  dealer_name: string;
  payment_method: string;
  dealer_category_name: string;
  region: string;
  service_facility: string;
  product_handling: string;
  address: string;
  district: string;
  city: string;
  phone_number1: string;
  phone_number2: string;
  phone_number3: string;
  fax1: string;
  fax2: string;
  mobile_phone_number1: string;
  mobile_phone_number2: string;
  mobile_phone_number3: string;
  email1: string;
  email2: string;
  email3: string;
  website: string;
  branch_name1: string;
  branch_desc1: string;
  branch_name2: string;
  branch_desc2: string;
  branch_name3: string;
  branch_desc3: string;
  additional_information: string;
  number_of_technician: string;
  number_of_sales: string;
  number_of_employee: string;
  pProfile: string;

  tabInfo1: boolean;
  tabInfo2: boolean;
  tabInfo3: boolean;
  tabInfo4: boolean;

  chartOne: Object;
  chartTwo: Object;
  chartThree: Object;
  chartFour: Object;
  chartFive: Object;
  starting_bussiness: string;
  user_permissions: boolean = false;
  user1S: boolean = false;
  user2S: boolean = false;
  user3S: boolean = false;
  store_photo1: string;
  store_photo2: string;
  store_photo3: string;
  store_photo4: string;
  image: any;
  evaluation_desc1: string;
  evaluation_desc2: string;
  evaluation_desc3: string;
  evaluation_file1: string;
  evaluation_file2: string;
  evaluation_file3: string;
  legal_documents: any = [];
  service_evaluation: string;
  sales_evaluation: string;
  imageLegalDocument: any;
  id_card_name: string;
  id_card_path: string;
  imageIdCard: string;

  canPreviewDealerProfileMenu: boolean = false


  constructor(
    // private router: Router,
    // private auth: AuthService,
    private modalService: BsModalService,
    private dealprof: DealerProfileService,
    private spinner: NgxSpinnerService,
    public sanitized: DomSanitizer
  ) {
    let monthly = JSON.parse(localStorage.getItem("monthlyTargetActual"));
    this.chartOne = {
      chart: {
        theme: "fusion",
        yAxisName: "Quantity",
        // "divLineIsDashed": "1",
        // "divLineDashLen": "1",
        // "divLineGapLen": "1",
        // "showPlotBorder": "1",
        // "plotBorderDashed": "1",
        // "plotBorderDashLen": "5",
        // "plotBorderDashGap": "5",
        showPlotBorder: "1",
        plotBorderThickness: "1",
        plotBorderColor: "#000000",
        showValues: "1",
        rotateValues: "1",
        placeValuesInside: "1",
        palettecolors: "78C5FD,FFCC00",
        showLegend: "0",
        formatnumberscale: "0"
      },
      categories: [
        {
          category: [
            {
              label: "Apr"
            },
            {
              label: "May"
            },
            {
              label: "Jun"
            },
            {
              label: "Jul"
            },
            {
              label: "Aug"
            },
            {
              label: "Sep"
            },
            {
              label: "Oct"
            },
            {
              label: "Nov"
            },
            {
              label: "Des"
            },
            {
              label: "Jan"
            },
            {
              label: "Feb"
            },
            {
              label: "Mar"
            }
          ]
        }
      ],
      dataset: [
        {
          seriesname: "Target",
          data: [
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].april
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].may
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].june
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].july
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].august
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].september
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].october
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].november
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].december
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].january
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].february
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyCBU.target[0]
                ? monthly.monthlyCBU.target[0].march
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            }
          ]
        },
        {
          seriesname: "Actual",
          data: [
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].april
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].may
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].june
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].july
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].august
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].september
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].october
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].november
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].december
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].january
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].february
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyCBU.actual[0]
                ? monthly.monthlyCBU.actual[0].march
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            }
          ]
        }
      ]
    }; // end of this.dataSource

    let firstYear = new Date().getFullYear() - 1;
    let secondYear = new Date().getFullYear() - 2;
    let thirdYear = new Date().getFullYear() - 3;
    let fourthYear = new Date().getFullYear() - 4;
    let fifthYear = new Date().getFullYear() - 5;

    let salesHistory: [] = JSON.parse(localStorage.getItem("salesHistory"));
    // salesHistory.filter(val => console.log(val))
    let salesHistoryCBUActual = [];
    let salesHistoryCBUTarget = [];
    let salesHistorySparepartActual = [];
    let salesHistorySparepartTarget = [];

    for (const iterator of salesHistory["actual"]) {
      if (iterator.division == "10") {
        salesHistoryCBUActual.push(iterator);
      } else if (iterator.division == "20") {
        salesHistorySparepartActual.push(iterator);
      }
    }

    for (const iterator of salesHistory["target"]) {
      if (iterator.division == "10") {
        salesHistoryCBUTarget.push(iterator);
      } else if (iterator.division == "20") {
        salesHistorySparepartTarget.push(iterator);
      }
    }
    // let salesHistoryCBU = {
    //   year
    // }
    let chartTwo = {
      chart: {
        theme: "fusion",
        yAxisName: "Quantity",
        // "divLineIsDashed": "1",
        // "divLineDashLen": "1",
        // "divLineGapLen": "1",
        // "showPlotBorder": "1",
        // "plotBorderDashed": "1",
        // "plotBorderDashLen": "5",
        // "plotBorderDashGap": "5",
        showPlotBorder: "1",
        plotBorderThickness: "1",
        plotBorderColor: "#000000",
        showValues: "1",
        rotateValues: "1",
        placeValuesInside: "1",
        palettecolors: "78C5FD,BB76FF",
        showLegend: "0",
        formatnumberscale: "0",
        subCaption: "*Periode Ki = April – March"
      },
      categories: [
        {
          category: [
            // { label: fifthYear.toString() },
            // { label: fourthYear.toString() },
            // { label: thirdYear.toString() },
            // { label: secondYear.toString() },
            // { label: firstYear.toString() }
            { label: "91 ki" },
            { label: "92 ki" },
            { label: "93 ki" },
            { label: "94 ki" },
            { label: "95 ki" }
          ]
        }
      ],
      dataset: [
        {
          seriesname: "Target",
          data: [
            {
              value: salesHistoryCBUTarget[0]
                ? salesHistoryCBUTarget[0].year1
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: salesHistoryCBUTarget[0]
                ? salesHistoryCBUTarget[0].year2
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: salesHistoryCBUTarget[0]
                ? salesHistoryCBUTarget[0].year3
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: salesHistoryCBUTarget[0]
                ? salesHistoryCBUTarget[0].year4
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: salesHistoryCBUTarget[0]
                ? salesHistoryCBUTarget[0].year5
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            }
          ]
        },
        {
          seriesname: "Actual",
          data: [
            {
              value: salesHistoryCBUActual[0]
                ? salesHistoryCBUActual[0].year1
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: salesHistoryCBUActual[0]
                ? salesHistoryCBUActual[0].year2
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: salesHistoryCBUActual[0]
                ? salesHistoryCBUActual[0].year3
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: salesHistoryCBUActual[0]
                ? salesHistoryCBUActual[0].year4
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: salesHistoryCBUActual[0]
                ? salesHistoryCBUActual[0].year5
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            }
          ]
        }
      ]
    }; // end of this.dataSource
    this.chartTwo = chartTwo;
    this.chartThree = {
      chart: {
        theme: "fusion",
        yAxisName: "Amount (In Thousands)",
        // "divLineIsDashed": "1",
        // "divLineDashLen": "1",
        // "divLineGapLen": "1",
        // "showPlotBorder": "1",
        // "plotBorderDashed": "1",
        // "plotBorderDashLen": "5",
        // "plotBorderDashGap": "5",
        showPlotBorder: "1",
        plotBorderThickness: "1",
        plotBorderColor: "#000000",
        showValues: "1",
        rotateValues: "1",
        placeValuesInside: "1",
        palettecolors: "78C5FD,98FB98",
        showLegend: "0",
        formatnumberscale: "0"
      },
      categories: [
        {
          category: [
            {
              label: "Apr"
            },
            {
              label: "May"
            },
            {
              label: "Jun"
            },
            {
              label: "Jul"
            },
            {
              label: "Aug"
            },
            {
              label: "Sep"
            },
            {
              label: "Oct"
            },
            {
              label: "Nov"
            },
            {
              label: "Des"
            },
            {
              label: "Jan"
            },
            {
              label: "Feb"
            },
            {
              label: "Mar"
            }
          ]
        }
      ],
      dataset: [
        {
          seriesname: "Target",
          data: [
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].april
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].may
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].june
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].july
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].august
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].september
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].october
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].november
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].december
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].january
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].february
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: monthly.monthlyPart.target[0]
                ? monthly.monthlyPart.target[0].march
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            }
          ]
        },
        {
          seriesname: "Actual",
          data: [
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].april
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].may
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].june
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].july
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].august
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].september
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].october
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].november
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].december
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].january
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].february
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: monthly.monthlyPart.actual[0]
                ? monthly.monthlyPart.actual[0].march
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            }
          ]
        }
      ]
    }; // end of this.dataSource
    this.chartFour = {
      chart: {
        theme: "fusion",
        yAxisName: "Amount (In Thousands)",
        // "divLineIsDashed": "1",
        // "divLineDashLen": "1",
        // "divLineGapLen": "1",
        // "showPlotBorder": "1",
        // "plotBorderDashed": "1",
        // "plotBorderDashLen": "5",
        // "plotBorderDashGap": "5",
        showPlotBorder: "1",
        plotBorderThickness: "1",
        plotBorderColor: "#000000",
        showValues: "1",
        rotateValues: "1",
        placeValuesInside: "1",
        palettecolors: "78C5FD,FF6699",
        showLegend: "0",
        formatnumberscale: "0",
        subCaption: "*Periode Ki = April – March"
      },
      categories: [
        {
          category: [
            // { label: fifthYear.toString() },
            // { label: fourthYear.toString() },
            // { label: thirdYear.toString() },
            // { label: secondYear.toString() },
            // { label: firstYear.toString() }
            { label: "91 ki" },
            { label: "92 ki" },
            { label: "93 ki" },
            { label: "94 ki" },
            { label: "95 ki" }
          ]
        }
      ],
      dataset: [
        {
          seriesname: "Target",
          data: [
            {
              value: salesHistorySparepartTarget[0]
                ? salesHistorySparepartTarget[0].year1
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: salesHistorySparepartTarget[0]
                ? salesHistorySparepartTarget[0].year2
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: salesHistorySparepartTarget[0]
                ? salesHistorySparepartTarget[0].year3
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: salesHistorySparepartTarget[0]
                ? salesHistorySparepartTarget[0].year4
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            },
            {
              value: salesHistorySparepartTarget[0]
                ? salesHistorySparepartTarget[0].year5
                : null,
              Dashed: "1",
              DashLen: "4",
              DashGap: "4"
            }
          ]
        },
        {
          seriesname: "Actual",
          data: [
            {
              value: salesHistorySparepartActual[0]
                ? salesHistorySparepartActual[0].year1
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: salesHistorySparepartActual[0]
                ? salesHistorySparepartActual[0].year2
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: salesHistorySparepartActual[0]
                ? salesHistorySparepartActual[0].year3
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: salesHistorySparepartActual[0]
                ? salesHistorySparepartActual[0].year4
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            },
            {
              value: salesHistorySparepartActual[0]
                ? salesHistorySparepartActual[0].year5
                : null,
              Dashed: "1",
              DashLen: "0",
              DashGap: "0"
            }
          ]
        }
      ]
    }; // end of this.dataSource
    this.chartFive = {
      chart: {
        theme: "fusion",
        yAxisName: "Quantity",
        // "divLineIsDashed": "1",
        // "divLineDashLen": "1",
        // "divLineGapLen": "1",
        // "showPlotBorder": "1",
        // "plotBorderDashed": "1",
        // "plotBorderDashLen": "5",
        // "plotBorderDashGap": "5",
        // "showPlotBorder": "1",
        // "plotBorderThickness": "1",
        // "plotBorderColor": "#000000",
        showValues: "1",
        rotateValues: "0",
        placeValuesInside: "1",
        palettecolors: "C2F300,FF0000",
        showLegend: "0"
      },
      categories: [
        {
          category: [
            {
              label: "Jan"
            },
            {
              label: "Feb"
            },
            {
              label: "Mar"
            },
            {
              label: "Apr"
            },
            {
              label: "May"
            },
            {
              label: "Jun"
            },
            {
              label: "Jul"
            }
          ]
        }
      ],
      dataset: [
        {
          seriesName: "Dealer Stock",
          data: [
            {
              value: null
            },
            {
              value: null
            },
            {
              value: null
            },
            {
              value: null
            },
            {
              value: null
            },
            {
              value: null
            },
            {
              value: null
            }
          ]
        },
        {
          seriesName: "Average Stock",
          renderAs: "spline",
          data: [
            {
              value: null
            },
            {
              value: null
            },
            {
              value: null
            },
            {
              value: null
            },
            {
              value: null
            },
            {
              value: null
            },
            {
              value: null
            }
          ]
        }
      ]
    }; // end of this.dataSource
  }

  BarChart = [];

  // data_1 = {
  //     labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
  //     datasets: [
  //         {
  //             label: "Target",
  //             backgroundColor: "#78C5FD",
  //             data: [80,78,67,92,55,83,58]
  //         },
  //         {
  //             label: "Actual",
  //             backgroundColor: "#FFCC00",
  //             data: [67,73,60,78,50,83,42]
  //         }
  //     ]
  // };

  data_5 = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
    datasets: [
      {
        type: "line",
        label: "Average Stock",
        data: [55, 78, 63, 75, 50, 80, 50],
        fill: false,
        borderColor: "#FF0000",
        backgroundColor: "#FF0000",
        pointBorderColor: "#FF0000",
        pointBackgroundColor: "#FF0000",
        pointHoverBackgroundColor: "#FF0000",
        pointHoverBorderColor: "#FF0000"
      },
      {
        type: "bar",
        label: "Dealer Stock",
        backgroundColor: "#C2F300",
        data: [80, 78, 67, 70, 55, 83, 58]
      }
    ]
  };

  ngOnInit() {
    var user = JSON.parse(localStorage.getItem("user")).sap_code;
    this.canPreviewDealerProfileMenu = JSON.parse(localStorage.getItem("user")).permissions["Preview Dealer Profile menu"]
    var photoProfile = JSON.parse(localStorage.getItem("user"))
      .profile_picture_path;
    let userClassification = JSON.parse(localStorage.getItem("classification"));
    if (userClassification == "1S" || userClassification == "2S") {
      this.user1S = true;
      this.user2S = true;
    } else if (userClassification == "3S") this.user3S = true;
    // this.user_permissions = JSON.parse(
    //   localStorage.getItem("user")
    // ).permissions["Dealer Profile"];
    this.spinner.show()
    this.dealprof.getInfoUser(user).subscribe(
      res => {
        localStorage.setItem("dealer_name", res["dealer_name"]);
        this.saveDealerInfo(res);
        this.dealer_name = res["dealer_name"];
        this.payment_method = res["terms_of_payment_description"];
        this.dealer_category_name = res["dealer_category_name"];
        this.region = res["region"];
        this.service_facility = res["service_facility"];
        this.product_handling = res["product_handling"];
        this.address = res["address"];
        this.district = res["district"];
        this.city = res["city"];
        this.phone_number1 = res["phone_number1"];
        this.phone_number2 = res["phone_number2"];
        this.phone_number3 = res["phone_number3"];
        this.fax1 = res["fax1"];
        this.fax2 = res["fax2"];
        this.mobile_phone_number1 = res["mobile_phone_number1"];
        this.mobile_phone_number2 = res["mobile_phone_number2"];
        this.mobile_phone_number3 = res["mobile_phone_number3"];
        this.email1 = res["email1"];
        this.email2 = res["email2"];
        this.email3 = res["email3"];
        this.website = res["website"];
        this.branch_name1 = res["branch_name1"];
        this.branch_desc1 = res["branch_desc1"];
        this.branch_name2 = res["branch_name2"];
        this.branch_desc2 = res["branch_desc2"];
        this.branch_name3 = res["branch_name3"];
        this.branch_desc3 = res["branch_desc3"];
        this.additional_information = res["additional_information"];
        this.number_of_technician = res["number_of_technician"];
        this.number_of_sales = res["number_of_sales"];
        this.number_of_employee = res["number_of_employee"];
        this.starting_bussiness = res["starting_bussiness"]
        this.id_card_name = res["id_card_name"]
        this.id_card_path = "data:image/jpeg;base64, " + res["id_card_path"];
        this.store_photo1 =
          res["store_photo1"].toString() !== ""
            ? "data:image/jpeg;base64, " + res["store_photo1"].toString()
            : "./assets/img/thumbnail_new.jpg";
        this.store_photo2 =
          res["store_photo2"].toString() !== ""
            ? "data:image/jpeg;base64, " + res["store_photo2"].toString()
            : "./assets/img/thumbnail_new.jpg";
        this.store_photo3 =
          res["store_photo3"].toString() !== ""
            ? "data:image/jpeg;base64, " + res["store_photo3"].toString()
            : "./assets/img/thumbnail_new.jpg";
        this.store_photo4 =
          res["store_photo4"].toString() !== ""
            ? "data:image/jpeg;base64, " + res["store_photo4"].toString()
            : "./assets/img/thumbnail_new.jpg";

        this.pProfile = "data:image/jpeg;base64, " + res["profile_picture"];

        this.service_evaluation = "data:image/jpeg;base64, " + res["service_evaluation"].toString();
        this.sales_evaluation = "data:image/jpeg;base64, " + res["sales_evaluation"].toString();

        this.evaluation_desc1 = res["evaluation_desc1"].toString();
        this.evaluation_desc2 = res["evaluation_desc2"].toString();
        this.evaluation_desc3 = res["evaluation_desc3"].toString();
        this.evaluation_file1 = "data:image/jpeg;base64, " + res["evaluation_file1"].toString();
        this.evaluation_file2 = "data:image/jpeg;base64, " + res["evaluation_file2"].toString();
        this.evaluation_file3 = "data:image/jpeg;base64, " + res["evaluation_file3"].toString();
        this.legal_documents = res["legal_documents"];
        this.spinner.hide()
      },
      err => {
        console.log("data empty");
        this.spinner.hide()
      }
    );
    // bar-1
    // this.BarChart = new Chart('bar-1', {
    //   type: 'bar',
    //   data: this.data_1,
    //   responsive: true,
    //   options: {
    //       barValueSpacing: 20,
    //       scales: {
    //         yAxes: [{
    //           scaleLabel: {
    //             display: true,
    //             labelString: 'Quantity  (In Thousands)'
    //           }
    //         }]
    //       }
    //   }
    // })

    this.BarChart = new Chart("bar-5", {
      type: "bar",
      data: this.data_5,
      responsive: true,
      options: {
        barValueSpacing: 20,
        legend: { display: false },
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Quantity"
              }
            }
          ]
        }
      }
    });
    this.classActive();
    this.tab1 = "tab-active";
    this.tabInfo1 = true;

    // jquery lightslider
    $("#lightSlider").lightSlider({
      gallery: false,
      item: 1,
      loop: true,
      slideMargin: 0,
      thumbItem: 4
    });
  }
  saveDealerInfo(res: Object) {
    let dealerData = JSON.parse(JSON.stringify(res));
    delete dealerData["evaluation_desc1"]
    delete dealerData["evaluation_desc2"];
    delete dealerData["evaluation_desc3"];
    delete dealerData["evaluation_file1"]
    delete dealerData["evaluation_file2"];
    delete dealerData["evaluation_file3"];
    delete dealerData["id_card_name"]
    delete dealerData["id_card_path"];
    delete dealerData["legal_documents"];
    delete dealerData["profile_picture"]
    delete dealerData["sales_evaluation"];
    delete dealerData["service_evaluation"];
    delete dealerData["store_photo1"]
    delete dealerData["store_photo2"];
    delete dealerData["store_photo3"];
    delete dealerData["store_photo4"];
    delete dealerData["data"];
    // console.log(dealerData);
    if (localStorage.getItem("dealer") == null) {
      localStorage.setItem("dealer", JSON.stringify(dealerData));
    } else {
      localStorage.removeItem("dealer")
      localStorage.setItem("dealer", JSON.stringify(dealerData));
    }
  }

  test(template2, base64) {
    this.image = base64;
    this.modalRef = this.modalService.show(template2);
  }

  showLegalDocument(legalDocument, base64) {
    this.imageLegalDocument = "data:application/pdf;base64, " + encodeURIComponent(base64);
    this.modalRef = this.modalService.show(legalDocument);
  }

  cleanURL(oldURL: string): SafeResourceUrl {
    return this.sanitized.bypassSecurityTrustResourceUrl(oldURL);
    // return this.sanitized.sanitize(SecurityContext.URL, oldURL)
  }

  showIDCard(idCard, base64) {
    this.imageIdCard = "data:image/jpeg;base64, " + base64;
    this.modalRef = this.modalService.show(idCard);
  }

  classActive() {
    this.tab1 = "";
    this.tab2 = "";
    this.tab3 = "";
    this.tab4 = "";
    this.tabInfo1 = false;
    this.tabInfo2 = false;
    this.tabInfo3 = false;
    this.tabInfo4 = false;
  }
  openInfo_tab(val) {
    this.classActive();
    if (val == 1) {
      this.tab1 = "tab-active";
      this.tabInfo1 = true;
    } else if (val == 2) {
      this.tab2 = "tab-active";
      this.tabInfo2 = true;
    } else if (val == 3) {
      this.tab3 = "tab-active";
      this.tabInfo3 = true;
    } else {
      this.tab4 = "tab-active";
      this.tabInfo4 = true;
    }
  }
}
