import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  email: string
  constructor(
    private auth: AuthService
  ) { }

  ngOnInit() {
  }
  onSubmit(){
    console.log(this.email)
    let data = {
      email: this.email
    }
    this.auth.forgotPass(data).subscribe(res=>{
      alert(res["message"]);
    },err=>{
      console.log(err)
    });
  }

}
