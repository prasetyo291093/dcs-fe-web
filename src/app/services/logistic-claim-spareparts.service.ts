import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class LogisticClaimSparepartsService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getListDNSparepart() {
    let sap_code = JSON.parse(localStorage.getItem("user")).sap_code;
    return this.http.get(
      `${this.baseUrl}/api/logistic-claim/sparepart/${sap_code}`
    );
  }

  getDetailDNSparepart(dn_number) {
    return this.http.get(
      `${this.baseUrl}/api/logistic-claim/sparepart/detail/${dn_number}`
    );
  }

  saveClaimSparepart(body) {
    return this.http.post(
      `${this.baseUrl}/api/logistic-claim/sparepart/input`,
      body
    );
  }

  uploadFile(file) {
    return this.http.post(
      `${this.baseUrl}/api/logistic-claim/sparepart/upload`,
      file
    );
  }

  getClaimSparepart(claimNumber) {
    return this.http.get(
      `${this.baseUrl}/api/logistic-claim/sparepart/edit/${claimNumber}`
    );
  }

  updateClaimSparepart(claimNumber, body) {
    return this.http.post(
      `${this.baseUrl}/api/logistic-claim/sparepart/edit/${claimNumber}`,
      body
    );
  }
}
