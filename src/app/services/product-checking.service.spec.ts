import { TestBed } from '@angular/core/testing';

import { ProductCheckingService } from './product-checking.service';

describe('ProductCheckingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductCheckingService = TestBed.get(ProductCheckingService);
    expect(service).toBeTruthy();
  });
});
