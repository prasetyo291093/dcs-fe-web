import { TestBed } from '@angular/core/testing';

import { ApAgingService } from './ap-aging.service';

describe('ApAgingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApAgingService = TestBed.get(ApAgingService);
    expect(service).toBeTruthy();
  });
});
