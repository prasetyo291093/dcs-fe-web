import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delivery-policy',
  templateUrl: './delivery-policy.component.html',
  styleUrls: ['./delivery-policy.component.css']
})
export class DeliveryPolicyComponent implements OnInit {

  constructor() { }

  show1:boolean=false;
  show2:boolean=false;
  show3:boolean=false;
  jabalClass:boolean=false;
  sumkalClass:boolean=false;
  daerahClass:boolean=false;

  ngOnInit() {
  }

  accordion(val){
    if(val == "jabal"){
      this.show1 = !this.show1;
      this.show2 = false;
      this.show3 = false;
      this.jabalClass = !this.jabalClass;
      this.sumkalClass =false;
      this.daerahClass = false;
    } else if(val == "sumkal"){
      this.show2 = !this.show2;
      this.show1 = false;
      this.show3 = false;
      this.sumkalClass = !this.sumkalClass;
      this.jabalClass = false;
      this.daerahClass = false
    } else{
      this.show3 = !this.show3;
      this.show1 = false;
      this.show2 = false;
      this.daerahClass = !this.daerahClass;
      this.jabalClass = false;
      this.sumkalClass = false;
    }
  }

}
