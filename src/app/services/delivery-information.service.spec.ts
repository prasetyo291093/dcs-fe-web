import { TestBed } from '@angular/core/testing';

import { DeliveryInformationService } from './delivery-information.service';

describe('DeliveryInformationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeliveryInformationService = TestBed.get(DeliveryInformationService);
    expect(service).toBeTruthy();
  });
});
