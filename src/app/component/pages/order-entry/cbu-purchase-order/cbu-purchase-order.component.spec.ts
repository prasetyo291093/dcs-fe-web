import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CbuPurchaseOrderComponent } from './cbu-purchase-order.component';

describe('CbuPurchaseOrderComponent', () => {
  let component: CbuPurchaseOrderComponent;
  let fixture: ComponentFixture<CbuPurchaseOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CbuPurchaseOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CbuPurchaseOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
