import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-how-to-pay',
  templateUrl: './how-to-pay.component.html',
  styleUrls: ['./how-to-pay.component.css']
})
export class HowToPayComponent implements OnInit {

  constructor() { }

  show1:boolean = false;
  show2:boolean = false;
  virtualClass:boolean=false;
  paymentClass:boolean=false;

  ngOnInit() {
  }

  accordion(val){

    if(val == 'virtual'){
      this.virtualClass = !this.virtualClass;
      this.show1 = !this.show1;
      this.show2 = false;
      this.paymentClass=false;
    } else{
      this.paymentClass = !this.paymentClass;
      this.show2 = !this.show2;
      this.show1 = false;
      this.virtualClass=false;
    }
    console.log(val)

  }

}
