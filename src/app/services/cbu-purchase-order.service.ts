import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class CbuPurchaseOrderService {

  constructor(
    private http: HttpClient
  ) { }

  baseUrl = environment.baseUrl

  getBillToCode() {
    var sap_code = JSON.parse(localStorage.getItem('user')).sap_code;
    return this.http.get(this.baseUrl + '/api/bill-to-list/' + sap_code);
  }

  getShipToCode() {
    var sap_code = JSON.parse(localStorage.getItem('user')).sap_code;
    return this.http.get(this.baseUrl + '/api/ship-to-list/' + sap_code);
  }

  getListMaterial(val, ship_to_code) {
    var sap_code = JSON.parse(localStorage.getItem('user')).sap_code;
    return this.http.get(this.baseUrl + '/api/order-cbu/search-code/' + sap_code + '/' + ship_to_code + '?search=' + val);
  }

  getMaterial(val, ship_to_code) {
    var sap_code = JSON.parse(localStorage.getItem('user')).sap_code;
    return this.http.get(this.baseUrl + '/api/order-cbu/search-code2/' + sap_code + '/' + ship_to_code + '?search=' + val);
  }

  getMaterialUploadFix(ship_to_code, idtrs) {
    var sap_code = JSON.parse(localStorage.getItem('user')).sap_code;
    return this.http.get(this.baseUrl + '/api/order-cbu/upload-fix/' + sap_code + '/' + ship_to_code + '/' + idtrs);
  }

  saveAsDraft(val) {
    // console.log(val);
    return this.http.post(this.baseUrl + '/api/order-cbu/draft', val);
  }

  getDraftList() {
    var id = JSON.parse(localStorage.getItem('user')).id;
    return this.http.get(this.baseUrl + '/api/order-cbu/draftList/' + id);
  }

  getDraft(param) {
    return this.http.get(this.baseUrl + '/api/order-cbu/contDraft/' + param);
  }
  updateDraft(param, val) {
    return this.http.post(this.baseUrl + '/api/order-cbu/updateDraft/' + param, val)
  }
  submitOrder(val) {
    console.log(val);
    return this.http.post(this.baseUrl + '/api/order-cbu/confirm', val);
  }

  storeHeaderOrder(val) {
    var data = JSON.stringify(val);
    var orderHeaderData = localStorage.setItem('orderHeaderData', data);
  }

  paymentOrder(val) {
    console.log(val);
    return this.http.post(this.baseUrl + '/api/dpr', val);
  }

  simulateOrder(val) {
    return this.http.post(this.baseUrl + '/api/order-cbu/simulate', val);
  }

  getTOPFromBillTo(billTo, division) {
    return this.http.get(`${this.baseUrl}/api/order/${billTo}/${division}`);
  }

  sendPayment(data) {
    return this.http.post(`${this.baseUrl}/api/payment-gateway`, data);
  }

  getPaymentMessage(soNumber) {
    return this.http.get(`${this.baseUrl}/api/payment-message/${soNumber}`);
  }
  
  sendRedirectData(data) {
    // let header = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
    // return this.http.post(url, data, {headers: header});
    return this.http.post(`${this.baseUrl}/api/payment-gateway/redirect`, data, {responseType: 'text'})
  }

  downloadSO(data) {
    return this.http.post(`${this.baseUrl}/api/download-so`, data, {responseType: "blob"});
  }
}
