import { TestBed } from '@angular/core/testing';

import { DealerTargetCbuService } from './dealer-target-cbu.service';

describe('DealerTargetCbuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DealerTargetCbuService = TestBed.get(DealerTargetCbuService);
    expect(service).toBeTruthy();
  });
});
